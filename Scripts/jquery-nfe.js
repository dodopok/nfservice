﻿$(document).ready(function () {

    deleteNota = function (id) {
        $("#loading-waiting-"+id).show();
        $.ajax(
        {
            type: "DELETE",
            url: "notasfiscais/deletenota",
            cache: false,
            data: { id: id },
            success: function (data) {
                $("#tr-notafiscal-" + id).remove();
                $("#loading-waiting-"+id).hide();
            },
            error: function (req, status, error) {
                alert("Erro na execução da ação. Tente mais tarde ou contacte o nosso suporte.");
                $("#loading-waiting-"+id).hide();
            }
        });
    }

});