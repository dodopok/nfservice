﻿function Validate() {
    $.validator.addMethod(
        "greatherThan",
        function (value, element, params) {

            if (value.length == 0 || value == 0 || $(params).val().length == 0) {
                return true;
            }

            var result = isNaN(value) && isNaN($(params).val()) || (parseFloat(value) > parseFloat($(params).val()))

            return result;
        },
        'Must be greater than {0}.'
    );

        $.validator.addMethod('noneSelected',
           function (value, element) {
               return this.optional(element) || (value.indexOf("Selecione") == -1);
           }, "Please select an option");  
}