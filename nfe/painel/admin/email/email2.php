<?php
/*
 * test_html_mail_message.php
 *
 * @(#) $Header: /home/mlemos/cvsroot/mimemessage/test_html_mail_message.php,v 1.11 2003/10/30 21:36:12 mlemos Exp $
 *
 */

	require("email/email_message.php");


/*
 *  Trying to guess your e-mail address.
 *  It is better that you change this line to your address explicitly.
 *  $from_address="me@mydomain.com";
 *  $from_name="My Name";
 */
	$from_address="delivery@tiradentes.com.br";
	$from_name="Delivery on-line";

	$reply_name=$from_name;
	$reply_address=$from_address;
	$reply_address=$from_address;
	$error_delivery_name=$from_name;
	$error_delivery_address=$from_address;

/*
 *  Change these lines or else you will be mailing the class author.
 */
	$to_name=$nomecliente;
	$to_address=$destinatario;

	$subject="Seu cadastro foi efetuado com sucesso!";
	$email_message=new email_message_class;
	$email_message->SetEncodedEmailHeader("To",$to_address,$to_name);
	$email_message->SetEncodedEmailHeader("From",$from_address,$from_name);
	$email_message->SetEncodedEmailHeader("Reply-To",$reply_address,$reply_name);
	$email_message->SetHeader("Sender",$from_address);

/*
 *  Set the Return-Path header to define the envelope sender address to which bounced messages are delivered.
 *  If you are using Windows, you need to use the smtp_message_class to set the return-path address.
 */
	if(defined("PHP_OS")
	&& strcmp(substr(PHP_OS,0,3),"WIN"))
		$email_message->SetHeader("Return-Path",$error_delivery_address);

	$email_message->SetEncodedHeader("Subject",$subject);

/*
 *  An HTML message that requires any dependent files to be sent,
 *  like image files, style sheet files, HTML frame files, etc..,
 *  needs to be composed as a multipart/related message part.
 *  Different parts need to be created before they can be added
 *  to the message.
 *
 *  Parts can be created from files that can be opened and read.
 *  The data content type needs to be specified. The can try to guess
 *  the content type automatically from the file name.
 */
	$image=array(
		"FileName"=>"delivery_01.gif",
		"Content-Type"=>"automatic/name",
		"Disposition"=>"inline"
	);
	$email_message->CreateFilePart($image,$image_part);

/*
 *  Parts that need to be referenced from other parts,
 *  like images that have to be hyperlinked from the HTML,
 *  are referenced with a special Content-ID string that
 *  the class creates when needed.
 */
	$image_content_id=$email_message->GetPartContentID($image_part);

/*
 *  Many related file parts may be embedded in the message.
 */
	$image=array(
		"FileName"=>"delivery_02.gif",
		"Content-Type"=>"automatic/name",
		"Disposition"=>"inline"
	);
	$email_message->CreateFilePart($image,$background_image_part);

	$background_image_content_id=$email_message->GetPartContentID($background_image_part);

/*
 *  Related file parts may also be embedded in the actual HTML code in the
 *  form of URL like those referenced by the SRC attribute of IMG tags.
 */
	$image=array(
		"FileName"=>"delivery_04.gif",
		"Content-Type"=>"automatic/name",
		"Disposition"=>"inline"
	);
	$email_message->CreateFilePart($image,$image2_part);
	$img_image_content_id=$email_message->GetPartContentID($image2_part);

$image=array(
		"FileName"=>"delivery_05.gif",
		"Content-Type"=>"automatic/name",
		"Disposition"=>"inline"
	);
	$email_message->CreateFilePart($image,$image3_part);
	$img2_image_content_id=$email_message->GetPartContentID($image3_part);

/*
 *  Use different identifiers to reference different related file parts.
 */

/*
 *  The URL of referenced parts in HTML starts with cid:
 *  followed by the Contentp-ID string. Notice the image link below.
 */
	$html_message="<table id='Table_01' width=500 border=0 cellpadding=0 cellspacing=0 align=center>
	<tr>
		<td colspan=3>
			<img src=\"cid:".$image_content_id."\"></td>
	</tr>
	<tr>
		<td width=31 ><img src=\"cid:".$background_image_content_id."\"></td>
		<td width=443><font face=Arial size=2>Ol�, ".strtok($to_name," ")." <br> <br> 
		Seu cadastro foi efetuado com sucesso na se��o delivery do site <a href=www.tiradentes.com.br target=_blank>www.tiradentes.com.br</a><br><br>Seu dados para acesso s�o:<br> Nome de Usu�rio:<b> ".$destinatario."</b><br>Senha: <b>".$senhacliente."<br>
<br><br>Obrigado e Boas Compras!<br>Departamento de Vendas
		</font></td>
		<td width=26 background=''><img src=\"cid:".$img_image_content_id."\"></td>
	</tr>
	<tr>
		<td height=42 colspan=3><img src=\"cid:".$img2_image_content_id."\"></td>
  </tr>
</table>
</body>
</html>";
	$email_message->CreateQuotedPrintableHTMLPart($html_message,"",$html_part);

/*
 *  The complete HTML parts are gathered in a single multipart/related part.
 */
	$related_parts=array(
		$html_part,
		$image_part,
		$image2_part,
		$image3_part,
		$background_image_part
	);
	$email_message->CreateRelatedMultipart($related_parts,$html_parts);

/*
 *  It is strongly recommended that when you send HTML messages,
 *  also provide an alternative text version of HTML page,
 *  even if it is just to say that the message is in HTML,
 *  because more and more people tend to delete HTML only
 *  messages assuming that HTML messages are spam.
 */
	$text_message="Seu cadastro foi efetuado com sucesso na se��o delivery do site www.tiradentes.com.br. Seu dados para acesso s�o: Nome de Usu�rio: ".$destinatario." Senha: ".$senhacliente."
  Obrigado e Boas Compras! - Departamento de Vendas";
	$email_message->CreateQuotedPrintableTextPart($email_message->WrapText($text_message),"",$text_part);

/*
 *  Multiple alternative parts are gathered in multipart/alternative parts.
 *  It is important that the fanciest part, in this case the HTML part,
 *  is specified as the last part because that is the way that HTML capable
 *  mail programs will show that part and not the text version part.
 */
	$alternative_parts=array(
		$text_part,
		$html_parts
	);
	$email_message->AddAlternativeMultipart($alternative_parts);

/*
 *  One or more additional parts may be added as attachments.
 *  In this case a file part is added from data provided directly from this script.
 */
	
	$error=$email_message->Send();
	if(strcmp($error,"")) {
		echo "Error: $error";
	} else {
	print "<div class=style9><b>$nome_cliente, seu cadastro foi efetivado com sucesso!<br><br>ATEN��O : SEU NOME DE USU�RIO � SEU E-MAIL<br><br><input name=Submit2 type=button class=bot12 value='Volte � p�gina principal.' onClick=\"javascript:window.open('delivery.php','_self');\"><br><br>Boas Compras!</b></div>";
    print "<script Language=\"JavaScript\">";
    print 'function closeWindow(){';
    print '  window.close();}';
    print '  setTimeout("closeWindow()", 4000);';
    print "</script>";
}
?>