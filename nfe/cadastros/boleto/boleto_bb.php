<?php
// +----------------------------------------------------------------------+
// | BoletoPhp - Vers�o Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo est� dispon�vel sob a Licen�a GPL dispon�vel pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Voc� deve ter recebido uma c�pia da GNU Public License junto com     |
// | esse pacote; se n�o, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colabora��es de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de Jo�o Prado Maia e Pablo Martins F. Costa				        |
// | 														                                   			  |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +--------------------------------------------------------------------------------------------------------+
// | Equipe Coordena��o Projeto BoletoPhp: <boletophp@boletophp.com.br>              		             				|
// | Desenvolvimento Boleto Banco do Brasil: Daniel William Schultz / Leandro Maniezo / Rog�rio Dias Pereira|
// +--------------------------------------------------------------------------------------------------------+


// ------------------------- DADOS DIN�MICOS DO SEU CLIENTE PARA A GERA��O DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formul�rio c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
include "conect.php";

$sql_boleto=mysql_query("select * from boletos where id=".$_GET['boleto']."");
$bancoboleto=mysql_fetch_array($sql_boleto);

$sql_cliente=mysql_query("select * from clientes where id=".$bancoboleto["refid"]."");
$clienteboleto=mysql_fetch_array($sql_cliente);

//$dias_de_prazo_para_pagamento = $bancoboleto['diasvenc'];
$taxa_boleto = 0.00;


list ($y, $m, $d) = split ('[/.-]', $bancoboleto["vencimento"]);
$data_venc = "$d/$m/$y";  // Prazo de X dias OU informe data: "13/04/2006"; 
$valor_cobrado = $bancoboleto['valorpagar']; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

$dadosboleto["nosso_numero"] = "8".str_pad($bancoboleto['id'], 9, "0", STR_PAD_LEFT);
$dadosboleto["numero_documento"] = "8".str_pad($bancoboleto['id'], 9, "0", STR_PAD_LEFT);	// Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emiss�o do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $clienteboleto['razao_social'];

$dadosboleto["endereco1"] = $clienteboleto['endereco']." ".$clienteboleto['numero']." - ".$clienteboleto['bairro'];

$dadosboleto["endereco2"] = $clienteboleto['cidade']." - ".$clienteboleto['uf'] ." - CEP: ".$clienteboleto['cep'];


// INFORMACOES PARA O CLIENTE

$dadosboleto["demonstrativo1"] = "Mensalidade referente  ".$bancoboleto['periodo'];
if ($taxa_boleto <>0.00) {
$dadosboleto["demonstrativo2"] = "- Taxa Banc�ria R$ ".$taxa_boleto;
} else {
$dadosboleto["demonstrativo2"] = "";
}

$dadosboleto["demonstrativo3"] = "";



$dadosboleto["instrucoes1"] = "";

$dadosboleto["instrucoes2"] = "";

$dadosboleto["instrucoes3"] = "";

$dadosboleto["instrucoes4"] = "";



// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "N";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DM";


// ---------------------- DADOS FIXOS DE CONFIGURA��O DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - BANCO DO BRASIL
$dadosboleto["agencia"] = "6862"; // Num da agencia, sem digito
$dadosboleto["conta"] = "005701";  	// Num da conta, sem digito

// DADOS PERSONALIZADOS - BANCO DO BRASIL
$dadosboleto["convenio"] = "2292819";  // Num do conv�nio - REGRA: 6 ou 7 ou 8 d�gitos
$dadosboleto["contrato"] = "18775385"; // Num do seu contrato
$dadosboleto["carteira"] = "18";
$dadosboleto["variacao_carteira"] = "-019";  // Varia��o da Carteira, com tra�o (opcional)

// TIPO DO BOLETO
$dadosboleto["formatacao_convenio"] = "7"; // REGRA: 8 p/ Conv�nio c/ 8 d�gitos, 7 p/ Conv�nio c/ 7 d�gitos, ou 6 se Conv�nio c/ 6 d�gitos
$dadosboleto["formatacao_nosso_numero"] = "1"; // REGRA: Usado apenas p/ Conv�nio c/ 6 d�gitos: informe 1 se for NossoN�mero de at� 5 d�gitos ou 2 para op��o de at� 17 d�gitos

/*
#################################################
DESENVOLVIDO PARA CARTEIRA 18

- Carteira 18 com Convenio de 8 digitos
  Nosso n�mero: pode ser at� 9 d�gitos

- Carteira 18 com Convenio de 7 digitos
  Nosso n�mero: pode ser at� 10 d�gitos

- Carteira 18 com Convenio de 6 digitos
  Nosso n�mero:
  de 1 a 99999 para op��o de at� 5 d�gitos
  de 1 a 99999999999999999 para op��o de at� 17 d�gitos

#################################################
*/


// SEUS DADOS
$dadosboleto["identificacao"] = "Carlos Alberto Fuganholi Junior M.E";
$dadosboleto["cpf_cnpj"] = "03.510.148/0001-64";
$dadosboleto["endereco"] = "Avenida Washington Luiz , 250 � Sala 301 - Caixa Postal 324 - CENTRO - CEP: 13600-720";
$dadosboleto["cidade_uf"] = "ARARAS-SP";
$dadosboleto["cedente"] = "Carlos Alberto Fuganholi Junior M.E";

// N�O ALTERAR!
include("include/funcoes_bb.php"); 
include("include/layout_bb.php");
?>
