<?php
// +----------------------------------------------------------------------+
// | BoletoPhp - Vers�o Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo est� dispon�vel sob a Licen�a GPL dispon�vel pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Voc� deve ter recebido uma c�pia da GNU Public License junto com     |
// | esse pacote; se n�o, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colabora��es de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de Jo�o Prado Maia e Pablo Martins F. Costa			       	  |
// | 																	                                    |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Equipe Coordena��o Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto Bradesco: Ramon Soares						            |
// +----------------------------------------------------------------------+


// ------------------------- DADOS DIN�MICOS DO SEU CLIENTE PARA A GERA��O DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formul�rio c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE

include "conect.php";
      
$sql_boleto=mysql_query("select * from boletos_bradesco where id=".$_GET['boletoid']."");
$bancoboleto=mysql_fetch_array($sql_boleto);

$dias_de_prazo_para_pagamento = $bancoboleto['diasvenc'];
$taxa_boleto = $bancoboleto['taxabanc'];
		
		
$data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
$valor_cobrado = $bancoboleto['ValorDocumento']; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

$dadosboleto["nosso_numero"] = str_pad($bancoboleto['NossoNumero'], 8, "0", STR_PAD_LEFT);  
$dadosboleto["numero_documento"] = str_pad($bancoboleto['NumeroDocumento'], 8, "0", STR_PAD_LEFT);  // Num do pedido ou do documento
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emiss�o do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $bancoboleto['NomeSacado'];

$dadosboleto["endereco1"] = $bancoboleto['EnderecoSacado']." - ".$bancoboleto['BairroSacado'];

$dadosboleto["endereco2"] = $bancoboleto['CidadeSacado']." - ".$bancoboleto['EstadoSacado'] ." - CEP: ".$bancoboleto['CepSacado'];

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = $bancoboleto['Demonstrativo1']." - Pedido no. ".$bancoboleto['NumeroDocumento'];
if ($taxa_boleto <>0.00) {
$dadosboleto["demonstrativo2"] = $bancoboleto['Demonstrativo2']."- Taxa Banc�ria R$ ".$taxa_boleto;
} else {
$dadosboleto["demonstrativo2"] = $bancoboleto['Demonstrativo2'];
}

$dadosboleto["demonstrativo3"] = $bancoboleto['Demonstrativo3'];



$dadosboleto["instrucoes1"] = $bancoboleto['Instrucao1'];

$dadosboleto["instrucoes2"] = $bancoboleto['Instrucao2'];

$dadosboleto["instrucoes3"] = $bancoboleto['Instrucao3'];

$dadosboleto["instrucoes4"] = $bancoboleto['Instrucao4'];

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "001";
$dadosboleto["valor_unitario"] = $valor_boleto;
$dadosboleto["aceite"] = "";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DS";


// ---------------------- DADOS FIXOS DE CONFIGURA��O DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - Bradesco
$dadosboleto["agencia"] =  $bancoboleto['agencia']; // Num da agencia, sem digito
$dadosboleto["agencia_dv"] =  $bancoboleto['dgagencia']; // Digito do Num da agencia
$dadosboleto["conta"] =  $bancoboleto['conta'];; 	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $bancoboleto['dgconta'];; 	// Digito do Num da conta

// DADOS PERSONALIZADOS - Bradesco
$dadosboleto["conta_cedente"] =  $bancoboleto['conta']; // ContaCedente do Cliente, sem digito (Somente N�meros)
$dadosboleto["conta_cedente_dv"] =  $bancoboleto['dgconta'];; // Digito da ContaCedente do Cliente
$dadosboleto["carteira"] = $bancoboleto['cartcob'];  // C�digo da Carteira: pode ser 06 ou 03

// SEUS DADOS
$dadosboleto["identificacao"] = "BOMBACHINI MOTOS";
$dadosboleto["cpf_cnpj"] = "10.526.906/0001-06";
$dadosboleto["endereco"] = "Rua Conselheiro N�bias, 433 - Centro - CEP: 01203-001";
$dadosboleto["cidade_uf"] = "S�O PAULO/SP";
$dadosboleto["cedente"] = "Andreia Bombachini de Oliveira Moto Pe�as LTDA";

// N�O ALTERAR!
include("include/funcoes_bradesco.php"); 
include("include/layout_bradesco.php");
?>
