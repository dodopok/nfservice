function select_innerHTML(objeto,innerHTML){
/******
* select_innerHTML - corrige o bug do InnerHTML em selects no IE
* Veja o problema em: http://support.microsoft.com/default.aspx?scid=kb;en-us;276228
* Vers�o: 2.1 - 04/09/2007
* Autor: Micox - N�iron Jos� C. Guimar�es - micoxjcg@yahoo.com.br
* @objeto(tipo HTMLobject): o select a ser alterado
* @innerHTML(tipo string): o novo valor do innerHTML
*******/
    objeto.innerHTML = ""
    var selTemp = document.createElement("micoxselect")
    var opt;
    selTemp.id="micoxselect1"
    document.body.appendChild(selTemp)
    selTemp = document.getElementById("micoxselect1")
    selTemp.style.display="none"
    if(innerHTML.toLowerCase().indexOf("<option")<0){//se n�o � option eu converto
        innerHTML = "<option>" + innerHTML + "</option>"
    }
    innerHTML = innerHTML.toLowerCase().replace(/<option/g,"<span").replace(/<\/option/g,"</span")
    selTemp.innerHTML = innerHTML
      
    
    for(var i=0;i<selTemp.childNodes.length;i++){
  var spantemp = selTemp.childNodes[i];
  
        if(spantemp.tagName){     
            opt = document.createElement("OPTION")
    
   if(document.all){ //IE
    objeto.add(opt)
   }else{
    objeto.appendChild(opt)
   }       
    
   //getting attributes
   for(var j=0; j<spantemp.attributes.length ; j++){
    var attrName = spantemp.attributes[j].nodeName;
    var attrVal = spantemp.attributes[j].nodeValue;
    if(attrVal){
     try{
      opt.setAttribute(attrName,attrVal);
      opt.setAttributeNode(spantemp.attributes[j].cloneNode(true));
     }catch(e){}
    }
   }
   //getting styles
   if(spantemp.style){
    for(var y in spantemp.style){
     try{opt.style[y] = spantemp.style[y];}catch(e){}
    }
   }
   //value and text
   opt.value = spantemp.getAttribute("value")
   opt.text = spantemp.innerHTML
   //IE
   opt.selected = spantemp.getAttribute('selected');
   opt.className = spantemp.className;
  } 
 }    
 document.body.removeChild(selTemp)
 selTemp = null
}

function completaCEP(cep,c)
 {
	 qtdcaracteres = $("input[name=cep_user"+c+"]").val();
	 qtdcaracteres = qtdcaracteres.length;

      if(qtdcaracteres == 5){
	    $("input[name=cep_user"+c+"]").val($("input[name=cep_user"+c+"]").val()+ "-");
	  }
 }
 
function completaCEP2(cep)
 {
      qtdcaracteres = (document.form_sistema.cep_divulgacao.value).length;

      if(qtdcaracteres == 5)
	    document.form_sistema.cep_divulgacao.value = document.form_sistema.cep_divulgacao.value + "-";
 } 
 
function pesquisar_dados(valor,c)
{
  campo_select_rua = $("input[name=end_user"+c+"]");
  campo_select_bairro = $("input[name=bairro_user"+c+"]");
  campo_select_cidade = $("select[name=cidade_user"+c+"]");
  campo_select_uf = $("select[name=estado_user"+c+"]");
  campo_select_rua.prop('disabled', true);
  campo_select_rua.val("Carregando...");
  campo_select_bairro.prop('disabled', true);
  campo_select_bairro.val("Carregando...");
  campo_select_cidade.prop('disabled', true);
  campo_select_cidade.val("Carregando...");
  campo_select_uf.prop('disabled', true);
  campo_select_uf.val("Carregando...");  
  http.open("GET", "../consultar_cep.php?cep=" + valor, true);
  http.setRequestHeader("Content-Type", "text/xml; charset=windows-1252") ;
  http.setRequestHeader('encoding','ISO-8859-1');
  http.onreadystatechange = function(){handleHttpResponse(c)};
  http.send(null);  
}

function handleHttpResponse(c)
{
	if (http.readyState==4){
	  //console.debug('This HTTP Request is now exterminated.');
	  campo_select_rua = $("input[name=end_user"+c+"]");
	  campo_select_bairro = $("input[name=bairro_user"+c+"]");
	  campo_select_cidade = $("select[name=cidade_user"+c+"]");
	  campo_select_uf = $("select[name=estado_user"+c+"]");
	  campo_select_rua.prop('disabled', false);
	  campo_select_rua.val("");
	  campo_select_bairro.prop('disabled', false);
	  campo_select_bairro.val("");
	  campo_select_cidade.prop('disabled', false);
	  campo_select_cidade.val("");
	  campo_select_uf.prop('disabled',false);
	  campo_select_uf.val("");
	
	  results = http.responseText.split(";");
	   // for( i = 0; i < results.length; i++ )
		//{ 
	  var string = results[0].split( ":" );	  
	  campo_select_rua.val(string[0]);
	  $("#estado_user"+c+" option:contains(" + string[3] + ")").prop('selected', 'selected');
	  campo_select_bairro.val(string[1]);
	  CarregaCidades(string[3],'cidade_user'+c,string[2]);
	}  
}

function CarregaCidades(codEstado,campo,valor)
{
	//$('#'+campo).load('../../carrega_cidades.php?codEstado='+codEstado,function() {
	//	$('#'+campo).val(valor);
	//});
	// Verificando Browser
if(window.XMLHttpRequest) {
   req = new XMLHttpRequest();
}
else if(window.ActiveXObject) {
   req = new ActiveXObject("Microsoft.XMLHTTP");
}
 
// Arquivo PHP juntamente com o valor digitado no campo (m�todo GET)
var url = 'carrega_cidades.php?codEstado='+codEstado;
 
// Chamada do m�todo open para processar a requisi��o
req.open("Get", url+"&ck="+ (new Date()).getTime(), true); 
req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=iso-8859-1");
req.setRequestHeader("Cache-Control", "no-store, no-cache, must-revalidate");
req.setRequestHeader("Cache-Control", "post-check=0, pre-check=0");
req.setRequestHeader("Pragma", "no-cache");
document.getElementById('endentregar').innerHTML = '<div align="center"><img src="../../imagens/loading.gif" alt="Carregando..."/></div>';
document.getElementById('endcobrancar').innerHTML = '<div align="center"><img src="../../imagens/loading.gif" alt="Carregando..."/></div>';
// Quando o objeto recebe o retorno, chamamos a seguinte fun��o;
req3.onreadystatechange = function() {
 
	if(req3.readyState == 1) {
		document.getElementById('endentregar').innerHTML = '<div align="center"><img src="../../imagens/loading.gif" alt="Carregando..."/></div>';
		document.getElementById('endcobrancar').innerHTML = '<div align="center"><img src="../../imagens/loading.gif" alt="Carregando..."/></div>';
	}
	
	// Verifica se o Ajax realizou todas as opera��es corretamente
	if(req3.readyState == 4 && req3.status == 200) {
 
	// Resposta retornada pelo busca.php
	var resposta3 = req3.responseText;
 
	// Abaixo colocamos a(s) resposta(s) na div resultado
	select_innerHTML(document.getElementById("endentregar"),resposta3);
	select_innerHTML(document.getElementById("endcobrancar"),resposta3);
	
	}
}
req3.send(null);

}
	
function getHTTPObject() {
  var xmlhttp;
  /*@cc_on
  @if (@_jscript_version >= 5)
    try {
      xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e) {
        xmlhttp = false;
      }
    }
  @else
  xmlhttp = false;
  @end @*/
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}
var http = getHTTPObject();


function checa_senha(senha){
	if (senha!=document.form_sistema.senha.value){
	alert ("Senhas n�o conferem!!!")
	document.form_sistema.confirmacao_senha.value=""
	}
}

function abre_janela_cep(log,cep,bairro,municipio,uf,numlog){
	janela_busca_CEP = eval("window.open('/htm/busca_cep/default.asp?clog="+log+"&ccep="+cep+"&cbairro="+bairro+"&cmunicipio="+municipio+"&cuf="+uf+"&cnumlog="+numlog+"','buscacep','dependent=yes,alwaysRaised=yes,width=600,height=230')")
}



function autoTab(input, e)  {   
  var ind = 0;  
  var isNN = (navigator.appName.indexOf("Netscape")!=-1);  
  var keyCode = (isNN) ? e.which : e.keyCode;   
  var nKeyCode = e.keyCode;   
  if(keyCode == 13){   
    if (!isNN) {window.event.keyCode = 0;} // evitar o beep  
    ind = getIndex(input);  
    if (input.form[ind].type == 'textarea') {  
      return;  
    }  
    ind++;  
    input.form[ind].focus();   
    if (input.form[ind].type == 'text') {  
      input.form[ind].select();   
    }  
  }   
  
  function getIndex(input) {   
    var index = -1, i = 0, found = false;   
    while (i < input.form.length && index == -1)   
      if (input.form[i] == input) {  
        index = i;  
          if (i < (input.form.length -1)) {  
           if (input.form[i+1].type == 'hidden') {  
       index++;   
     }  
     if (input.form[i+1].type == 'button' && input.form[i+1].id == 'tabstopfalse') {  
       index++;   
     }  
   }  
      }  
      else   
   i++;   
    return index;   
  }  
}  

function validacao(){
docObj = document.form_sistema;

if ((docObj.tipo_cliente[0].checked==false) && (docObj.tipo_cliente[1].checked==false)){alert("Selecione o Tipo de Cliente");return false;}
else if (docObj.nome_user.value==""){alert("Raz�o Social � campo obrigat�rio");return false;}
else if (docObj.cpfcnpj.value==""){alert("CNPJ � campo obrigat�rio");return false;}
//else if (docObj.inscrest.value==""){alert("Inscri��o Estadual � campo obrigat�rio");return false;}
else if (docObj.cep_user.value==""){alert("CEP � campo obrigat�rio");return false;}
else if (docObj.end_user.value==""){alert("Endere�o � campo obrigat�rio");return false;}
else if (docObj.numero_user.value==""){alert("N�mero do Endere�o � campo obrigat�rio");return false;}
else if (docObj.bairro_user.value==""){alert("Bairro � campo obrigat�rio");return false;}
else if (docObj.cidade_user.value==""){alert("Cidade � campo obrigat�rio");return false;}
else if ((docObj.estado_user.value=="0") || (docObj.estado_user.value=="")){alert("Estado � campo obrigat�rio");return false;}


else {
docObj.submit();
}
}

function validacao1(){
docObj = document.form_sistema;

if ((docObj.tipo_cliente[0].checked==false) && (docObj.tipo_cliente[1].checked==false)){
	alert("Selecione o Tipo de Cliente");return false;
	}
else if (docObj.nome_user.value==""){
	alert("Raz�o Social � campo obrigat�rio");return false;
	}
else if (docObj.cpfcnpj.value==""){
	alert("CNPJ � campo obrigat�rio");return false;
	}
//else if (docObj.inscrest.value==""){alert("Inscri��o Estadual � campo obrigat�rio");return false;}
else if (docObj.cep_user1.value==""){
	alert("CEP � campo obrigat�rio");return false;
	}
else if (docObj.end_user1.value==""){
	alert("Endere�o � campo obrigat�rio");return false;
	}
else if (docObj.numero_user1.value==""){
	alert("N�mero do Endere�o � campo obrigat�rio");return false;
	}
else if (docObj.bairro_user1.value==""){
	alert("Bairro � campo obrigat�rio");return false;
	}
else if (docObj.cidade_user1.value==""){
	alert("Cidade � campo obrigat�rio");return false;
	}
//else if ((docObj.estado_user.value=="0") || (docObj.estado_user1.value=="")){
	//alert("Estado � campo obrigat�rio");return false;
	//}

else {
docObj.submit();
}
}

function mascara(o,f){
v_obj=o
v_fun=f
setTimeout("execmascara()",1)
}

function execmascara(){
v_obj.value=v_fun(v_obj.value)
}

function leech(v){
v=v.replace(/o/gi,"0")
v=v.replace(/i/gi,"1")
v=v.replace(/z/gi,"2")
v=v.replace(/e/gi,"3")
v=v.replace(/a/gi,"4")
v=v.replace(/s/gi,"5")
v=v.replace(/t/gi,"7")
return v
}

function soNumeros(v){
return v.replace(/\D/g,"")
}

function telefonev(v){
v=v.replace(/\D/g,"") //Remove tudo o que n�o � d�gito
v=v.replace(/^(\d\d)(\d)/g,"($1) $2") //Coloca par�nteses em volta dos dois primeiros d�gitos
v=v.replace(/(\d{4})(\d)/,"$1-$2") //Coloca h�fen entre o quarto e o quinto d�gitos
return v
}

function cpfv(v){
v=v.replace(/\D/g,"") //Remove tudo o que n�o � d�gito
v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto d�gitos
v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto d�gitos
//de novo (para o segundo bloco de n�meros)
v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2") //Coloca um h�fen entre o terceiro e o quarto d�gitos
return v
}

function cepv(v){
v=v.replace(/\D/g,"") //Remove tudo o que n�o � d�gito
//v=v.replace(/(\d{2})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto d�gitos
v=v.replace(/(\d{3})(\d{1,3})$/,"$1-$2") //Coloca um h�fen entre o terceiro e o quarto d�gitos
return v
}

function cnpjv(v){
v=v.replace(/\D/g,"") //Remove tudo o que n�o � d�gito
v=v.replace(/^(\d{2})(\d)/,"$1.$2") //Coloca ponto entre o segundo e o terceiro d�gitos
v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3") //Coloca ponto entre o quinto e o sexto d�gitos
v=v.replace(/\.(\d{3})(\d)/,".$1/$2") //Coloca uma barra entre o oitavo e o nono d�gitos
v=v.replace(/(\d{4})(\d)/,"$1-$2") //Coloca um h�fen depois do bloco de quatro d�gitos
return v
}

function iev(v){
v=v.replace(/\D/g,"") //Remove tudo o que n�o � d�gito
v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto d�gitos
v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto d�gitos
//de novo (para o segundo bloco de n�meros)
v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto d�gitos
//de novo (para o segundo bloco de n�meros)
v=v.replace(/(\d{3})(\d)/,"$1.$2") //Coloca um ponto entre o terceiro e o quarto d�gitos
//de novo (para o segundo bloco de n�meros)
return v
}

function romanos(v){
v=v.toUpperCase() //Mai�sculas
v=v.replace(/[^IVXLCDM]/g,"") //Remove tudo o que n�o for I, V, X, L, C, D ou M
//Essa � complicada! Copiei daqui: http://www.diveintopython.org/refactoring/refactoring.html
while(v.replace(/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/,"")!="")
v=v.replace(/.$/,"")
return v
}

function site(v){
//Esse sem comentarios para que voc� entenda sozinho
v=v.replace(/^http:\/\/?/,"")
dominio=v
caminho=""
if(v.indexOf("/")>-1)
dominio=v.split("/")[0]
caminho=v.replace(/[^\/]*/,"")
dominio=dominio.replace(/[^\w\.\+-:@]/g,"")
caminho=caminho.replace(/[^\w\d\+-@:\?&=%\(\)\.]/g,"")
caminho=caminho.replace(/([\?&])=/,"$1")
if(caminho!="")dominio=dominio.replace(/\.+$/,"")
v="http://"+dominio+caminho
return v
}

function datav(v){
v=v.replace(/\D/g,"") //Remove tudo o que n�o � d�gito
v=v.replace(/^(\d{2})(\d)/,"$1/$2") //Coloca ponto entre o segundo e o terceiro d�gitos
v=v.replace(/\.(\d{2})(\d)/,".$1/$2") //Coloca uma barra entre o oitavo e o nono d�gitos
v=v.replace(/(\d{2})(\d)/,"$1/$2") //Coloca um h�fen depois do bloco de quatro d�gitos
return v
}

function moeda(v){
v=v.replace(/\D/g,"") // permite digitar apenas numero
//v=v.replace(/(\d{1})(\d{17})$/,"$1.$2") // coloca ponto antes dos ultimos digitos
//v=v.replace(/(\d{1})(\d{13})$/,"$1.$2") // coloca ponto antes dos ultimos 13 digitos
//v=v.replace(/(\d{1})(\d{10})$/,"$1.$2") // coloca ponto antes dos ultimos 10 digitos
//v=v.replace(/(\d{1})(\d{7})$/,"$1.$2") // coloca ponto antes dos ultimos 7 digitos
v=v.replace(/(\d{1})(\d{1,2})$/,"$1.$2") // coloca virgula antes dos ultimos 2 digitos
return v;
} 
function moeda2(v){
v=v.replace(/\D/g,"") // permite digitar apenas numero
//v=v.replace(/(\d{1})(\d{17})$/,"$1.$2") // coloca ponto antes dos ultimos digitos
//v=v.replace(/(\d{1})(\d{13})$/,"$1.$2") // coloca ponto antes dos ultimos 13 digitos
//v=v.replace(/(\d{1})(\d{10})$/,"$1.$2") // coloca ponto antes dos ultimos 10 digitos
//v=v.replace(/(\d{1})(\d{7})$/,"$1.$2") // coloca ponto antes dos ultimos 7 digitos
v=v.replace(/(\d{1})(\d{1,4})$/,"$1.$2") // coloca virgula antes dos ultimos 2 digitos
return v;
} 

//valida telefone
function ValidaTelefone(tel){
        exp = /\(\d{2}\)\ \d{4}\-\d{4}/
        if(!exp.test(tel.value))
                alert('Numero de Telefone Invalido!');
}

//valida CEP
function ValidaCep(cep){
        exp = /\d{2}\.\d{3}\-\d{3}/
        if(!exp.test(cep.value))
                alert('Numero de Cep Invalido!');               
}

//valida data
function ValidaData(data){
        exp = /\d{2}\/\d{2}\/\d{4}/
        if(!exp.test(data.value))
                alert('Data Invalida!');                        
}

//valida o CPF digitado
function ValidarCPF(Objcpf){
        var cpf = Objcpf.value;
        exp = /\.|\-/g
        cpf = cpf.toString().replace( exp, "" ); 
        var digitoDigitado = eval(cpf.charAt(9)+cpf.charAt(10));
        var soma1=0, soma2=0;
        var vlr =11;
        
        for(i=0;i<9;i++){
                soma1+=eval(cpf.charAt(i)*(vlr-1));
                soma2+=eval(cpf.charAt(i)*vlr);
                vlr--;
        }       
        soma1 = (((soma1*10)%11)==10 ? 0:((soma1*10)%11));
        soma2=(((soma2+(2*soma1))*10)%11);
        
        var digitoGerado=(soma1*10)+soma2;
        if(digitoGerado!=digitoDigitado)        
                alert('CPF Invalido!');         
}

//valida numero inteiro com mascara
function mascaraInteiro(){
        if (event.keyCode < 48 || event.keyCode > 57){
                event.returnValue = false;
                return false;
        }
        return true;
}

//valida o CNPJ digitado
function ValidarCNPJ(ObjCnpj){
        var cnpj = ObjCnpj.value;
        var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
        var dig1= new Number;
        var dig2= new Number;
        
        exp = /\.|\-|\//g
        cnpj = cnpj.toString().replace( exp, "" ); 
        var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));
                
        for(i = 0; i<valida.length; i++){
                dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);  
                dig2 += cnpj.charAt(i)*valida[i];       
        }
        dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
        dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));
        
        if(((dig1*10)+dig2) != digito)  {
                alert('CNPJ Invalido!');
				ObjCnpj.value="";
		}
                
}

//formata de forma generica os campos
function formataCampo(campo, Mascara, evento) { 
        var boleanoMascara; 
        
        var Digitato = evento.keyCode;
        exp = /\-|\.|\/|\(|\)| /g
        campoSoNumeros = campo.value.toString().replace( exp, "" ); 
   
        var posicaoCampo = 0;    
        var NovoValorCampo="";
        var TamanhoMascara = campoSoNumeros.length;; 
        
        if (Digitato != 8) { // backspace 
                for(i=0; i<= TamanhoMascara; i++) { 
                        boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                                                || (Mascara.charAt(i) == "/")) 
                        boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") 
                                                                || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 
                        if (boleanoMascara) { 
                                NovoValorCampo += Mascara.charAt(i); 
                                  TamanhoMascara++;
                        }else { 
                                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
                                posicaoCampo++; 
                          }              
                  }      
                campo.value = NovoValorCampo;
                  return true; 
        }else { 
                return true; 
        }
}