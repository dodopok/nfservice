<?
 include "../../admin/config.php"; //Conecta com a nosso banco de dados MySQL
 include "../../bloc.php"; //Verifica se a sessão está ativa
?>
<html>
<head>
	<meta charset="utf-8" />
 <title>Gerenciamento de Certificados</title>
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript">
jQuery(function($){
   $("#pesq").mask("99/99/9999");
});
</script>
<script type="text/javascript">
$(function() {
	$("#pesq").datepicker({
dateFormat: 'dd/mm/yy',
dayNames: [
'Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'
],
dayNamesMin: [
'D','S','T','Q','Q','S','S','D'
],
dayNamesShort: [
'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'
],
monthNames: [
'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',
'Outubro','Novembro','Dezembro'
],
monthNamesShort: [
'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set',
'Out','Nov','Dez'
],
nextText: 'Próximo',
prevText: 'Anterior'

});
});

</script>
</head>
<link href="../../css/css.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/themes/redmond/jquery-ui.css" type="text/css" media="all" />
 <body bgcolor="#FFFFFF" text="#000000">
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="top" bgcolor="#FFFFFF" >
		<fieldset id="GPSTATUS" style="-moz-border-radius:3pt;width:95%;" class="Group">
		<legend class="titulo">Cadastros Gerais &gt;&gt; Certificados</legend>
		<div align="justify" class="corpoprin">
		  <?
  $sql_cliente = "SELECT * FROM tb_cert WHERE id_dono = ".$_SESSION["numerocli"]." AND (id_user = ".$_SESSION['numerocli']." OR id_user = -1)";
 
  if($_POST['pesquisar'] == 'sim'){
      $sql_cliente = $sql_cliente." AND dtinicio='".formatData($_POST["pesq"])."'";
  }
  
	  
	  $sql_cliente = mysql_query($sql_cliente) or die("Erro no SQL: ".mysql_error());

 ?>
          <br>
          <table width="60%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td height="60"><div align="center" class="style2"><b>Gerenciamento de Certificados</b></div></td>
            </tr>
          </table>
          <form name="frm_pesq" method="post" action="<? echo $PHP_SELF?>">
            <table width="75%" border="0" cellspacing="1" cellpadding="0" align="center">
              <tr bgcolor="#6699CC">
                <td colspan="2"><div align="center"><font face="Arial" size="2"><b>Pesquisa</b></font></div></td>
              </tr>
              <tr bgcolor="ebebeb">
                <td width="32%"><font size="2" face="Arial" class="style1">Pesquisar data de In&iacute;cio:</font></td>
                <td width="68%"><font face="Arial" size="2">
                  <input name="pesq" type="text" id="pesq" onKeyPress="mascara(this,datav);" value="<?=date("d/m/Y");?>" maxlength="10" style="width:80px">
                  <input name="pesq2" type="submit" class="btn3" value="Pesquisar &gt;&gt;">
                  <input type="hidden" name="pesquisar" value="sim">
                </font></td>
              </tr>
            </table>
          </form>
<br>
          
         <form name="cadastro" action="cadcert.php" method="post">
  <input type="hidden" name="acao" value="entrar">
          <input name="Button" type="submit" class="btn3" value="Novo">
		  </form>
          <? if(mysql_num_rows($sql_cliente) > 0) {?>
          <table width="95%" border="0" cellspacing="1" cellpadding="0" align="center">
            <tr bgcolor="#6699CC">
              <td colspan="7"><div align="center"><font face="Arial" size="2"><b><font color="#FFFFFF">Certificados cadastrados</font></b></font></div></td>
            </tr>
            <tr bgcolor="cccccc">
              <td class="style1"><div align="center"><b><font face="Arial" size="2">C&oacute;digo</font></b></div></td>
			  <td  class="style1"><div align="center"><b><font face="Arial" size="2">Nome do Arquivo</font></b></div></td>
              <td class="style1"><div align="center"><b><font face="Arial" size="2">Data In&iacute;cio</font></b></div></td>
              <td class="style1"><div align="center"><b><font face="Arial" size="2">Data Validade</font></b></div></td>
              <td  class="style1"><div align="center"><b><font face="Arial" size="2">Status</font></b></div></td>
              <td  class="style1"><div align="center"><b><font face="Arial" size="2">Padr&atilde;o</font></b></div></td>
			  <td width="19%" class="style1"><div align="center"><b><font face="Arial" size="2">Excluir</font></b></div></td>
            </tr>
            <? while($array_cliente = mysql_fetch_array($sql_cliente)) {?>
            <tr bgcolor="ebebeb">
              <td  height="25" align="center"><font face="Arial" size="2" color="#000000"> <? echo $array_cliente['id'];?> </font></td>
			  <td  height="25"><font face="Arial" size="2" color="#000000"> <? echo $array_cliente['cert'];?> </font></td>
              <?php
			  	include_once('../../include/nfephp/libs/ToolsNFePHP.class.php');
				$cert = '../../include/nfephp/certs/'.$array_cliente['cert'];
				//carrega o certificado em um string
				$key = file_get_contents($cert);
				//carrega os certificados e chaves para um array denominado $x509certdata
				if (!openssl_pkcs12_read($key,$x509certdata,$array_cliente['senha']) ){
					$errorMsg = "O certificado n&atilde;o pode ser lido!! Provavelmente corrompido ou com formato inv&aacute;lido!!\n";
					$flagOK = false;
				}else{
					$errorMsg = "";
					$flagOK = true;
				}
				if (!openssl_x509_read($x509certdata['cert'])){
					$errorMsg = "O certificado n&atilde;o pode ser lido!! Provavelmente corrompido ou com formato inv&aacute;lido!!\n";
					$flagOK = false;
				}else{
					$errorMsg = "";
					$flagOK = true;
					$data = openssl_x509_read($x509certdata['cert']);
				}
				if (!openssl_x509_parse($data)){
					$errorMsg = "O certificado n&atilde;o pode ser lido!! Provavelmente corrompido ou com formato inv&aacute;lido!!\n";
					$flagOK = false;
				}else{
					$errorMsg = "";
					$flagOK = true;
					$cert_data = openssl_x509_parse($data);
				}				
				// reformata a data de validade;
				$ano = substr($cert_data['validTo'],0,2);
				$mes = substr($cert_data['validTo'],2,2);
				$dia = substr($cert_data['validTo'],4,2);
				//obtem o timeestamp da data de validade do certificado
				$dValid = date('d/m/Y',strtotime($dia.'/'.$mes.'/'.$ano));
				// obtem o timestamp da data de hoje
				$dHoje = date('d/m/Y');
				// compara a data de validade com a data atual
				if (strtotime($dValid) < strtotime($dHoje) ){
					$flagOK = false;
					$errorMsg = "A Validade do certificado expirou em ["  . $dia.'/'.$mes.'/'.$ano . "]";
				} else {					
					$errorMsg = "";
					$flagOK = true;
				}
				//diferença em segundos entre os timestamp
				$diferenca = $dValid - $dHoje;
				// convertendo para dias
				$diferenca = round($diferenca /(60*60*24),0);
				//carregando a propriedade
				$daysToExpire = $diferenca;
				// convertendo para meses e carregando a propriedade
				$m = ($ano * 12 + $mes);
				$n = (date("y") * 12 + date("m"));
				//numero de meses até o certificado expirar
				$monthsToExpire = ($m-$n);
				$certMonthsToExpire = $monthsToExpire;
				$certDaysToExpire = $daysToExpire;
					$certificado = array('status'=>$flagOK,'error'=>$errorMsg,'expira'=>$dia.'/'.$mes.'/'.$ano);
					if($certificado['error'] == 'A Validade do certificado expirou em [//]'){
						$certificado['error'] = "O certificado n&atilde;o pode ser lido!! Provavelmente corrompido ou com formato inv&aacute;lido!!\n";
					}
			  ?>
              <td  height="25" align="center"><font face="Arial" size="2" color="#000000"> <?=formatData($array_cliente['dtinicio']);?> </font></td>
              <td  height="25" align="center"><font face="Arial" size="2" color="#000000"> <?=$certificado['expira'];?> </font></td>
              <td height="25" align="center"><font face="Arial" size="2" color="#000000"> <? 
			  if (empty($certificado['error'])) {
								 echo "Ativo";
								 } else {
							     echo $certificado['error'];
								 }?> </font></td>
                                 <td align="center">
                                 	<?php
										$certificado = '';
                                    	$querycert = 'SELECT cert_padrao FROM tb_emitente WHERE cod_usuario = '.$_SESSION['numerocli'];
										$querycert = mysql_query($querycert) or die(mysql_error());
										$certpadrao = mysql_fetch_array($querycert);
										if($array_cliente['id'] == $certpadrao['cert_padrao']){
											echo '<img src="../imagens/allonsy.png" width="25px" alt="Certificado"/>';	
										}
									?>
                                 </td>
              <td width="19%" height="25"><div align="center"><font face="Arial" size="2" color="#000000">[ <a href='javascript: if (confirm("Deseja realmente excluir o cliente: <? echo $array_cliente['nome_user'];?>?" )) { window.open("excluircert.php?id_cliente=<? echo $array_cliente['id'];?>","_self");};'>Excluir</a> ]</font></div></td>
            </tr>
            <? }?>
          </table>
          <? }/* fecha mysql_num_rows > 0 */
  else{
   echo "<br><br><div align=center><font face=Arial size=2>
       Nenhum Certificado Cadastrado.<br><br></font></div>";
  }?>
  <form name="cadastro" action="cadcert.php" method="post">
  <input type="hidden" name="acao" value="entrar">
          <input name="Button" type="submit" class="btn3" value="Novo">
		  </form>
          <br>
          <br>
		</div>
		</fieldset>
		<p>&nbsp;</p></td>
      </tr>
      
      
      <tr>
        <td valign="top" bgcolor="#FFFFFF" ></td>
      </tr>
 </table>
 </body>
</html>

