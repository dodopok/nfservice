<?php
/**
 * Parâmetros de configuração do sistema
 * Última alteração em 07-05-2012 16:16:36 
 **/

//###############################
//########## GERAL ##############
//###############################
// tipo de ambiente esta informação deve ser editada pelo sistema
// 1-Produção 2-Homologação
// esta variável será utilizada para direcionar os arquivos e
// estabelecer o contato com o SEFAZ
if(empty($_POST['ambientecod'])){
	$_POST['ambientecod'] = $ambientecod;	
}
$ambiente=$_POST['ambientecod'];
//esta variável contêm o nome do arquivo com todas as url dos webservices do sefaz
//incluindo a versao dos mesmos, pois alguns estados não estão utilizando as
//mesmas versões
$arquivoURLxml="def_ws2.xml";
//Diretório onde serão mantidos os arquivos com as NFe em xml
//a partir deste diretório serão montados todos os subdiretórios do sistema
//de manipulação e armazenamento das NFe
$arquivosDir="xml";
//URL base da API, passa a ser necessária em virtude do uso dos arquivos wsdl
//para acesso ao ambiente nacional
$baseurl="http://www.nfstore.com.br/nfe/include/nfephp";
//Versão em uso dos shemas utilizados para validação dos xmls
$schemes="PL_006g";

//###############################
//###### EMPRESA EMITENTE #######
//###############################
//Nome da Empresa
$empresa=$razaosocial;
//Sigla da UF
$UF=$_POST['uf'];
//Código da UF
$cUF=$codibge['uf'];
//Número do CNPJ
$cnpjemi = $_POST['cnpj'];
$cnpjemi = str_replace('.','',$cnpjemi);
$cnpjemi = str_replace('/','',$cnpjemi);
$cnpjemi = str_replace('-','',$cnpjemi);
$cnpjemi = str_replace(' ','',$cnpjemi);
$cnpj=$cnpjemi;

//###############################
//#### CERITIFICADO DIGITAL #####
//###############################
//Nome do certificado que deve ser colocado na pasta certs da API
$certName=$querycert['cert'];
//Senha da chave privada
$keyPass=$querycert['senha'];
//Senha de decriptaçao da chave, normalmente não é necessaria
$passPhrase="";

//###############################
//############ DANFE ############
//###############################
//Configuração do DANFE
$danfeFormato=$_POST["tipoimpressao"]; //P-Retrato L-Paisagem
$danfePapel="A4"; //Tipo de papel utilizado 
$danfeCanhoto=1; //se verdadeiro imprime o canhoto na DANFE 
$danfeLogo="/var/www/nfstore.com.br/web/nfe/cadastros/emitente/logo/".$_SESSION["numerocli"].".jpg"; //passa o caminho para o LOGO da empresa 
$danfeLogoPos="L"; //define a posição do logo na Danfe L-esquerda, C-dentro e R-direta 
$danfeFonte="Times"; //define a fonte do Danfe limitada as fontes compiladas no FPDF (Times) 
$danfePrinter="hpteste"; //define a impressora para impressão da Danfe 

//###############################
//############ EMAIL ############
//###############################
//Configuração do email
$mailAuth="1"; //ativa ou desativa a obrigatoriedade de autenticação no envio de email, na maioria das vezes ativar 
$mailFROM="nfe@nfservice.com.br"; //identificação do emitente 
$mailHOST="smtp.nfservice.com.br"; //endereço do servidor SMTP 
$mailUSER="nfe@nfservice.com.br"; //username para autenticação, usando quando mailAuth é 1
$mailPASS="pwdh1245"; //senha de autenticação do serviço de email
$mailPROTOCOL=""; //protocolo de email utilizado (classe alternate)
$mailPORT="25"; //porta utilizada pelo smtp (classe alternate)
$mailFROMmail="nfe@nfservice.com.br"; //para alteração da identificação do remetente, pode causar problemas com filtros de spam 
$mailFROMname="Emissor NFService"; //para indicar o nome do remetente 
$mailREPLYTOmail="nfe@nfservice.com.br"; //para indicar o email de resposta
$mailREPLYTOname="Emissor NFService"; //para indicar email de cópia

//###############################
//############ PROXY ############
//###############################
//Configuração de Proxy
$proxyIP=""; //ip do servidor proxy, se existir 
$proxyPORT=""; //numero da porta usada pelo proxy 
$proxyUSER=""; //nome do usuário, se o proxy exigir autenticação
$proxyPASS=""; //senha de autenticação do proxy 

?>