<?
	ini_set('display_errors','On');
	ini_set('error_reporting', E_ALL);
	error_reporting(E_ALL);
	function geraDacce($chave){		
            require_once("html2pdf/html2pdf.class.php");
			require_once("../../include/nfephp/libs/ToolsNFePHP.class.php");            
            $nfe         = new ToolsNFePHP();         
			$sqlnseq = 'SELECT nseq FROM tb_cce WHERE chave = "'.$chave.'" ORDER BY id DESC LIMIT 1';   
			$sqlnseq = mysql_query($sqlnseq);
			$sqlnseq = mysql_fetch_array($sqlnseq);
			$nseq = $sqlnseq['nseq'];
            $evento = simplexml_load_file("../xml/procEnviCCe/NFe".$chave."-".$nseq."-procEnviCCe.xml");
            $nfeoriginal = simplexml_load_file("../xml/NFe/NFe".$chave."-nfe.xml");		
			$eventos['Id']         = $evento->evento->infEvento['Id'];
            $eventos['dhEvento']   = $evento->evento->infEvento->dhEvento;
            $eventos['nProt']      = $evento->retEvento->infEvento->nProt;
			$i++;
            $chave = substr($eventos['Id'],2);
            $chave1 = substr($chave, 0,4)." ".substr($chave, 4,4)." ".substr($chave, 8,4)." ".substr($chave, 12,4)." ".substr($chave, 16,4)." ".substr($chave, 20,4)." ".substr($chave, 24,4)." ".substr($chave, 28,4);
            $chave2 = substr($chave, 32,4)." ".substr($chave, 36,4)." ".substr($chave, 40,4)." ".substr($chave, 44,4)." ".substr($chave, 48,4);            
            $html = '<style>
						table#table100{
							width:100%;
							border-collapse: collapse;
						} 
                    </style>
                    <table id="table100">
                        <tr>
                            <td rowspan="3" style="border: 1px solid; text-align: center; width: 45%">
                                <img alt="Logo" src="../../cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg" >
                            </td>
                            <td rowspan="3" style="border: 1px solid; text-align: center; width: 20%">
                                <strong>DACCE</strong><br/>DOCUMENTO AUXILIDAR DA CARTA DE CORRE&Ccedil;&Atilde;O ELETR&Ocirc;NICA<br />
                                S&Eacute;RIE '.$nfeoriginal->infNFe->ide->serie.' - SEQ '.$nseq.'</b>
                            </td>
                            <td style="border: 1px solid; padding-left: 5px; width: 35%">
                                <span style="font-size: 8px">PROTOCOLO DE AUTORIZA&Ccedil;&Atilde;O DE USO</span><br />
                                '.$eventos['nProt'].'
                            </td>
                        </tr>
                        <tr>
                            <td  style="border-right: 1px solid; border-bottom: 1px solid; padding-left: 5px; ">
                                <span style="font-size: 8px">DATA/HORA DE REGISTRO DO EVENTO</span><br />
                                '.$eventos['dhEvento'].'
                            </td>
                        </tr>
                        <tr>
                            <td  style="border-right: 1px solid; border-bottom: 1px solid; padding-left: 5px; ">
                                <span style="font-size: 8px">CHAVE DE ACESSO</span><br />
                                '.$chave1.'<br />'.$chave2.'
                            </td>
                        </tr>
                        <tr>
                            <td  colspan="2" style="border: 1px solid; padding-left: 5px;">
                                <span style="font-size: 8px">CNPJ</span><br />
                                  '.$nfeoriginal->infNFe->emit->CNPJ.'
                            </td>
                            <td  style="border: 1px solid; padding-left: 5px;">
                                <span style="font-size: 8px">INSCRI&Ccedil;&Atilde;O ESTADUAL</span><br />
                                 '.$nfeoriginal->infNFe->emit->IE.'
                            </td>
                        </tr>
                    </table>
                    <span style="font-size: 8px"><b>DESTINAT&Aacute;RIO</b></span>
                    <table id="table100"  >
                        <tr>
                            <td style="border: 1px solid; padding-left: 5px; width: 70%"  colspan="2">
                                <span style="font-size: 8px">NOME/RAZAO SOCIAL</span><br />
                                '.$nfeoriginal->infNFe->dest->xNome.'
                            </td>
                            <td style="border: 1px solid; padding-left: 5px; width: 15%" >
                                <span style="font-size: 8px">CNPJ/CPF</span><br />
                                '.$nfeoriginal->infNFe->dest->CNPJ.' 
                            </td>
                            <td style="border: 1px solid; padding-left: 5px; width: 15%" >
                                <span style="font-size: 8px">DATA DA EMISS&Atilde;O DA NF-e</span><br />
                                '.substr($nfeoriginal->infNFe->ide->dSaiEnt, 8,2).'/'.substr($nfeoriginal->infNFe->ide->dSaiEnt, 5,2).'/'.substr($nfeoriginal->infNFe->ide->dSaiEnt, 0,4).' 
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid; padding-left: 5px" >
                                <span style="font-size: 8px">ENDERE&Ccedil;O</span><br />
                                '.$nfeoriginal->infNFe->dest->enderDest->xLgr.', '.$nfeoriginal->infNFe->dest->enderDest->nro.' 
                            </td>
                            <td style="border: 1px solid; padding-left: 5px" colspan="2">
                                <span style="font-size: 8px">BAIRRO/DISTRITO</span><br />
                                '.$nfeoriginal->infNFe->dest->enderDest->xBairro.' 
                            </td>
                            <td style="border: 1px solid; padding-left: 5px" >
                                <span style="font-size: 8px">CEP</span><br />
                                '.$nfeoriginal->infNFe->dest->enderDest->CEP.'  
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid; padding-left: 5px" >
                                <span style="font-size: 8px">MUNIC&Iacute;PIO</span><br />
                                '.$nfeoriginal->infNFe->dest->enderDest->xMun.' 
                            </td>
                            <td style="border: 1px solid; padding-left: 5px" >
                                <span style="font-size: 8px">FONE/FAX</span><br />
                                '.$nfeoriginal->infNFe->dest->enderDest->fone.' 
                            </td>
                            <td style="border: 1px solid; padding-left: 5px" >
                                <span style="font-size: 8px">UF</span><br />
                                '.$nfeoriginal->infNFe->dest->enderDest->UF.' 
                            </td>
                            <td style="border: 1px solid; padding-left: 5px" >
                                <span style="font-size: 8px">INSCRI&Ccedil;&Atilde;O ESTADUAL</span><br />
                                '.$nfeoriginal->infNFe->dest->IE.' 
                            </td>
                        </tr>
                    </table>
                    <span style="font-size: 8px"><b>CORRE&Ccedil;&Atilde;O A SER CONSIDERADA</b></span>
                    <table id="table100"  >
                        <tr>
                            <td style="height: 400px; vertical-align: top; border: 1px solid; padding-left: 5px; width: 100%;font:12;">
                                ';
								$correcao = (string)$evento->evento->infEvento->detEvento->xCorrecao;
								$correcao = str_replace('\n','',$correcao);
								for($cont=0; $cont <= strlen($correcao); $cont++){
									$html .= $correcao[$cont];
									if($cont%79 == 0 && $cont !== 0){
										$html .= '<br />';   
									}
								}
								$html.=' 
                            </td>
                        </tr>
                    </table>                   
                    <span style="font-size: 8px"><b>CONDI&Ccedil;&Atilde;O DE USO</b></span>
                    <table id="table100"  >
                        <tr>
                            <td style="height: 200px; vertical-align: top; border: 1px solid; padding-left: 5px; width: 100%">
                                A Carta de Correcao e disciplinada pelo paragrafo 1o-A do art. 7o do Convenio S/N, de 15 de dezembro de 1970 e pode ser utilizada para regularizacao de erro ocorrido na emissao de documento fiscal, desde que o erro nao esteja relacionado com: I - as variaveis que determinam o valor do imposto tais como: base de calculo, aliquota, diferenca de preco, quantidade, valor da operacao ou da prestacao; II - a correcao de dados cadastrais que implique mudanca do remetente ou do destinatario; III - a data de emissao ou de saida.
                            </td>
                        </tr>
                    </table>
                    <span style="font-size: 8px">DACCE gerada pelo NFePHP</span>';
    $html = utf8_encode($html);
	// converte o conteudo para uft-8
	
	define('MPDF_PATH', 'MPDF54/');
	include(MPDF_PATH.'mpdf.php');
	// inclui a classe
	
	$mpdf = new mPDF();
	// cria o objeto
	$mpdf->allow_charset_conversion=true;
	// permite a conversao (opcional)
	$mpdf->charset_in='UTF-8';
	// converte todo o PDF para utf-8
	$mpdf->WriteHTML($html);
	// escreve definitivamente o conteudo no PDF
	
	$pdf = $mpdf->Output('Dup-'.$chave.'.pdf','S');
	if(!empty($_SESSION['numerocli'])){
		header('Content-type: application/pdf');
		echo $pdf;
	}      
            

        }
        ?>