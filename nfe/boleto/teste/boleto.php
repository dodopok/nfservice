<?php
/*
C�digo reestruturado por: Daniel William Schultz
Email: hospedavip@hospedavip.com

Este c�digo foi construido atrav�s de reutiliza��o de c�digos do PHPBoleto para BB.
Fique livre pra mudar este programa, redistribuir de gra�a, vender...
S� pe�o que n�o roube os creditos, que pertencem em grande parte � equipe do PHPBoleto...E em �nfima parte, � mim.

Os valores ai embaixo podem ser colocados manualmente, atrav�s de formul�rio com POST, GET
Ou retirados de uma tabela mysql, pgsql....ou qualquer nome-sql

Ensinar isso seria demais, vai em www.php.net e come�e a ler o manual ;o)
*/

/*
Dados do boleto - Obrigat�rios
*/

$dadosboleto["data_vencimento"] = "31/05/2004"; // Data de Vencimento do Boleto
$dadosboleto["data_documento"] = "27/05/2004"; // Data de emiss�o do Boleto
$dadosboleto["data_processamento"] = ""; // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = "1,00"; 	// Valor do Boleto, com v�rgula, sempre com duas casas depois da virgula

//opcionais
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "N";		
$dadosboleto["uso_banco"] = ""; 	
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DM";

//dados da sua conta e conv�nio
$dadosboleto["agencia"] = "9999"; // Num da agencia, sem digito
$dadosboleto["conta"] = "9999"; 	// Num da conta, sem digito
//convenio e contrato podem ser vistos no gerenciador financeiro do BB
$dadosboleto["convenio"] = "999999";  // Num do conv�nio
$dadosboleto["contrato"] = "999999"; // Num do seu contrato

/*
FORMATA��O DO NOSSO NUMERO
*/

$dadosboleto["formatacao_nosso_numero"] = "1";

/*
#################################################
Sei que isso funciona pra carteira 18....pras outras, deixe op��o 1

1	=	Formata��o gerada: Num do convenio + 5 digitos informados por voc� + digito verificador
		(neste caso, informe de 1 a 5 digitos somente)

2	=	para 17 digitos informados por voc� ( de 1 a 99999999999999999)

Se voc� n�o entendeu, deixe a op��o 1 e informe at� 5 digitos no nosso numero

Nosso n�mero:
de 1 a 99999 para op��o de 12 d�gitos
de 1 a 99999999999999999 para op��o de 17 d�gitos
#################################################
*/

$dadosboleto["nosso_numero"] = "95866";
$dadosboleto["numero_documento"] = "95866";	// Num do pedido ou nosso numero
$dadosboleto["carteira"] = "18";  // C�digo da Carteira 18 - 17 ou 11
$dadosboleto["variacao_carteira"] = "-019";  // Varia��o da Carteira, com tra�o (opcional)

/*
SEUS DADOS
*/
$dadosboleto["cpf_cnpj"] = "000.000.000-00";
$dadosboleto["endereco"] = "Rua do chapeuzinho vermelho";
$dadosboleto["cidade"] = "Juiz de Fora - MG";
$dadosboleto["cedente"] = "Minha empresa";

/*
DADOS DO SEU CLIENTE
*/
$dadosboleto["sacado"] = "Lobo Mau";
$dadosboleto["endereco1"] = "Rua do lobo mau";
$dadosboleto["endereco2"] = "Juiz de Fora - MG - CEP: 000000-000";

/*
INSTRU��ES PARA O CLIENTE
*/
$dadosboleto["instrucoes"] = "Mensalidade referente ao dominio bla bla - Plano bla bla<br>Taxa banc�ria - R$ 2,00";
$dadosboleto["instrucoes1"] = "HospedaVIP - Solu��es em hospedagem - http://www.hospedavip.com";
$dadosboleto["instrucoes2"] = "- Sr. Caixa, cobrar multa de 2% ap�s o vencimento";
$dadosboleto["instrucoes3"] = "- Receber at� 10 dias ap�s o vencimento";
$dadosboleto["instrucoes4"] = "- Em caso de d�vidas entre em contato conosco: financeiro@hospedavip.com";

//S� MEXA DEPOIS DISSO SE VOC� FOR EXPERIENTE EM PHP
include("include/funcoesbb.php"); 
include("include/layoutbbhtml.php");
?>