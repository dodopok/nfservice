<?
	//ini_set('display_errors','On');
	//ini_set('error_reporting', E_ALL);
	//error_reporting(E_ALL);
 include "../../admin/config.php"; //Conecta com a nosso banco de dados MySQL
 include "../../bloc.php"; //Verifica se a sess�o est� ativa
?>
<!DOCTYPE HTML>
<html>
<head>
 <title>Emitir Nova NF-e</title>
 <link rel="stylesheet" href="../../css/css.css" type="text/css" />
 <body bgcolor="#FFFFFF" text="#000000">
 <div style=" background:#FFFFFF;width:100%">
 <fieldset id="GPSTATUS" style="-moz-border-radius:3pt;width:98%; background:#FFFFFF; padding:5px;" class="Group">
<legend class="titulo">Notas Fiscais Eletr�nicas &gt;&gt; Emitir Nova NF-e</legend>	
<div align="center" class="style2"><b>Cadastro de Nova NF-e</b></div><br/>
<span class="afsystems" style="color:#000">
<?php
/**
  Ultima atualiza��o = 21/04/2011
  Exemplo de TXT para nota 2.0
 
  R�gis Matos
  Site       = http://www.gestorcustom.com.br
  E-Mail/MSN = regismatos@douradosvirtual.com.br
  skype      = regis_matos

**/
//var_dump($_POST);
//fun��o para gerar o c�digo da nf
function geraCN($length=8){
    $numero = '';    
    for ($x=0;$x<$length;$x++){
        $numero .= rand(0,9);
    }
    return $numero;
}
function calcula_dv($chave43) { 
    $soma_ponderada = 0; 
    $multiplicadores = array(2,3,4,5,6,7,8,9); 
    $i = 42; 
    while ($i >= 0) { 
        for ($m=0; $m<count($multiplicadores) && $i>=0; $m++) { 
            $soma_ponderada+= $chave43[$i] * $multiplicadores[$m]; 
            $i--; 
        } 
    } 
    $resto = $soma_ponderada % 11; 
    if ($resto == '0' || $resto == '1') { 
        return 0; 
    } else { 
        return (11 - $resto); 
   } 

}
$regimetrib=$_POST['regimetrib'];
$contadorprod=$_POST['contadorprod'];
$contadordup=$_POST['contadordup'];
$ambientecod=$_POST['ambientecod'];
$razaosocial=$_POST['razaosocial'];
$aliquota_pisp=$_POST['aliquota_pisp'];
$aliquota_cofinsp=$_POST['aliquota_cofinsp'];
$aliquota_icmsp=$_POST['aliquota_icmsp'];

if (empty($_POST["total_frete"])){
$total_frete="0.00";
} else {
$total_frete=$_POST["total_frete"];					 
}

if (empty($_POST["total_seguro"])){
$total_seguro="0.00";
} else {
$total_seguro=$_POST["total_seguro"];					 
}

if (empty($_POST["total_desconto"])){
$total_desconto="0.00";
} else {
$total_desconto=$_POST["total_desconto"];					 
}


if (empty($_POST["outrasdespesas"])){
$outrasdespesas="0.00";
} else {
$outrasdespesas=$_POST["outrasdespesas"];					 
}

$cnpjemi = $_POST['cnpj'];
$cnpjemi = str_replace('-','',$cnpjemi);
$cnpjemi = str_replace('/','',$cnpjemi);
$cnpjemi = str_replace('.','',$cnpjemi);
$cnpjemi = str_replace(' ','',$cnpjemi);
$aa = substr($_POST['dataemissao'],8,2);
$mm = substr($_POST['dataemissao'],3,2);
$queryest = "SELECT cod_ibge,id_estado,uf FROM tb_estados WHERE uf = '".$_POST['uf']."'";
$codibge = mysql_fetch_array(mysql_query($queryest));
$cUF = $codibge['cod_ibge'];    //C�digo da UF [02] 
$aamm = $aa.$mm;     //AAMM da emiss�o [4]
$cnpj = $cnpjemi;     //CNPJ do Emitente [14]
$mod='55';      //Modelo [02]
$serie = str_pad($_POST['serie'], 3, '0',STR_PAD_LEFT);
$querynumnota = "SELECT CAST(max(idnota+1) as CHAR) as idnotachar FROM tb_notas WHERE id_user = '".$_SESSION["numerocli"]."' and ambiente='".$ambientecod."' and notaaut=1 ORDER BY idnota DESC LIMIT 1";
$querynumnota = mysql_query($querynumnota) or die(mysql_error());
$numnota = mysql_fetch_array($querynumnota);
$num=$numnota['idnotachar'];  
if ($num<=0){
$num="".($_POST['ninicial']+1)."";	
}

//N�mero da NF-e [09]
$num1=$numnota['idnotachar'];
if ($num1<=0){
$num1="".($_POST['ninicial']+1)."";
}

$tpEmis=$_POST['formaemissao'];     //forma de emiss�o da NF-e [01] 1 � Normal � emiss�o normal; 2 � Conting�ncia FS; 3 � Conting�ncia SCAN; 4 � Conting�ncia DPEC; 5 � Conting�ncia FS-DA 
$cn= geraCN(8);         //C�digo Num�rico [08]
$dv='';         //DV [01]
//ajusta comprimento do numero
$num = str_pad($num, 9, '0',STR_PAD_LEFT);
//calcula codigo num�rico aleat�rio

//monta a chave sem o digito verificador
$chave = $cUF.$aamm.$cnpj.$mod.$serie.$num.$tpEmis.$cn;
$dv = calcula_dv($chave);
$chave = $chave.$dv;

$n = strlen($chave);

$nomemun = $_POST['municipio'];
$querymun = mysql_fetch_array(mysql_query("SELECT * FROM tab_municipios WHERE nome = '".$nomemun."'"));

$certpadrao = $_POST['cert_padrao'];
$querycert = "SELECT * FROM tb_cert WHERE id = ".$certpadrao. " and id_user = ".$_SESSION['numerocli'];
$querycert = mysql_query($querycert);
$querycert = mysql_fetch_array($querycert);

//Gerou a chave de acesso
require_once("../../include/NFeTXT2.class.php");
   
$nfe = new  NFeTXT2;

$nfe->setVersao("2.00");
//$nfe->id = $chave; // O id � calculado automaticamente
$nfe->setCUF($codibge['cod_ibge']);
$nfe->setCNF($cn);
$nfe->setNatOp($_POST['natop']);
$nfe->setIndPag($_POST['formapagamento']);
$nfe->setMod("55");
$nfe->setSerie($_POST['serie']);
$nfe->setNNF($num1);
$data_nova = implode(preg_match("~\/~", $_POST['dataemissao']) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $_POST['dataemissao']) == 0 ? "-" : "/", $_POST['dataemissao'])));
$nfe->setDEmi($data_nova); // ( aaaa-mm-dd ) ficar atento com o formato
$data_nova = implode(preg_match("~\/~", $_POST['datase']) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $_POST['datase']) == 0 ? "-" : "/", $_POST['datase'])));
$nfe->setDSaiEnt($data_nova);
$nfe->setHSaiEnt($_POST["horase"]);
$nfe->setTpNF($_POST['tipodocumento']);
$nfe->setCMunFG($querymun["id"]);
$nfe->setTpImp("1");
$nfe->setTpEmis($_POST['formaemissao']);
//$nfe->setCDV("0"); // � gerado automaticamente
$nfe->setTpAmb($ambientecod); // 1-Produ��o/ 2-Homologa��o
$nfe->setFinNFe($_POST['finalidadeemissao']);
$nfe->setProcEmi("0");
$nfe->setVerProc("2.1.4");
$nfe->setDhCont("");
$nfe->setXJust("");

//Dados do emitente
$emi[XNome]  = $razaosocial;
$emi[XFant]  = $_POST['nomefantasia'];
$emi[IE] 	 = $_POST['inscricaoestadual'];
$emi[IEST]   = '';
$emi[IM]     = $_POST['inscricaomunicipal'];
$emi[CNAE]   = $_POST['cnae'];
$emi[CRT]    = $regimetrib;
$emi[CNPJ]   = $cnpjemi;
//$emi[CPF]  = "";
$emi[xLgr]   = $_POST['logradouro'];
$emi[nro]    = $_POST['numero'];
$emi[Cpl] 	 = $_POST['complemento'];
$emi[Bairro] = $_POST['bairro'];
$emi[CMun]   = $querymun['id'];
$emi[XMun]   = $nomemun;
$emi[UF]     = $_POST['uf'];
$emi[CEP]    = str_replace('-','',$_POST['cep']);
$emi[cPais]  = "1058";
$emi[xPais]  = "BRASIL";
$telefone_emitente = str_replace('(','',$_POST['telefone']);
$telefone_emitente = str_replace(')','',$telefone_emitente);
$telefone_emitente = str_replace('-','',$telefone_emitente);
$telefone_emitente = str_replace(' ','',$telefone_emitente);
$emi[fone]   =$telefone_emitente;

$nfe->setEmi($emi);


//destinatario
$dest[xNome]   = $_POST['nome_dest'];
$iedest = str_replace('.','',$_POST['inscricaoestadual_dest']);
$iedest = str_replace('-','',$iedest);
$dest[IE]      = $iedest;
$dest[ISUF]    = $_POST['inscricaosuframa_dest'];
$dest[email]   = $_POST['email_dest'];
$cnpjdest = str_replace('/','',$_POST['cpfcnpj_dest']);
$cnpjdest = str_replace('-','',$cnpjdest);
$cnpjdest = str_replace('.','',$cnpjdest);
$dest[CNPJ]    = $cnpjdest;
//$dest[CPF]   = "";
$dest[xLgr]    = $_POST['logradouro_dest'];
$dest[nro]     = $_POST['numero_dest'];;
$dest[xCpl]    = $_POST['complemento_dest'];
$dest[xBairro] = $_POST['bairro_dest'];
$nomemun = $_POST['municipio_dest'];
$querymun = mysql_fetch_array(mysql_query("SELECT * FROM tab_municipios WHERE nome = '".$nomemun."'"));
$dest[cMun]   = $querymun['id'];
$dest[xMun]    = $_POST['municipio_dest'];
$dest[UF]      = $_POST['uf_dest'];
$dest[CEP]     = str_replace('-','',$_POST['cep_dest']);
$dest[cPais]   = "1058";
$dest[xPais]   = "BRASIL";
$telefone_dest = str_replace('(','',$_POST['telefone_dest']);
$telefone_dest = str_replace(')','',$telefone_dest);
$telefone_dest = str_replace('-','',$telefone_dest);
$telefone_dest = str_replace(' ','',$telefone_dest);
$dest[fone]    = $telefone_dest;

$nfe->setDest($dest);

/** // Informar apenas quando for diferente do endere�o do remetente.
if($_POST['endentregar']<>$_POST['idend']){
	$queryendereco = "SELECT * FROM tb_enderecos WHERE id = ".$_POST['endentregar'];
	$queryendereco = mysql_query($queryendereco);
	$enderecomd = mysql_fetch_array($queryendereco);
	$retirada[CNPJ] = $cnpjdest;
	$retirada[xLgr] = $enderecomd['logradouro'];
	$retirada[nro] = $enderecomd['numero'];
	$retirada[XCpl] = $enderecomd['complemento'];
	$retirada[XBairro] = $enderecomd['bairro'];
	$nomemun = $enderecomd['cidade'];
	$querymun = mysql_fetch_array(mysql_query("SELECT * FROM tab_municipios WHERE nome = '".$nomemun."'"));
	//$retirada[CMun] = $querymun['id'];
	$retirada[XMun] = $enderecomd['cidade'];
	$queryest = "SELECT uf FROM tb_estados WHERE estado = '".$enderecomd['id_estado']."'";
	$id = mysql_fetch_array(mysql_query($queryest));
	$retirada[UF] = $id['uf'];	
	$nfe->setRetirada($retirada);
}
/** //Informar apenas quando for diferente do endere�o do destinat�rio.
if($_POST['endcobrancar']<>$_POST['idend']){
	$queryendereco = "SELECT * FROM tb_enderecos WHERE id = ".$_POST['endcobrancar'];
	$queryendereco = mysql_query($queryendereco);
	$enderecomd = mysql_fetch_array($queryendereco);
	$entrega[CNPJ] = $cnpjdest;
	$entrega[xLgr] = $enderecomd['logradouro'];
	$entrega[nro] = $enderecomd['numero'];
	$entrega[XCpl] = $enderecomd['complemento'];
	$entrega[xBairro] = $enderecomd['bairro'];
	$nomemun = $enderecomd['cidade'];
	$querymun = mysql_fetch_array(mysql_query("SELECT * FROM tab_municipios WHERE nome = '".$nomemun."'"));
	//$entrega[CMun] = $querymun['id'];
	$entrega[XMun] = $enderecomd['cidade'];
	$queryest = "SELECT uf FROM tb_estados WHERE estado = '".$enderecomd['id_estado']."'";
	$id = mysql_fetch_array(mysql_query($queryest));
	$entrega[UF] = $id['uf'];	
	$nfe->setEntrega($entrega);
}
**/

//produtos
//Obs. A variavel $i tem que ser iniciado com ( 0 ) zero 
$valortotal_produtos=0;
$valortotal_bcicms=0;
$valortotal_icms=0;
$valortotal_pis=0;
$valortotal_cofins=0;
$valortotal_ipi=0;

$vrproduto_frete=(float)$total_frete/$contadorprod;
$vrproduto_seguro=(float)$total_seguro/$contadorprod;
$vrproduto_desconto=(float)$total_desconto/$contadorprod;
$vrproduto_outras=(float)$outrasdespesas/$contadorprod;


for ($i = 0; $i < $contadorprod; $i++){
	$queryproduto = "SELECT * FROM tb_produtos WHERE id_user = ".$_SESSION['numerocli']."  and cod_produto = ".addslashes($_POST['cod_produto'.$i]);
	$produto = mysql_fetch_array(mysql_query($queryproduto));
    $prod[$i][infAdProd] = "INFORMACOES ADICIONAIS DO PRODUTO";
	
    $prod[$i][CProd]     = $produto['cod_produto'];
    $prod[$i][CEAN]      = ""; //$produto['cod_ean']
    $prod[$i][XProd]     = $produto['descricao'];
    $prod[$i][NCM]       = str_replace('.','',$produto['cod_ncm']);
    $prod[$i][EXTIPI]    = $produto['ex_ipi'];
    $prod[$i][CFOP]      = $produto['cfop'];
    $prod[$i][UCom]      = $produto['unid_comercial'];
    $prod[$i][QCom]      = sprintf("%8.2f", $_POST['qtd'.$i]);
    $prod[$i][VUnCom]    = sprintf("%8.2f", $produto['valor_unit']);
    $prod[$i][VProd]     = sprintf("%8.2f", $_POST['vl_unit'.$i]*$_POST['qtd'.$i]);   
	//var_dump($prod[0][VProd]);
    $prod[$i][CEANTrib]  = $codigoe;
    $prod[$i][UTrib]     = sprintf("%8.2f", $produto['unid_trib']);
    $prod[$i][QTrib]     = sprintf("%8.2f", $_POST['qtd'.$i]);
    $prod[$i][VUnTrib]   = sprintf("%8.2f", $produto['valor_unitrib']);
	if ($vrproduto_frete>=1){
    $prod[$i][VFrete]    = sprintf("%8.2f", $vrproduto_frete);
	} else {
	$prod[$i][VFrete]    = "$vrproduto_frete";	
	}
	if ($vrproduto_seguro>=1){
    $prod[$i][VSeg] 	 = sprintf("%8.2f", $vrproduto_seguro);
	} else {
	$prod[$i][VSeg] 	 = "$vrproduto_seguro";
	}
	if ($vrproduto_desconto>=1){
    $prod[$i][VDesc]     = sprintf("%8.2f", $vrproduto_desconto);
	} else {
	$prod[$i][VDesc]     = "$vrproduto_desconto";
	}
	if ($vrproduto_outras>=1){
    $prod[$i][vOutro] 	 = sprintf("%8.2f", $vrproduto_outras);
	} else {
	$prod[$i][vOutro] 	 = "$vrproduto_outras";
	}
    $prod[$i][indTot] 	 = "1";
	//echo $prod[$i][indTot];
    $prod[$i][xPed] 	 = "";
    $prod[$i][nItemPed]  = str_replace(' ','',$i+1);	
	if($regimetrib==1){
		//icms
		$icms[$i][Orig]    = "0";
		$icms[$i][CST]     = "00";
		//$icms[$i][CSOSN] = "101";
		$icms[$i][ModBC]   = "0";
		$icms[$i][VBC] 	   = "0";
		$icms[$i][PICMS]   = "0";
		$icms[$i][VICMS]   = "0";
		
		//ipi
		$ipi[$i][ClEnq]    = "";
		$ipi[$i][CNPJProd] = "";
		$ipi[$i][CSelo]    = "";
		$ipi[$i][QSelo]    = "";
		$ipi[$i][CEnq] 	   = "999";
		$ipi[$i][CST]      = "52";
	
		//pis
    $pis[$i][CST]  = str_pad($produto['situacaotrib_pis'], 2, '0',STR_PAD_LEFT);
    $pis[$i][VBC]  = sprintf("%8.2f", $prod[$i][VProd]); 
    $pis[$i][PPIS] = sprintf("%8.2f", $aliquota_pisp); 
	$VPIS=round(($aliquota_pisp*$prod[$i][VProd])/100,2);
    $pis[$i][VPIS] = sprintf("%8.2f", $VPIS);
	$valortotal_pis=$VPIS+$valortotal_pis;

    //cofins
    $cofins[$i][CST]     = str_pad($produto['situacaotrib_cofins'], 2, '0',STR_PAD_LEFT);
    $cofins[$i][VBC]     = sprintf("%8.2f", $prod[$i][VProd]); 
    $cofins[$i][PCOFINS] = sprintf("%8.2f", $aliquota_cofinsp); 
	$VCOFINS=round(($aliquota_cofinsp*$prod[$i][VProd])/100,2);
    $cofins[$i][VCOFINS] = sprintf("%8.2f", $VCOFINS);
	$valortotal_cofins=$VCOFINS+$valortotal_cofins;
	
		//cofins st
		$cofinsst[$i][VCOFINS]   = "";
		$cofinsst[$i][VBC]       = "";
		$cofinsst[$i][PCOFINS]   = "";
		$cofinsst[$i][QBCProd]   = "";
		$cofinsst[$i][VAliqProd] = "";
	}else{		
		 //icms
    $icms[$i][Orig]    = $produto['origem'];
    $icms[$i][CST]     = str_pad($produto['tipo_trib_icms'], 2, '0',STR_PAD_LEFT);
	//$icms[$i][CSOSN] = "101";
	$icms[$i][ModBC]   = $produto['modalidade_determinacaobc'];
    $icms[$i][VBC] 	   = sprintf("%8.2f", $produto['basecalculo_icms']*$_POST['qtd'.$i]);
    $icms[$i][PICMS]   = sprintf("%8.2f", $produto['aliquota_icms']);
	$VICMS=round((($produto['basecalculo_icms']*$_POST['qtd'.$i])*$produto['aliquota_icms'])/100,2);
    $icms[$i][VICMS]   = sprintf("%8.2f", $VICMS);
	$valortotal_bcicms=($produto['basecalculo_icms']*$_POST['qtd'.$i])+$valortotal_bcicms;
	$valortotal_icms=$VICMS+$valortotal_icms;
	
    //ipi
    $ipi[$i][ClEnq]    = $produto['classe_enquadramentoipi'];
    $ipi[$i][CNPJProd] = $produto['cnpj_produtos'];
    $ipi[$i][CSelo]    = $produto['cod_selocontrole'];
    $ipi[$i][QSelo]    = $produto['qtd_selocontrole'];
    $ipi[$i][CEnq] 	   = $produto['cod_enquadramentoipi'];
    $ipi[$i][CST]      = str_pad($produto['situacaotrib_ipi'], 2, '0',STR_PAD_LEFT);
	$ipi[$i][VBC]  = sprintf("%8.2f", $produto['basecalculo_ipi']); 
	$ipi[$i][PIPI] = sprintf("%8.2f", $produto['aliquota_ipi']);
	$VIPI=round((($produto['basecalculo_ipi']*$_POST['qtd'.$i])*$produto['aliquota_ipi'])/100,2);
	$ipi[$i][VIPI] = sprintf("%8.2f", $VIPI);
	$valortotal_ipi=$VIPI+$valortotal_ipi;
	
    //pis
    $pis[$i][CST]  = str_pad($produto['situacaotrib_pis'], 2, '0',STR_PAD_LEFT);
    $pis[$i][VBC]  = sprintf("%8.2f", $prod[$i][VProd]); 
    $pis[$i][PPIS] = sprintf("%8.2f", $aliquota_pisp); 
	$VPIS=round(($aliquota_pisp*$prod[$i][VProd])/100,2);
    $pis[$i][VPIS] = sprintf("%8.2f", $VPIS);
	$valortotal_pis=$VPIS+$valortotal_pis;

    //cofins
    $cofins[$i][CST]     = str_pad($produto['situacaotrib_cofins'], 2, '0',STR_PAD_LEFT);
    $cofins[$i][VBC]     = sprintf("%8.2f", $prod[$i][VProd]); 
    $cofins[$i][PCOFINS] = sprintf("%8.2f", $aliquota_cofinsp); 
	$VCOFINS=round(($aliquota_cofinsp*$prod[$i][VProd])/100,2);
    $cofins[$i][VCOFINS] = sprintf("%8.2f", $VCOFINS);
	$valortotal_cofins=$VCOFINS+$valortotal_cofins;

    //cofins st
    $cofinsst[$i][VCOFINS]   = "";
    $cofinsst[$i][VBC]       = "";
    $cofinsst[$i][PCOFINS]   = "";
    $cofinsst[$i][QBCProd]   = "";
    $cofinsst[$i][VAliqProd] = "";
	}
	
	$valortotal_produtos=$valortotal_produtos+$prod[$i][VProd];

} // fim dos produtos


$nfe->setProd($prod);
$nfe->setIcms($icms);
$nfe->setIpi($ipi);
$nfe->setPis($pis);
$nfe->setCofins($cofins);
$nfe->setCofinsst($cofinsst);


$vlcalculado=($valortotal_produtos+(float)$total_frete+(float)$total_seguro+(float)$outrasdespesas+(float)$valortotal_ipi)-(float)$total_desconto;
//$valortotal_bcicms=$valortotal_icms+(float)$total_frete+(float)$total_seguro+(float)$outrasdespesas;
//totais
if($regimetrib==1){
	$total[vBC]     = "0.00";
	$total[vICMS]   = "0.00";
	$total[vBCST]   = "0.00";
	$total[vST]     = "0.00";
	$total[vProd]   = sprintf("%8.2f", $valortotal_produtos);
	$total[vFrete]  = sprintf("%8.2f", $total_frete);
	$total[vSeg]    = sprintf("%8.2f", $total_seguro);
	$total[vDesc]   = sprintf("%8.2f", $total_desconto);
	$total[vII]     = "0.00";
	$total[vIPI]    = "0.00";
	$total[vPIS]    = sprintf("%8.2f", $valortotal_pis);
	$total[vCOFINS] = sprintf("%8.2f", $valortotal_cofins);
	$total[vOutro]  = sprintf("%8.2f", $outrasdespesas);
	$total[vNF]     = sprintf("%8.2f", $vlcalculado);
}else{
	$total[vBC]     = sprintf("%8.2f", $valortotal_bcicms);
	$total[vICMS]   = sprintf("%8.2f", $valortotal_icms);
	$total[vBCST]   = "0.00";
	$total[vST]     = "0.00";
	$total[vProd]   = sprintf("%8.2f", $valortotal_produtos);
	$total[vFrete]  = sprintf("%8.2f", $total_frete);
	$total[vSeg]    = sprintf("%8.2f", $total_seguro);
	$total[vDesc]   = sprintf("%8.2f", $total_desconto);
	$total[vII]     = "0.00";
	$total[vIPI]    = sprintf("%8.2f", $valortotal_ipi);
	$total[vPIS]    = sprintf("%8.2f", $valortotal_pis);
	$total[vCOFINS] = sprintf("%8.2f", $valortotal_cofins);
	$total[vOutro]  = sprintf("%8.2f", $outrasdespesas);
	$total[vNF]     = sprintf("%8.2f", $vlcalculado);
}
//$total[VRetPIS] = "";

$nfe->setTotal($total);


// Transporte

$transp[ModFrete] = $_POST['tipo_frete'];
$transp[XNome]    = $_POST['nome_transp'];
$cnpjcpf = $_POST['cpfcnpj_transp'];
$cnpjcpf = str_replace(' ','',$cnpjcpf);
$cnpjcpf = str_replace('-','',$cnpjcpf);
$cnpjcpf = str_replace('.','',$cnpjcpf);
$cnpjcpf = str_replace('/','',$cnpjcpf);
if(strlen($cnpjcpf)==11){
	$transp[CPF]    = $cnpjcpf;
	$transp[CNPJ] = "";
}elseif(strlen($cnpjcpf)==14){
	$transp[CNPJ]     = $cnpjcpf;
	$transp[CPF] = "";
}
$_POST['inscrest_transp'] = str_replace('.','',$_POST['inscrest_transp']);
$_POST['inscrest_transp'] = str_replace('-','',$_POST['inscrest_transp']);
$_POST['inscrest_transp'] = str_replace('/','',$_POST['inscrest_transp']);
$_POST['inscrest_transp'] = str_replace(' ','',$_POST['inscrest_transp']);
$transp[IE]       = $_POST['inscrest_transp'];
$transp[XEnder]   = $_POST['logradouro_transp'];
$transp[UF]       = $_POST['uf_transp'];
$transp[XMun]     = $_POST['municipio_transp'];
$transp[QVol]     = $_POST['qtde_volume1'];
$transp[Esp]      = $_POST['especie_volume1'];
$transp[Marca]    = $_POST['marca_volume1'];
$transp[NVol]     = $_POST['numeracao_volume1'];
if ($_POST['pesoliq_volume1']>=1){
$transp[PesoL]    = sprintf("%8.3f", $_POST['pesoliq_volume1']);
} else {
$transp[PesoL]    = $_POST['pesoliq_volume1'];	
}
if ($_POST['pesobruto_volume1']>=1){
$transp[PesoB]    = sprintf("%8.3f", $_POST['pesobruto_volume1']);
} else {
$transp[PesoB]    = $_POST['pesobruto_volume1'];
}


$nfe->setTransp($transp);



// dados da fatura
$fatura[NFat]  = $_POST['numero_cobranca'];
if ($_POST['vlorig_cobranca']>=1){
$fatura[VOrig] = sprintf("%8.2f", $_POST['vlorig_cobranca']);
} else {
$fatura[VOrig] =  $_POST['vlorig_cobranca'];
}
if ($_POST['vldesc_cobranca']>=1){
$fatura[VDesc] = sprintf("%8.2f", $_POST['vldesc_cobranca']);
} else {
$fatura[VDesc] =  $_POST['vldesc_cobranca'];
}
if ($_POST['vlliq_cobranca']>=1){
$fatura[VLiq] = sprintf("%8.2f", $_POST['vlliq_cobranca']);
} else {
$fatura[VLiq] =  $_POST['vlliq_cobranca'];
}

$nfe->setFatura($fatura);


// dados da duplicata(s)
for ($i = 0; $i < $contadordup; $i++){
	$parcela[$i][NDup]  = $_POST['numero_cobranca']."-".$_POST['numero_dp'.$i];
	$parcela[$i][DVenc] =  implode(preg_match("~\/~", $_POST['datav_dp'.$i]) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $_POST['datav_dp'.$i]) == 0 ? "-" : "/", $_POST['datav_dp'.$i])));
	$parcela[$i][VDup]  = sprintf("%8.2f", $_POST['vl_dp'.$i]);
}

$nfe->setParcela($parcela);


$infoAdd[InfAdFisco] = $_POST["inffisco"];
$infoAdd[InfCpl] = $_POST["infcomp"];

$nfe->setInfoAdd($infoAdd);
//$nfe->id = $chave;

if ($nfe->validaTxt() != "OK"){

    //imprime o erro na tela
	//$erro = $nfe->validaTxt();
    print "<center><strong class=style1>".$nfe->validaTxt()."</strong><BR><BR>";
}
else{
		
       $nfe->montaTXT();

    //endere�o onde o txt sera gravado
    $path = "../txt/";
    $nfe->geraArquivo($path);
	
	require_once('../../include/nfephp/libs/ConvertNFePHP.class.php');
	$arq = '../txt/NFe'.$chave.'-nfe.txt';
	
	//instancia a classe
	$nfe = new ConvertNFePHP();
	
	if ( is_file($arq) ){
	    $xml = $nfe->nfetxt2xml($arq);
	    if ($xml != ''){
	        echo '<PRE>';
	        echo htmlspecialchars($xml);
	        echo '</PRE><BR>';	        
			require_once('../../include/nfephp/libs/ToolsNFePHP.class.php');
		
			// Instancia um objeto da classe "Tools" da API.
			$tools = new ToolsNFePHP();
			
			// Esta vari�vel deve conter o XML inteiro da NF-e a ser assinada.
			$sXml = $xml;			
			// Chama o m�todo "signXML()" da API.
			$sXmlAssinado = $tools->signXML($sXml, 'infNFe');
			
			// Caso a assinatura tenha falhado por algum motivo, o m�todo retorna FALSE.
			if ($sXmlAssinado === FALSE) {
			   die("<center><strong class=style1>Erro na assinatura do XML de NF-e</strong><BR><BR>");
			}else{
				if (!file_put_contents('../xml/NFe'.$chave.'-nfe.xml',$sXmlAssinado)){
	            	echo "<center><strong class=style1>ERRO na grava��o</strong><BR><BR>";
	        	}  
			}
			
			// Se o m�todo n�o retornou FALSE, ent�o ele retornou a nova string do XML com as tags referentes � assinatura.
			//var_dump(htmlentities($sXmlAssinado));  
	    }
	}
	echo "<br/><br/><br/>";
	$file = '../xml/NFe'.$chave.'-nfe.xml';
	
	$modSOAP = '2'; 

	//path do arquivo 
	$filename = '../xml/NFe'.$chave.'-nfe.xml'; 
	
	//obter um numero de lote, esse numero n�o pode repetir 
	$lote = substr(str_replace(',','',number_format(microtime(true)*1000000,0)),0,15); 
	
	// montar o array com a NFe 
	$aNFe = array(0=>file_get_contents($filename)); 
	
	//enviar o lote 
	if ($envia = $tools->sendLot($aNFe, $lote, $modSOAP)){ 	
		//var_dump($envia);	
	$aRetorno = $tools->getProtocol($envia['nRec']);
		//var_dump($aRetorno);	
	} 
	
	               

	if ( $aRetorno["aProt"][0]['cStat'] == 100 || $aRetorno["aProt"][0]['cStat'] == 101 || $aRetorno["aProt"][0]['cStat'] == 110 || $aRetorno["aProt"][0]['cStat']=="104" ){
	$notaaut=1;	
	$cStat=$aRetorno["aProt"][0]['cStat'];
	$xMotivo=$aRetorno["aProt"][0]['xMotivo'];
	$querygravanota = "INSERT INTO tb_notas(chave, id_user,recibo, serie, idnota, NFeV,ambiente,notaaut,cStat,xMotivo,verAplic,dhRecbto,nProt,digVal,datahora,dataemissao,datase,horase,cliente,tipodocumento,tipoimpressao,formapagamento,formaemissao ,finalidadeemissao,natop,condpag,regimetrib,transportadora) VALUES ('".$chave."','".$_SESSION["numerocli"]."','".$envia['nRec']."','".$serie."','".$num1."','2.00','".$ambientecod."','".$notaaut."','".$aRetorno["aProt"][0]['cStat']."','".utf8_decode($aRetorno["aProt"][0]['xMotivo'])."','".$aRetorno["aProt"][0]['verAplic']."','".$aRetorno["aProt"][0]['dhRecbto']."','".$aRetorno["aProt"][0]['nProt']."','".$aRetorno["aProt"][0]['digVal']."',NOW(),'".formatData($_POST['dataemissao'])."','".formatData($_POST['datase'])."','".$_POST['horase']."','".$_POST['cliente']."','".$_POST['tipodocumento']."','".$_POST['tipoimpressao']."','".$_POST['formapagamento']."','".$_POST['formaemissao']."' ,'".$_POST['finalidadeemissao']."','".$_POST['natop']."','".$_POST['condpag']."','".$_POST['regimetrib']."','".$_POST['transportadora']."')";
	} else {
	$notaaut=0;	
	$cStat=$aRetorno['cStat'];
	$xMotivo=$aRetorno['xMotivo'];
	$querygravanota = "INSERT INTO tb_notas(chave, id_user,recibo, serie, idnota, NFeV,ambiente,notaaut,cStat,xMotivo,verAplic,dhRecbto,nProt,digVal,datahora,dataemissao,datase,horase,cliente,tipodocumento,tipoimpressao,formapagamento,formaemissao ,finalidadeemissao,natop,condpag,regimetrib,transportadora) VALUES ('".$chave."','".$_SESSION["numerocli"]."','".$envia['nRec']."','".$serie."','".$num1."','2.00','".$ambientecod."','".$notaaut."','".$aRetorno['cStat']."','".utf8_decode($aRetorno['xMotivo'])."','".$aRetorno['verAplic']."','".$aRetorno['dhRecbto']."','".$aRetorno['nProt']."','".$aRetorno['digVal']."',NOW(),'".formatData($_POST['dataemissao'])."','".formatData($_POST['datase'])."','".$_POST['horase']."','".$_POST['cliente']."','".$_POST['tipodocumento']."','".$_POST['tipoimpressao']."','".$_POST['formapagamento']."','".$_POST['formaemissao']."' ,'".$_POST['finalidadeemissao']."','".$_POST['natop']."','".$_POST['condpag']."','".$_POST['regimetrib']."','".$_POST['transportadora']."')";
	}
	
	
	$querygravanota = mysql_query($querygravanota) or die(mysql_error());
	
	
	if ( $aRetorno["aProt"][0]['cStat'] == 100 || $Retorno["aProt"][0]['cStat'] == 101 || $aRetorno["aProt"][0]['cStat'] == 110 || $aRetorno["aProt"][0]['cStat']=="104" ){
		//var_dump($envia);
		echo "<center class=style1>NF-e Gerada com sucesso: <strong class=style1>".utf8_decode($xMotivo)."</strong><BR><BR>";
	echo "<a href='danfe.php?chave=".$chave."' target='_blank'>CLIQUE AQUI PARA GERAR A DANFE PDF</a></center><br><br>";
	echo "<center class=style1><a href='duplicata.php?chave=".$chave."' target='_blank'>CLIQUE AQUI PARA GERAR A(S) DUPLICATA(S) PDF</a></center><br><br>";
	} else {
		echo "<div align=center><span class=style1><b>ATEN��O</b><br><br>Foram encontrados na gera��o da NF-e: <b>".utf8_decode($xMotivo)."</b>         
        <br><br><a href='javascript:history .go(-1)'><< Voltar</a></span></div><br><br>";
   }
	
		
	
}	
		

?>
</span> 
</div>
 </fieldset>
  </div>
  
 </body>
</html>