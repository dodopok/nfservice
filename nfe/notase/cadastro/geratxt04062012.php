<?
 include "../../admin/config.php"; //Conecta com a nosso banco de dados MySQL
 include "../../bloc.php"; //Verifica se a sess�o est� ativa
?>
<?php
/**
  Ultima atualiza��o = 21/04/2011
  Exemplo de TXT para nota 2.0
 
  R�gis Matos
  Site       = http://www.gestorcustom.com.br
  E-Mail/MSN = regismatos@douradosvirtual.com.br
  skype      = regis_matos

**/
//var_dump($_POST);
//fun��o para gerar o c�digo da nf
function geraCN($length=8){
    $numero = '';    
    for ($x=0;$x<$length;$x++){
        $numero .= rand(0,9);
    }
    return $numero;
}
function calcula_dv($chave43) { 
    $soma_ponderada = 0; 
    $multiplicadores = array(2,3,4,5,6,7,8,9); 
    $i = 42; 
    while ($i >= 0) { 
        for ($m=0; $m<count($multiplicadores) && $i>=0; $m++) { 
            $soma_ponderada+= $chave43[$i] * $multiplicadores[$m]; 
            $i--; 
        } 
    } 
    $resto = $soma_ponderada % 11; 
    if ($resto == '0' || $resto == '1') { 
        return 0; 
    } else { 
        return (11 - $resto); 
   } 

}

$regimetrib=$_POST['regimetrib'];
$cnpjemi = $_POST['cnpj'];
$cnpjemi = str_replace('-','',$cnpjemi);
$cnpjemi = str_replace('/','',$cnpjemi);
$cnpjemi = str_replace('.','',$cnpjemi);
$cnpjemi = str_replace(' ','',$cnpjemi);
$aa = substr($_POST['dataemissao'],8,2);
$mm = substr($_POST['dataemissao'],3,2);
$queryest = "SELECT cod_ibge FROM tb_estados WHERE uf = '".$_POST['uf']."'";
$codibge = mysql_fetch_array(mysql_query($queryest));
$cUF = $codibge['cod_ibge'];    //C�digo da UF [02] 
$aamm = $aa.$mm;     //AAMM da emiss�o [4]
$cnpj = $cnpj = $cnpjemi;     //CNPJ do Emitente [14]
$mod='55';      //Modelo [02]
$serie = str_pad($_POST['serie'], 3, '0',STR_PAD_LEFT);
$querynumnota = "SELECT CAST(idnota+1 AS CHAR) idnotachar FROM tb_notas WHERE id_user = '".$_SESSION["numerocli"]."' ORDER BY idnota DESC LIMIT 1";
$querynumnota = mysql_query($querynumnota) or die(mysql_error());
$numnota = mysql_fetch_array($querynumnota);
$num=$numnota['idnotachar'];  
if ($num<=0){
$num="1";	
}

//N�mero da NF-e [09]
$num1=$numnota['idnotachar'];
if ($num1<=0){
$num1="1";	
}

$tpEmis=$_POST['formaemissao'];     //forma de emiss�o da NF-e [01] 1 � Normal � emiss�o normal; 2 � Conting�ncia FS; 3 � Conting�ncia SCAN; 4 � Conting�ncia DPEC; 5 � Conting�ncia FS-DA 
$cn= geraCN(8);         //C�digo Num�rico [08]
$dv='';         //DV [01]
//ajusta comprimento do numero
$num = str_pad($num, 9, '0',STR_PAD_LEFT);
//calcula codigo num�rico aleat�rio

//monta a chave sem o digito verificador
$chave = $cUF.$aamm.$cnpj.$mod.$serie.$num.$tpEmis.$cn;
$dv = calcula_dv($chave);
$chave = $chave.$dv;

$n = strlen($chave);


//Gerou a chave de acesso
require_once("../../include/NFeTXT2.class.php");
   
$nfe = new  NFeTXT2;

$nfe->setVersao("2.00");
//$nfe->id = $chave; // O id � calculado automaticamente
$nfe->setCUF($codibge['cod_ibge']);
$nfe->setCNF($cn);
$nfe->setNatOp($_POST['natop']);
$nfe->setIndPag($_POST['formapagamento']);
$nfe->setMod("55");
$nfe->setSerie($_POST['serie']);
$nfe->setNNF($num1);
$data_nova = implode(preg_match("~\/~", $_POST['dataemissao']) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $_POST['dataemissao']) == 0 ? "-" : "/", $_POST['dataemissao'])));
$nfe->setDEmi($data_nova); // ( aaaa-mm-dd ) ficar atento com o formato
$data_nova = implode(preg_match("~\/~", $_POST['datase']) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $_POST['datase']) == 0 ? "-" : "/", $_POST['datase'])));
$nfe->setDSaiEnt($data_nova);
$nfe->setHSaiEnt("");
$nfe->setTpNF($_POST['tipodocumento']);
$nfe->setCMunFG("3550308");
$nfe->setTpImp("1");
$nfe->setTpEmis($_POST['formaemissao']);
//$nfe->setCDV("0"); // � gerado automaticamente
$nfe->setTpAmb($_POST['ambientecod']); // 1-Produ��o/ 2-Homologa��o
$nfe->setFinNFe($_POST['finalidadeemissao']);
$nfe->setProcEmi("0");
$nfe->setVerProc("2.1.4");
$nfe->setDhCont("");
$nfe->setXJust("");

//Dados do emitente
$emi[XNome]  = $_POST['razaosocial'];
$emi[XFant]  = $_POST['nomefantasia'];
$emi[IE] 	 = $_POST['inscricaoestadual'];
$emi[IEST]   = '';
$emi[IM]     = $_POST['inscricaomunicipal'];
$emi[CNAE]   = $_POST['cnae'];
$emi[CRT]    = $_POST['regimetrib'];
$emi[CNPJ]   = $cnpjemi;
//$emi[CPF]  = "";
$emi[xLgr]   = $_POST['logradouro'];
$emi[nro]    = $_POST['numero'];
$emi[Cpl] 	 = $_POST['complemento'];
$emi[Bairro] = $_POST['bairro'];
$nomemun = $_POST['municipio'];
$querymun = mysql_fetch_array(mysql_query("SELECT * FROM tab_municipios WHERE nome = '".$nomemun."'"));
$emi[CMun]   = $querymun['id'];
$emi[XMun]   = $nomemun;
$emi[UF]     = $_POST['uf'];
$certpadrao = $_POST['cert_padrao'];
$querycert = "SELECT * FTOM tb_cert WHERE id = ".$certpadrao;
$querycert = mysql_query($querycert);
$querycert = mysql_fetch_array($querycert);
$queryest = "SELECT uf FROM tb_estados WHERE estado = '".$_POST['uf']."'";
$id = mysql_fetch_array(mysql_query($queryest));
$emi[CEP]    = str_replace('-','',$_POST['cep']);
$emi[cPais]  = "1058";
$emi[xPais]  = "BRASIL";
$telefone_emitente = str_replace('(','',$_POST['telefone']);
$telefone_emitente = str_replace(')','',$telefone_emitente);
$telefone_emitente = str_replace('-','',$telefone_emitente);
$telefone_emitente = str_replace(' ','',$telefone_emitente);
$emi[fone]   =$telefone_emitente;

$nfe->setEmi($emi);


//destinatario
$dest[xNome]   = $_POST['nome_dest'];
$iedest = str_replace('.','',$_POST['inscricaoestadual_dest']);
$iedest = str_replace('-','',$iedest);
$dest[IE]      = $iedest;
$dest[ISUF]    = $_POST['inscricaosuframa_dest'];
$dest[email]   = $_POST['email_dest'];
$cnpjdest = str_replace('/','',$_POST['cpfcnpj_dest']);
$cnpjdest = str_replace('-','',$cnpjdest);
$cnpjdest = str_replace('.','',$cnpjdest);
$dest[CNPJ]    = $cnpjdest;
//$dest[CPF]   = "";
$dest[xLgr]    = $_POST['logradouro_dest'];
$dest[nro]     = $_POST['numero_dest'];;
$dest[xCpl]    = $_POST['complemento_dest'];
$dest[xBairro] = $_POST['bairro_dest'];
$nomemun = $_POST['municipio_dest'];
$querymun = mysql_fetch_array(mysql_query("SELECT * FROM tab_municipios WHERE nome = '".$nomemun."'"));
$dest[cMun]   = $querymun['id'];
$dest[xMun]    = $_POST['municipio_dest'];
$dest[UF]      = $_POST['uf_dest'];
$dest[CEP]     = str_replace('-','',$_POST['cep_dest']);
$dest[cPais]   = "1058";
$dest[xPais]   = "BRASIL";
$telefone_dest = str_replace('(','',$_POST['telefone_dest']);
$telefone_dest = str_replace(')','',$telefone_dest);
$telefone_dest = str_replace('-','',$telefone_dest);
$telefone_dest = str_replace(' ','',$telefone_dest);
$dest[fone]    = $telefone_dest;

$nfe->setDest($dest);

/** // Informar apenas quando for diferente do endere�o do remetente.
if($_POST['endentregar']<>$_POST['idend']){
	$queryendereco = "SELECT * FROM tb_enderecos WHERE id = ".$_POST['endentregar'];
	$queryendereco = mysql_query($queryendereco);
	$enderecomd = mysql_fetch_array($queryendereco);
	$retirada[CNPJ] = $cnpjdest;
	$retirada[xLgr] = $enderecomd['logradouro'];
	$retirada[nro] = $enderecomd['numero'];
	$retirada[XCpl] = $enderecomd['complemento'];
	$retirada[XBairro] = $enderecomd['bairro'];
	$nomemun = $enderecomd['cidade'];
	$querymun = mysql_fetch_array(mysql_query("SELECT * FROM tab_municipios WHERE nome = '".$nomemun."'"));
	//$retirada[CMun] = $querymun['id'];
	$retirada[XMun] = $enderecomd['cidade'];
	$queryest = "SELECT uf FROM tb_estados WHERE estado = '".$enderecomd['id_estado']."'";
	$id = mysql_fetch_array(mysql_query($queryest));
	$retirada[UF] = $id['uf'];	
	$nfe->setRetirada($retirada);
}
/** //Informar apenas quando for diferente do endere�o do destinat�rio.
if($_POST['endcobrancar']<>$_POST['idend']){
	$queryendereco = "SELECT * FROM tb_enderecos WHERE id = ".$_POST['endcobrancar'];
	$queryendereco = mysql_query($queryendereco);
	$enderecomd = mysql_fetch_array($queryendereco);
	$entrega[CNPJ] = $cnpjdest;
	$entrega[xLgr] = $enderecomd['logradouro'];
	$entrega[nro] = $enderecomd['numero'];
	$entrega[XCpl] = $enderecomd['complemento'];
	$entrega[xBairro] = $enderecomd['bairro'];
	$nomemun = $enderecomd['cidade'];
	$querymun = mysql_fetch_array(mysql_query("SELECT * FROM tab_municipios WHERE nome = '".$nomemun."'"));
	//$entrega[CMun] = $querymun['id'];
	$entrega[XMun] = $enderecomd['cidade'];
	$queryest = "SELECT uf FROM tb_estados WHERE estado = '".$enderecomd['id_estado']."'";
	$id = mysql_fetch_array(mysql_query($queryest));
	$entrega[UF] = $id['uf'];	
	$nfe->setEntrega($entrega);
}
**/

//produtos
//Obs. A variavel $i tem que ser iniciado com ( 0 ) zero 
$valortotal_produtos=0;
$valortotal_bcicms=0;
$valortotal_icms=0;
$valortotal_pis=0;
$valortotal_cofins=0;

for ($i = 0; $i < 1; $i++){
	$queryproduto = "SELECT * FROM tb_produtos WHERE (id_user = ".$_SESSION['numerocli']." OR id_user = -1) and cod_produto = ".addslashes($_POST['cod_produto'.$i]);
	$produto = mysql_fetch_array(mysql_query($queryproduto));
    $prod[$i][infAdProd] = "INFORMACOES ADICIONAIS DO PRODUTO";
	
    $prod[$i][CProd]     = $produto['cod_produto'];
    $prod[$i][CEAN]      = ""; //$produto['cod_ean']
    $prod[$i][XProd]     = $produto['descricao'];
    $prod[$i][NCM]       = str_replace('.','',$produto['cod_ncm']);
    $prod[$i][EXTIPI]    = $produto['ex_ipi'];
    $prod[$i][CFOP]      = $produto['cfop'];
    $prod[$i][UCom]      = $produto['unid_comercial'];
    $prod[$i][QCom]      = $_POST['qtd'.$i];
    $prod[$i][VUnCom]    = $produto['valor_unit'];
    $prod[$i][VProd]     = "".$_POST['vl_unit'.$i]*$_POST['qtd'.$i]."";
	//var_dump($prod[0][VProd]);
    $prod[$i][CEANTrib]  = $codigoe;
    $prod[$i][UTrib]     = $produto['unid_trib'];
    $prod[$i][QTrib]     = $_POST['qtd'.$i];
    $prod[$i][VUnTrib]   = $produto['valor_unitrib'];
    $prod[$i][VFrete]    = "";
    $prod[$i][VSeg] 	 = "";
    $prod[$i][VDesc]     = "";
    $prod[$i][vOutro] 	 = "";
	$ttprox=$i+1;
    $prod[$i][indTot] 	 = "$ttprox";
	//echo $prod[$i][indTot];
    $prod[$i][xPed] 	 = "060110-1030";
    $prod[$i][nItemPed]  = str_replace(' ','',$i+1);	
	if($regimetrib==1){
		//icms
		$icms[$i][Orig]    = "0";
		$icms[$i][CST]     = "00";
		//$icms[$i][CSOSN] = "101";
		$icms[$i][ModBC]   = "0";
		$icms[$i][VBC] 	   = "0";
		$icms[$i][PICMS]   = "0";
		$icms[$i][VICMS]   = "0";
		
		//ipi
		$ipi[$i][ClEnq]    = "";
		$ipi[$i][CNPJProd] = "";
		$ipi[$i][CSelo]    = "";
		$ipi[$i][QSelo]    = "";
		$ipi[$i][CEnq] 	   = "999";
		$ipi[$i][CST]      = "52";
	
		//pis
		$pis[$i][CST]  = "01";
		$pis[$i][VBC]  = "0";
		$pis[$i][PPIS] = "0";
		$pis[$i][VPIS] = "0";
	
		//cofins
		$cofins[$i][CST]     = "01";
		$cofins[$i][VBC]     = "0";
		$cofins[$i][PCOFINS] = "0";
		$cofins[$i][VCOFINS] = "0";
	
		//cofins st
		$cofinsst[$i][VCOFINS]   = "";
		$cofinsst[$i][VBC]       = "";
		$cofinsst[$i][PCOFINS]   = "";
		$cofinsst[$i][QBCProd]   = "";
		$cofinsst[$i][VAliqProd] = "";
	}else{		
		 //icms
    $icms[$i][Orig]    = "0";
    $icms[$i][CST]     = "00";
	//$icms[$i][CSOSN] = "101";
	$icms[$i][ModBC]   = $produto['modalidade_determinacaobc'];
    $icms[$i][VBC] 	   = $produto['basecalculo_icms'];
    $icms[$i][PICMS]   = $produto['aliquota_icms'];
	$VICMS=round(($produto['basecalculo_icms']*$produto['aliquota_icms'])/100,2);
    $icms[$i][VICMS]   = "$VICMS";
	$valortotal_bcicms=$produto['basecalculo_icms']+$valortotal_bcicms;
	$valortotal_icms=$VICMS+$valortotal_icms;
    //ipi
    $ipi[$i][ClEnq]    = "";
    $ipi[$i][CNPJProd] = "";
    $ipi[$i][CSelo]    = "";
    $ipi[$i][QSelo]    = "";
    $ipi[$i][CEnq] 	   = "999";
    $ipi[$i][CST]      = "52";

    //pis
    $pis[$i][CST]  = "01";
    $pis[$i][VBC]  = "".$prod[$i][VProd]."";
    $pis[$i][PPIS] = "0.65";
	$VPIS=round((0.65*$prod[$i][VProd])/100,2);
    $pis[$i][VPIS] = "$VPIS";
	$valortotal_pis=$VPIS+$valortotal_pis;

    //cofins
    $cofins[$i][CST]     = "01";
    $cofins[$i][VBC]     = "".$prod[$i][VProd]."";
    $cofins[$i][PCOFINS] = "3.00";
	$VCOFINS=round((3.00*$prod[$i][VProd])/100,2);
    $cofins[$i][VCOFINS] = "$VCOFINS";
	$valortotal_cofins=$VCOFINS+$valortotal_cofins;

    //cofins st
    $cofinsst[$i][VCOFINS]   = "";
    $cofinsst[$i][VBC]       = "";
    $cofinsst[$i][PCOFINS]   = "";
    $cofinsst[$i][QBCProd]   = "";
    $cofinsst[$i][VAliqProd] = "";
	}
	
	$valortotal_produtos=$valortotal_produtos+$prod[$i][VProd];

} // fim dos produtos


$nfe->setProd($prod);
$nfe->setIcms($icms);
$nfe->setIpi($ipi);
$nfe->setPis($pis);
$nfe->setCofins($cofins);
$nfe->setCofinsst($cofinsst);


$vlcalculado=$valortotal_produtos;
//totais
if($regimetrib==1){
	$total[vBC]     = "0.00";
	$total[vICMS]   = "0.00";
	$total[vBCST]   = "0.00";
	$total[vST]     = "0.00";
	$total[vProd]   = "$valortotal_produtos";
	$total[vFrete]  = "0.00";
	$total[vSeg]    = "0.00";
	$total[vDesc]   = "0.00";
	$total[vII]     = "0.00";
	$total[vIPI]    = "0.00";
	$total[vPIS]    = "0.00";
	$total[vCOFINS] = "0.00";
	$total[vOutro]  = "0.00";
	$total[vNF]     = $vlcalculado;
}else{
	$total[vBC]     = "$valortotal_bcicms";
	$total[vICMS]   = "$valortotal_icms";
	$total[vBCST]   = "0.00";
	$total[vST]     = "0.00";
	$total[vProd]   = "$valortotal_produtos";
	$total[vFrete]  = "0.00";
	$total[vSeg]    = "0.00";
	$total[vDesc]   = "0.00";
	$total[vII]     = "0.00";
	$total[vIPI]    = "0.00";
	$total[vPIS]    = "$valortotal_pis";
	$total[vCOFINS] = "$valortotal_cofins";
	$total[vOutro]  = "0.00";
	$total[vNF]     = "$vlcalculado";
}
//$total[VRetPIS] = "";

$nfe->setTotal($total);


// Transporte

$transp[ModFrete] = $_POST['tipo_frete'];
$transp[XNome]    = $_POST['tipo_frete'.$_POST['tipo_frete']];
$cnpjcpf = $_POST['cnpjcpf'];
$cnpjcpf = str_replace(' ','',$cnpjcpf);
$cnpjcpf = str_replace('-','',$cnpjcpf);
$cnpjcpf = str_replace('.','',$cnpjcpf);
$cnpjcpf = str_replace('/','',$cnpjcpf);
if(strlen($cnpjcpf)==11){
	$transp[CPF]    = $cnpjcpf;
	$transp[CNPJ] = "";
}elseif(strlen($cnpjcpf)==14){
	$transp[CNPJ]     = $cnpjcpf;
	$transp[CPF] = "";
}
$_POST['inscrest_transp'] = str_replace('.','',$_POST['inscrest_transp']);
$_POST['inscrest_transp'] = str_replace('-','',$_POST['inscrest_transp']);
$_POST['inscrest_transp'] = str_replace('/','',$_POST['inscrest_transp']);
$_POST['inscrest_transp'] = str_replace(' ','',$_POST['inscrest_transp']);
$transp[IE]       = $_POST['inscrest_transp'];
$transp[XEnder]   = $_POST['logradouro_transp'];
$transp[UF]       = $_POST['uf_transp'];
$transp[XMun]     = $_POST['municipio_transp'];
$transp[QVol]     = $_POST['qtde_volume1'];
$transp[Esp]      = $_POST['especie_volume1'];
$transp[Marca]    = $_POST['marca_volume1'];
$transp[NVol]     = $_POST['numeracao_volume1'];
$transp[PesoL]    = $_POST['pesoliq_volume1'];
$transp[PesoB]    = $_POST['pesobruto_volume1'];

$nfe->setTransp($transp);



// dados da fatura
$fatura[NFat]  = $_POST['numero_cobranca'];
$fatura[VOrig] = $vlcalculado;
$fatura[VDesc] = $_POST['vldesc_cobranca'];
$fatura[VLiq]  = $_POST['vldesc_cobranca'];

$nfe->setFatura($fatura);


// dados da duplicata(s)
for ($i = 0; $i < 1; $i++){
	$parcela[$i][NDup]  = $num1;
	$parcela[$i][DVenc] = "2010-12-20";
	$parcela[$i][VDup]  = $vlcalculado;;
}

$nfe->setParcela($parcela);

if($regimetrib==1){	
	$infoAdd[InfAdFisco] = "DOCUMENTO EMITIDO POR ME OPTANTE PELO SIMPLES NACIONAL. NAO GERA DIREITO A CREDITO FISCAL DE ICMS, ISS E IPI.";
}else{
	$infoAdd[InfAdFisco] = "EMITIDO NOS TERMOS DO ARTIGO 400-C DO DECRETO 48042/03 SAIDA COM SUSPENSAO DO IPI CONFORME ART 29 DA LEI 10.637";
}
$infoAdd[InfCpl] = "";

$nfe->setInfoAdd($infoAdd);
//$nfe->id = $chave;

if ($nfe->validaTxt() != "OK"){

    //imprime o erro na tela
	//$erro = $nfe->validaTxt();
    print $nfe->validaTxt();
}
else{
		
       echo $nfe->montaTXT();

    //endere�o onde o txt sera gravado
    $path = "../txt/";
    $nfe->geraArquivo($path);
	
	require_once('../../include/nfephp/libs/ConvertNFePHP.class.php');
	$arq = '../txt/NFe'.$chave.'-nfe.txt';
	
	//instancia a classe
	$nfe = new ConvertNFePHP();
	
	if ( is_file($arq) ){
	    $xml = $nfe->nfetxt2xml($arq);
	    if ($xml != ''){
	        echo '<PRE>';
	        echo htmlspecialchars($xml);
	        echo '</PRE><BR>';	        
			require_once('../../include/nfephp/libs/ToolsNFePHP.class.php');
		
			// Instancia um objeto da classe "Tools" da API.
			$tools = new ToolsNFePHP();
			
			// Esta vari�vel deve conter o XML inteiro da NF-e a ser assinada.
			$sXml = $xml;			
			// Chama o m�todo "signXML()" da API.
			$sXmlAssinado = $tools->signXML($sXml, 'infNFe');
			
			// Caso a assinatura tenha falhado por algum motivo, o m�todo retorna FALSE.
			if ($sXmlAssinado === FALSE) {
			   die("Erro na assinatura do XML de NF-e");
			}else{
				if (!file_put_contents('../xml/NFe'.$chave.'-nfe.xml',$sXmlAssinado)){
	            	echo "ERRO na grava��o";
	        	}  
			}
			
			// Se o m�todo n�o retornou FALSE, ent�o ele retornou a nova string do XML com as tags referentes � assinatura.
			var_dump(htmlentities($sXmlAssinado));  
	    }
	}
	echo "<br/><br/><br/>";
	$file = '../xml/NFe'.$chave.'-nfe.xml';
	
	$modSOAP = '2'; 

	//path do arquivo 
	$filename = '../xml/NFe'.$chave.'-nfe.xml'; 
	
	//obter um numero de lote, esse numero n�o pode repetir 
	$lote = substr(str_replace(',','',number_format(microtime(true)*1000000,0)),0,15); 
	
	// montar o array com a NFe 
	$aNFe = array(0=>file_get_contents($filename)); 
	
	//enviar o lote 
	if ($envia = $tools->sendLot($aNFe, $lote, $modSOAP)){ 	
		var_dump($envia);	
	$aRetorno = $tools->getProtocol($envia['nRec']);
		var_dump($aRetorno);	
	} 
	
	//$id = "35120546312138000127550060000001421245131750";
	//$protId = "135120260530692";
	//$xJust = "erro no calculo da nota";
	//$modSOAP = '1';
	//var_dump($resp = $tools->cancelNF($id, $protId, $xJust, $modSOAP));

    //echo'<br/><br/><br/>Enviado? - ';var_dump($envia = $tools->sendLot('../xml/NFe'.$chave.'-nfe.xml',$_POST['serie']));	
	//echo'<br/><br/><br/>Consulta pelo recibo - ';var_dump($tools->getProtocol($envia['nRec']));
	$querygravanota = "INSERT INTO tb_notas(chave, id_user,recibo, idnota) VALUES ('".$chave."','".$_SESSION["numerocli"]."','".$envia['nRec']."','".$num1."')";
	$querygravanota = mysql_query($querygravanota) or die(mysql_error());
	
	//echo "<script>window.location='danfe.php?chave=".$chave."';</script>";
		
	
}	
		
		
?>