<? 
	include '../../admin/config.php'; //Conecta com a nosso banco de dados MySQL
	include '../../bloc.php'; //Verifica se a sess�o est� ativa
	$produtoarray = explode('--',$_POST['produtos']);
	$consultapadrao = "SELECT regimetributario,ambiente,calculo,natoppadrao,cert_padrao,seriepadrao,condpgtopadrao,aliquota_pis,aliquota_cofins, aliquota_icms,ninicial,ipi_dividido FROM tb_emitente WHERE cod_usuario = ".$_SESSION['numerocli'];
	$cp = mysql_query($consultapadrao);
	$padrao = mysql_fetch_array($cp);
	
	$aliquota_pisp = $padrao['aliquota_pis'];
    $aliquota_cofinsp = $padrao['aliquota_cofins'];		
	
	$prod['VFrete'] = (float)$_POST['frete'];
	$prod['VSeg'] = (float)$_POST['seguro'];
	$prod['vOutro'] = (float)$_POST['despesas'];
	$prod['VDesc'] = (float)$_POST['desconto'];
		
	foreach($produtoarray as $produtod){
		$produtoa = explode('|',$produtod);
		$produto = $produtoa[0];
		$cfop = $produtoa[1];
		$valor = $produtoa[2];
		$qtd= $produtoa[3];		
		$prod['VProd'] = (float)$valor*(float)$qtd;
		$produtovalor_calc = $produtovalor_calc+$prod['VProd'];
		$produto = 'SELECT * FROM tb_produtos WHERE cod_produto = '.$produto.' AND id_user = '.$_SESSION['numerocli'];
		$produto = mysql_query($produto);
		$produto = mysql_fetch_array($produto);
		$emi['UF']     = $_POST['uf'];
		$dest['UF']      = $_POST['uf_dest'];
		if ($emi['UF']==$dest['UF'] or(empty($dest['UF']))){
			$queryncm = "SELECT ncm, aliquota_ipi, aliquota_icms,valor_icmsst,reducao_bcicms FROM dialog WHERE cod_ncm = '".$produto['cod_ncm']."' AND tipo = 1 AND (id_user = -1 OR id_user = ".$_SESSION['numerocli'].")";
		} else {
			$queryncm = "SELECT ncm, aliquota_ipi,valor_icmsst, (select ".$dest['UF']." from tb_icms_est_ncm where id_user='".$_SESSION['numerocli']."' and id_ncm='".$produto['cod_ncm']."' and XX='".$emi['UF']."') as aliquota_icms,reducao_bcicms FROM dialog WHERE cod_ncm = '".$produto['cod_ncm']."' AND tipo = 1 AND (id_user = -1 OR id_user = ".$_SESSION['numerocli'].")";	
		}
		$queryncm = mysql_query($queryncm);		
		$ncm = mysql_fetch_array($queryncm);
		
 		$natopr = 'SELECT * FROM tb_natop WHERE cfop = '.$cfop.' AND id_user = '.$_SESSION['numerocli'];
		$natopr = mysql_query($natopr);
		$natopr = mysql_fetch_array($natopr);
		
		//Condicionais - Base de C�lculo do IPI
		if($natopr['afeta_calculos_ipi'] == 1){
			$totalbaseipi = $prod['VProd'];
		} else {
			$totalbaseipi = 0;
		}
		if($natopr['afeta_bipi_desconto'] == 0){
			$totalbaseipi = $totalbaseipi-(float)$prod['VDesc'];
		}
		if($natopr['afeta_bipi_despesas'] == 0){
			$totalbaseipi = $totalbaseipi+(float)$prod['vOutro'];
		}
		if($natopr['afeta_bipi_frete'] == 0){
			$totalbaseipi = $totalbaseipi+(float)$prod['VFrete'];
		}
		if($natopr['afeta_bipi_seguro'] == 0){
			$totalbaseipi = $totalbaseipi+(float)$prod['VSeg'];
		}
		$base_calculo_ipi = $totalbaseipi;
		$base_calculo_ipi_calc = $base_calculo_ipi_calc+$base_calculo_ipi;
		
		//ipi
		$ipi['CST']  = str_pad($produto['situacaotrib_ipi'], 2, '0',STR_PAD_LEFT);
		$ipi['VBC']  = sprintf("%8.2f", $base_calculo_ipi); 
		$ipi['PIPI'] = sprintf("%8.2f", $ncm['aliquota_ipi']);
		$VIPI=round(($base_calculo_ipi*$ncm['aliquota_ipi'])/100,2);
		$ipi['VIPI'] = sprintf("%8.2f", $VIPI);
		$valortotal_ipi=$VIPI+$valortotal_ipi;
				
		
		if($natopr['afeta_calculos_icms'] == 1){
			$totalbaseicms = $prod['VProd'];
		} else {
			$totalbaseicms = 0;
		}		
		if($natopr['afeta_bicms_desconto'] == 0){
			$totalbaseicms = $totalbaseicms-(float)$prod['VDesc'];
		}
		if($natopr['afeta_bicms_despesas'] == 0){
			$totalbaseicms = $totalbaseicms+(float)$prod['vOutro'];
		}
		if($natopr['afeta_bicms_frete'] == 0){
			$totalbaseicms = $totalbaseicms+(float)$prod['VFrete'];
		}
		if($natopr['afeta_bicms_seguro'] == 0){
			$totalbaseicms = $totalbaseicms+(float)$prod['VSeg'];
		}
		if($natopr['afeta_bicms_ipi'] == 0){
			$totalbaseicms = $totalbaseicms+(float)$ipi['VIPI'];
		}		
		$icms['pRedBC'] = sprintf("%8.2f", $ncm['reducao_bcicms']);
		if($ncm['reducao_bcicms']<>"0.00"){
			$totalbaseicms_red= ($totalbaseicms*((float)$ncm['reducao_bcicms']/100));
			$totalbaseicms = $totalbaseicms-$totalbaseicms_red;
		}				
		$base_calculo_icms = $totalbaseicms;
		$base_calculo_icms_calc = $base_calculo_icms_calc+$base_calculo_icms;	
		//icms
		$icms['CST']     = str_pad($produto['tipo_trib_icms'], 2, '0',STR_PAD_LEFT);
		$icms['ModBC']   = $produto['modalidade_determinacaobc'];
		
		$icms['VBC'] 	 = sprintf("%8.2f", $totalbaseicms);
		$icms['PICMS']   = sprintf("%8.2f", $ncm['aliquota_icms']);		
		$VICMS=($totalbaseicms*$ncm['aliquota_icms'])/100;
		$valoricms_calc = $valoricms_calc + $VICMS;
		$valortotal_bcicms=$totalbaseicms+$valortotal_bcicms;  
		if($natopr['afeta_calculos_st'] == 1){
			if(($icms['CST'] == 10) || ($icms['CST'] == 30) || ($icms['CST'] == 60) || ($icms['CST'] == 70) || ($icms['CST'] == 90)){
				//icms ST
				$vricmsst=(($prod['VProd'])*$ncm["valor_icmsst"])/100;
				$totalbaseicmsst=$prod['VProd']+$vricmsst;
				if($natopr['afeta_bicmsst_ipi'] == 0){
					$totalbaseicmsst = $totalbaseicmsst+$ipi['VIPI'];
				}
				if($natopr['afeta_bicmsst_desconto'] == 0){
					$totalbaseicmsst = $totalbaseicmsst-(float)$prod['VDesc'];
				}
				if($natopr['afeta_bicmsst_despesas'] == 0){
					$totalbaseicmsst = $totalbaseicmsst+(float)$prod['vOutro'];
				}
				if($natopr['afeta_bicmsst_frete'] == 0){
					$totalbaseicmsst = $totalbaseicmsst+(float)$prod['VFrete'];
				}
				if($natopr['afeta_bicmsst_seguro'] == 0){
					$totalbaseicmsst = $totalbaseicmsst+(float)$prod['VSeg'];
				}				
				$icms['VBCST'] 	   = sprintf("%8.2f", $totalbaseicmsst);
				$icms['PICMSST']   = sprintf("%8.2f", $ncm['aliquota_icms']);
				$VICMSST=($totalbaseicmsst*$ncm['aliquota_icms'])/100;				
				$VICMSST = $VICMSST - $VICMS;
				$valortotal_bcicmsst=$totalbaseicmsst+$valortotal_bcicmsst;
				$valortotal_icmsst=$VICMSST+$valortotal_icmsst;  
			}
		}		
		//pis
		if($natopr['afeta_calculos_pis'] == 1){
			$pis['CST']  = str_pad($produto['situacaotrib_pis'], 2, '0',STR_PAD_LEFT);
			$pis['VBC']  = sprintf("%8.2f", $prod['VProd']); 
			$pis['PPIS'] = sprintf("%8.2f", $aliquota_pisp); 
			$VPIS=round(($aliquota_pisp*$prod['VProd'])/100,2);
			$pis['VPIS'] = sprintf("%8.2f", $VPIS);
			$valortotal_pis=$valortotal_pis+(($aliquota_pisp*$prod['VProd'])/100);
		}
	
		//cofins
		if($natopr['afeta_calculos_cofins'] == 1){
			$cofins['CST']     = str_pad($produto['situacaotrib_cofins'], 2, '0',STR_PAD_LEFT);
			$cofins['VBC']     = sprintf("%8.2f", $prod['VProd']); 
			$cofins['PCOFINS'] = sprintf("%8.2f", $aliquota_cofinsp); 
			$VCOFINS=round(($aliquota_cofinsp*$prod['VProd'])/100,2);
			$cofins['VCOFINS'] = sprintf("%8.2f", $VCOFINS);
			$valortotal_cofins=$valortotal_cofins+(($aliquota_cofinsp*$prod['VProd'])/100);		
		}
	}
	$total_frete = (float)$_POST['frete'];
	$total_seguro = (float)$_POST['seguro'];
	$outrasdespesas = (float)$_POST['despesas'];
	$total_desconto = (float)$_POST['desconto'];
	$vlcalculado=($produtovalor_calc+(float)$total_frete+(float)$total_seguro+(float)$outrasdespesas+(float)$valortotal_ipi)-(float)$total_desconto+(float)$valortotal_icmsst;
?>

<table border="0" width="100%"  cellpadding="3" cellspacing="3">
                            	<tr>
                                	<td width="11%">Base de C�lculo ICMS</td>
                                	<td width="20%"><input name="base_calculo_icms" value="<?=round($base_calculo_icms_calc,2)?>" type="text"  size="15"  readonly value="0.00"/></td>
                                	<td width="11%">Base de C�lculo IPI</td>
                                	<td width="20%"><input name="base_calculo_ipi" value="<?=round($base_calculo_ipi_calc,2)?>" type="text"  size="15"  readonly value="0.00"/></td>
                                	<td width="11%">Base de C�lculo ICMS ST</td>
                                	<td width="20%"><input name="base_calculo_icmsst" value="<?=round($valortotal_bcicmsst,2)?>" type="text"  size="15"  readonly value="0.00"/></td>
                            	</tr>                                  
                                <tr>
                                	<td width="13%">Total do ICMS</td>
                                	<td width="23%"><input name="total_icms" id="total_icms" type="text" value="<?=round($valoricms_calc,2)?>" size="15"readonly value="0.00"/></td>
                                	<td>Total do IPI</td>
                                    <td><input name="total_ipi" id="total_ipi" type="text" size="15" value="<?=round($valortotal_ipi,2)?>" readonly value="0.00"/></td>  
                                  	<td>Total do ICMS ST</td>
                                    <td><input name="total_icmsst" id="total_icmsst" type="text" size="15" value="<?=round($valortotal_icmsst,2)?>" readonly value="0.00"/></td>
                                    
                                </tr>
                                <tr>
                                	<td width="13%">Total do IRRF</td>
                                	<td width="23%"><input name="total_irrf" id="total_irrf"type="text"  size="15" readonly value="0.00"/></td>
                                	<td>Total do INSS</td>
                                    <td><input name="total_inss" id="total_inss" type="text" size="15" readonly value="0.00"/></td>  
                                  	<td>Total CSLL</td>
                                    <td><input name="total_csll" type="text"  size="15" readonly value="0.00"/></td>
                                    
                                </tr>
                                <tr>
                                   
                                     	
                                    <td>PIS</td>
                                    <td><input name="total_pis" type="text" value="<?=round($valortotal_pis,2)?>"  size="15" readonly value="0.00"/></td>
                                    <td>COFINS</td>
                                    <td><input name="total_cofins" type="text" value="<?=round($valortotal_cofins,2)?>"  size="15" readonly value="0.00"/></td>
                                    <td>Total Produtos e Serv.</td>
                                    <td><input name="total_prodserv" type="text"  size="15" value="<?=round($produtovalor_calc,2)?>" readonly value="0.00"/></td>                        
                                 </tr>
                                <tr>
                                	<td width="13%">Total do PIS ST</td>
                                	<td width="23%"><input name="total_pisst" id="total_pisst" type="text" size="15"  onKeyPress="mascara(this,moeda);" readonly value="0.00"/></td>
                                	<td>Total do COFINS ST</td>
                                    <td><input name="total_cofinsst" id="total_cofinsst" type="text"  size="15" onKeyPress="mascara(this,moeda);" readonly value="0.00"/></td>                                       
                                    <td><strong>Total NF</strong></td>
                                    <td><input name="total_nf" id="total_nf" value="<?=round($vlcalculado,2)?>" type="text"  size="20" readonly value="0.00"/><input name="total_nfh" value="<?=round($vlcalculado,2)?>" id="total_nfh" type="hidden"/>
                                    </td>                                    
                                </tr>
                                <tr>
                                	 <td><input type="button" value="Calcular" class="btn3" onClick="calculanota1();"></td>
                                </tr>
                            </table>
                            <script>
                            	CarregaDup(document.form_sistema.condpag.value);
                            </script>