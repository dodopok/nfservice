<?php	
	include "../../admin/config.php"; //Conecta com a nosso banco de dados MySQL
 	include "../../bloc.php"; //Verifica se a sess�o est� ativa
	$html = '';
	function extenso( $valor, $moedaSing, $moedaPlur, $centSing, $centPlur ) {
	
	   $centenas = array( 0,
		   array(0, "cento",        "cem"),
		   array(0, "duzentos",     "duzentos"),
		   array(0, "trezentos",    "trezentos"),
		   array(0, "quatrocentos", "quatrocentos"),
		   array(0, "quinhentos",   "quinhentos"),
		   array(0, "seiscentos",   "seiscentos"),
		   array(0, "setecentos",   "setecentos"),
		   array(0, "oitocentos",   "oitocentos"),
		   array(0, "novecentos",   "novecentos") ) ;
	
	   $dezenas = array( 0,
				"dez",
				"vinte",
				"trinta",
				"quarenta",
				"cinq�enta",
				"sessenta",
				"setenta",
				"oitenta",
				"noventa" ) ;
	
	   $unidades = array( 0,
				"um",
				"dois",
				"tr�s",
				"quatro",
				"cinco",
				"seis",
				"sete",
				"oito",
				"nove" ) ;
	
	   $excecoes = array( 0,
				"onze",
				"doze",
				"treze",
				"quatorze",
				"quinze",
				"dezeseis",
				"dezesete",
				"dezoito",
				"dezenove" ) ;
	
	   $extensoes = array( 0,
		   array(0, "",       ""),
		   array(0, "mil",    "mil"),
		   array(0, "milh�o", "milh�es"),
		   array(0, "bilh�o", "bilh�es"),
		   array(0, "trilh�o","trilh�es") ) ;
	
	   $valorForm = trim( number_format($valor,2,".",",") ) ;
	
	   $inicio    = 0 ;
	
	   if ( $valor <= 0 ) {
		  return ( $valorExt ) ;
	   }
	
	   for ( $conta = 0; $conta <= strlen($valorForm)-1; $conta++ ) {
		  if ( strstr(",.",substr($valorForm, $conta, 1)) ) {
			 $partes[] = str_pad(substr($valorForm, $inicio, $conta-$inicio),3," ",STR_PAD_LEFT) ;
			 if ( substr($valorForm, $conta, 1 ) == "." ) {
				break ;
			 }
			 $inicio = $conta + 1 ;
		  }
	   }
	
	   $centavos = substr($valorForm, strlen($valorForm)-2, 2) ;
	
	   if ( !( count($partes) == 1 and intval($partes[0]) == 0 ) ) {
		  for ( $conta=0; $conta <= count($partes)-1; $conta++ ) {
	
			 $centena = intval(substr($partes[$conta], 0, 1)) ;
			 $dezena  = intval(substr($partes[$conta], 1, 1)) ;
			 $unidade = intval(substr($partes[$conta], 2, 1)) ;
	
			 if ( $centena > 0 ) {
	
				$valorExt .= $centenas[$centena][($dezena+$unidade>0 ? 1 : 2)] . ( $dezena+$unidade>0 ? " e " : "" ) ;
			 }
	
			 if ( $dezena > 0 ) {
				if ( $dezena>1 ) {
				   $valorExt .= $dezenas[$dezena] . ( $unidade>0 ? " e " : "" ) ;
	
				} elseif ( $dezena == 1 and $unidade == 0 ) {
				   $valorExt .= $dezenas[$dezena] ;
	
				} else {
				   $valorExt .= $excecoes[$unidade] ;
				}
	
			 }
	
			 if ( $unidade > 0 and $dezena != 1 ) {
				$valorExt .= $unidades[$unidade] ;
			 }
	
			 if ( intval($partes[$conta]) > 0 ) {
				$valorExt .= " " . $extensoes[(count($partes)-1)-$conta+1][(intval($partes[$conta])>1 ? 2 : 1)] ;
			 }
	
			 if ( (count($partes)-1) > $conta and intval($partes[$conta])>0 ) {
				$conta3 = 0 ;
				for ( $conta2 = $conta+1; $conta2 <= count($partes)-1; $conta2++ ) {
				   $conta3 += (intval($partes[$conta2])>0 ? 1 : 0) ;
				}
	
				if ( $conta3 == 1 and intval($centavos) == 0 ) {
				   $valorExt .= " e " ;
				} elseif ( $conta3>=1 ) {
				   $valorExt .= ", " ;
				}
			 }
	
		  }
	
		  if ( count($partes) == 1 and intval($partes[0]) == 1 ) {
			 $valorExt .= $moedaSing ;
	
		  } elseif ( count($partes)>=3 and ((intval($partes[count($partes)-1]) + intval($partes[count($partes)-2]))==0) ) {
			 $valorExt .= " de " + $moedaPlur ;
	
		  } else {
			 $valorExt = trim($valorExt) . " " . $moedaPlur ;
		  }
	
	   }
	
	   if ( intval($centavos) > 0 ) {
	
		  $valorExt .= (!empty($valorExt) ? " e " : "") ;
	
		  $dezena  = intval(substr($centavos, 0, 1)) ;
		  $unidade = intval(substr($centavos, 1, 1)) ;
	
		  if ( $dezena > 0 ) {
			 if ( $dezena>1 ) {
				$valorExt .= $dezenas[$dezena] . ( $unidade>0 ? " e " : "" ) ;
	
			 } elseif ( $dezena == 1 and $unidade == 0 ) {
				$valorExt .= $dezenas[$dezena] ;
	
			 } else {
				$valorExt .= $excecoes[$unidade] ;
			 }
	
		  }
	
		  if ( $unidade > 0 and $dezena != 1 ) {
			 $valorExt .= $unidades[$unidade] ;
		  }
	
		  $valorExt .= " " . ( intval($centavos)>1 ? $centPlur : $centSing ) ;
	
	   }
	
	   return ( $valorExt ) ;
	
	}
	function mascara_string($mascara,$string)
	{
	   $string = str_replace(" ","",$string);
	   for($i=0;$i<strlen($string);$i++)
	   {
		  $mascara[strpos($mascara,"#")] = $string[$i];
	   }
	   return $mascara;
	}
	$nfefile = '../xml/NFe/NFe'.$_GET["chave"].'-nfe.xml';
$html .= '
<!DOCTYPE HTML>
<html>
<head>
 <title>Duplicata</title>
 </head>
 <body>
	<style type="text/css">
		td,th{
			border : 1px solid #000;
		}
		.dadosbemi{
			font: 16px Verdana, Geneva, sans-serif;			
			font-weight: bold;
		}
		.telemi{
			font: 12px Geneva, sans-serif;			
			font-weight: bold;	
		}
		.info{
			font: 12px Verdana, Geneva, sans-serif;	
		}
		th{
			text-align:center;
			font: 14px Verdana, Geneva, sans-serif;	
			font-weight: bold;
		}
		.p{
			font: 10px Verdana, Geneva, sans-serif;
		}
		.textovertical {
			-moz-transform: rotate(270deg);
  			-moz-transform-origin: 50% 50%;
 			-webkit-transform: rotate(270deg);
  			-webkit-transform-origin: 50% 50%;
		}
    </style>';
	if ($nfefile != ''){	
			$nfefile = file_get_contents($nfefile);
			$dom = new DomDocument;
            $dom->loadXML($nfefile);
            $dup        = $dom->getElementsByTagName('dup');
			$ide        = $dom->getElementsByTagName("ide")->item(0);
			$emit       = $dom->getElementsByTagName("emit")->item(0);
			$dest       = $dom->getElementsByTagName("dest")->item(0);
			$enderEmit = $dom->getElementsByTagName("enderEmit")->item(0);
			$enderDest  = $dom->getElementsByTagName("enderDest")->item(0);
			$ICMSTot    = $dom->getElementsByTagName("ICMSTot")->item(0);
		foreach($dup as $k => $d){		
		$via = 1;
		for($i=0;$i<=1;$i++){
			$html.='
			<table width="910px" cellpadding="5">
				<tr>
					<td valign="top" rowspan="2" colspan="2">            	
						<div class="dadosbemi">
							'.$emit->getElementsByTagName("xNome")->item(0)->nodeValue.'<br/>
						</div>
						<div class="telemi">'.mascara_string('(##) ####-####',$enderEmit->getElementsByTagName("fone")->item(0)->nodeValue).'</div>
						<div id="imgemi"><img src="../../cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg" width="250px"/></div>                
				  </td>
					<td class="info" colspan="4">
						'.$enderEmit->getElementsByTagName("xLgr")->item(0)->nodeValue.' <strong>N:</strong> '.$enderEmit->getElementsByTagName("nro")->item(0)->nodeValue.'<br/>
						'.mascara_string('#####-###',$enderEmit->getElementsByTagName("CEP")->item(0)->nodeValue).' - '.$enderEmit->getElementsByTagName("xBairro")->item(0)->nodeValue.' - 
						'.$enderEmit->getElementsByTagName("xMun")->item(0)->nodeValue.' - '.$enderEmit->getElementsByTagName("UF")->item(0)->nodeValue.'<br/>
						<strong>CNPJ:</strong> '.mascara_string('##.###.###/####-##',$emit->getElementsByTagName("CNPJ")->item(0)->nodeValue).' - 
						<strong>INSCR. EST.:</strong> '.mascara_string('###.###.###.###',$emit->getElementsByTagName("IE")->item(0)->nodeValue).'
					</td>
				</tr>
				<tr>
					<td class="info" colspan="3">
						<strong>Dt. Emiss�o:</strong> '.implode("/",array_reverse(explode("-",$ide->getElementsByTagName("dEmi")->item(0)->nodeValue))).'
					</td>
					<th>DUPLICATA</th>
				</tr>
				<tr>
					<th class="p" colspan="2"><strong>Nota Fiscal Fatura</strong></th>
					<th class="p" colspan="2"><strong>Duplicata</strong></th>
					<th class="p" rowspan="2"><strong>Vencimento</strong></th>
					<td class="p" rowspan="3" valign="top"><strong>';
				if($via == 1){
					$html .='Uso da Inst. Financ.';
					$via = 2;	
				}else{
					$html .='Uso para Cont. Int.';
					$via = 1;
				}
				$html .= '</strong></td>
				</tr>
				<tr>
					<th class="p"><strong>Valor R$</strong></th>
					<th class="p"><strong>N�mero</strong></th>
					<th class="p"><strong>Valor R$</strong></th>
					<th class="p"><strong>N�mero</strong></th>
				</tr>
				<tr>
					<td class="info" align="center">R$ '.str_replace('.',',',$ICMSTot->getElementsByTagName("vNF")->item(0)->nodeValue).'</td>
					<td class="info" align="center">'.$ide->getElementsByTagName("nNF")->item(0)->nodeValue.'</td>
					<td class="info" align="center">R$ '.str_replace('.',',',$dup->item($k)->getElementsByTagName('vDup')->item(0)->nodeValue).'</td>
					<td class="info" align="center">'.$dup->item($k)->getElementsByTagName('nDup')->item(0)->nodeValue.'</td>
					<td class="info" align="center">'.implode("/",array_reverse(explode("-",$dup->item($k)->getElementsByTagName('dVenc')->item(0)->nodeValue))).'</td>
				</tr>
				<tr>
					<td rowspan="4">
						<div style="border-right:1px solid black;">							
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>					
						</div>
					</td>
					<td class="info" colspan="5">
						<strong>Nome do Sacado:</strong> '.$dest->getElementsByTagName("xNome")->item(0)->nodeValue.'<br/>
						<strong>Endere�o:</strong> '.$enderDest->getElementsByTagName("xLgr")->item(0)->nodeValue.' <strong>N:</strong> '.$enderDest->getElementsByTagName("nro")->item(0)->nodeValue.' <strong>Bairro:</strong> '.$enderDest->getElementsByTagName("xBairro")->item(0)->nodeValue.'<br/>
						<strong>Munic�pio:</strong> '.$enderDest->getElementsByTagName("xMun")->item(0)->nodeValue.' <strong>CEP:</strong> '.mascara_string('#####-###',$enderDest->getElementsByTagName("CEP")->item(0)->nodeValue).' <strong>Estado:</strong> '.$enderDest->getElementsByTagName("UF")->item(0)->nodeValue.'<br/>'.
						' <strong>Telefone:</strong> '.mascara_string('(##) ####-####',$enderDest->getElementsByTagName("fone")->item(0)->nodeValue).'<br/>
						<strong>Inscri��o no CNPJ n�</strong> '.mascara_string('##.###.###/####-##',$dest->getElementsByTagName("CNPJ")->item(0)->nodeValue).' <strong>Inscri��o Estadual:</strong>';
						if(!empty($dest->getElementsByTagName("IE")->item(0)->nodeValue)){ $html.= mascara_string('###.###.###.###',$dest->getElementsByTagName("IE")->item(0)->nodeValue);}else{$html .= ' ISENTO';}
						$html .= '<br/>
					</td>
				</tr>
				<tr>
					<td class="p"><strong>Valor por Extenso</strong></td>
					<td class="info" colspan="4">'.ucwords(extenso( str_replace('.',',',$dup->item($k)->getElementsByTagName('vDup')->item(0)->nodeValue), "real", "reais", "centavo", "centavos" )) .'</td>
				</tr>
				<tr>
					<td colspan="5" align="center" class="info">
						Reconhe�o(emos) a exatid�o desta duplicata, na import�ncia acima, que pagarei(emos) a '.$emit->getElementsByTagName("xNome")->item(0)->nodeValue.' ou a sua ordem na pra�a e vencimentos indicados.
					</td>
				</tr>
				<tr>
					<td colspan="5" class="info">
						Na falta de Pagamento no vencimento, ser�o cobrados juros legais e despesas.<br/><br/><br/><br/>
						<div align="center">
							EM: ____/____/______
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							__________________________________________________<br/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Assinatura do Sacado
						</div>
					</td>
				</tr>
			</table>
		';
		$html .= '<pagebreak/>';
				}
			}
		}
	$html .= '</body></html>';
	$html = utf8_encode($html);
	// converte o conteudo para uft-8
	
	define('MPDF_PATH', 'MPDF54/');
	include(MPDF_PATH.'mpdf.php');
	// inclui a classe
	
	$mpdf = new mPDF();
	// cria o objeto
	$mpdf->allow_charset_conversion=true;
	// permite a conversao (opcional)
	$mpdf->charset_in='UTF-8';
	// converte todo o PDF para utf-8
	$mpdf->WriteHTML($html);
	// escreve definitivamente o conteudo no PDF
	
	$pdf = $mpdf->Output('duplicata'.$chave.'.pdf','S');
	if(!empty($_SESSION['numerocli'])){
		header('Content-type: application/pdf');
		echo $pdf;
	}
	// imprime
?>
