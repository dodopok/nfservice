<?php

$xml = addslashes($_GET['xml']);

require('debuglib.php');

class valida {

/**
* Esta classe ira validar um arquivo xml com um arquivo xsd e ira retornar um array de erros caso
* encontre erros no xml ou retornará a string "OK" caso nao encontre erros
* A função print_a da biblioteca debuglib retorna os erros do arquivo xml
* return @var valida
*/

protected $arquivoXSD = "../../include/nfephp/schemes/PL_006g/nfe_v2.00.xsd";
protected $arquivoXML;

public function arquivoXML ($arq) {
$this->arquivoXML=$arq;
}

public function verxsd () {

/**
* Ao tentar validar um arquivo XML, se algum erro
* for encontrado a libxml irá gerar Warnings.
*
* 'libxml_use_internal_errors(true)' deve ser chamada antes de
* instanciar qualquer objeto da classe DomDocument!
* 
* @return $ret retorna os erros encontrados pelo parser DOM XML
*/
libxml_use_internal_errors(true);

/* Cria um novo objeto da classe DomDocument */
$objDom = new DomDocument();

/* Carrega o arquivo XML */
$oarq = $objDom->load($this->arquivoXML);
$ret = "";

/* Validar os dados utilizando o arquivo XSD */
if ( !$objDom->schemaValidate($this->arquivoXSD) ) {

/**
* Se não foi possível validar, você pode capturar
* todos os erros em um array
* Cada elemento do array $arrayErrors
* será um objeto do tipo LibXmlError
* 
*/ 
$arrayErrors = libxml_get_errors();

/**
* Descomente linha abaixo para visualizar o array completo
* de erros retornados pela funcao libxml_get_errors()
*/
//echo print_a($arrayErrors);
$erro = true;

/**
* Decompondo o objeto $arrayErrors e
* utilizando a função print_a da biblioteca debulib para
* visualização mais organizada do array
*/

foreach ($arrayErrors as $error) {
print_a($error->level)."
";
print_a($error->code)."
";
print_a($error->column)."
";
print_a($error->message)."
";
print_a($error->file)."
";
print_a($error->line)."
"; 
}	
$ret .= "";
libxml_clear_errors();

/* Caso não encontre erros na validação */
} else {

$erro = false;
$ret = "OK";
}
return $ret; 
}
}

$valida = new valida;
$valida->arquivoXML('../xml/NFe'.$xml.'-nfe.xml');
if($valida->verxsd()){
	echo 'V&aacute;lida.';	
}
?>