<?php
/**
 * Este arquivo � parte do projeto NFePHP - Nota Fiscal eletr�nica em PHP.
 *
 * Este programa � um software livre: voc� pode redistribuir e/ou modific�-lo
 * sob os termos da Licen�a P�blica Geral GNU como � publicada pela Funda��o
 * para o Software Livre, na vers�o 3 da licen�a, ou qualquer vers�o posterior.
 * e/ou
 * sob os termos da Licen�a P�blica Geral Menor GNU (LGPL) como � publicada pela
 * Funda��o para o Software Livre, na vers�o 3 da licen�a, ou qualquer vers�o posterior.
 *
 * Este programa � distribu�do na esperan�a que ser� �til, mas SEM NENHUMA
 * GARANTIA; nem mesmo a garantia expl�cita definida por qualquer VALOR COMERCIAL
 * ou de ADEQUA��O PARA UM PROP�SITO EM PARTICULAR,
 * veja a Licen�a P�blica Geral GNU para mais detalhes.
 *
 * Voc� deve ter recebido uma c�pia da Licen�a Publica GNU e da
 * Licen�a P�blica Geral Menor GNU (LGPL) junto com este programa.
 * Caso contr�rio consulte
 * <http://www.fsfla.org/svnwiki/trad/GPLv3>
 * ou
 * <http://www.fsfla.org/svnwiki/trad/LGPLv3>.
 * 
 * @package   NFePHP
 * @name      enviaMail
 * @version   1.05
 * @license   http://www.gnu.org/licenses/gpl.html GNU/GPL v.3
 * @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL v.3
 * @copyright 2009-2011 &copy; NFePHP
 * @link      http://www.nfephp.org/
 * @author    Roberto L. Machado <roberto.machado@superig.com.br>
 * 
 */
require_once('../../include/nfephp/libs/DanfeNFePHP.class.php');
//require_once('../libs/MailNFePHPAlternate.class.php');
require_once('../../include/nfephp/libs/MailNFePHP.class.php');
include "../../admin/config.php"; //Conecta com a nosso banco de dados MySQL
include "../../bloc.php"; //Verifica se a sessão está ativa

//inicalizar a classe de envio
$nfeMail = new MailNFePHP();

$nfefile='';
if ( isset($_GET['chave']) ){
    $nfefile = '../xml/NFe/NFe'.$_GET['chave'].'-nfe.xml';
}	
$para = '';

$html = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>
<html>
<head>
    <title>enviaMail</title>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
    <link rel='stylesheet' type='text/css' href='images/fimatec.css'>
    <script language='javascript'>
    <!--
        setTimeout('self.close();',20000)
    //-->
    </script> 
</head>
<body>";

if ($nfefile != ''){
	
    //carregar o xml
    $docXML = file_get_contents($nfefile);
    $dom = new DomDocument;
    $dom->loadXML($docXML);
    $ide        = $dom->getElementsByTagName("ide")->item(0);
    $emit       = $dom->getElementsByTagName("emit")->item(0);
    $dest       = $dom->getElementsByTagName("dest")->item(0);
    $obsCont    = $dom->getElementsByTagName("obsCont")->item(0);
    $ICMSTot    = $dom->getElementsByTagName("ICMSTot")->item(0);

    $razao = utf8_decode($dest->getElementsByTagName("xNome")->item(0)->nodeValue);
    $cnpj = $dest->getElementsByTagName("CNPJ")->item(0)->nodeValue;
    $numero = str_pad($ide->getElementsByTagName('nNF')->item(0)->nodeValue, 9, "0", STR_PAD_LEFT);
    $serie = str_pad($ide->getElementsByTagName('serie')->item(0)->nodeValue, 3, "0", STR_PAD_LEFT);
    $emitente = utf8_decode($emit->getElementsByTagName("xNome")->item(0)->nodeValue);
    $vtotal = number_format($ICMSTot->getElementsByTagName("vNF")->item(0)->nodeValue, 2, ",", ".");
    $email[] = !empty($dest->getElementsByTagName("email")->item(0)->nodeValue) ? utf8_decode($dest->getElementsByTagName("email")->item(0)->nodeValue) : '';
	$email="andre@afsystems.com.br";
	echo $email;
    if (isset($obsCont)){
        foreach ($obsCont as $obs){
            $campo =  $obsCont->item($i)->getAttribute("xCampo");
            $xTexto = !empty($obsCont->item($i)->getElementsByTagName("xTexto")->item(0)->nodeValue) ? $obsCont->item($i)->getElementsByTagName("xTexto")->item(0)->nodeValue : '';
            if (substr($campo, 0, 5) == 'email' && $xTexto != '') {
                $email[] = $xTexto;
            }
            $i++;
        }
    }
    foreach($email as $e){
        if ($nfeMail->validEmailAdd($e)){
            $mailto[] = $e; 
        } else {
            echo "<p align='center'><b>O e-mail $e Não é um endereço válido, CORRIGIR !!<b></p><BR>";
        } //fimda valida�ao
    }            
  //  echo $html;
    flush();
    //verificar se tem endere�os validos
	$mailto=1;
    if ( count($mailto)== 0 ){
        echo "<p align='center'><b>$razao - N&atilde;o h&eacute; registro de e-mail para deste cliente.</b></p><BR>";
	flush();
    } else {
	//inicializar a DANFE
	$danfe = new DanfeNFePHP($docXML, 'P', 'A4',$_SERVER['DOCUMENT_ROOT'].'/nfe/cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg','I','');
	//montar o PDF e o nome do arquivo PDF
	echo $_SERVER['DOCUMENT_ROOT'].'/nfe/cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg';
	$nome = $danfe->montaDANFE();
	$nomePDF = $nome . '.pdf';
	$nomeXML = $nome . '-nfe.xml';
	//carregar o arquivo pdf numa variavel
	$docPDF = $danfe->printDANFE($nomePDF,'S');
	//enviar o email e testar
        foreach($mailto as $para){
            //para testes
            //$para = 'roberto.machado@superig.com.br';
            $aMail = array('emitente'=>$emitente,'para'=>$para,'contato'=>$contato,'razao'=>$razao,'numero'=>$numero,'serie'=>$serie,'vtotal'=>$vtotal);
       	    echo "<p align='center'>Enviando e-mail com a NFe N. $numero para $para - $razao </p>" ;
            flush();
            if ( $nfeMail->sendNFe($docXML,$docPDF,$nomeXML,$nomePDF,$aMail,'1') ){
                echo '<p align="center">E-mail enviado com sucesso!! </p><br>';
            } else {
                echo "<p>$nfeMail->mailERROR</p><br>";
            }
        }    
    }//fim dos emails
    echo "<div><center><form method='POST' action=''><p><input type='button' value='Fechar' name='B1' onclick='self.close()'></p></form></center></div>";
    echo "</body></html>";
    flush();
}//fim nfe

?>