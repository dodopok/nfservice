<?php
include "../../admin/config.php"; //Conecta com a nosso banco de dados MySQL
include "../../bloc.php"; //Verifica se a sess�o est� ativa
require_once('../../include/nfephp/libs/DanfeNFePHP.class.php');
//require_once('../libs/MailNFePHPAlternate.class.php');
//require_once('../../include/nfephp/libs/MailNFePHP.class.php');
?>
<html>
<head><title>Envia NFE</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<link rel="stylesheet" href="../../css/css.css" type="text/css" />
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.3/themes/redmond/jquery-ui.css" type="text/css" media="all" />
 <style type="text/css">
<!--
.style4 {color: #FF0000}
-->
 </style>
<body bgcolor="#FFFFFF" text="#000000">
<form action="" method="post" name="form_sistema" id="form_sistema" style="width:100%;">
<fieldset id="GPSTATUS" style="-moz-border-radius:3pt;width:98%; background:#FFFFFF; padding:5px;" class="Group">
<legend class="titulo">Notas Fiscais Eletr&ocirc;nicas &gt;&gt; Enviar NF-e</legend>	
<div align="center" class="style2"><b>Enviado NF-e</b></div>
<br/>
<div id="geranfe"></div>  
<fieldset>
 <?

//inicalizar a classe de envio
//$nfeMail = new MailNFePHP();

$idnota = addslashes($_GET['idchave']);

$nfefile='';
if ( isset($idnota) ){
		$querynota = 'SELECT *,str_to_date(dataemissao, "%d/%m/%Y") AS dtemissao, str_to_date(datase, "%d/%m/%Y") AS datasaidaentrada  FROM tb_notas WHERE id_user = '.$_SESSION['numerocli'].' AND id = '.$idnota;
		$querynota = mysql_query($querynota);
		$nota = mysql_fetch_array($querynota);

    $nfefile = '../xml/NFe/NFe'.$nota['chave'].'-nfe.xml';

if(file_exists($nfefile)){

$hora=getdate(); 
$horacerta=($hora['hours'].':'.$hora['minutes']);
$nnota=$nota['idnota'];
$chave=$nota['chave'];
$dtemissao = formatData($nota['dataemissao']);

 //carregar o xml
    $docXML = file_get_contents($nfefile);
    $dom = new DomDocument;
    $dom->loadXML($docXML);
	
    $ide        = $dom->getElementsByTagName("ide")->item(0);
    $emit       = $dom->getElementsByTagName("emit")->item(0);
    $dest       = $dom->getElementsByTagName("dest")->item(0);
    $obsCont    = $dom->getElementsByTagName("obsCont")->item(0);
    $ICMSTot    = $dom->getElementsByTagName("ICMSTot")->item(0);

    $razao = utf8_decode($dest->getElementsByTagName("xNome")->item(0)->nodeValue);
    $cnpj = $dest->getElementsByTagName("CNPJ")->item(0)->nodeValue;
    $numero = str_pad($ide->getElementsByTagName('nNF')->item(0)->nodeValue, 9, "0", STR_PAD_LEFT);
    $serie = str_pad($ide->getElementsByTagName('serie')->item(0)->nodeValue, 3, "0", STR_PAD_LEFT);
    $emitente = utf8_decode($emit->getElementsByTagName("xNome")->item(0)->nodeValue);
	 $cidade = utf8_decode($emit->getElementsByTagName("xMun")->item(0)->nodeValue);
	  $uf = utf8_decode($emit->getElementsByTagName("UF")->item(0)->nodeValue);
    $vtotal = number_format($ICMSTot->getElementsByTagName("vNF")->item(0)->nodeValue, 2, ",", ".");


 						$sqlcliente= "SELECT  email_user FROM tb_clientes WHERE id='".$nota['cliente']."' and id_cliente= ".$_SESSION["numerocli"]." AND (id_user = ".$_SESSION['numerocli']." OR id_user = -1) and stts_excluido=0";
					  $sqlrcli = mysql_query($sqlcliente);
					  $rdadoscli = mysql_fetch_array($sqlrcli);
					   $clienteemail =$rdadoscli["email_user"];
					  
					  
					  
					  $sqltransp= "SELECT  email FROM tb_transportadora WHERE id='".$nota['transportadora']."' and id_cliente= ".$_SESSION["numerocli"]. " AND id_user = ".$_SESSION['numerocli']." and stts_excluido=0";
					  $sqlrtransp = mysql_query($sqltransp);
					  $rdadostransp = mysql_fetch_array($sqlrtransp);
					  $transportadoraemail=$rdadostransp["razao"];



$htmlcliente = '<html>
<head>
    <title>enviaMail</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body><img src="http://'.$_SERVER["SERVER_NAME"].'/nfe/cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg">
<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0.35cm"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=4 STYLE="font-size: 13pt"><B>NF-e
Nota Fiscal Eletr�nica</B></FONT></FONT></FONT></P>
<P CLASS="western" STYLE="margin-bottom: 0cm; line-height: 100%">&nbsp;</P>
<hr color="#000000" size="1">
<P CLASS="western" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm;  line-height: 100%">
<FONT COLOR="#000000"><span class="style1"><FONT FACE="Arial, serif">Prezado
cliente,<BR>
<BR>
Voc� est� recebendo a Nota Fiscal Eletr�nica
n�mero '.$nnota.', s�rie '.$serie.' de '.$emitente.' , Cidade
'. $cidade.'/'. $uf.', no valor de R$ '.$vtotal.', data emiss�o de '.$dtemissao .', Chave
de Acesso: </FONT><strong><FONT STYLE="font-size: 13pt">'.$chave.',</FONT></strong><FONT FACE="Arial, serif">.
Al�m disso, junto com a mercadoria seguir� o DANFE (Documento
Auxiliar da Nota Fiscal Eletr�nica), impresso em papel que acompanha
o transporte das mesmas.<BR>
<BR>
Anexo � este e-mail voc� est�
recebendo tamb�m o arquivo XML da Nota Fiscal Eletr�nica. O Arquivo xml deve ser armazenado
eletronicamente por sua empresa pelo prazo de 05 (cinco) anos,
conforme previsto na legisla��o tribut�ria (Art. 173 do C�digo
Tribut�rio Nacional e � 4� da Lei 5.172 de 25/10/1966).<BR>
<BR>
O
DANFE em papel pode ser arquivado para apresenta��o ao fisco quando
solicitado. Todavia, se sua empresa tamb�m for emitente de NF-e, o
arquivamento eletr�nico do XML de seus fornecedores � obrigat�rio,
sendo pass�vel de fiscaliza��o.<BR>
<BR>
Para se certificar que esta
NF-e � v�lida, queira por favor consultar sua autenticidade no site
nacional do projeto NF-e (www.nfe.fazenda.gov.br), utilizando a chave
de acesso contida no DANFE.</FONT></span></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;  </P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%"><FONT COLOR="#000000">Em
  caso de d�vidas, por favor, entrar em contato com a unidade emissora
  da NF-e (Faturamento) ou com a �rea de Comercial do seu atendimento, <B>(N�o
    Responda este E-mail).</B></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;  </P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
  <FONT COLOR="#000000"><FONT FACE="Arial, serif"><BR>
  Atenciosamente,<BR>
  COMERCIAL
ARARENSE LTDA.</FONT></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;</P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
  <FONT COLOR="#000000"><FONT FACE="Arial, serif">E-mail
gerado eletronicamente pelo Software Emissor NF-e da NFSERVICE.</FONT></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
<A HREF="http://www.nfservice.com.br"><FONT FACE="Arial, serif">www.nfservice.com.br</FONT></A>   <A HREF="http://www.nfservice.blog.br"><FONT FACE="Arial, serif">www.nfservice.blog.br</FONT></A>  <A HREF="http://www.nfstore.com.br"><FONT FACE="Arial, serif">www.nfstore.com.br</FONT></A></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;</P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%"><A NAME="_GoBack"></A>
  <FONT COLOR="#000000"><B>Aviso
Legal:</B>
esta mensagem eletr�nica pode conter informa��es privilegiadas
e/ou confidenciais, portanto, fica o seu receptor notificado de que
qualquer dissemina��o, distribui��o ou c�pia n�o autorizada �
estritamente proibida. Se voc� entende que recebeu esta mensagem
indevidamente ou por engano, por favor, informe este fato ao
remetente e apague-a de seu computador.</FONT></P>
</body>
</html>';

$htmlcliente2= '<html>
<head>
    <title>enviaMail</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body><img src="http://'.$_SERVER["SERVER_NAME"].'/nfe/cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg">
<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0.35cm"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=4 STYLE="font-size: 13pt"><B>NF-e
Nota Fiscal Eletr�nica</B></FONT></FONT></FONT></P>
<P CLASS="western" STYLE="margin-bottom: 0cm; line-height: 100%">&nbsp;</P>
<hr color="#000000" size="1">
<P CLASS="western" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm;  line-height: 100%">
<FONT COLOR="#000000"><span class="style1"><FONT FACE="Arial, serif">Prezado
cliente,<BR>
<BR>
Voc� est� recebendo a Nota Fiscal Eletr�nica
n�mero '.$nnota.', s�rie '.$serie.' de '.$emitente.' , Cidade
'. $cidade.'/'. $uf.', no valor de R$ '.$vtotal.', data emiss�o de '.$dtemissao .', Chave
de Acesso: </FONT><strong><FONT STYLE="font-size: 13pt">'.$chave.',</FONT></strong><FONT FACE="Arial, serif">.
Al�m disso, junto com a mercadoria seguir� o DANFE (Documento
Auxiliar da Nota Fiscal Eletr�nica), impresso em papel que acompanha
o transporte das mesmas.<BR>
<BR>
Anexo � este e-mail voc� est�
recebendo tamb�m o Danfe em arquivo pdf. <BR>
<BR>
O
DANFE em papel pode ser arquivado para apresenta��o ao fisco quando
solicitado. Todavia, se sua empresa tamb�m for emitente de NF-e, o
arquivamento eletr�nico do XML de seus fornecedores � obrigat�rio,
sendo pass�vel de fiscaliza��o.<BR>
<BR>
Para se certificar que esta
NF-e � v�lida, queira por favor consultar sua autenticidade no site
nacional do projeto NF-e (www.nfe.fazenda.gov.br), utilizando a chave
de acesso contida no DANFE.</FONT></span></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;  </P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%"><FONT COLOR="#000000">Em
  caso de d�vidas, por favor, entrar em contato com a unidade emissora
  da NF-e (Faturamento) ou com a �rea de Comercial do seu atendimento, <B>(N�o
    Responda este E-mail).</B></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;  </P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
  <FONT COLOR="#000000"><FONT FACE="Arial, serif"><BR>
  Atenciosamente,<BR>
  COMERCIAL
ARARENSE LTDA.</FONT></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;</P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
  <FONT COLOR="#000000"><FONT FACE="Arial, serif">E-mail
gerado eletronicamente pelo Software Emissor NF-e da NFSERVICE.</FONT></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
<A HREF="http://www.nfservice.com.br"><FONT FACE="Arial, serif">www.nfservice.com.br</FONT></A>   <A HREF="http://www.nfservice.blog.br"><FONT FACE="Arial, serif">www.nfservice.blog.br</FONT></A>  <A HREF="http://www.nfstore.com.br"><FONT FACE="Arial, serif">www.nfstore.com.br</FONT></A></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;</P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%"><A NAME="_GoBack"></A>
  <FONT COLOR="#000000"><B>Aviso
Legal:</B>
esta mensagem eletr�nica pode conter informa��es privilegiadas
e/ou confidenciais, portanto, fica o seu receptor notificado de que
qualquer dissemina��o, distribui��o ou c�pia n�o autorizada �
estritamente proibida. Se voc� entende que recebeu esta mensagem
indevidamente ou por engano, por favor, informe este fato ao
remetente e apague-a de seu computador.</FONT></P>
</body>
</html>';

$htmltransp= '<html>
<head>
    <title>enviaMail</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body><img src="http://'.$_SERVER["SERVER_NAME"].'/nfe/cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg">
<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0.35cm"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=4 STYLE="font-size: 13pt"><B>NF-e
Nota Fiscal Eletr�nica</B></FONT></FONT></FONT></P>
<P CLASS="western" STYLE="margin-bottom: 0cm; line-height: 100%">&nbsp;</P>
<hr color="#000000" size="1">
<P CLASS="western" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm;  line-height: 100%">
<FONT COLOR="#000000"><span class="style1"><FONT FACE="Arial, serif">Prezado
cliente,<BR>
<BR>
Voc� est� recebendo a Nota Fiscal Eletr�nica
n�mero '.$nnota.', s�rie '.$serie.' de '.$emitente.' , Cidade
'. $cidade.'/'. $uf.', no valor de R$ '.$vtotal.', data emiss�o de '.$dtemissao .', Chave
de Acesso: </FONT><strong><FONT STYLE="font-size: 13pt">'.$chave.',</FONT></strong><FONT FACE="Arial, serif">.
Al�m disso, junto com a mercadoria seguir� o DANFE (Documento
Auxiliar da Nota Fiscal Eletr�nica), impresso em papel que acompanha
o transporte das mesmas.<BR>
<BR>
Anexo � este e-mail voc� est�
recebendo tamb�m o arquivo XML da Nota Fiscal Eletr�nica. O Arquivo xml deve ser armazenado
eletronicamente por sua empresa pelo prazo de 05 (cinco) anos,
conforme previsto na legisla��o tribut�ria (Art. 173 do C�digo
Tribut�rio Nacional e � 4� da Lei 5.172 de 25/10/1966).<BR>
<BR>
O
DANFE em papel pode ser arquivado para apresenta��o ao fisco quando
solicitado. Todavia, se sua empresa tamb�m for emitente de NF-e, o
arquivamento eletr�nico do XML de seus fornecedores � obrigat�rio,
sendo pass�vel de fiscaliza��o.<BR>
<BR>
Para se certificar que esta
NF-e � v�lida, queira por favor consultar sua autenticidade no site
nacional do projeto NF-e (www.nfe.fazenda.gov.br), utilizando a chave
de acesso contida no DANFE.</FONT></span></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;  </P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%"><FONT COLOR="#000000">Em
  caso de D�vidas, por favor, entrar em contato com a unidade emissora
  da NF-e ou com a �rea de Log�stica do seu atendimento, <B>(N�o
    Responda este E-mail).</B></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
  <FONT COLOR="#000000"><FONT FACE="Arial, serif"><BR>
  Atenciosamente,<BR>
  COMERCIAL
ARARENSE LTDA.</FONT></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;</P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
  <FONT COLOR="#000000"><FONT FACE="Arial, serif">E-mail
gerado eletronicamente pelo Software Emissor NF-e da NFSERVICE.</FONT></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
<A HREF="http://www.nfservice.com.br"><FONT FACE="Arial, serif">www.nfservice.com.br</FONT></A>   <A HREF="http://www.nfservice.blog.br"><FONT FACE="Arial, serif">www.nfservice.blog.br</FONT></A>  <A HREF="http://www.nfstore.com.br"><FONT FACE="Arial, serif">www.nfstore.com.br</FONT></A></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;</P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%"><A NAME="_GoBack"></A>
  <FONT COLOR="#000000"><B>Aviso
Legal:</B>
esta mensagem eletr�nica pode conter informa��es privilegiadas
e/ou confidenciais, portanto, fica o seu receptor notificado de que
qualquer dissemina��o, distribui��o ou c�pia n�o autorizada �
estritamente proibida. Se voc� entende que recebeu esta mensagem
indevidamente ou por engano, por favor, informe este fato ao
remetente e apague-a de seu computador.</FONT></P>
</body>
</html>';

$htmltransp2= '<html>
<head>
    <title>enviaMail</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body><img src="http://'.$_SERVER["SERVER_NAME"].'/nfe/cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg">
<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0.35cm"><FONT COLOR="#000000"><FONT FACE="Arial, serif"><FONT SIZE=4 STYLE="font-size: 13pt"><B>NF-e
Nota Fiscal Eletr�nica</B></FONT></FONT></FONT></P>
<P CLASS="western" STYLE="margin-bottom: 0cm; line-height: 100%">&nbsp;</P>
<hr color="#000000" size="1">
<P CLASS="western" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm;  line-height: 100%">
<FONT COLOR="#000000"><span class="style1"><FONT FACE="Arial, serif">Prezado
cliente,<BR>
<BR>
Voc� est� recebendo a Nota Fiscal Eletr�nica
n�mero '.$nnota.', s�rie '.$serie.' de '.$emitente.' , Cidade
'. $cidade.'/'. $uf.', no valor de R$ '.$vtotal.', data emiss�o de '.$dtemissao .', Chave
de Acesso: </FONT><strong><FONT STYLE="font-size: 13pt">'.$chave.',</FONT></strong><FONT FACE="Arial, serif">.
Al�m disso, junto com a mercadoria seguir� o DANFE (Documento
Auxiliar da Nota Fiscal Eletr�nica), impresso em papel que acompanha
o transporte das mesmas.<BR>
<BR>
Anexo � este e-mail voc� est�
recebendo o Danfe em arquivo pdf. <BR>
<BR>
O
DANFE em papel pode ser arquivado para apresenta��o ao fisco quando
solicitado. Todavia, se sua empresa tamb�m for emitente de NF-e, o
arquivamento eletr�nico do XML de seus fornecedores � obrigat�rio,
sendo pass�vel de fiscaliza��o.<BR>
<BR>
Para se certificar que esta
NF-e � v�lida, queira por favor consultar sua autenticidade no site
nacional do projeto NF-e (www.nfe.fazenda.gov.br), utilizando a chave
de acesso contida no DANFE.</FONT></span></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;  </P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%"><FONT COLOR="#000000">Em
  caso de D�vidas, por favor, entrar em contato com a unidade emissora
  da NF-e ou com a �rea de Log�stica do seu atendimento, <B>(N�o
    Responda este E-mail).</B></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
  <FONT COLOR="#000000"><FONT FACE="Arial, serif"><BR>
  Atenciosamente,<BR>
  COMERCIAL
ARARENSE LTDA.</FONT></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;</P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
  <FONT COLOR="#000000"><FONT FACE="Arial, serif">E-mail
gerado eletronicamente pelo Software Emissor NF-e da NFSERVICE.</FONT></FONT></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">
<A HREF="http://www.nfservice.com.br"><FONT FACE="Arial, serif">www.nfservice.com.br</FONT></A>   <A HREF="http://www.nfservice.blog.br"><FONT FACE="Arial, serif">www.nfservice.blog.br</FONT></A>  <A HREF="http://www.nfstore.com.br"><FONT FACE="Arial, serif">www.nfstore.com.br</FONT></A></P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%">&nbsp;</P>
<P CLASS="western style1" STYLE="margin-top: 0.05cm; margin-bottom: 0.05cm; N�o line-height: 100%"><A NAME="_GoBack"></A>
  <FONT COLOR="#000000"><B>Aviso
Legal:</B>
esta mensagem eletr�nica pode conter informa��es privilegiadas
e/ou confidenciais, portanto, fica o seu receptor notificado de que
qualquer dissemina��o, distribui��o ou c�pia n�o autorizada �
estritamente proibida. Se voc� entende que recebeu esta mensagem
indevidamente ou por engano, por favor, informe este fato ao
remetente e apague-a de seu computador.</FONT></P>
</body>
</html>';


//inicializar a DANFE
	$danfe = new DanfeNFePHP($docXML, 'P', 'A4',$_SERVER['DOCUMENT_ROOT'].'/nfe/cadastros/emitente/logo/'.$_SESSION["numerocli"].'.jpg','I','');
	//montar o PDF e o nome do arquivo PDF
	$nome = $danfe->montaDANFE();
	$nomePDF = $nome . '.pdf';
	//carregar o arquivo pdf numa variavel
	$docPDF = $danfe->printDANFE($nomePDF,'S');
	
	$filePDF=$_SERVER['DOCUMENT_ROOT'].'/nfe/notase/pdf/'.$nomePDF;
	
	@file_put_contents($filePDF,$docPDF);
	
	
$remetente ="nfe@nfservice.com.br";
$nome="NF.SERVICE - NF-e Online";
$assunto = "NF-e Nota Fiscal Eletr�nica - XML";
$assunto2 = "NF-e Nota Fiscal Eletr�nica - DANFE";

$boundary = strtotime('NOW');


$random_hash = md5(date('r', time())); 
$headers = "From: ".$nome." <" . $remetente . ">\r\n";
$headers .= "Return-Path: <" . $remetente . ">\r\n";
$headers .= "Reply-To: <" . $remetente . ">\r\n";
$headers .= "X-Errors-To: <" . $remetente . ">\r\n";
$headers .= "X-Sender: <" . $remetente . ">\r\n";
$headers .= "X-Mailer:PHP/".phpversion()."\r\n"; // mailer
$headers .= "MIME-Version: 1.1\r\n";
$headers .= "Content-Type: multipart/mixed; boundary=\"" . $boundary . "\"\n";

 
 $arquivonome = basename  ($nfefile); 
 
  
 //arquivo anexado no corpo da mensagem.
$msg = "--" . $boundary . "\n";
$msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
$msg .= "Content-Transfer-Encoding: quoted-printable\n\n"; 

$msg .= $htmlcliente."\n";
 
$msg .= "--" . $boundary . "\n";
$msg .= "Content-Transfer-Encoding: base64\n";
$msg .= "Content-Disposition: attachment; filename=\"".$arquivonome ."\"\n\n";


 //arquivo anexado no corpo da mensagem.
$msga = "--" . $boundary . "\n";
$msga .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
$msga .= "Content-Transfer-Encoding: quoted-printable\n\n"; 

$msga .= $htmlcliente2."\n";
 
$msga .= "--" . $boundary . "\n";
$msga .= "Content-Transfer-Encoding: base64\n";
$msga .= "Content-Disposition: attachment; filename=\"".$nomePDF ."\"\n\n";

 //arquivo anexado no corpo da mensagem.
$msg2 = "--" . $boundary . "\n";
$msg2.= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
$msg2 .= "Content-Transfer-Encoding: quoted-printable\n\n"; 

$msg2 .= $htmltransp."\n";
 
$msg2 .= "--" . $boundary . "\n";
$msg2 .= "Content-Transfer-Encoding: base64\n";
$msg2 .= "Content-Disposition: attachment; filename=\"".$arquivonome ."\"\n\n";

 //arquivo anexado no corpo da mensagem.
$msg2a = "--" . $boundary . "\n";
$msg2a.= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
$msg2a .= "Content-Transfer-Encoding: quoted-printable\n\n"; 

$msg2a .= $htmltransp2."\n";
 
$msg2a .= "--" . $boundary . "\n";
$msg2a .= "Content-Transfer-Encoding: base64\n";
$msg2a .= "Content-Disposition: attachment; filename=\"".$nomePDF ."\"\n\n";

ob_start();
   readfile($nfefile);
   $enc = ob_get_contents();
ob_end_clean();

ob_start();
   readfile($filePDF);
   $enc2 = ob_get_contents();
ob_end_clean();

$msg_temp = base64_encode($enc). "\n";
$tmp[1] = strlen($msg_temp);
$tmp[2] = ceil($tmp[1]/76);

$msg_temp2 = base64_encode($enc2). "\n";
$tmp2[1] = strlen($msg_temp2);
$tmp2[2] = ceil($tmp2[1]/76);

for ($b = 0; $b <= $tmp[2]; $b++) {
    $tmp[3] = $b * 76;
    $msg .= substr($msg_temp, $tmp[3], 76) . "\n";
	$msg2 .= substr($msg_temp, $tmp[3], 76) . "\n";
}

for ($b2 = 0; $b2 <= $tmp2[2]; $b2++) {
    $tmp2[3] = $b2 * 76;
    $msga .= substr($msg_temp2, $tmp2[3], 76) . "\n";
	$msg2a .= substr($msg_temp2, $tmp2[3], 76) . "\n";
}

unset($msg_temp, $tmp, $enc);
unset($msg_temp2, $tmp2, $enc2);


    
	 
	 $nfeonlineemail = "nfe@nfservice.com.br";
     $titulo=$remetente."\n";
	 $titulo .= "X-Mailer:PHP/".phpversion()."\n"; 
     $titulo .= "Mime-Versao: 1.0\n"; 
     $titulo .= "Content-type: text/html; charset=iso-8859-1"; 

@mail("$clienteemail","$assunto","$msg",$headers);
@mail("$clienteemail","$assunto2","$msga",$headers);
@mail("$transportadoraemail","$assunto","$msg2",$headers);
@mail("$transportadoraemail","$assunto2","$msg2a",$headers);
@mail("$nfeonlineemail","$assunto","$msg",$headers);
@mail("$nfeonlineemail","$assunto2","$msga",$headers);
echo "<script>alert('NF-e encaminhada com sucesso.'); window.location.href='gerne.php';</script>";

}	else {
?>
 <span class="style1 style4"> XML Inv&aacute;lido, tente novamente.</span>
<?
}
}	else {
?>
<span class="style4">NF-e Inv&aacute;lida, tente novamente</span>.
<?
}

?>
</fieldset>
</fieldset></form>
</body>
</head>
</html>
