﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div id="adminbar">Bem-vindo, 
<strong>
<%: Html.ActionLink(new MvcExtensions.Security.Session.Context(this.ViewContext.HttpContext).GetLoggedUser().Name, "Edit", "MeusDados") %>
</strong> <%: Html.ActionLink("Sair", "LogOut", "Home") %>
</div>