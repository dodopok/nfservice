﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<% var loggedUser =  new MvcExtensions.Security.Session.Context(this.ViewContext.HttpContext).GetLoggedUser(); %>

<br />
<% if (loggedUser.AccessGroups.Contains("Usuário Principal")) { %> 

	<% Html.RenderAction("UsuarioMenu", "Menus"); %>

<% } else { %>
	
	<% var temEmpresaSelecionada = this.Url.RequestContext.RouteData.Values["empresaID"] != null; %>
	
	<% if (temEmpresaSelecionada){ %>

		<% Html.RenderAction("CaminhoRatoMenu", "Menus", new { empresaID = this.Url.RequestContext.RouteData.Values["empresaID"] }); %>
		<br /><br />
		<% Html.RenderAction("GerenteNotasEmpresaMenu", "Menus", new { empresaID = this.Url.RequestContext.RouteData.Values["empresaID"] }); %>
	
	<% } else { %>
		
		<% Html.RenderAction("GerenteNotasMenu", "Menus"); %>

	<% } %>

<% } %>










		