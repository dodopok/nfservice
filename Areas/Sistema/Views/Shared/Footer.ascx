﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="container_12">
	<div class="grid_12" id="footer">
		<p>© Copyright 2011 by <%: Html.RouteLink("NFService", "Default", new { controller = "Home" })%> </p>
		<br />
		<p>Desenvolvido por <a href="http://www.websocorro.com.br/" target="_blank">Agência WebSocorro</a></p>
	</div>
	<div class="clearingfix"></div>
</div>