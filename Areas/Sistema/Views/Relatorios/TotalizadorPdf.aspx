﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master"
    Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioTotalizador>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<header runat="server">
<script type="text/javascript" src="/Scripts/jquery.maskMoney.0.2.js"></script>
<title>
</title>
</header>
<body>
    <%
        var empresas = ViewData["Empresas"] == null ? new List<Domain.Entities.Empresa>() : (List<Domain.Entities.Empresa>)ViewData["Empresas"];
    %>

    <div class="container_12">
        <div class="grid_12">
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Relatório - Totalizador</h3>
                    </div>
                    <div class="bcont">
                        <center>
                            <b>Total de Notas no Sistema</b></center>
                        <br />
                        <div>
                            <table style="margin-bottom:15px;text-align:left;" cellspacing="0" cellpadding="0" width="100%">
                                <thead>
                                    <tr>
                                        <td style="border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left; font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            <center>
                                                Situação
                                            </center>
                                        </td>
                                        <td style="border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left; font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Notas de Entrada
                                        </td>
                                        <td style="border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left; font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Notas de Saída
                                        </td>
                                        <td style="border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left; font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Notas de Serviço
                                        </td>
                                        <td style="border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left; font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Notas de Transporte
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (Model.ItensRelatorio.Where(i => i.Baixada).Count() > 0)
                                       { %>
                                    <tr>
                                        <td>
                                            <center>
                                                Baixadas
                                            </center>
                                        </td>
                                        <td>
                                            <% var entrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada && i.Baixada); %>
                                            <%: entrada != null ? entrada.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var saida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida && i.Baixada); %>
                                            <%: saida != null ? saida.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var servico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço && i.Baixada); %>
                                            <%: servico != null ? servico.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var transporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte && i.Baixada); %>
                                            <%: transporte != null ? transporte.QuantidadeNotas : 0%>
                                        </td>
                                    </tr>
                                    <% } %>
                                    <% else if (Model.ItensRelatorio.Where(i => !i.Baixada).Count() > 0)
                                       { %>
                                    <tr>
                                        <td>
                                            <center>
                                                Não Baixadas
                                            </center>
                                        </td>
                                        <td>
                                            <% var Nentrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada && !i.Baixada); %>
                                            <%: Nentrada != null ? Nentrada.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var Nsaida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida && !i.Baixada); %>
                                            <%: Nsaida != null ? Nsaida.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var Nservico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço && !i.Baixada); %>
                                            <%: Nservico != null ? Nservico.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var Ntransporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte && !i.Baixada); %>
                                            <%: Ntransporte != null ? Ntransporte.QuantidadeNotas : 0%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Total de Notas do Sistema:
                                            <%: Model.TotalNotas %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <p>
                                <button id="enviar" class="button green" type="submit" value="Save">
                                    <span>Gerar Relatório</span>
                                </button>
                                <%--	<span id="processando" class="note loading">Processando...</span>--%>
                            </p>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</body>
</html>
