﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioTotalizador>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
        <style type="text/css">
            html, body {height:100%;}
            body {font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;}
            .tbRelatorio{margin-left: 200px;}
            table.tbRelatorio tr th{font-size: 15px;font-weight: bold;}
            
            table.infotable {margin-bottom:15px;text-align:left;}
            table.infotable  tr td, table.infotable tr th {border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left;}
            table.infotable  tr td.small, table.infotable tr th.small {width:20px;text-align:center;}
            table.infotable  tr td.small input[type="checkbox"], table.infotable tr th.small  input[type="checkbox"] {vertical-align:middle;}
            table.infotable thead tr {background-color:#f5f5f5;}
            table.infotable th {font-weight:bold;color: #305B7F;text-shadow: 1px 1px 0px rgba(255,255,255,1);}
            table.infotable tbody tr.selected {background-color:#fdffea !important;}
        </style>
    
    <title>Totalizador</title>
</head>
<body>
    <div>
                            <%: Html.Hidden("tipoDestino") %>
        <center><b>TOTAL DE NOTAS DO SISTEMA</b></center>
        <center>
            <table class="infotable" cellspacing="0" cellpadding="0" width="800px">
                <thead>
                    <tr>
                    <th>
                        <center>
                            Situação
                        </center>
                    </th>
                    <th>
                        Notas de Entrada
                    </th>
                    <th>
                        Notas de Saída
                    </th>
                    <th>
                        Notas de Serviço
                    </th>
                    <th>
                        Notas de Transporte
                    </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <center>
                                BAIXADAS
                            </center>
                        </td>
                        <td>
                            <% var entrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada && i.Baixada); %>
                            <%: entrada != null ? entrada.QuantidadeNotas : 0 %>
                        </td>
                        <td>
                            <% var saida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida && i.Baixada); %>
                            <%: saida != null ? saida.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var servico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço && i.Baixada); %>
                            <%: servico != null ? servico.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var transporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte && i.Baixada); %>
                            <%: transporte != null ? transporte.QuantidadeNotas : 0%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <center>
                                NÃO BAIXADAS
                            </center>
                        </td>
                        <td>
                            <% var Nentrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada && !i.Baixada); %>
                            <%: Nentrada != null ? Nentrada.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var Nsaida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida && !i.Baixada); %>
                            <%: Nsaida != null ? Nsaida.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var Nservico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço && !i.Baixada); %>
                            <%: Nservico != null ? Nservico.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var Ntransporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte && !i.Baixada); %>
                            <%: Ntransporte != null ? Ntransporte.QuantidadeNotas : 0%>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Total de Notas do Sistema: <%: Model.TotalNotas %> </th>
                    </tr>
                </tbody>
            </table>
        </center>
    </div>

</body>
</html>


