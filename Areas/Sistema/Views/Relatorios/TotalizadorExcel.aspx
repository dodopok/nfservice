﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioTotalizador>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatório - Totalizador
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%
var empresas = ViewData["Empresas"] == null ? new List<Domain.Entities.Empresa>() : (List<Domain.Entities.Empresa>)ViewData["Empresas"];
%>
<div class="BreadCrumb">
    <%: Html.ActionLink("Relatórios","Relatorios")%> / Totalizador
</div>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Relatório - Totalizador</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "Relatorios") %></li>
						</ul>
					</div>
					<div class="bcont">
						<center><b>Total de Notas no Sistema</b></center>
                        <br />  
                        <% using (Html.BeginForm("PrintTotalizador", "Relatorios", FormMethod.Post, new { id = "form", name = "form", @target = "_blank" })) { %>
						<div>

                        <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                            <thead>
                                <tr>
                                <th>
                                    <center>
                                        Situação
                                    </center>
                                </th>
                                <th>
                                    Notas de Entrada
                                </th>
                                <th>
                                    Notas de Saída
                                </th>
                                <th>
                                    Notas de Serviço
                                </th>
                                <th>
                                    Notas de Transporte
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                <% if (Model.ItensRelatorio.Where(i => i.Baixada).Count() > 0)
                                   { %>
                                <tr>
                                    <td>
                                        <center>
                                            Baixadas
                                        </center>
                                    </td>
                                    <td>
                                        <% var entrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada && i.Baixada); %>
                                        <%: entrada != null ? entrada.QuantidadeNotas : 0%>
                                    </td>
                                    <td>
                                        <% var saida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida && i.Baixada); %>
                                        <%: saida != null ? saida.QuantidadeNotas : 0%>
                                    </td>
                                    <td>
                                        <% var servico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço && i.Baixada); %>
                                        <%: servico != null ? servico.QuantidadeNotas : 0%>
                                    </td>
                                    <td>
                                        <% var transporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte && i.Baixada); %>
                                        <%: transporte != null ? transporte.QuantidadeNotas : 0%>
                                    </td>
                                </tr>
                                <% } %>
                                <% else if (Model.ItensRelatorio.Where(i => !i.Baixada).Count() > 0)
                                    { %>
                                <tr>
                                    <td>
                                        <center>
                                            Não Baixadas
                                        </center>
                                    </td>
                                    <td>
                                        <% var Nentrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada && !i.Baixada); %>
                                        <%: Nentrada != null ? Nentrada.QuantidadeNotas : 0%>
                                    </td>
                                    <td>
                                        <% var Nsaida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida && !i.Baixada); %>
                                        <%: Nsaida != null ? Nsaida.QuantidadeNotas : 0%>
                                    </td>
                                    <td>
                                        <% var Nservico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço && !i.Baixada); %>
                                        <%: Nservico != null ? Nservico.QuantidadeNotas : 0%>
                                    </td>
                                    <td>
                                        <% var Ntransporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte && !i.Baixada); %>
                                        <%: Ntransporte != null ? Ntransporte.QuantidadeNotas : 0%>
                                    </td>
                                </tr>
                                <% } %>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Total de Notas do Sistema: <%: Model.TotalNotas %> </th>
                                </tr>
                            </tbody>
                        </table>

						<hr />
						<p>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Gerar Relatório</span>
							</button>
						<%--	<span id="processando" class="note loading">Processando...</span>--%>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

    $(document).ready(function () {


        $("#erro").hide();
        $(".note.loading").hide();


        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
//                $("#processando").show();
                form.submit();
            }
        });
    });
	</script>
</asp:Content>


