﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatorios
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<div class="container_12">
		<div class="grid_12">
			
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Relatórios - Menu de Navegação</h3>
					</div>
					<div class="bcont">
						<%: Html.ActionLink("Totalizador", "Totalizador", "Relatorios")%>
						<br />
						<br />
						<%: Html.ActionLink("Notas de Entrada", "Periodo", "Relatorios", new { tipoID = (int)Domain.Entities.TipoEnum.Entrada }, null)%>
						<br />
						<br />
						<%: Html.ActionLink("Notas de Saída", "Periodo", "Relatorios", new { tipoID = (int)Domain.Entities.TipoEnum.Saida }, null)%>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
