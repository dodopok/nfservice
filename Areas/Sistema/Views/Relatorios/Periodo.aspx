﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioNotasBaixadas>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatório - Por Período
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%
	var dataInicio = (DateTime)ViewData["dataInicio"];
	var dataFim = (DateTime)ViewData["DataFim"];
	var tipoID = ViewData["Tipo"] != null ? (int)ViewData["Tipo"] : 0;
%>
<div class="BreadCrumb">
    <%: Html.ActionLink("Relatórios","Relatorios")%> / Nota de <%: (Domain.Entities.TipoEnum)tipoID %> - Por Período
</div>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Relatório - Nota de <%: (Domain.Entities.TipoEnum)tipoID %> - Por Período</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "Relatorios") %></li>
						</ul>
					</div>
					<div class="bcont">
						
						<% using (Html.BeginForm("Periodo", "Relatorios", FormMethod.Post, new { id = "form", name = "form" }))
						   { %>
							<%: Html.Hidden("tipoID", tipoID)%>
                            <%: Html.Hidden("Carregar")%>
							<p>
								<b>Período Inicial </b><br />
								<%: Html.TextBox("dataInicio", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							</p>
							<p>
								<b>Período Final </b><br />
								<%: Html.TextBox("dataFim", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							</p>
							<p>
								<button id="enviar" class="button green" type="submit" value="Save">
									<span>Gerar Relatório</span>
								</button>
							<span id="processando" class="note loading">Processando...</span>
							</p>
						<% } %>
					<% using (Html.BeginForm("PrintPeriodo", "Relatorios", FormMethod.Post, new { id = "form", name = "form" , @target = "_blank"})){ %>
						<div>
						<%: Html.Hidden("dataInicioPrint", dataInicio) %>
						<%: Html.Hidden("dataFimPrint", dataFim) %>
						<%: Html.Hidden("tipoIDPrint", tipoID)%>
						<center><b>Total de Notas de <%: (Domain.Entities.TipoEnum)tipoID %></b></center>
						<center>Período: De <%: dataInicio.ToShortDateString() %> a <%: dataFim.ToShortDateString() %></center>
						<br /><br />
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								    <th>
									    <center>Situação</center>
								    </th>   
								    <th>
									    <center>
                                            Ano
                                        </center>
								    </th>
								    <th>
									    Mês
								    </th>
								    <th>
									    <center>Ativa</center>
								    </th>
								    <th>
									    <center>Cancelada</center>
								    </th>
								    <th>
									    <center>Denegada</center>
								    </th>
								    <th>
									    <center>Não Verificada</center>
								    </th>  
								</tr>
							</thead>
							<tbody>
								<%foreach (var item in Model.linhasRelatorio ) 
								  { %>										
									<tr>
									    <td>
										    <center><%: item.FoiBaixada.Value ? "Baixada" : "Não Baixada" %></center>
									    </td>		
									    <td>
										    <center><%: item.Ano %></center>
									    </td>
									    <td>
										    <b><%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Mes)%></b>
									    </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeAtiva %>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeCancelada %>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeDenegada %>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeNaoVerificada %>
                                            </center>
                                        </td>
									</tr>
								<% }  %>
									<tr>
									    <th>
                                        </th>
                                        <th>
                                            Sub-Total do Período:
                                        </th>
                                        <th></th>
                                        <th>
                                            <center>
                                                <%: Model.linhaTotal.QuantidadeAtiva %>
                                            </center>
                                        </th>
                                        <th>
                                            <center>
                                                <%: Model.linhaTotal.QuantidadeCancelada %>
                                            </center>
                                        </th>
                                        <th>
                                            <center>
                                                <%: Model.linhaTotal.QuantidadeDenegada %>
                                            </center>
                                        </th>
                                        <th>
                                            <center>
                                                <%: Model.linhaTotal.QuantidadeNaoVerificada %>
                                            </center>
                                        </th>
                                    </tr>
								<tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>
										Total geral de Notas no período : <%: Model.TotalNotas %> 
									</th>									
								</tr>
							</tbody>
						</table>

						<hr />
						<p>
							<button class="button green" type="submit" value="Save">
								<span>Gerar Impressão</span>
							</button>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

    $(document).ready(function () {


        $("#erro").hide();
        $(".note.loading").hide();

        $("#dataInicio").mask("99/99/9999");
        $("#dataFim").mask("99/99/9999");

        $("#dataInicio").blur(function () {
            $("#dataInicioPrint").val($("#dataInicio").val());
        });

        $("#dataFim").blur(function () {
            $("#dataFimPrint").val($("#dataFim").val());
        });

        $("#enviar").click(function () {
            $("#Carregar").val(true);
        });

        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            }           
        });
    });
	</script>
</asp:Content>


