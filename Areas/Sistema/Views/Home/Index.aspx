﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Usuario>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Home
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="container_12">
	<%--<h3 class="head2">Olá, <%: Model.Nome %></h3>
	<b>Utilize o menu para dar início ao gerenciamento de empresas!<br /><br />
	<br /><br />
	<!-- Aqui entra a lista dos créditos disponíveis -->--%>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

<style type="text/css">
h3 {font-size:2.25em; line-height:1.111em; text-transform:uppercase; margin:0px 0 20px; font-weight:normal; color:#000;}
.head2 {padding-bottom:6px; width:100%; text-transform:none; background:url(/Content/images/tail-head.gif) 0 bottom repeat-x; margin-bottom:25px; margin-top:10px;}
label
{
	background-color:White;
	color:#f24c4c;
	font-size:18px;
	font-weight:bold;
}
</style>

</asp:Content>
