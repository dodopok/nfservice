﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Acesso Bloqueado
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Acesso Bloqueado</h3>
					</div>
					<div class="bcont">
						<div id="erro" class="message error">
							<span class="strong">ATENÇÃO!</span> O seu acesso a este item foi bloqueado. Consulte o responsável.
						</div>
					</div>
				</div>
			</div>
			<div class="clearingfix">
			</div>
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>