﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.NotaFiscal>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<%--    <style type="text/css">
        html, body
        {
            height: 100%;
        }
        body
        {
            font: 13.34px helvetica,arial,freesans,clean,sans-serif;
            min-height: 100%;
            background-color: #f7f7f7;
        }
        .tbRelatorio
        {
            margin-left: 200px;
        }
        table.tbRelatorio tr th
        {
            font-size: 15px;
            font-weight: bold;
        }
        
        table.infotable
        {
            margin-bottom: 15px;
            text-align: left;
        }
        table.infotable tr td, table.infotable tr th
        {
            border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;
        }
        table.infotable tr td.small, table.infotable tr th.small
        {
            width: 20px;
            text-align: center;
        }
        table.infotable tr td.small input[type="checkbox"], table.infotable tr th.small input[type="checkbox"]
        {
            vertical-align: middle;
        }
        table.infotable thead tr
        {
            background-color: #f5f5f5;
        }
        table.infotable th
        {
            font-weight: bold;
            color: #305B7F;
            text-shadow: 1px 1px 0px rgba(255,255,255,1);
        }
        
        
    </style>--%>
</head>
<body>
    <div>
    <br />
            <table border="1" cellpadding="3" cellspacing="3" style=" margin-bottom: 15px;
            text-align: left;">
                <thead>
                    <tr bgcolor="#f5f5f5" color="#305B7F" style="font-size: 8px; font-weight: bold;" >
                        <td>
                            Código
                        </td>
                        <td>
                            Nome do Emitente
                        </td>
                        <td>
                            Cidade do Emitente
                        </td>
                        <td>
                            Cidade do Destinatário
                        </td>
                        <td>
                            Data de Emissão
                        </td>
                        <td>
                            CNPJ do Emitente
                        </td>
                        <td>
                            CNPJ do Destinatário
                        </td>
                        <td>
                            Nome do Destinatário
                        </td>
                        <td>
                            Status
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <% foreach (var item in Model)
                       { %>
                    <tr style="font-size: 8px;">
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;">
                            <%: Convert.ToInt32(item.CodigoNFe).ToString(@"000\.000\.000")%>
                        </td>
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;">
                            <%: item.Emitente.Nome %>
                        </td>
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;">
                            <%: item.Destinatario.Endereco == null? string.Empty: item.Destinatario.Endereco.Cidade == null ? string.Empty : item.Destinatario.Endereco.Cidade.Nome%><%--cidadeEmitente--%>
                        </td>
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;"> 
                            <%: item.Destinatario.Endereco == null? string.Empty: item.Destinatario.Endereco.CEP %>
                        </td>
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;">
                            <%: item.DataEmissao.ToShortDateString() %>
                        </td>
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;">
                            <%: !string.IsNullOrEmpty(item.Emitente.CNPJ) ? item.Emitente.CNPJ : "00.000.000/0000-00"%>
                        </td>
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;">
                            <%: !string.IsNullOrEmpty(item.Destinatario.CNPJ)? item.Destinatario.CNPJ: "00.000.000/0000-00"%>
                        </td>
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;">
                            <%: item.Destinatario.Nome %>
                        </td>
                        <td style=" border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;">
                            <%: item.Status.Rotulo %>
                        </td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
    </div>
</body>
</html>
