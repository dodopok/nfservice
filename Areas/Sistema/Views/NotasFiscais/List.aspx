<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master"
    Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.NotaFiscal>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Notas Fiscais
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% 
        var status = ViewData["StatusID"] == null ? new List<Domain.Entities.StatusNotaFiscal>() : (IEnumerable<Domain.Entities.StatusNotaFiscal>)ViewData["StatusID"];
        var estados = ViewData["Estados"] == null ? new List<Domain.Entities.Estado>() : (IEnumerable<Domain.Entities.Estado>)ViewData["Estados"];
        var cidades = ViewData["Cidades"] == null ? new List<Domain.Entities.Cidade>() : (IEnumerable<Domain.Entities.Cidade>)ViewData["Cidades"];
        var empresaSelecionada = (Domain.Entities.Empresa)ViewData["EmpresaSelecionada"];
        var tipo = ViewData["Tipo"] == null ? new List<Domain.Entities.Tipo>() : (IEnumerable<Domain.Entities.Tipo>)ViewData["Tipo"];

        var Filtros = (WebUI.Areas.Sistema.ViewModels.FiltroNotasFiscais)ViewData["Filtros"];
        var CidadeDestSelecionado = ViewData["CidadeDestSelecionado"] != null ? (Domain.Entities.Cidade)ViewData["CidadeDestSelecionado"] : new Domain.Entities.Cidade();
        var CidadeEmitSelecionado = ViewData["CidadeEmitSelecionado"] != null ? (Domain.Entities.Cidade)ViewData["CidadeEmitSelecionado"] : new Domain.Entities.Cidade();
        var StatusSelecionado = ViewData["StatusSelecionado"] != null ? (Domain.Entities.StatusNotaFiscal)ViewData["StatusSelecionado"] : new Domain.Entities.StatusNotaFiscal();
        var TipoSelecionado = ViewData["TipoSelecionado"] != null ? (Domain.Entities.TipoEnum)(int)ViewData["TipoSelecionado"] : new Domain.Entities.TipoEnum();
        var isTransporte = ViewData["isTransporte"];
        var isServico = ViewData["isServico"];
        var FoiBaixada = ViewData["FoiBaixada"];

        var Carregar = ViewData["Carregar"] == null ? false : Convert.ToBoolean(ViewData["Carregar"]);
        var empresaID = ViewData["empresaID"] != null ? (int)ViewData["empresaID"] : 0;

        var total = ((ViewData["Total"] == null) ? 0 : (int)ViewData["Total"]);
        var pagina = Convert.ToInt32(ViewData["Pagina"]);
        var filtros = ((ViewData["paramfiltro"] == null) ? new WebUI.Areas.Sistema.ViewModels.FiltroNotasFiscais() : (WebUI.Areas.Sistema.ViewModels.FiltroNotasFiscais)ViewData["paramfiltro"]);
        var config = new Domain.Core.Configuration();
    %>
    <div class="container_12">
        <div class="grid_12">
            <div id="erro" class="message error">
                <span class="strong">ERRO!</span> Operação Cancelada!
            </div>
            <%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString())
              { %>
            <div id="sucesso" class="message success">
                <span class="strong">SUCESSO!</span> Nota Fiscal enviada com Sucesso!
            </div>
            <%} %>
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div id="headerFiltro">
                        <h3>
                            Filtragem</h3>
                        &nbsp;&nbsp;
                        <img src="/Content/images/function-icons/search_48.png" alt="Mostrar ou Esconder"
                            class="fright" width="35px" />
                    </div>
                    <div class="bcont">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <% using (Html.BeginForm("List", "NotasFiscais", FormMethod.Post, new { id = "form", name = "form" }))
                                           {  %>
                                        <%: Html.Hidden("tipoDestino") %>
                                        <%: Html.Hidden("Carregar",Carregar) %>
                                        <div id="formularioDanfe">
                                            <div class="esquerda">
                                                <b>Nome do Emitente</b><br />
                                                <p>
                                                    <%: Html.TextBox("NomeEmitente", null, new { @maxlength = "128" })%>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>Nome do Destinatário</b><br />
                                                <p>
                                                    <%: Html.TextBox("NomeDestinatario")%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>CNPJ do Emitente</b><br />
                                                <p>
                                                    <%: Html.TextBox("CNPJEmitente")%>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>CNPJ do Destinatário</b><br />
                                                <p>
                                                    <%: Html.TextBox("CNPJDestinatario")%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>Estado e Cidade do Emitente</b><br />
                                                <p>
                                                    <%: Html.DropDownList("EstadoEmitente", new SelectList(estados, "ID", "Nome",Filtros != null && Filtros.CidadeEmitenteID != null && Filtros.CidadeEmitenteID > 0 ? CidadeEmitSelecionado.Estado.ID.ToString() : ""),"Selecione")%>
                                                    <select id="CidadeEmitenteID" name="CidadeEmitenteID">
                                                        <option value="">Selecione</option>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>Estado e Cidade do Destinatário</b><br />
                                                <p>
                                                    <%: Html.DropDownList("EstadoDestinatario", new SelectList(estados, "ID", "Nome", Filtros != null && Filtros.CidadeDestinatarioID != null && Filtros.CidadeDestinatarioID > 0 ? CidadeDestSelecionado.Estado.ID.ToString() : "" ), "Selecione")%>
                                                    <select id="CidadeDestinatarioID" name="CidadeDestinatarioID">
                                                        <option value="">Selecione</option>
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>Data de Início</b><br />
                                                <p>
                                                    <%: Html.TextBox("DataInicio")%>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>Data de Fim</b><br />
                                                <p>
                                                    <%: Html.TextBox("DataFim")%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>Número NF-e</b><br />
                                                <p>
                                                    <%: Html.TextBox("Codigo")%>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>Tipo da Nota</b><br />
                                                <p>
                                                    <%: Html.DropDownList("TipoID", new SelectList(tipo, "ID", "Rotulo", Filtros != null && Filtros.TipoID > 0 ? TipoSelecionado.ToString() : ""), "Selecione")%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>Status</b><br />
                                                <p>
                                                    <%: Html.DropDownList("StatusID", new SelectList(status, "ID", "Rotulo"), "Selecione")%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>Foi Baixada?</b><br />
                                                <p>
                                                    <%: Html.RadioButton("FoiBaixada", 1, false, new { @class = "radio" })%>
                                                    Sim
                                                    <%: Html.RadioButton("FoiBaixada", 0, false, new { @class = "radio" })%>
                                                    Não
                                                    <%: Html.RadioButton("FoiBaixada", 2, true, new { @class = "radio" })%>
                                                    Indiferente
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <p>
                                                    <button id="enviar" class="button green" type="submit" value="Save">
                                                        <span>Filtrar</span>
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                        <% } %>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br />
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Notas Fiscais</h3>
                        <ul class="tabs">
                            <%-- <li class="active" >
								<%: Html.ActionLink("Excel", "ListExcel", new { empresaID = Model.}) %></li>   --%>
                            <% if (Model != null && Model.Count() > 0)
                               { %>
                            <li id="enviarPdf" class="active" value="Save"><a href="#">
                                <img alt="Versão em PDF" width="14px" src="/Content/images/function-icons/pdf_ico.jpg" />
                                Obter PDF </a></li>
                            <% } %>
                            <% if ((bool)ViewData["isGerenteNotas"])
                               { %>
                            <li id="downloadSelected" class="active"><a href="javascript:void(0);" id="lnkDownloadSelected">
                                Fazer Download das Selecionadas</a> </li>
                            <% } %>
                            <% if (Model != null && Model.Count() > 0)
                               { %>
                            <li id="enviarXls" class="active" value="Save"><a href="#">
                                <img alt="Versão em Excel" width="14px" src="/Content/images/function-icons/excel_icon1.png" />
                                Obter Excel </a></li>
                            <li id="enviarHtm" class="active" value="Save"><a href="#">
                                <img alt="Versão para Impressão" width="13px" src="/Content/images/function-icons/paper_48.png" />
                                Obter Versão para Impressão </a></li>
                            <% } %>
                            <li class="active">
                                <%: Html.ActionLink("Adicionar Notas", "Create")%></li>
                        </ul>
                    </div>
                    <div id="notasFiscais" class="bcont">
                        <% if (Model.Any())
                           { %>
                        <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                            <thead>
                                <tr>
                                    <% if ((bool)ViewData["isGerenteNotas"])
                                       { %>
                                    <th>
                                        <input style="width: auto;" type="checkbox" id="chkCheckAll" value="False" />
                                    </th>
                                    <%  } %>
                                    <th>
                                        Número NF-e
                                    </th>
                                    <th>
                                        Nome do Emitente
                                    </th>
                                    <%--<th>
									CNPJ do Emitente
								</th>--%>
                                    <th>
                                        Nome do Destinatário
                                    </th>
                                    <%--<th>
									CNPJ do Destinatário
								</th>--%>
                                    <th>
                                        Data de Criação
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Tipo da Nota
                                    </th>
                                    <%--
								<th>
									Nota de Transporte
								</th>--%>
                                    <th>
                                        Foi Baixada
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <% if (Model != null)
                                   { %>
                                <% using (Html.BeginForm("DownloadSelected", "NotasFiscais", FormMethod.Post, new { id = "downloadSelectedForm", name = "downloadSelectedForm", @target = "_blank" }))//, @target = "download" }))
                                   { %>
                                <% foreach (var item in Model)
                                   { %>
                                <%: Html.Hidden("id", item.ID)%>
                                <tr id="tr-notafiscal-<%: item.ID %>">
                                    <% if ((bool)ViewData["isGerenteNotas"])
                                       { %>
                                    <td class="small">
                                        <input type="checkbox" id="Selecionados" name="Selecionados" value="<%: item.ID %>"
                                            style="width: 20px" />
                                    </td>
                                    <% } %>
                                    <td>
                                        <%: Convert.ToInt32(item.CodigoNFe).ToString(@"000\.000\.000") %>
                                    </td>
                                    <td>
                                        <%: item.Emitente.Nome%>
                                    </td>
                                    <%--<td>
											<%: item.Emitente.CNPJ%>
										</td>--%>
                                    <td>
                                        <%: item.Destinatario.Nome%>
                                    </td>
                                    <%--<td>
											<%: item.Destinatario.CNPJ%>
										</td>--%>
                                    <td>
                                        <%: item.DataEmissao.ToShortDateString()%>
                                    </td>
                                    <td>
                                        <%: item.Status.Rotulo%>
                                    </td>
                                    <td>
                                        <%: item.Tipo!= null? item.Tipo.Rotulo: string.Empty%>
                                    </td>
                                    <%--<td class="small">
											<%: item.isNotaTransporte ? "Sim" : "Não"%>
										</td>--%>
                                    <td class="small">
                                        <%: item.FoiBaixada ? "Sim" : "Não"%>
                                    </td>
                                    <td>
                                        <div id="loading-waiting-<%: item.ID %>" style="display: none">
                                            <img title="Excluir" alt="Loading" style="width: 30px; height: 30px;" src="/Content/images/loading.gif" />
                                        </div>
                                    </td>
                                    <td class="small">
                                        <a class="action" href="#"></a>
                                        <div class="opmenu">
                                            <ul>
                                                <li>
                                                    <%: Html.ActionLink("Enviar por E-mail", "EnviarEmail", new { id = item.ID })%><!-- a href="javascript:void(0);" onclick="EnviarEmail(<%:item.ID %>)">Enviar por E-mail</a --></li>
                                                <li>
                                                    <%: Html.ActionLink("Visualizar Nota Fiscal", "ViewNotaFiscal", new { id = item.ID, empresaID = item.EmpresaID })%><!-- a href="javascript:void(0);" onclick="EnviarEmail(<%:item.ID %>)">Enviar por E-mail</a --></li>
                                                <%  if ((bool)ViewData["isGerenteNotas"])
                                                    {%>
                                                <li><a href="javascript:void(0);" onclick="deleteNota(<%:item.ID %>)">Excluir Nota Fiscal</a></li>
                                                <%
                                                        }%>
                                            </ul>
                                            <div class="clear">
                                            </div>
                                            <div class="foot">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <% } %>
                                <% } %>
                                <% } %>
                            </tbody>
                        </table>
                        <% }
                           else
                           {%>
                        <center>
                            <h3 style="color: #FF0000;">
                                NENHUM REGISTRO FOI ENCONTRADO.</h3>
                        </center>
                        <%} %>
                        <center>
                            <% if (total > 0)
                               { %>
                            Total de Notas cadastradas no sistema: <b>
                                <% Response.Write(total); %></b><br />
                            <br />
                            <% } %>
                            <% if (total > config.QuantidadePorPagina)
                               { %>
                            <div class="paginacao">
                                <div class="numeros">
                                    <% int i = (total / config.QuantidadePorPagina);

                                       for (int j = 1; j <= (((i * config.QuantidadePorPagina) == total) ? i : (i + 1)); j++)
                                       {%>
                                    <a href="<%:Url.Action("List", new { pagina = j, Carregar = true, estdest = filtros.EstadoDestinatario, estemit = filtros.EstadoEmitente, ciddest = filtros.CidadeDestinatarioID, cidemit = filtros.CidadeEmitenteID, cnpjdest = filtros.CNPJDestinatario, cnpjemit = filtros.CNPJEmitente, cod = filtros.Codigo, datafim = filtros.DataFim, dataini = filtros.DataInicio, baixada = filtros.FoiBaixada, serv = filtros.isServico, transp = filtros.isTransporte, dest = filtros.NomeDestinatario, emit = filtros.NomeEmitente, stat = filtros.StatusID, tipo = filtros.TipoID  })%>"
                                        class="<%:j==pagina ? "current" : "" %>">
                                        <%:j%></a>
                                    <% } %>
                                </div>
                            </div>
                            <% } %>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <% 
        var empresaID = ViewData["empresaID"] != null ? (int)ViewData["empresaID"] : 0;
        var Carregar = ViewData["Carregar"] == null ? false : Convert.ToBoolean(ViewData["Carregar"]);
        var CidadeDestSelecionado = ViewData["CidadeDestSelecionado"] != null ? (Domain.Entities.Cidade)ViewData["CidadeDestSelecionado"] : new Domain.Entities.Cidade();
        var CidadeEmitSelecionado = ViewData["CidadeEmitSelecionado"] != null ? (Domain.Entities.Cidade)ViewData["CidadeEmitSelecionado"] : new Domain.Entities.Cidade();

        var EstadoDestSelecionado = ViewData["EstadoDestSelecionado"] != null ? (Int32)ViewData["EstadoDestSelecionado"] : new Int32();
        var EstadoEmitSelecionado = ViewData["EstadoEmitSelecionado"] != null ? (Int32)ViewData["EstadoEmitSelecionado"] : new Int32();    
    %>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#erro").hide();

            $("#anterior").click(function () {
                $("#Carregar").val(true);
            });

            $("#proximo").click(function () {
                $("#Carregar").val(true);
            });

            $("#chkCheckAll").click(function () {
                if ($("#chkCheckAll").val() == 'True') {
                    var chk = '';
                    $("#chkCheckAll").val('False');
                }
                else if ($("#chkCheckAll").val() == 'False') {
                    var chk = 'checked';
                    $("#chkCheckAll").val('True');
                }
                $("#notasFiscais INPUT[type='checkbox']:checkbox").each(function () {
                    if (chk == 'checked') {
                        $(this).attr('checked', true);
                        $("#downloadSelected").show();
                    }
                    else {
                        $(this).attr('checked', false);
                        $("#downloadSelected").hide();
                    }
                });
            });


            $("#lnkDownloadSelected").click(function () {
                var quantidadeNotasSelecionadas = $.grep($("#notasFiscais INPUT[type='checkbox']:checkbox"), function (item) {
                    return item.checked;
                }).length;

                if (quantidadeNotasSelecionadas != 0) {
                    $("#downloadSelectedForm").submit();
                    //document.getElementById('downloadSelectedForm').submit();
                    //$("#form-redirect").submit();
                    // window.location = "/Sistema/Empresas/<%: empresaID %>/NotasFiscais";
                }
                else
                    alert('Não há notas selecionadas!');
            });


            $("#notasFiscais INPUT[type='checkbox']:checkbox").click(function () {
                var quantidadeNotasSelecionadas = $.grep($("#notasFiscais INPUT[type='checkbox']:checkbox"), function (item) {
                    return item.checked;
                }).length;

                if (quantidadeNotasSelecionadas != 0)
                    $("#downloadSelected").show();
                else {
                    $("#downloadSelected").hide();
                }
            });

            $("#enviar").click(function () {
                $("#Carregar").val(true);
            });

            $("#DataInicio").mask("99/99/9999");
            $("#DataFim").mask("99/99/9999");
            $("#CNPJEmitente").mask("99.999.999/9999-99");
            $("#CNPJDestinatario").mask("99.999.999/9999-99");


            $("#downloadSelected").hide();

            if ($("#Carregar").val() == 'True') {
                $("#formularioDanfe").hide();
            }

            $("#headerFiltro").click(function () {
                var visible = $("#formularioDanfe").css("display");

                if (visible == "none")
                    $("#formularioDanfe").show();
                else
                    $("#formularioDanfe").hide();
            });

            $('#enviarXls').click(function () {
                $("#tipoDestino").val('xls');
                $("#form").submit();
                $("#tipoDestino").val('');
            });

            $('#enviarHtm').click(function () {
                $("#tipoDestino").val('htm');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });

            $('#enviarPdf').click(function () {
                $("#tipoDestino").val('pdf');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });

            var estadoDestinatario = '<%: EstadoDestSelecionado %>';

            if (estadoDestinatario != 0) {
                $("#EstadoDestinatario").val(estadoDestinatario);
                $("#EstadoDestinatario").change();
            }

            var estadoEmitente = '<%: EstadoEmitSelecionado %>';

            if (estadoEmitente != 0) {
                $("#EstadoEmitente").val(estadoEmitente);
                $("#EstadoEmitente").change();
            }


            $('#EstadoEmitente').change(function () {
                $.ajaxSetup({ cache: false, async: false });
                var selectedItem = $(this).val();
                if (selectedItem == "" || selectedItem == 0) {
                    //Do nothing or hide...?
                } else {
                    $.post("/Services/Localizacao/GetCidades/" + $("#EstadoEmitente > option:selected").attr("value"), function (data) {
                        var items = "";

                        items += "<option>Selecione</option>";
                        $.each(data, function (i, data) {
                            items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
                        });

                        $("#CidadeEmitenteID").removeAttr('disabled');
                        $("#CidadeEmitenteID").html(items);
                    });
                }
            });

            $('#EstadoDestinatario').change(function () {
                $.ajaxSetup({ cache: false, async: false });
                var selectedItem = $(this).val();
                if (selectedItem == "" || selectedItem == 0) {
                    //Do nothing or hide...?
                } else {
                    $.post("/Services/Localizacao/GetCidades/" + $("#EstadoDestinatario > option:selected").attr("value"), function (data) {
                        var items = "";

                        items += "<option>Selecione</option>";
                        $.each(data, function (i, data) {
                            items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
                        });

                        $("#CidadeDestinatarioID").removeAttr('disabled');
                        $("#CidadeDestinatarioID").html(items);
                    });
                }
            });
            //alert("EstadoEmit '" + $("#EstadoEmitente").val() + "'");
            //alert("CidadeEmitenteID Antes '" + $("#CidadeEmitenteID > option:selected").attr("value") + "'");

            if ($("#EstadoEmitente > option:selected").attr("value") == "")
                $("#CidadeEmitenteID").attr("disabled", true);
            else {
                $("#CidadeEmitenteID").attr("disabled", false);
                $('#EstadoEmitente').change();

                var cidadeEmitSelecionado = '<%: CidadeEmitSelecionado.ID %>';
                //alert("CidadeEmitSelecionado " + "<%: CidadeEmitSelecionado.ID %>");
                $("#CidadeEmitenteID").val(cidadeEmitSelecionado);
            }


            if ($("#EstadoDestinatario > option:selected").attr("value") == "")
                $("#CidadeDestinatarioID").attr("disabled", true);
            else {
                $("#CidadeDestinatarioID").attr("disabled", false);
                $('#EstadoDestinatario').change();

                var cidadeDestSelecionado = '<%: CidadeDestSelecionado.ID %>';
                //alert("CidadeDestSelecionado " + "<%: CidadeDestSelecionado.ID %>");
                $("#CidadeDestinatarioID").val(cidadeDestSelecionado);
            }
        });
    </script>
</asp:Content>
