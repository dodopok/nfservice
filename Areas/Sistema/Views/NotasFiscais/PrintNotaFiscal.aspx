﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.NotaFiscal>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Impresão da Nota Fiscal</title>
</head>
<body>
    <div>
        <form action="http://www.webdanfe.com.br/danfe/GeraDanfe.php" name="one" enctype="multipart/form-data"
        method="post">
        <input type="submit" value="enviar">
        <textarea name="arquivoXml" cols="150" rows="50" style="visibility: hidden">
        <%: Model.ArquivoXml %>
        </textarea>
        </form>
    </div>
    <script type="text/javascript" language="javascript">
        // descomente a linha abaixo para o arquivo ser enviado automaticamente para o WebDANFE
         document.one.submit();
    </script>
</body>
</html>
