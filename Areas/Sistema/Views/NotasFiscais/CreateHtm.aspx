﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Services.Erro>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link media="screen" href="/Content/css/styles.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        html, body
        {
            height: 100%;
        }
        body
        {
            font: 13.34px helvetica,arial,freesans,clean,sans-serif;
            min-height: 100%;
            background-color: #f7f7f7;
        }
    </style>
    <title>Cadastro de Danfe</title>
</head>
<body>
    <div>
        <div class="container_12">
            <div class="grid_12">
                <div class="sb-box">
                    <div class="sb-box-inner content">
                        <div class="header">
                            <h3>
                                Cadastro de Nota Fiscal</h3>
                            <% using (Html.BeginForm("Create", "NotasFiscais", FormMethod.Post, new { id = "form", name = "form", enctype = "multipart/form-data" }))
                               { %>
                        </div>
                        <div class="bcont">
                            <%: Html.Hidden("tipoDestino") %>
                            <% if (Model != null && Model.Count() > 0)
                               { %>
                            <%
                                   Session["Model"] = Model; %>
                            <div id="erro" class="message error">
                                <span class="strong">NOTAS ENVIADAS:
                                    <%: Model.Sum(s=>s.QtdeEnviada) %></span>
                                <br />
                                <span class="strong">NOTAS CADASTRADAS COM SUCESSO:
                                    <%: Model.Sum(s=>s.QtdeCadastrada) %></span>
                                <br />
                                <span class="strong">NOTAS RECUSADAS:
                                    <%: Model.Sum(s=>s.QtdeRecusada) %></span>
                                <br />
                                <br />
                                <span class="strong">MOTIVO DA RECUSA:</span><br />
                                <br />
                                <% foreach (var erro in Model)
                                   {%>
                                <%
                                       if (erro.Rotulo != "NOTAS ENVIADAS:" && erro.Rotulo != "SUCESSO")
                                       {%>
                                <span class="strong">
                                    <%:string.Format("{0} - {1}", erro.Rotulo, erro.Detalhes)%></span>
                                <br />
                                <%
                                       }%>
                                <%
                                   }%>
                                <br />
                            </div>
                            <% } %>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
            <div class="clearingfix">
            </div>
        </div>
    </div>
</body>
</html>
