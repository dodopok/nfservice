<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.NotaFiscal>>" %>
<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Notas-Fiscais.csv\""); %>

Número NF-e;Nome do Emitente;Cidade do Emitente;CNPJ do Emitente;Nome do Destinatário;Cidade do Destinatário;CNPJ do Destinatário;Data de Emissão;Status;Tipo da Nota;Foi Baixada 

<% foreach(var nota in Model)
   { %>
        <%= Convert.ToInt32(nota.CodigoNFe).ToString(@"000\.000\.000") %>;<%= nota.Emitente != null ? nota.Emitente.RazaoSocial : string.Empty %>;<%= nota.Emitente != null && nota.Emitente.Endereco != null && nota.Emitente.Endereco.Cidade != null ? nota.Emitente.Endereco.Cidade.Nome : string.Empty%>;<%= nota.Emitente!= null? nota.Emitente.CNPJ: string.Empty %>;<%= nota.Destinatario.Nome %>;<%= nota.Emitente!= null && nota.Emitente.Endereco != null && nota.Emitente.Endereco.Cidade != null ? nota.Emitente.Endereco.Cidade.Nome : string.Empty %>;<%= nota.Destinatario.CNPJ %>;<%= nota.DataEmissao.ToShortDateString() %>;<%= nota.Status.Rotulo %>;<%= nota.Tipo.Rotulo %>;<%= nota.FoiBaixada ? "BAIXADA" : "NÃO BAIXADA" %>
<% } %>