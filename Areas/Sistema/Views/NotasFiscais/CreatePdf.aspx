﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Services.Erro>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<header runat="server">
<script type="text/javascript" src="/Scripts/jquery.maskMoney.0.2.js"></script>
<title>
</title>
</header>
<body>
    <div class="container_12">
        <div class="grid_12">
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Cadastro de Nota Fiscal</h3>
                    </div>
                    <div class="bcont">
                        <div id="erro" class="message error">
                            <span class="strong">NOTAS ENVIADAS:
                                <%: Model.Sum(s=>s.QtdeEnviada) %></span>
                            <br />
                            <span class="strong">NOTAS CADASTRADAS COM SUCESSO:
                                <%: Model.Sum(s=>s.QtdeCadastrada) %></span>
                            <br />
                            <span class="strong">NOTAS RECUSADAS:
                                <%: Model.Sum(s=>s.QtdeRecusada) %></span>
                            <br />
                            <br />
                            <span class="strong">MOTIVO DA RECUSA:</span><br />
                            <br />
                            <% foreach (var erro in Model)
                               { %>
                            <% if (erro.Rotulo != "NOTAS ENVIADAS:" && erro.Rotulo != "SUCESSO")
                               {%>
                            <span class="strong">
                                <%:string.Format("{0} - {1}", erro.Rotulo, erro.Detalhes)%></span>
                            <br />
                            <%
                                }%>
                            <% } %>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</body>
</html>
