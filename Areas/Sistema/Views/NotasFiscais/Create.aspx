﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master"
    Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Services.Erro>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Notas Fiscais - Inserção
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container_12">
        <div class="grid_12">
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Cadastro de Nota Fiscal</h3>
                        <% using (Html.BeginForm("Create", "NotasFiscais", FormMethod.Post, new { id = "form", name = "form", enctype = "multipart/form-data" }))
                           { %>
                        <ul class="tabs">
                            <% if (Model != null && Model.Count() > 0)
                               { %>
                            <li id="enviarPdf" class="active" value="Save"><a href="#">
                                <img alt="Versão em PDF" width="14px" src="/Content/images/function-icons/pdf_ico.jpg" />
                                Obter PDF </a></li>
                            <li id="enviarXls" class="active" value="Save"><a href="#">
                                <img alt="Versão em Excel" width="14px" src="/Content/images/function-icons/excel_icon1.png" />
                                Obter Excel </a></li>
                            <li id="enviarHtm" class="active" value="Save"><a href="#">
                                <img alt="Versão para Impressão" width="13px" src="/Content/images/function-icons/paper_48.png" />
                                Obter Versão para Impressão </a></li>
                            <% } %>
                            <li class="active">
                                <%: Html.ActionLink("Voltar", "List") %></li>
                        </ul>
                    </div>
                    <div class="bcont">
                        <%: Html.Hidden("tipoDestino") %>
                        <% if (Model != null && Model.Count() > 0)
                           { %>
                        <%
                               Session["Model"] = Model; %>
                        <div id="erro" class="message error">
                            <span class="strong">NOTAS ENVIADAS:
                                <%: Model.Sum(s=>s.QtdeEnviada) %></span>
                            <br />
                            <span class="strong">NOTAS CADASTRADAS COM SUCESSO:
                                <%: Model.Sum(s=>s.QtdeCadastrada) %></span>
                            <br />
                            <span class="strong">NOTAS RECUSADAS:
                                <%: Model.Sum(s=>s.QtdeRecusada) %></span>
                            <br />
                            <br />
                            <span class="strong">MOTIVO DA RECUSA:</span><br />
                            <br />
                            <% foreach (var erro in Model)
                               {%>
                            <%
                                   if (erro.Rotulo != "NOTAS ENVIADAS:" && erro.Rotulo != "SUCESSO")
                            {%>
                            <span class="strong">
                                <%:string.Format("{0} - {1}", erro.Rotulo, erro.Detalhes)%></span>
                            <br />
                            <%
                                }%>
                            <%
                            }%>
                            <br />
                        </div>
                        <% } %>
                        <div>
                            <b>Cadastro de Notas Fiscais - Envie Notas Fiscais no formato XML</b>
                            <p>
                                <input type="file" id="arquivos[]" name="arquivos[]" class="files" multiple="true" />
                            </p>
                            <hr />
                            <p>
                                <button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
                                    <span>Cancelar</span>
                                </button>
                                <button id="enviar" class="button green" type="submit" value="Save">
                                    <span>Enviar Arquivos</span>
                                </button>
                            </p>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#enviarXls').click(function () {
                $("#tipoDestino").val('xls');
                $("#form").submit();
                $("#tipoDestino").val('');
            });

            $('#enviarHtm').click(function () {
                $("#tipoDestino").val('htm');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });

            $('#enviarPdf').click(function () {
                $("#tipoDestino").val('pdf');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });
        });
    </script>
</asp:Content>
