﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master"
    Inherits="System.Web.Mvc.ViewPage<Domain.InvoicesDTO.Reception.ProcessDTO>" %>

<%@ Import Namespace="DomainExtensions.Util.ReceitaFederal" %>
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Visualização da Nota Fiscal
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container_12">
        <div class="grid_12">
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h2>
                            <input type="hidden" id="Selecionados" value='<%: string.Format("{0},",ViewData["id"]) %>' />
                            Nota Fiscal</h2>
                    </div>
                    <table cellspacing="5" cellpadding="5" style="width: 100%; font-size: 85%;">
                        <tbody>
                            <tr>
                                <td style="width: 60%; font-weight: bold;">
                                    Chave de acesso
                                </td>
                                <td style="width: 20%; font-weight: bold;">
                                    Número NF-e
                                </td>
                                <td style="width: 20%; font-weight: bold;">
                                    Versão XML
                                </td>
                            </tr>
                            <tr style="background-color: #F0F0F0;">
                                <td>
                                    <span>
                                        <%:
        string.Format("{0}-{1}-{2}.{3}.{4}/{5}-{6}-{7}-{8}-{9}.{10}.{11}-{12}.{13}.{14}-{15}",
                      Model.Reception.InvoiceInformation.Id.Substring(3, 2),
                      Model.Reception.InvoiceInformation.Id.Substring(5, 4),
                      Model.Reception.InvoiceInformation.Id.Substring(9, 2),
                      Model.Reception.InvoiceInformation.Id.Substring(11, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(14, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(17, 4),
                      Model.Reception.InvoiceInformation.Id.Substring(21, 2),
                      Model.Reception.InvoiceInformation.Id.Substring(23, 2),
                      Model.Reception.InvoiceInformation.Id.Substring(25, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(28, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(31, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(34, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(37, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(40, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(43, 3),
                      Model.Reception.InvoiceInformation.Id.Substring(46, 1))%>
                                    </span>
                                </td>
                                <td>
                                    <%:Model.Reception.InvoiceInformation.Identification.InvoiceNumber.ToString(@"000\.000\.000")%>
                                </td>
                                <td>
                                    <%:Model.Reception.InvoiceInformation.Version%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="tabs">
                        <ul>
                            <li><a href="#NFe">NFe</a></li>
                            <li><a href="#Emitente">Emitente</a></li>
                            <li><a href="#DestRem">Destinatário</a></li>
                            <li><a href="#Prod">Produtos / Serviços </a></li>
                            <li><a href="#Totais">Totais</a></li>
                            <li><a href="#Transporte">Transporte</a></li>
                            <li><a href="#Cobranca">Cobrança</a></li>
                            <li><a href="#Inf">Inf. Adicionais</a></li>
                            <li><a href="#Avulsa">Avulsa</a></li>
                        </ul>
                        <div id="NFe">
                            <fieldset>
                                <legend class="titulo-aba">Dados da NF-e</legend>
                                <table>
                                    <tbody>
                                        <tr class="col-5">
                                            <td>
                                                <label>
                                                    Número</label>
                                                <span class="linha">
                                                    <%:Model.Reception.InvoiceInformation.Identification.InvoiceNumber%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Série</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Identification.Series%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Data de Emissão</label>
                                                <span class="linha">
                                                    <%:
        string.Format(@"{0}/{1}/{2}", Model.Reception.InvoiceInformation.Identification.IssuanceDate.Substring(8, 2),
                      Model.Reception.InvoiceInformation.Identification.IssuanceDate.Substring(5, 2),
                      Model.Reception.InvoiceInformation.Identification.IssuanceDate.Substring(0, 4))%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Data/Hora Saída/Entrada
                                                </label>
                                                <span class="linha">
                                                    <%:!string.IsNullOrEmpty(Model.Reception.InvoiceInformation.Identification.OutInDate)
                          ? string.Format(@"{0}/{1}/{2} às {3}",
                                          Model.Reception.InvoiceInformation.Identification.OutInDate.Substring(8, 2),
                                          Model.Reception.InvoiceInformation.Identification.OutInDate.Substring(5, 2),
                                          Model.Reception.InvoiceInformation.Identification.OutInDate.Substring(0, 4),
                                          Model.Reception.InvoiceInformation.Identification.OutInHour)
                          : string.Empty%>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    Valor&nbsp;Total&nbsp;da&nbsp;Nota&nbsp;Fiscal&nbsp;&nbsp;</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Total.ICMSTot.InvoiceValue%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            <fieldset>
                                <legend class="titulo-aba-interna">Emitente</legend>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="fixo-nfe-cpf-cnpj">
                                                <label>
                                                    CNPJ / CPF</label>
                                                <span class="linha">
                                                    <%:
        !string.IsNullOrEmpty(Model.Reception.InvoiceInformation.Company.CNPJ)
            ? new Cnpj(Model.Reception.InvoiceInformation.Company.CNPJ).ToString()
            : new Cpf(Model.Reception.InvoiceInformation.Company.CPF).ToString()%>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    Nome / Razão Social</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.Name%>
                                                </span>
                                            </td>
                                            <td class="fixo-nfe-iest">
                                                <label>
                                                    Inscrição Estadual</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.StateRegistration%></span>
                                            </td>
                                            <td class="fixo-nfe-uf">
                                                <label>
                                                    UF</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.IssuerAddress.FederalUnit%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            <fieldset>
                                <legend class="titulo-aba-interna">Destinatário</legend>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="fixo-nfe-cpf-cnpj">
                                                <label>
                                                    CNPJ / CPF</label>
                                                <span class="linha">
                                                    <%: Model.Reception.InvoiceInformation.Customer.IssuerAddress.CountryCode == 1058? 
        (!string.IsNullOrEmpty(Model.Reception.InvoiceInformation.Customer.CNPJ)
            ? new Cnpj(Model.Reception.InvoiceInformation.Customer.CNPJ).ToString()
            : new Cpf(Model.Reception.InvoiceInformation.Customer.CPF).ToString()):string.Empty %></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Nome / Razão Social</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Customer.Name%>
                                                </span>
                                            </td>
                                            <td class="fixo-nfe-iest">
                                                <label>
                                                    Inscrição Estadual</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Customer.StateRegistration%></span>
                                            </td>
                                            <td class="fixo-nfe-uf">
                                                <label>
                                                    UF</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Customer.IssuerAddress.FederalUnit%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            <fieldset>
                                <legend class="titulo-aba-interna">Emissão</legend>
                                <table>
                                    <tbody>
                                        <tr class="col-4">
                                            <td>
                                                <label>
                                                    Processo</label>
                                                <span class="linha">
                                                    <%
                                                        var processo =
                                                            Model.Reception.InvoiceInformation.Identification.
                                                                IssuanceProcess == 0
                                                                ? "com aplicativo do contribuinte"
                                                                : Model.Reception.InvoiceInformation.Identification.
                                                                      IssuanceProcess == 1
                                                                      ? "avulsa peloFisco"
                                                                      : Model.Reception.InvoiceInformation.
                                                                            Identification.IssuanceProcess == 2
                                                                            ? "avulsa, pelo contribuinte com seu certificado digital, através do site"
                                                                            : "pelo contribuinte com aplicativo fornecido pelo Fisco";
                                                    %>
                                                    <%:
        string.Format("{0} - {1} ", Model.Reception.InvoiceInformation.Identification.IssuanceProcess, processo)%>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    Versão do Processo</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Identification.VersionProcess%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Tipo de Emissão</label>
                                                <span class="linha">
                                                    <%
                                                        var tipo =
                                                            Model.Reception.InvoiceInformation.Identification.
                                                                IssuanceType == 1
                                                                ? "Normal"
                                                                : Model.Reception.InvoiceInformation.
                                                                      Identification.IssuanceType == 2
                                                                      ? "Contingência FS"
                                                                      : Model.Reception.InvoiceInformation.
                                                                            Identification.IssuanceType == 3
                                                                            ? "Contingência SCAN"
                                                                            : Model.Reception.
                                                                                  InvoiceInformation.
                                                                                  Identification.IssuanceType ==
                                                                              4
                                                                                  ? "Contingência DPEC"
                                                                                  : "Contingência FS-DA";%>
                                                    <%:string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Identification.IssuanceType, tipo)%>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    Finalidade</label>
                                                <span class="linha">
                                                    <%
                                                        var finalidade =
                                                            Model.Reception.InvoiceInformation.Identification.
                                                                IssuancePurpose == 1
                                                                ? "Normal"
                                                                : Model.Reception.InvoiceInformation.
                                                                      Identification.IssuancePurpose == 2
                                                                      ? "Complementar"
                                                                      : "Ajuste";%>
                                                    <%:
        string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Identification.IssuancePurpose, finalidade)%>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Natureza da Operação</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Identification.NatureOperation%>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    Tipo da Operação</label>
                                                <span class="linha">
                                                    <%
                                                        var tpoperacao =
                                                            Model.Reception.InvoiceInformation.Identification.
                                                                TypeOperation == 0
                                                                ? "Entrada"
                                                                : "Saída";%>
                                                    <%:
        string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Identification.TypeOperation, tpoperacao)%>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    Forma de Pagamento</label>
                                                <span class="linha">
                                                    <%
                                                        var pagamento =
                                                            Model.Reception.InvoiceInformation.Identification.
                                                                IdentifyPayment == "0"
                                                                ? " À vista"
                                                                : Model.Reception.InvoiceInformation.
                                                                      Identification.IdentifyPayment == "1"
                                                                      ? "À prazo"
                                                                      : "Outros";%>
                                                    <%:
        string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Identification.IdentifyPayment, pagamento)%>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    <i>Digest</i> Value da NF-e
                                                </label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.Signature.SignatureInformation.Reference.DigestValue%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            <fieldset>
                                <legend class="titulo-aba-interna">Situação Atual: &nbsp;<%:Model.Protocol.ProtocolInformation.Motive.ToUpper()%>
                                    (Ambiente de autorização: produção) </legend>
                                <table>
                                    <tbody>
                                        <tr class="col-3">
                                            <td>
                                                <label>
                                                    Ocorrência</label>
                                            </td>
                                            <td>
                                                <label>
                                                    Protocolo</label>
                                            </td>
                                            <td>
                                                <label>
                                                    Data / Hora</label>
                                            </td>
                                        </tr>
                                        <tr class="col-3">
                                            <td>
                                                <span class="linha">
                                                    <%:
        Model.Protocol.ProtocolInformation.Motive%></span>
                                            </td>
                                            <td>
                                                <span class="linha">
                                                    <%:
        Model.Protocol.ProtocolInformation.ProtocolNumber%></span>
                                            </td>
                                            <td>
                                                <span class="linha">
                                                    <%
                                                        var datahora =
                                                            Model.Protocol.ProtocolInformation.DateTimeReceiving.
                                                                Split('T');%>
                                                    <%:
        string.Format("{0}/{1}/{2} à {3}", datahora[0].Substring(8, 2), datahora[0].Substring(5, 2),
                      datahora[0].Substring(0, 4), datahora[1])%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                        </div>
                        <div id="Emitente">
                            <fieldset>
                                <legend class="titulo-aba">Dados do Emitente</legend>
                                <table>
                                    <tbody>
                                        <tr class="col-2">
                                            <td>
                                                <label>
                                                    Nome / Razão Social</label>
                                                <span class="multiline">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.Name%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Nome Fantasia</label>
                                                <span class="multiline">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.Fantasy%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    CNPJ / CPF</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.CNPJ != null
            ? new Cnpj(Model.Reception.InvoiceInformation.Company.CNPJ).ToString()
            : new Cpf(Model.Reception.InvoiceInformation.Company.CPF).ToString()%>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    Endereço</label>
                                                <span class="multiline">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.IssuerAddress.Street%>
                                                    ,&nbsp;
                                                    <%:Model.Reception.InvoiceInformation.Company.IssuerAddress.Number%>&nbsp; </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Bairro / Distrito</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.IssuerAddress.Neighborhood%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    CEP</label>
                                                <span class="linha">
                                                    <%:
        Convert.ToDouble(Model.Reception.InvoiceInformation.Company.IssuerAddress.PostalCode).ToString("0000-000")%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Município</label>
                                                <span class="linha">
                                                    <%:string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Company.IssuerAddress.CityCode,
                                    Model.Reception.InvoiceInformation.Company.IssuerAddress.City)%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Telefone</label>
                                                <span class="linha">
                                                    <%: 
                                                    string.IsNullOrEmpty(Model.Reception.InvoiceInformation.Company.IssuerAddress.Phone) ? string.Empty : Model.Reception.InvoiceInformation.Company.IssuerAddress.Phone.Length < 10 ? Model.Reception.InvoiceInformation.Company.IssuerAddress.Phone : Convert.ToDouble(Model.Reception.InvoiceInformation.Company.IssuerAddress.Phone).ToString("(00)0000-0000")%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    UF</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.IssuerAddress.FederalUnit%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    País</label>
                                                <span class="linha">
                                                    <%:
        string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Company.IssuerAddress.CountryCode,
                      Model.Reception.InvoiceInformation.Company.IssuerAddress.Country)%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Inscrição Estadual</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.StateRegistration%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Inscrição Estadual do Substituto Tributário</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.StateRegistrationST%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Inscrição Municipal</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.CityRegistration%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Município da Ocorrência do Fato Gerador do ICMS</label>
                                                <span class="linha">
                                                    <%:
        Model.Reception.InvoiceInformation.Company.IssuerAddress.CityCode%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    CNAE Fiscal</label>
                                                <span class="linha"></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Código de Regime Tributário</label>
                                                <span class="linha">
                                                    <%
                                                        var regime = Model.Reception.InvoiceInformation.Company.CRT ==
                                                                     "1"
                                                                         ? "Simples Nacional"
                                                                         : Model.Reception.InvoiceInformation.Company.
                                                                               CRT == "2"
                                                                               ? "Simples Nacional"
                                                                               : "Regime Normal";
                                                    %>
                                                    <%:
        string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Company.CRT, regime)%>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                        </div>
                        <div id="DestRem">
                            <fieldset>
                                <legend class="titulo-aba">Dados do Destinatário</legend>
                                <table>
                                    <tbody>
                                        <tr class="col-2">
                                            <td colspan="2">
                                                <label>
                                                    Nome / Razão Social</label>
                                                <span class="linha">
                                                    <%:Model.Reception.InvoiceInformation.Customer.Name%></span>
                                            </td>
                                        </tr>
                                        <tr class="col-2">
                                            <td>
                                                <label>
                                                    CNPJ / CPF</label>
                                                <span class="linha">
                                                    <%: Model.Reception.InvoiceInformation.Customer.IssuerAddress.CountryCode == 1058?
        (Model.Reception.InvoiceInformation.Customer.CNPJ != null
            ? new Cnpj(Model.Reception.InvoiceInformation.Customer.CNPJ).ToString()
            : new Cpf(Model.Reception.InvoiceInformation.Customer.CPF).ToString()): string.Empty %>
                                                </span>
                                            </td>
                                            <td>
                                                <label>
                                                    Endereço</label>
                                                <span class="linha">
                                                    <%:Model.Reception.InvoiceInformation.Customer.IssuerAddress.Street%>,&nbsp;
                                                    <%:Model.Reception.InvoiceInformation.Customer.IssuerAddress.Number%>&nbsp;
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Bairro / Distrito</label>
                                                <span class="linha">
                                                    <%:Model.Reception.InvoiceInformation.Customer.IssuerAddress.Neighborhood%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    CEP</label>
                                                <span class="linha">
                                                    <%:
        Convert.ToDouble(Model.Reception.InvoiceInformation.Customer.IssuerAddress.PostalCode).ToString("00000-000")%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Município</label>
                                                <span class="linha">
                                                    <%:string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Customer.IssuerAddress.CityCode,
                                    Model.Reception.InvoiceInformation.Customer.IssuerAddress.City)%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Telefone</label>
                                                <span class="linha">
                                                    <%:
                                                    string.IsNullOrEmpty(Model.Reception.InvoiceInformation.Customer.IssuerAddress.Phone) ? string.Empty: Model.Reception.InvoiceInformation.Customer.IssuerAddress.Phone.Length < 10 ? Model.Reception.InvoiceInformation.Customer.IssuerAddress.Phone:  Convert.ToDouble(Model.Reception.InvoiceInformation.Customer.IssuerAddress.Phone).ToString("(00)0000-0000")%></span></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    UF</label>
                                                <span class="linha">
                                                    <%:Model.Reception.InvoiceInformation.Customer.IssuerAddress.FederalUnit%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    País</label>
                                                <span class="linha">
                                                    <%:string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Customer.IssuerAddress.CountryCode,
                                    Model.Reception.InvoiceInformation.Customer.IssuerAddress.Country)%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Inscrição Estadual</label>
                                                <span class="linha">
                                                    <%:Model.Reception.InvoiceInformation.Customer.StateRegistration%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Inscrição SUFRAMA</label>
                                                <span class="linha">
                                                    <%:Model.Reception.InvoiceInformation.Customer.RegistrationSUFRAMA%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label>
                                                    E-mail</label>
                                                <span class="linha">
                                                    <%:Model.Reception.InvoiceInformation.Customer.Email%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                        </div>
                        <div id="Prod" class="Formulario">
                            <fieldset>
                                <legend class="titulo-aba">Dados dos Produtos e Serviços</legend>
                                <div>
                                    <table class="prod-serv-header">
                                        <tbody>
                                            <tr>
                                                <td class="fixo-prod-serv-numero">
                                                    <label>
                                                        Num.</label>
                                                </td>
                                                <td class="fixo-prod-serv-descricao">
                                                    <label>
                                                        Descrição</label>
                                                </td>
                                                <td class="fixo-prod-serv-qtd">
                                                    <label>
                                                        Qtd.</label>
                                                </td>
                                                <td class="fixo-prod-serv-uc">
                                                    <label>
                                                        Unidade Comercial</label>
                                                </td>
                                                <td class="fixo-prod-serv-vb">
                                                    <label>
                                                        Valor(R$)</label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <% foreach (var produto in Model.Reception.InvoiceInformation.Details)
                                       {
                                    %>
                                    <table class="toggle">
                                        <tbody>
                                            <tr class="highlighted">
                                                <td class="fixo-prod-serv-numero">
                                                    <span class="linha">
                                                        <%:
                                               produto.Item%></span>
                                                </td>
                                                <td class="fixo-prod-serv-descricao">
                                                    <span class="multiline">
                                                        <%:
                                               produto.Product.ProductName%></span>
                                                </td>
                                                <td class="fixo-prod-serv-qtd">
                                                    <span class="linha">
                                                        <%:
                                               produto.Product.BusinessQuantity%></span>
                                                </td>
                                                <td class="fixo-prod-serv-uc">
                                                    <span class="linha">
                                                        <%:
                                               produto.Product.BusinessUnit%></span>
                                                </td>
                                                <td class="fixo-prod-serv-vb">
                                                    <span class="linha">
                                                        <%:
                                               produto.Product.ProductValue%></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="toggable">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tbody>
                                                            <tr class="col-4">
                                                                <td colspan="4">
                                                                    <label>
                                                                        Código do Produto</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.ProductCode%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Código NCM</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.NCM%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Gênero</label>
                                                                    <span class="linha"></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Código EX da TIPI</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.EXTIPI%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        CFOP</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.CFOP%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Outras Despesas Acessórias</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.OtherValue%></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Valor do Desconto</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.DiscountValue%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Valor Total do Frete</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.FreightValue%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Valor do Seguro</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.InsuranceValue%></span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table>
                                                        <tbody>
                                                            <tr class="col-12">
                                                                <td colspan="12">
                                                                    <label>
                                                                        Indicador de Composição do Valor Total da NF-e
                                                                    </label>
                                                                    <span class="linha">
                                                                        <%
                                           var indProd = produto.Product.IndicationTotalInvoice == 0
                                                             ? "O valor do item (vProd) compõe o valor total da NF-e (vProd)"
                                                             : "O valor do item (vProd) não compõe o valor total da NF-e (vProd)";%>
                                                                        <%:
                                               string.Format("{0} - {1}", produto.Product.IndicationTotalInvoice,
                                                             indProd)%>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr class="col-3">
                                                                <td colspan="4">
                                                                    <label>
                                                                        Código EAN Comercial</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.BarCode%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Unidade Comercial</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.BusinessUnit%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Quantidade Comercial</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.BusinessQuantity%></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Código EAN Tributável</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.BarCodeTax%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Unidade Tributável</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.TaxUnit%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Quantidade Tributável</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.TaxQuantity%></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Valor unitário de comercialização</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.BusinessValueUnit%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Valor unitário de tributação</label>
                                                                    <span class="linha">
                                                                        <%:
                                               produto.Product.TaxValueUnit%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Número do pedido de compra</label>
                                                                    <span class="linha">
                                                                        <%:produto.Product.Additionals != null
                                                                 ? produto.Product.Additionals.RequestNumber
                                                                 : string.Empty%></span>
                                                                </td>
                                                                <td colspan="4">
                                                                    <label>
                                                                        Item do pedido de compra</label>
                                                                    <span class="linha">
                                                                        <%:produto.Product.Additionals != null
                                                                 ? produto.Product.Additionals.ItemRequest
                                                                 : string.Empty%></span>
                                                                </td>
                                                            </tr>
                                                            <% if (produto.Tax != null)
                                                               {%>
                                                            <tr>
                                                                <td colspan="12">
                                                                    <fieldset class="fieldset-internal">
                                                                        <legend class="titulo-aba-interna">ICMS NORMAL e ST</legend>
                                                                        <%
                                                                   if (produto.Tax.ICMS != null)
                                                                       Html.RenderPartial("ICMS", produto.Tax.ICMS);
                                                                        %>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                            <% if (produto.Tax.IPI != null)
                                                               {%>
                                                            <tr>
                                                                <td colspan="12">
                                                                    <fieldset class="fieldset-internal">
                                                                        <legend class="titulo-aba-interna">IMPOSTO SOBRE PRODUTOS INDUSTRIALIZADOS</legend>
                                                                        <%
                                                                   Html.RenderPartial("IPI", produto.Tax.IPI);
                                                                        %>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                            <%if (produto.Tax.ImportTax != null)
                                                              {%>
                                                            <tr>
                                                                <td colspan="12">
                                                                    <fieldset class="fieldset-internal">
                                                                        <legend class="titulo-aba-interna">IMPOSTO DE IMPORTAÇÃO</legend>
                                                                        <div class="toggable" style="display: none;">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr class="col-3">
                                                                                        <td colspan="3">
                                                                                            <label>
                                                                                                Valor da BC do Imposto</label><br />
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.ImportTax.CalculationBaseValue%></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="col-3">
                                                                                        <td>
                                                                                            <label>
                                                                                                Valor das despesas aduaneiras</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.ImportTax.ExpenditureValue%></span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <label>
                                                                                                Valor do Imposto de Importação</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.ImportTax.CalculationBaseValue%></span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <label>
                                                                                                Valor</label>
                                                                                            <span class="linha">><%: produto.Tax.ImportTax.CalculationBaseValue%></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                            <%
                                                              }%>
                                                            <% if (produto.Tax.PIS != null)
                                                               {%>
                                                            <tr>
                                                                <td colspan="12">
                                                                    <fieldset class="fieldset-internal">
                                                                        <%Html.RenderPartial("PIS", produto.Tax.PIS); %>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                            <tr>
                                                                <td colspan="12">
                                                                    <% if (produto.Tax.PISST != null)
                                                                       {%>
                                                                    <fieldset class="fieldset-internal">
                                                                        <legend class="toggle">PIS ST</legend>
                                                                        <div class="toggable" style="display: none;">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr class="col-3">
                                                                                        <td>
                                                                                            <label>
                                                                                                Base de Cálculo</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.PISST.CalculationBaseValue%></span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <label>
                                                                                                Alíquota (%)</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.PISST.PercentagePis%></span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <label>
                                                                                                Quantidade Vendida</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.PISST.SellQuantity%></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="col-3">
                                                                                        <td>
                                                                                            <label>
                                                                                                Alíquota do PIS (em reais)</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.PISST.PercentagePisReal%></span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <label>
                                                                                                Valor</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.PISST.PisValue%></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </fieldset>
                                                                    <%} %>
                                                                </td>
                                                            </tr>
                                                            <% if (produto.Tax.COFINS != null)
                                                               {%>
                                                            <tr>
                                                                <td colspan="12">
                                                                    <fieldset class="fieldset-internal">
                                                                        <%Html.RenderPartial("COFINS", produto.Tax.COFINS); %>
                                                                    </fieldset>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                            <tr>
                                                                <td colspan="12">
                                                                    <% if (produto.Tax.COFINSST != null)
                                                                       {%>
                                                                    <fieldset class="fieldset-internal">
                                                                        <legend class="toggle">COFINS ST</legend>
                                                                        <div class="toggable" style="display: none;">
                                                                            <table>
                                                                                <tbody>
                                                                                    <tr class="col-3">
                                                                                        <td>
                                                                                            <label>
                                                                                                Base de Cálculo</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.COFINSST.CalculationBaseValue%></span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <label>
                                                                                                Alíquota (%)</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.COFINSST.PercentageConfins%></span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <label>
                                                                                                Quantidade Vendida</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.COFINSST.SellQuantity%></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr class="col-3">
                                                                                        <td>
                                                                                            <label>
                                                                                                Alíquota do PIS (em reais)</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.COFINSST.PercentageCofinsReal%></span>
                                                                                        </td>
                                                                                        <td>
                                                                                            <label>
                                                                                                Valor</label>
                                                                                            <span class="linha">
                                                                                                <%: produto.Tax.COFINSST.ConfinsValue%></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </fieldset>
                                                                    <%} %>
                                                                </td>
                                                            </tr>

                                                            <%} %>
                                                            <tr>
                                                                <td colspan="12">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <%
                                       } %>
                                </div>
                            </fieldset>
                        </div>
                        <div id="Totais">
                            <fieldset>
                                <legend class="titulo-aba">Totais</legend>
                                <br>
                                <fieldset>
                                    <legend class="titulo-aba-interna">ICMS </legend>
                                    <table>
                                        <tbody>
                                            <tr class="col-4">
                                                <td>
                                                    <label>
                                                        Base de Cálculo ICMS</label>
                                                    <span class="linha">
                                                        <%: Model.Reception.InvoiceInformation.Total.ICMSTot.CalculationBaseValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor do ICMS</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.ICMSValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Base de Cálculo ICMS ST</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.CalculationBaseValueST%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor ICMS Substituição</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.ICMSValueST%></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        Valor Total dos Produtos
                                                    </label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.ProductValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor do Frete</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.FreightValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor do Seguro</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.InsuranceValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Outras Despesas Acessórias</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.OtherValue%></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        Valor Total do IPI</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.IPIValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor Total da NFe</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.InvoiceValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor Total dos Descontos</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.DiscountValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor Total do II</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.ImportTaxValue%></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        Valor do PIS</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.PisValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor da COFINS</label>
                                                    <span class="linha">
                                                        <%:Model.Reception.InvoiceInformation.Total.ICMSTot.ConfinsValue%></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </fieldset>
                        </div>
                        <div id="Transporte">
                            <fieldset>
                                <legend class="titulo-aba">Dados do Transporte</legend>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    Modalidade do Frete</label>
                                                <span class="linha">
                                                    <%
                                                        var conta =
                                                            Model.Reception.InvoiceInformation.Transport.ModelShipping ==
                                                            0
                                                                ? "Por conta do Emitente"
                                                                : Model.Reception.InvoiceInformation.Transport.
                                                                      ModelShipping == 1
                                                                      ? "Por conta do Destinatário/Remetente"
                                                                      : Model.Reception.InvoiceInformation.Transport
                                                                            .ModelShipping == 2
                                                                            ? "Por conta de Terceiros"
                                                                            : "Sem frete";%>
                                                    <%:string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Transport.ModelShipping, conta)%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            <%
                                if (Model.Reception.InvoiceInformation.Transport.Carrier != null)
                                {%>
                            <fieldset>
                                <legend class="titulo-aba-interna">Transportador</legend>
                                <table>
                                    <tbody>
                                        <tr class="col-3">
                                            <td>
                                                <label>
                                                    CNPJ / CPF</label>
                                                <span class="linha">
                                                    <%:
                                                                !string.IsNullOrEmpty(Model.Reception.InvoiceInformation.Transport.Carrier.
                                                                    CNPJ) 
                                                                    ? new Cnpj(
                                                                          Model.Reception.InvoiceInformation.Transport
                                                                              .Carrier.CNPJ).ToString()
                                                                    :!string.IsNullOrEmpty(Model.Reception.InvoiceInformation.Transport.
                                                                              Carrier.CPF)? new Cpf(
                                                                          Model.Reception.InvoiceInformation.Transport.
                                                                              Carrier.CPF).ToString():string.Empty%></span>
                                            </td>
                                            <td colspan="2">
                                                <label>
                                                    Razão Social / Nome</label>
                                                <span class="multiline">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Carrier.
                                                                    Name%></span>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="col-3">
                                            <td>
                                                <label>
                                                    Inscrição Estadual</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Carrier.
                                                                    StateRegistration%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Endereço Completo</label>
                                                <span class="multiline">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Carrier.
                                                                    Street%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Município</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Carrier.
                                                                    City%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    UF</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Carrier.
                                                                    FederalUnit%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            <%
                                }%>
                            <%
                                if (
                                    Model.Reception.InvoiceInformation.Transport.VehicleTransp !=
                                    null)
                                {%>
                            <fieldset>
                                <legend class="titulo-aba-interna">Veículo</legend>
                                <table>
                                    <tbody>
                                        <tr class="col-3">
                                            <td>
                                                <label>
                                                    Placa</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.
                                                                    VehicleTransp.Board%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    UF</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.
                                                                    VehicleTransp.FederalUnit%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    RNTC</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.
                                                                    VehicleTransp.NationalRegistry%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            <%
                                }%>
                            <%
                                if (Model.Reception.InvoiceInformation.Transport.Volume != null)
                                {%>
                            <fieldset>
                                <legend class="titulo-aba-interna">Volumes</legend>
                                <table>
                                    <tbody>
                                        <tr class="col-3">
                                            <td>
                                                <label>
                                                    Quantidade</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Volume.
                                                                    VolumeQuantity%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Espécie</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Volume.
                                                                    Species%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Marca dos Volumes</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Volume.Mark%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>
                                                    Numeração</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Volume.
                                                                    VolumeNumber%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Peso Líquido</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Volume.
                                                                    LiquidWeight%></span>
                                            </td>
                                            <td>
                                                <label>
                                                    Peso Bruto</label>
                                                <span class="linha">
                                                    <%:
                                                                Model.Reception.InvoiceInformation.Transport.Volume.
                                                                    BruteWeight%></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            <%
                                }%>
                        </div>
                        <div id="Cobranca">
                            <b>Dados de Cobrança</b>
                            <fieldset>
                                <legend class="titulo-aba">Dados de Cobrança</legend>
                                <br />
                                <%
                                    if (Model.Reception.InvoiceInformation.Encashment != null)
                                    {%>
                                <fieldset>
                                    <legend class="titulo-aba-interna">Fatura</legend>
                                    <%
                                        if (Model.Reception.InvoiceInformation.Encashment.Billing != null)
                                        {%>
                                    <table>
                                        <tbody>
                                            <tr class="col-3">
                                                <td>
                                                    <label>
                                                        Número</label>
                                                    <span class="linha">
                                                        <%:
                                                Model.Reception.InvoiceInformation.Encashment.Billing.
                                                    BillingNumber%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor Original</label>
                                                    <span class="linha">
                                                        <%:
                                                Model.Reception.InvoiceInformation.Encashment.Billing.
                                                    OriginValue%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor do Desconto</label>
                                                    <span class="linha">
                                                        <%:
                                                Model.Reception.InvoiceInformation.Encashment.Billing.
                                                    Discount%></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        Valor Líquido</label>
                                                    <span class="linha">
                                                        <%:
                                                Model.Reception.InvoiceInformation.Encashment.Billing.
                                                    LiquidValue%></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <%
                                        }%>
                                </fieldset>
                                <br />
                                <fieldset>
                                    <legend class="titulo-aba-interna">Duplicatas</legend>
                                    <table>
                                        <tbody>
                                            <tr class="col-3">
                                                <td>
                                                    <label>
                                                        Número</label>
                                                </td>
                                                <td>
                                                    <label>
                                                        Vencimento</label>
                                                </td>
                                                <td>
                                                    <label>
                                                        Valor</label>
                                                </td>
                                            </tr>
                                            <%
                                        foreach (
                                            var duplicate in
                                                Model.Reception.InvoiceInformation.Encashment.
                                                    Duplicate)
                                        {
                                            %>
                                            <tr class="col-3">
                                                <td>
                                                    <span class="linha">
                                                        <%:duplicate.DuplicateNumber%></span>
                                                </td>
                                                <td>
                                                    <span class="linha">
                                                        <%:duplicate.MaturityDate%></span>
                                                </td>
                                                <td>
                                                    <span class="linha">
                                                        <%:duplicate.DuplicateValue%></span>
                                                </td>
                                            </tr>
                                            <%
                                        }%>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <%
                                    }%>
                            </fieldset>
                        </div>
                        <div id="Inf">
                            <fieldset>
                                <legend class="titulo-aba">Informações Adicionais </legend>
                                <div align="right" style="font-size=10px">
                                    XSLT: v1.0.0
                                </div>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    Formato de Impressão DANFE</label>
                                                <span class="linha">
                                                    <%
                                                        var impressao =
                                                            Model.Reception.InvoiceInformation.Identification.
                                                                FormatPrintDanfe ==
                                                            1
                                                                ? "Retrato"
                                                                : "Paisagem";%>
                                                    <%:
        string.Format("{0} - {1}", Model.Reception.InvoiceInformation.Identification.FormatPrintDanfe, impressao)%>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <%
                                    if (
                                        Model.Reception.InvoiceInformation.Export != null)
                                    {%>
                                <fieldset>
                                    <legend class="titulo-aba-interna">Exportação</legend>
                                    <table>
                                        <tbody>
                                            <tr class="col-2">
                                                <td>
                                                    <label>
                                                        Local de Embarque</label>
                                                    <span class="linha">
                                                        <%: 
                                                                        Model.Reception.InvoiceInformation.Export.LocalBoard%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        UF de Embarque</label>
                                                    <span class="linha">
                                                        <%: 
                                                                        Model.Reception.InvoiceInformation.Export.FederalUnitBoarding%></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <%
                                    }%>
                                <%
                                    if (Model.Reception.InvoiceInformation.Buy != null)
                                    { %>
                                <fieldset>
                                    <legend class="titulo-aba-interna">Informações de Compra</legend>
                                    <table>
                                        <tbody>
                                            <tr class="col-3">
                                                <td>
                                                    <label>
                                                        Nota de Empenho</label>
                                                    <span class="linha">
                                                        <%:
                                                                        Model.Reception.InvoiceInformation.Buy.NoteCommit %></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Pedido</label>
                                                    <span class="linha">
                                                        <%: 
                                                                        Model.Reception.InvoiceInformation.Buy.Request%></span>
                                                </td>
                                                <td>
                                                    <label>
                                                        Contrato</label>
                                                    <span class="linha">
                                                        <%: 
                                                                        Model.Reception.InvoiceInformation.Buy.Contract%></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <%
                                    }%>
                                <% if (Model.Reception.InvoiceInformation.AditionalInformation != null)
                                   {%>
                                <fieldset>
                                    <legend class="titulo-aba-interna">Informações Complementares de Interesse do Contribuinte</legend>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <label>
                                                        Descrição</label>
                                                    <span class="multiline">
                                                        <%:  
                                                                            Model.Reception.InvoiceInformation.AditionalInformation.ComplementInformation%></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <%
                                   }%>
                            </fieldset>
                            <% if (Model.Reception.InvoiceInformation.Identification.InvoiceInformationReferences != null)
                               {%>
                            <fieldset>
                                <legend>Documentos Fiscais Referenciados</legend>
                                <br />
                                <fieldset>
                                    <legend class="titulo-aba-interna">Nota Fiscal Eletrônica</legend>
                                    <br />
                                    <table>
                                        <tbody>
                                            <tr class="col-3">
                                                <td colspan="3">
                                                    <label>
                                                        Chave de Acesso</label>
                                                    <span>
                                                        <%: Model.Reception.InvoiceInformation.Identification.InvoiceInformationReferences.ReferenceNFe %></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </fieldset>
                            <%
                               }%>
                        </div>
                        <div id="Avulsa">
                            <fieldset>
                                <legend class="titulo-aba">Dados de Nota Fiscal Avulsa</legend>
                                <table>
                                    <tbody>
                                    </tbody>
                                </table>
                            </fieldset>
                        </div>
                    </div>
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <span class="timestampConsulta">Data/Hora:
                                        <%: DateTime.Now.ToString("dd/MM/yyyy hh:mm") %></span>
                                </td>
                                <td style="width: 50%; text-align: right;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="header">
                                        <ul class="tabs">
                                            <li id="btnImprimir" class="active" value="Imprimir">
                                                <%: Html.ActionLink("Imprimir", "PrintNotaFiscal", null, new {target="_blank"})%>
                                            </li>
                                            <li id="btnDownloadXml" class="active" value="Save">
                                                <%: Html.ActionLink("Download XML", "DownloadXml", null, new { target = "_blank" })%></li>
                                            <li class="active"><a id="btnVoltar" href="#" onclick="javascript:history.go(-1);return false">
                                                Voltar</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Content/css/nfe-vis.css" media="screen" />
    <link href="/Content/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tabs").tabs();
            $('.toggle').click(function () {
                $(this).toggleClass('opened').next('.toggable').toggle();
            }).next('.toggable').css('display', 'none');
        });
    </script>
</asp:Content>
