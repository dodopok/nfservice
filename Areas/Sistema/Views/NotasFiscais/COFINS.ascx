﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Domain.InvoicesDTO.Reception.COFINSDTO>" %>
<script runat="server">
    private static string GetTributacao(string id)
    {
        var tributacao = string.Empty;
        switch (id)
        {
            case "01":
                tributacao = "Operação Tributável (base de cálculo = valor da operação alíquota normal (cumulativo/não cumulativo))";
                break;
            case "02":
                tributacao = "Operação Tributável (base de cálculo = valor da operação (alíquota diferenciada))";
                break;
            case "03":
                tributacao = "Operação Tributável (base de cálculo = quantidade vendida x alíquota por unidadede produto)";
                break;
            case "04":
                tributacao = "Operação Tributável (tributação monofásica (alíquota zero))";
                break;
            case "06":
                tributacao = "Operação Tributável (alíquota zero)";
                break;
            case "07":
                tributacao = "Operação Isenta da Contribuição";
                break;
            case "08":
                tributacao = "Operação Sem Incidência da Contribuição";
                break;
            case "09":
                tributacao = "Operação com Suspensão da Contribuição";
                break;
            case "99":
                tributacao = "Outras Operações";
                break;
        }
        var retorno = string.Format("{0} - {1}", id, tributacao);
        return retorno;
    }
</script>
<legend class="toggle">COFINS</legend>
<% if (Model.COFINSAliq != null)
   {%>
<div class="toggable" style="display: none;">
    <table>
        <tbody>
            <tr class="col-3">
                <td colspan="3">
                    <label>
                        CST</label>
                    <span class="multiline">
                        <%: GetTributacao(Model.COFINSAliq.TaxSituationClass)%></span>
                </td>
                <td>
                    <label>
                        Base de Cálculo</label>
                    <span class="linha">
                        <%: Model.COFINSAliq.CalculationBaseValue%></span>
                </td>
            </tr>
            <tr class="col-3">
                <td>
                    <label>
                        Alíquota (%)</label>
                    <span class="linha">
                        <%: Model.COFINSAliq.PercentageConfins%></span>
                </td>
                <td>
                    <label>
                        Valor</label>
                    <span class="linha">
                        <%: Model.COFINSAliq.ConfinsValue%></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<%}
   else %>
<% if (Model.COFINSQtde != null)
   {%>
<div class="toggable" style="display: none;">
    <table>
        <tbody>
            <tr class="col-3">
                <td colspan="3">
                    <label>
                        CST</label>
                    <span class="multiline">
                        <%: GetTributacao(Model.COFINSQtde.TaxSituationClass)%></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<%}
   else %>
<% if (Model.COFINSNT != null)
   {%>
<div class="toggable" style="display: none;">
    <table>
        <tbody>
            <tr class="col-3">
                <td colspan="3">
                    <label>
                        CST</label>
                    <span class="multiline">
                        <%: GetTributacao(Model.COFINSNT.TaxSituationClass)%></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<%}
   else %>
<% if (Model.COFINSOutr != null)
   {%>
<div class="toggable" style="display: none;">
    <table>
        <tbody>
            <tr class="col-3">
                <td colspan="3">
                    <label>
                        CST</label>
                    <span class="multiline">
                        <%: GetTributacao(Model.COFINSOutr.TaxSituationClass)%></span>
                </td>
                <td>
                    <label>
                        Base de Cálculo</label>
                    <span class="linha">
                        <%: Model.COFINSOutr.CalculationBaseValue%></span>
                </td>
                <td>
                    <label>
                        Alíquota (%)</label>
                    <span class="linha">
                        <%: Model.COFINSOutr.PercentageConfins%></span>
                </td>
            </tr>
            <tr class="col-3">
                <td>
                    <label>
                        Quantidade Vendida</label>
                    <span class="linha">
                        <%: Model.COFINSOutr.SellQuantity%></span>
                </td>
                <td>
                    <label>
                        Alíquota do COFINS (em reais)</label>
                    <span class="linha">
                        <%: Model.COFINSOutr.PercentageCofinsReal%></span>
                </td>
                <td>
                    <label>
                        Valor</label>
                    <span class="linha">
                        <%: Model.COFINSOutr.ConfinsValue%></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<%} %>
