﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Services.Erro>>" %>

<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Notas-Fiscais.csv\""); %>
NOTAS ENVIADAS: <%= Model.Sum(s=>s.QtdeEnviada) %>
NOTAS CADASTRADAS COM SUCESSO: <%= Model.Sum(s=>s.QtdeCadastrada) %>
NOTAS RECUSADAS: <%= Model.Sum(s=>s.QtdeRecusada) %>

MOTIVO DA RECUSA:
<% foreach (var erro in Model)
   { %>
<% if (erro.Rotulo != "NOTAS ENVIADAS:" && erro.Rotulo != "SUCESSO")
  {%>
<%= string.Format("{0} - {1}", erro.Rotulo, erro.Detalhes).Trim() %>
<%
  }%>
<% } %>