﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Domain.InvoicesDTO.Reception.IPIDTO>" %>
<script runat="server">
    private static string GetTributacao(string id)
    {
        var tributacao = string.Empty;
        switch (id)
        {
            case "00":
                tributacao = "Entrada com recuperação de crédito";
                break;
            case "49":
                tributacao = "Outras entradas";
                break;
            case "50":
                tributacao = "Saída tributada";
                break;
            case "99":
                tributacao = "Outras saídas";
                break;

        }
                var retorno = string.Format("{0} - {1}", id, tributacao);
        return retorno;
    }
</script>
<% 
    if(
        Model != null)  {%>
<table align="center" width="98%" class="textoVerdana7">
    <tbody>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Classe de enquadramento do para Cigarros e Bebidas<br />
                </span><span class="linha">
                    <%:
                                           Model.GuideLines %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Código de Enquadramento<br />
                </span><span class="linha">
                    <%:
                    Model.GuideLinesCode %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">CNPJ do Produtor
                    <br />
                </span><span class="linha">
                    <%: 
                    Model.CNPJProduct %></span>
            </td>
        </tr>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Código do selo de controle
                    <br />
                </span><span class="linha">
                    <%: 
                    Model.SealCode %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Quantidade de selo de controle
                    <br />
                </span><span class="linha">
                    <%: 
                    Model.SealQuantity %></span>
            </td>
        </tr>
        <%if(Model.IPITax != null)
{%>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Base de Cálculo
                    <br />
                </span><span class="linha">
                    <%:
                                           Model.IPITax.CalculationBaseValue %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Alíquota<br />
                </span><span class="linha">
                    <%:
                                           Model.IPITax.PercentageIPI %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Valor IPI<br />
                </span><span class="linha">
                    <%:Model.IPITax.IPIValue %></span>
            </td>
        </tr>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">CST<br />
                </span><span class="linha">
                    <%: GetTributacao(Model.IPITax.TaxSituationClass) %>
                </span>
            </td>
        </tr>
        <%
        }%>
        <%if(Model.IPINT != null)
        {%>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Base de Cálculo
                    <br />
                </span><span class="linha">
                    <%:
        Model.IPINT.TaxSituationClass%></span>
            </td>
        </tr>
        <%
        }%>
    </tbody>
</table>
<%} %>