﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Domain.InvoicesDTO.Reception.ICMSDTO>" %>
<script runat="server">
    private static string GetOrigem(int id)
    {
        var origem = string.Empty;
        switch (id)
        {
            case 0:
                origem = "Nacional";
                break;
            case 1:
                origem = "Estrangeira – Importação";
                break;
            case 2:
                origem = "Estrangeira – Adquirida no mercado interno";
                break;
        }
        var retorno = string.Format("{0} - {1}", id, origem);
        return retorno;
    }
    private static string GetTributacao(string id)
    {
        var tributacao = string.Empty;
        switch (id)
        {
            case "00":
                tributacao = "Tributada integralmente";
                break;
            case "10":
                tributacao = "Tributada e com cobrança do ICMS por substituição tributária";
                break;
            case "20":
                tributacao = "Com redução de base de cálculo";
                break;
            case "30":
                tributacao = "Isenta ou não tributada e com cobrança do ICMS por substituição tributária";
                break;
            case "40":
                tributacao = "Isenta";
                break;
            case "41":
                tributacao = "Não tributada";
                break;
            case "50":
                tributacao = "Suspensão";
                break;
            case "51":
                tributacao = "Diferimento";
                break;
            case "60":
                tributacao = "ICMS cobrado anteriormente por substituição tributária";
                break;
            case "70":
                tributacao = "Com redução de base de cálculo e cobrança do ICMS por substituição tributária ICMS por substituição tributária";
                break;
            case "90":
                tributacao = "Outros";
                break;
            case "101":
                tributacao = "Tributada pelo Simples Nacional com permissão de crédito";
                break;
            case "102":
                tributacao = "Tributada pelo Simples Nacional sem permissão de crédito";
                break;
            case "103":
                tributacao = "Isenção do ICMS no Simples Nacional para faixa de receita bruta";
                break;
            case "201":
                tributacao = "Tributada pelo Simples Nacional com permissão de crédito e com cobrança do ICMS por Substituição Tributária";
                break;
            case "202":
                tributacao = "Tributada pelo Simples Nacional sem permissão de crédito e com cobrança do ICMS por Substituição Tributária";
                break;
            case "203":
                tributacao = "Isenção do ICMS nos Simples Nacional para faixa de receita bruta e com cobrança do ICMS por Substituição Tributária";
                break;
            case "300":
                tributacao = "Imune";
                break;
            case "400":
                tributacao = "Não tributada pelo Simples Nacional";
                break;
            case "500":
                tributacao = "ICMS cobrado anteriormente por substituição tributária (substituído) ou por antecipação";
                break;
            case "900":
                tributacao = "Outros";
                break;
        }
        var retorno = string.Format("{0} - {1}", id, tributacao);
        return retorno;

    }

    private static string GetModalidade(int id)
    {
        var modalidade = string.Empty;
        switch (id)
        {
            case 0:
                modalidade = "Margem Valor Agregado (%)";
                break;
            case 1:
                modalidade = "Pauta (Valor)";
                break;
            case 2:
                modalidade = "Preço Tabelado Máx. (valor)";
                break;
            case 3:
                modalidade = "valor da operação";
                break;
        }
        var retorno = string.Format("{0} - {1}", id, modalidade);
        return retorno;
    }
    private static string GetModalidadeST(int id)
    {
        var modalidade = string.Empty;
        switch (id)
        {
            case 0:
                modalidade = "Preço tabelado ou máximo sugerido";
                break;
            case 1:
                modalidade = "Lista Negativa (valor)";
                break;
            case 2:
                modalidade = "Lista Positiva (valor)";
                break;
            case 3:
                modalidade = "Lista Neutra (valor)";
                break;
            case 4:
                modalidade = "Margem Valor Agregado (%)";
                break;
            case 5:
                modalidade = "Pauta (valor)";
                break;

        }
        var retorno = string.Format("{0} - {1}", id, modalidade);
        return retorno;
    }
    
</script>
<% if (Model.ICMS00 != null)
   { %>
<table align="center" width="98%" class="textoVerdana7">
    <tbody>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Origem da Mercadoria<br />
                </span><span class="linha">
                    <%: GetOrigem(Model.ICMS00.Origin) %>
                </span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Tributação do ICMS<br />
                </span><span class="linha">
                    <%: GetTributacao(Model.ICMS00.TaxSituationClass)%>
                </span>
            </td>
            <td valign="top" style="width: 33%;">
                <span class="TextoFundoBrancoNegrito">Modalidade Definição da BC ICMS NORMAL<br />
                </span><span class="linha">
                    <%: GetModalidade(Model.ICMS00.ModeCalculationBase) %></span>
            </td>
        </tr>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Base de Cálculo do ICMS Normal<br />
                </span><span class="linha">
                    <%: Model.ICMS00.CalculationBaseValue %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Alíquota do ICMS Normal
                    <br />
                </span><span class="linha">
                    <%: Model.ICMS00.PercentageAliquotTax %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Valor do ICMS Normal<br />
                </span><span class="linha">
                    <%: Model.ICMS00.ICMSValue %></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMS10 != null)
   { %>
<table align="center" width="98%" class="textoVerdana7">
    <tbody>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Origem da Mercadoria<br />
                </span><span class="linha">
                    <%: GetOrigem(Model.ICMS10.Origin) %>
                </span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Tributação do ICMS
                    <br />
                </span><span class="linha">
                    <%: GetTributacao(Model.ICMS10.TaxSituationClass)%>
                </span>
            </td>
            <td valign="top" style="width: 33%;">
                <span class="TextoFundoBrancoNegrito">Modalidade Definição da BC ICMS NORMAL
                    <br />
                </span><span class="linha">
                    <%: GetModalidade(Model.ICMS10.ModeCalculationBase)%>
                </span>
            </td>
        </tr>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Base de Cálculo do ICMS Normal<br />
                </span><span class="linha">
                    <%: Model.ICMS10.CalculationBaseValue %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Alíquota ICMS Normal
                    <br />
                </span><span class="linha">
                    <%: Model.ICMS10.PercentageAliquotTax %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Valor do ICMS Normal<br />
                </span><span class="linha">
                    <%: Model.ICMS10.ICMSValue %></span>
            </td>
        </tr>
        <tr class="TextoFundoBranco">
        </tr>
        <tr class="TextoFundoBranco">
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Base de Cálculo do ICMS ST<br />
                </span><span class="linha">
                    <%: Model.ICMS10.CalculationBaseValueST %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Alíquota do ICMS ST<br />
                </span><span class="linha">
                    <%: Model.ICMS10.PercentageICMSST %></span>
            </td>
            <td valign="top" style="height: 37px; width: 33%;">
                <span class="TextoFundoBrancoNegrito">Valor do ICMS ST
                    <br />
                </span><span class="linha">
                    <%: Model.ICMS10.ICMSSTValeuST %></span>
            </td>
        </tr>
        <tr class="TextoFundoBranco">
            <td valign="top" style="width: 33%;">
                <span class="TextoFundoBrancoNegrito">Modalidade Definição da BC ICMS ST
                    <br />
                </span><span class="linha">
                    <%: GetModalidadeST(Model.ICMS10.ModeCalculationBase)%>
                </span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMS20 != null)
   { %>
<table>
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha">
                    <%: GetOrigem(Model.ICMS20.Origin) %></span>
            </td>
            <td>
                <label>
                    Tributação do ICMS</label><br />
                <span class="multiline">
                    <%: GetTributacao(Model.ICMS20.TaxSituationClass)%></span>
            </td>
            <td>
                <label>
                    Modalidade Definição da BC do ICMS</label><br />
                <span class="linha">
                    <%: GetModalidade(Model.ICMS20.ModeCalculationBase)%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Base de Cálculo</label><br />
                <span class="linha">
                    <%: Model.ICMS20.CalculationBaseValue %></span>
            </td>
            <td>
                <label>
                    Alíquota</label><br />
                <span class="linha">
                    <%: Model.ICMS20.PercentageAliquotTax %></span>
            </td>
            <td>
                <label>
                    Valor</label><br />
                <span class="linha">
                    <%: Model.ICMS20.ICMSValue %></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Percentual Redução de BC do ICMS Normal</label><br />
                <span class="linha">
                    <%: Model.ICMS20.PercentageReductionBC %></span>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMS30 != null)
   { %>
<table>
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha">
                    <%: GetOrigem(Model.ICMS30.Origin)%></span>
            </td>
            <td>
                <label>
                    Tributação do ICMS</label><br />
                <span class="multiline">
                    <%: GetTributacao(Model.ICMS30.TaxSituationClass)%></span>
            </td>
            <td>
                <label>
                    Modalidade Definição da BC do ICMS ST</label><br />
                <span class="linha">
                    <%: GetModalidadeST(Model.ICMS30.ModeCalculationBaseST)%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Base de Cálculo ST</label><br />
                <span class="linha">
                    <%: Model.ICMS30.CalculationBaseValueST%></span>
            </td>
            <td>
                <label>
                    Alíquota ST</label><br />
                <span class="linha">
                    <%: Model.ICMS30.PercentageAliquotTaxST%></span>
            </td>
            <td>
                <label>
                    Valor ST</label><br />
                <span class="linha">
                    <%: Model.ICMS30.ICMSValueST%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMS40 != null)
   { %>
<table>
    <tbody>
        <tr class="TextoFundoBranco">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha">
                    <%: GetOrigem(Model.ICMS40.Origin)%></span>
            </td>
            <td>
                <label>
                    Tributação do ICMS</label><br />
                <span class="multiline">
                    <%: GetTributacao(Model.ICMS40.TaxSituationClass)%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMS51 != null)
   {%>
<table>
    <tbody>
        <tr class="col-2">
            <td>
                <label>
                    Origem da Mercadoria</label>
                <span class="linha">
                    <%: GetOrigem(Model.ICMS51.Origin)%></span>
            </td>
            <td>
                <label>
                    Tributação do ICMS</label>
                <span class="multiline">
                    <%: GetTributacao(Model.ICMS51.TaxSituationClass)%></span>
            </td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Modalidade de determinação da BC do ICMS</label>
                <span class="linha">
                    <%: GetModalidade(Model.ICMS51.ModeCalculationBase)%></span>
            </td>
            <td>
                <label>
                    Percentual da Redução de BC</label>
                <span class="linha">
                    <%: Model.ICMS51.PercentageReductionBC%>
                </span>
            </td>
            <td>
                <label>
                    Valor da BC do ICMS</label>
                <span class="linha">
                    <%: Model.ICMS51.CalculationBaseValue%>
                </span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Alíquota do imposto</label>
                <span class="linha">
                    <%: Model.ICMS51.PercentageAliquotTax%>
                </span>
            </td>
            <td>
                <label>
                    Valor do ICMS</label>
                <span class="linha">
                    <%: Model.ICMS51.ICMSValue%>
                </span>
            </td>
        </tr>
    </tbody>
</table>
<%
   }%>
<% if (Model.ICMS60 != null)
   {%>
<table>
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label>
                <span class="linha">
                    <%: GetOrigem(Model.ICMS60.Origin)%></span>
            </td>
            <td colspan="2">
                <label>
                    Tributação do ICMS</label>
                <span class="multiline">
                    <%: GetTributacao(Model.ICMS60.TaxSituationClass)%></span>
            </td>
            <td>
                <label>
                    Valor da BC do ICMS ST retido</label>
                <span class="linha">
                    <%: Model.ICMS60.CalculationBaseValueSTSender%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor do ICMS ST retido</label>
                <span class="linha">
                    <%: Model.ICMS60.ICMSValueSender%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMS70 != null)
   {%>
<table align="center" width="98%" class="textoVerdana7">
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha">
                    <%: GetOrigem(Model.ICMS70.Origin) %></span>
            </td>
            <td>
                <label>
                    Tributação do ICMS</label><br />
                <span class="multiline">
                    <%: GetTributacao(Model.ICMS70.TaxSituationClass)%></span>
            </td>
            <td>
                <label>
                    Modalidade Definição da BC do ICMS</label><br />
                <span class="linha">
                    <%: GetModalidade(Model.ICMS70.ModeCalculationBase)%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Percentual da Redução de BC</label><br />
                <span class="linha">
                    <%: Model.ICMS70.PercentageReduction%></span>
            </td>
            <td>
                <label>
                    Base de Cálculo</label><br />
                <span class="linha">
                    <%: Model.ICMS70.CalculationBaseValue%></span>
            </td>
            <td>
                <label>
                    Alíquota</label><br />
                <span class="linha">
                    <%: Model.ICMS70.PercentageAliquotTax%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor</label><br />
                <span class="linha">
                    <%: Model.ICMS70.ICMSValue%></span>
            </td>
            <td>
                <label>
                    Modalidade Definição da BC do ICMS ST</label><br />
                <span class="linha">
                    <%: GetModalidadeST(Model.ICMS70.ModeCalculationBaseST)%></span>
            </td>
            <td>
                <label>
                    Percentual da Redução de BC ST</label><br />
                <span class="linha">
                    <%: Model.ICMS70.PercentageReductionBCST%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Percentual da MVA do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMS70.PercentgeMarginValueST%></span>
            </td>
            <td>
                <label>
                    Valor da BC do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMS70.CalculationBaseValueST%></span>
            </td>
            <td>
                <label>
                    Alíquota do Imposto do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMS70.PercentageAliquotTaxST%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMS70.ICMSValueST%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMS90 != null)
   {%>
<table align="center" width="98%" class="textoVerdana7">
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha">
                    <%: GetOrigem(Model.ICMS90.Origin)%></span>
            </td>
            <td>
                <label>
                    Tributação do ICMS</label><br />
                <span class="multiline">
                    <%: GetTributacao(Model.ICMS90.TaxSituationClass)%></span>
            </td>
            <td>
                <label>
                    Modalidade Definição da BC do ICMS</label><br />
                <span class="linha">
                    <%: GetModalidade(Model.ICMS90.ModeCalculationBase)%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Percentual da Redução de BC</label><br />
                <span class="linha">
                    <%: Model.ICMS90.PercentageReduction%></span>
            </td>
            <td>
                <label>
                    Base de Cálculo</label><br />
                <span class="linha">
                    <%: Model.ICMS90.CalculationBaseValue%></span>
            </td>
            <td>
                <label>
                    Alíquota</label><br />
                <span class="linha">
                    <%: Model.ICMS90.PercentageAliquotTax%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor</label><br />
                <span class="linha">
                    <%: Model.ICMS90.ICMSValue%></span>
            </td>
            <td>
                <label>
                    Modalidade Definição da BC do ICMS ST</label><br />
                <span class="linha">
                    <%: GetModalidadeST(Model.ICMS90.ModeCalculationBaseST)%></span>
            </td>
            <td>
                <label>
                    Percentual da Redução de BC ST</label><br />
                <span class="linha">
                    <%: Model.ICMS90.PercentageReductionBCST%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Percentual da MVA do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMS90.PercentgeMarginValueST%></span>
            </td>
            <td>
                <label>
                    Valor da BC do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMS90.CalculationBaseValueST%></span>
            </td>
            <td>
                <label>
                    Alíquota do Imposto do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMS90.PercentageAliquotTaxST%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMS90.ICMSValueST%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMSSN101 != null)
   {%>
<table>
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha">
                    <%: GetOrigem(Model.ICMSSN101.Origin)%></span>
            </td>
            <td>
                <label>
                    Código de Situação da Operação - Simples Nacional</label><br />
                <span class="linha">
                    <%: GetTributacao(Model.ICMSSN101.CSOSN)%>
                </span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Alíquota aplicável de cálculo do crédito
                </label>
                <span class="linha">
                    <%: Model.ICMSSN101.PercentageCreditSN%></span>
            </td>
            <td>
                <label>
                    Valor de crédito do ICMS</label>
                <span class="linha">
                    <%: Model.ICMSSN101.CreditICMSSNValue%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMSSN102 != null)
   {%>
<table align="center" width="98%" class="textoVerdana7">
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha">
                    <%: GetOrigem(Model.ICMSSN102.Origin)%></span>
            </td>
            <td>
                <label>
                    Tributação do ICMS</label><br />
                <span class="multiline">
                    <%: GetTributacao(Model.ICMSSN102.CSOSN)%></span>
            </td>
            <td>
                <label>
                    Modalidade Definição da BC do ICMS ST</label><br />
                <span class="linha">
                    <%: GetModalidadeST(Model.ICMSSN102.ModeCalculationBaseST)%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Percentual da MVA do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMSSN102.PercentgeMarginValueST%></span>
            </td>
            <td>
                <label>
                    Percentual da Redução de BC ST</label><br />
                <span class="linha">
                    <%: Model.ICMSSN102.PercentageReductionBCST%></span>
            </td>
            <td>
                <label>
                    Valor da BC do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMSSN102.CalculationBaseValueST%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Alíquota do Imposto do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMSSN102.PercentageAliquotTaxST%></span>
            </td>
            <td>
                <label>
                    Valor do ICMS ST</label><br />
                <span class="linha">
                    <%: Model.ICMSSN102.ICMSValueST%></span>
            </td>
            <td>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Alíquota aplicável de cálculo do crédito
                </label>
                <span class="linha">
                    <%: Model.ICMSSN102.PercentageCreditSN%></span>
            </td>
            <td>
                <label>
                    Valor de crédito do ICMS</label>
                <span class="linha">
                    <%: Model.ICMSSN102.CreditICMSSNValue%></span>
            </td>
            <td>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMSSN201 != null)
   {%>
<table>
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha"><span class="linha">
                    <%: GetOrigem(Model.ICMSSN201.Origin)%></span> </span>
            </td>
            <td>
                <label>
                    Código de Situação da Operação</label><br />
                <span class="multiline">
                    <%: GetTributacao(Model.ICMSSN201.CSOSN)%></span>
            </td>
            <td>
                <label>
                    Modalidade de determinação da BC do ICMS ST</label><br />
                <span class="linha"><span class="multiline">
                    <%: GetModalidadeST(Model.ICMSSN201.ModeCalculationBaseST)%></span> </span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Percentual da margem de valor Adicionado do ICMS ST
                </label>
                <br />
                <span class="linha">
                    <%: Model.ICMSSN201.PercentgeMarginValueST%></span>
            </td>
            <td>
                <label>
                    Percentual da Redução de BC do ICMS ST</label>
                <span class="linha">
                    <%: Model.ICMSSN201.PercentageReductionBCST%></span>
            </td>
            <td>
                <label>
                    Valor da BC do ICMS ST</label>
                <span class="linha">
                    <%: Model.ICMSSN201.CalculationBaseValueST%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Alíquota do imposto do ICMS ST
                </label>
                <span class="linha">
                    <%: Model.ICMSSN201.PercentageAliquotTaxST%></span>
            </td>
            <td>
                <label>
                    Valor do ICMS ST
                </label>
                <span class="linha">
                    <%: Model.ICMSSN201.ICMSValueST%></span>
            </td>
            <td>
                <label>
                    Alíquota aplicável de cálculo do crédito</label>
                <span class="linha">
                    <%: Model.ICMSSN201.PercentageCreditSN%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor crédito do ICMS
                </label>
                <span class="linha">
                    <%: Model.ICMSSN201.CreditICMSSNValue%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMSSN500 != null)
   {%>
<table>
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha">
                    <%: GetOrigem(Model.ICMSSN500.Origin)%></span>
            </td>
            <td>
                <label>
                    Código de Situação da Operação</label>
                <span class="linha">
                    <%: GetTributacao(Model.ICMSSN500.CSOSN)%>
                </span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor da BC do ICMS ST retido
                </label>
                <span class="linha">
                    <%: Model.ICMSSN500.CalculationBaseValueSTSender%></span>
            </td>
            <td>
                <label>
                    Valor do ICMS ST retido
                </label>
                <span class="linha">
                    <%: Model.ICMSSN500.ICMSValueSender%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
<% if (Model.ICMSSN900 != null)
   {%>
<table>
    <tbody>
        <tr class="col-3">
            <td>
                <label>
                    Origem da Mercadoria</label><br />
                <span class="linha"><span class="linha">
                    <%: GetOrigem(Model.ICMSSN900.Origin)%></span> </span>
            </td>
            <td>
                <label>
                    Código de Situação da Operação</label><br />
                <span class="multiline">
                    <%: GetTributacao(Model.ICMSSN900.CSOSN)%></span>
            </td>
            <td>
                <label>
                    Modalidade de determinação da BC do ICMS ST</label><br />
                <span class="linha"><span class="multiline">
                    <%: GetModalidade(Model.ICMSSN900.ModeCalculationBaseST)%></span> </span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor da BC do ICMS</label>
                <span class="linha">
                    <%: Model.ICMSSN900.CalculationBaseValue%></span>
            </td>
            <td>
                <label>
                    Percentual da Redução de BC
                </label>
                <span class="linha">
                    <%: Model.ICMSSN900.PercentageReduction%></span>
            </td>
            <td>
                <label>
                    Alíquota do imposto
                </label>
                <br />
                <span class="linha">
                    <%: Model.ICMSSN900.PercentageAliquotTax%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor do ICMS
                </label>
                <br />
                <span class="linha">
                    <%: Model.ICMSSN900.ICMSValue%></span>
            </td>
            <td>
                <label>
                    Modalidade de determinação da BC do ICMS ST</label><br />
                <span class="linha"><span class="multiline">
                    <%: GetModalidadeST(Model.ICMSSN900.ModeCalculationBaseST)%></span> </span>
            </td>
            <td>
                <label>
                    Percentual da margem de valor Adicionado do ICMS ST
                </label>
                <br />
                <span class="linha">
                    <%: Model.ICMSSN900.PercentgeMarginValueST%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Percentual da Redução de BC do ICMS ST</label>
                <span class="linha">
                    <%: Model.ICMSSN900.PercentageReductionBCST%></span>
            </td>
            <td>
                <label>
                    Valor da BC do ICMS ST</label>
                <span class="linha">
                    <%: Model.ICMSSN900.CalculationBaseValueST%></span>
            </td>
            <td>
                <label>
                    Alíquota do imposto do ICMS ST
                </label>
                <span class="linha">
                    <%: Model.ICMSSN900.PercentageAliquotTaxST%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Valor do ICMS ST
                </label>
                <span class="linha">
                    <%: Model.ICMSSN900.ICMSValueST%></span>
            </td>
            <td>
                <label>
                    Valor da BC do ICMS ST retido</label><br />
                <span class="linha">
                    <%: Model.ICMSSN900.CalculationBaseValueSTSender%></span>
            </td>
            <td>
                <label>
                    Valor do ICMS ST retido</label>
                <span class="linha">
                    <%: Model.ICMSSN900.ICMSValueSender%></span>
            </td>
        </tr>
        <tr class="col-3">
            <td>
                <label>
                    Alíquota aplicável de cálculo do crédito</label>
                <span class="linha">
                    <%: Model.ICMSSN900.PercentageCreditSN%></span>
            </td>
            <td>
                <label>
                    Valor crédito do ICMS
                </label>
                <span class="linha">
                    <%: Model.ICMSSN900.CreditICMSSNValue%></span>
            </td>
        </tr>
    </tbody>
</table>
<%} %>
