﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Domain.InvoicesDTO.Reception.PISDTO>" %>
<script runat="server">
    private static string GetTributacao(string id)
    {
        var tributacao = string.Empty;
        switch (id)
        {
            case "01":
                tributacao = "Operação Tributável (base de cálculo = valor da operação alíquota normal (cumulativo/não cumulativo))";
                break;
            case "02":
                tributacao = "Operação Tributável (base de cálculo = valor da operação (alíquota diferenciada))";
                break;
            case "03":
                tributacao = "Operação Tributável (base de cálculo = quantidade vendida x alíquota por unidadede produto)";
                break;
            case "04":
                tributacao = "Operação Tributável (tributação monofásica (alíquota zero))";
                break;
            case "06":
                tributacao = "Operação Tributável (alíquota zero)";
                break;
            case "07":
                tributacao = "Operação Isenta da Contribuição";
                break;
            case "08":
                tributacao = "Operação Sem Incidência da Contribuição";
                break;
            case "09":
                tributacao = "Operação com Suspensão da Contribuição";
                break;
            case "99":
                tributacao = "Outras Operações";
                break;
        }
        var retorno = string.Format("{0} - {1}", id, tributacao);
        return retorno;
    }
</script>
<legend class="toggle">PIS</legend>
<% if (Model.PISAliq != null)
   {%>
<div class="toggable" style="display: none;">
    <table>
        <tbody>
            <tr class="col-3">
                <td colspan="3">
                    <label>
                        CST</label>
                    <span class="multiline">
                        <%: GetTributacao(Model.PISAliq.TaxSituationClass) %></span>
                </td>
                <td>
                    <label>
                        Base de Cálculo</label>
                    <span class="linha">
                        <%: Model.PISAliq.CalculationBaseValue %></span>
                </td>
            </tr>
            <tr class="col-3">
                <td>
                    <label>
                        Alíquota (%)</label>
                    <span class="linha">
                        <%: Model.PISAliq.PercentagePis %></span>
                </td>
                <td>
                    <label>
                        Valor</label>
                    <span class="linha">
                        <%: Model.PISAliq.PisValue %></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<%}
   else %>
<% if (Model.PISQtde != null)
   {%>
<div class="toggable" style="display: none;">
    <table>
        <tbody>
            <tr class="col-3">
                <td colspan="3">
                    <label>
                        CST</label>
                    <span class="multiline">
                        <%: GetTributacao(Model.PISQtde.TaxSituationClass)%></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<%}
   else %>
<% if (Model.PISNT != null)
   {%>
<div class="toggable" style="display: none;">
    <table>
        <tbody>
            <tr class="col-3">
                <td colspan="3">
                    <label>
                        CST</label>
                    <span class="multiline">
                        <%: GetTributacao(Model.PISNT.TaxSituationClass)%></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<%}
   else %>
<% if (Model.PISOutr != null)
   {%>
<div class="toggable" style="display: none;">
    <table>
        <tbody>
            <tr class="col-3">
                <td colspan="3">
                    <label>
                        CST</label>
                    <span class="multiline">
                        <%: GetTributacao(Model.PISOutr.TaxSituationClass)%></span>
                </td>
                <td>
                    <label>
                        Base de Cálculo</label>
                    <span class="linha">
                        <%: Model.PISOutr.CalculationBaseValue%></span>
                </td>
                <td>
                    <label>
                        Alíquota (%)</label>
                    <span class="linha">
                        <%: Model.PISOutr.PercentagePis%></span>
                </td>
            </tr>
            <tr class="col-3">
                <td>
                    <label>
                        Quantidade Vendida</label>
                    <span class="linha">
                        <%: Model.PISOutr.SellQuantity%></span>
                </td>
                <td>
                    <label>
                        Alíquota do PIS (em reais)</label>
                    <span class="linha">
                        <%: Model.PISOutr.PercentagePisReal%></span>
                </td>
                <td>
                    <label>
                        Valor</label>
                    <span class="linha">
                        <%: Model.PISOutr.PisValue%></span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<%} %>
