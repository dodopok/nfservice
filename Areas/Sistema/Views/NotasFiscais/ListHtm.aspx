﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.NotaFiscal>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        html, body
        {
            height: 100%;
        }
        body
        {
            font: 13.34px helvetica,arial,freesans,clean,sans-serif;
            min-height: 100%;
            background-color: #f7f7f7;
        }
        .tbRelatorio
        {
            margin-left: 200px;
        }
        table.tbRelatorio tr th
        {
            font-size: 15px;
            font-weight: bold;
        }
        
        table.infotable
        {
            margin-bottom: 15px;
            text-align: left;
        }
        table.infotable tr td, table.infotable tr th
        {
            border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;
        }
        table.infotable tr td.small, table.infotable tr th.small
        {
            width: 20px;
            text-align: center;
        }
        table.infotable tr td.small input[type="checkbox"], table.infotable tr th.small input[type="checkbox"]
        {
            vertical-align: middle;
        }
        table.infotable thead tr
        {
            background-color: #f5f5f5;
        }
        table.infotable th
        {
            font-weight: bold;
            color: #305B7F;
            text-shadow: 1px 1px 0px rgba(255,255,255,1);
        }
        table.infotable tbody tr.selected
        {
            background-color: #fdffea !important;
        }
        
        .divRelatNotas
        {
            float: left;
            padding: 20px;
            text-align: left;
        }
        
        span
        {
            color: #3B6CCA;
            font-size: 25px;
            font-weight: bold;
            padding: 35px;
            text-decoration: underline;
        }
    </style>
    <title>NOTAS FISCAIS </title>
</head>
<body>
    <%    
        var Filtros = (WebUI.Areas.Sistema.ViewModels.FiltroNotasFiscais)ViewData["Filtros"];
        var CidadeDestSelecionado = (Domain.Entities.Cidade)ViewData["CidadeDestSelecionado"];
        var CidadeEmitSelecionado = (Domain.Entities.Cidade)ViewData["CidadeEmitSelecionado"];
        var StatusSelecionado = (Domain.Entities.StatusNotaFiscal)ViewData["StatusSelecionado"];
        var TipoSelecionado = ViewData["TipoSelecionado"];
        var isTransporte = ViewData["isTransporte"];
        var isServico = ViewData["isServico"];
        var FoiBaixada = ViewData["FoiBaixada"];
    %>
    <div>
        <center>
            <b>NOTAS FISCAIS</b></center>
        <br />
        <center>
            <b>Total de Notas Cadastradas: </b>
            <%: Model.Count() %>
        </center>
        <br />
        <br />
        <span>Filtros </span>
        <center>
            <div class="divRelatNotas">
                <b>Codigo :</b>
                <%: Filtros.Codigo == null ? "Qualquer" : Filtros.Codigo%>
                <br />
                <br />
                <b>Nome do Emitente: </b>
                <%: Filtros.NomeEmitente == null ? "Qualquer" : Filtros.NomeEmitente%>
                <br />
                <br />
                <b>Nome do Destinatário:</b>
                <%: Filtros.NomeDestinatario == null ? "Qualquer" : Filtros.NomeDestinatario%>
                <br />
                <br />
                <b>Cidade do Emitente: </b>
                <%: CidadeEmitSelecionado == null ? "Qualquer" : CidadeEmitSelecionado.Nome %>
                <br />
                <br />
                <b>Cidade do Detinatário: </b>
                <%: CidadeDestSelecionado == null ? "Qualquer" : CidadeDestSelecionado.Nome %>
                <br />
                <br />
                <b>CNPJ do Emitente: </b>
                <%: Filtros.CNPJEmitente == null ? "Qualquer" : Filtros.CNPJEmitente%>
                <br />
                <br />
                <b>CNPJ do Destinatário: </b>
                <%: Filtros.CNPJDestinatario == null ? "Qualquer" : Filtros.CNPJDestinatario%>
            </div>
            <div class="divRelatNotas">
                <b>Status: </b>
                <%: StatusSelecionado == null ? "Qualquer" : StatusSelecionado.Rotulo %>
                <br />
                <br />
                <b>Data Início: </b>
                <%: (DateTime)Filtros.DataInicio == DateTime.MinValue ? "Qualquer" : ((DateTime)Filtros.DataInicio).ToShortDateString()%>
                <br />
                <br />
                <b>Data Fim: </b>
                <%: (DateTime)Filtros.DataFim == DateTime.MinValue ? "Qualquer" : ((DateTime)Filtros.DataFim).ToShortDateString()%>
                <br />
                <br />
                <b>Tipo de Nota: </b>
                <%:  !Filtros.TipoID.HasValue ? "Qualquer" : ((Domain.Entities.TipoEnum)Filtros.TipoID).ToString() %>
                <br />
                <br />
                <b>Foi Baixada: </b>
                <%: (int)Filtros.FoiBaixada == 0 ? "Não" : (int)Filtros.FoiBaixada == 1 ? "Sim" : (int)Filtros.FoiBaixada == 2 ? "Indiferente" : Filtros.FoiBaixada.ToString()%>
            </div>
        </center>
        <br />
        <br />
        <br />
        <center style="clear: both;">
            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                <thead>
                    <tr>
                        <th>
                            Código
                        </th>
                        <th>
                            Nome do Emitente
                        </th>
                        <th>
                            Cidade do Emitente
                        </th>
                        <th>
                            Cidade do Destinatário
                        </th>
                        <th>
                            Data de Emissão
                        </th>
                        <th>
                            CNPJ do Emitente
                        </th>
                        <th>
                            CNPJ do Destinatário
                        </th>
                        <th>
                            Nome do Destinatário
                        </th>
                        <th>
                            Status
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% foreach (var item in Model)
                       { %>
                    <tr>
                        <td>
                            <%: Convert.ToInt32(item.CodigoNFe).ToString(@"000\.000\.000")%>
                        </td>
                        <td>
                            <%: item.Emitente.Nome %>
                        </td>
                        <td>
                            <%: item.Destinatario.Endereco.Cidade == null ? string.Empty : item.Destinatario.Endereco.Cidade.Nome%><%--cidadeEmitente--%>
                        </td>
                        <td>
                            <%: item.Destinatario.Endereco.CEP %>
                        </td>
                        <td>
                            <%: item.DataEmissao.ToShortDateString() %>
                        </td>
                        <td>
                            <%: item.Emitente.CNPJ %>
                        </td>
                        <td>
                            <%: item.Destinatario.CNPJ %>
                        </td>
                        <td>
                            <%: item.Destinatario.Nome %>
                        </td>
                        <td>
                            <%: item.Status.Rotulo %>
                        </td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>
