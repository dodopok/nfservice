﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.GerenteNotas>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Gerentes de Notas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%
    var empresaID = (int)ViewData["EmpresaID"];
%>
<div class="container_12">
		<div class="grid_12">
			<% if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString()) { %>
				<div id="sucesso" class="message success">
					<span class="strong">SUCESSO!</span> Registro salvo com sucesso!
				</div>
			<% } %>
			<% else %>

            <% if (Request.QueryString["email"] == MvcExtensions.Controllers.Message.Sucess.ToString()) { %>
				<div id="email-sucesso" class="message success">
					<span class="strong">SUCESSO!</span> Email enviado com sucesso!
				</div>
			<% } %>
            <% else %>
            <% 
                   if (Request.QueryString["excluir"] == MvcExtensions.Controllers.Message.Sucess.ToString())
                   {%>
				<div id="excluir-sucesso" class="message success">
					<span class="strong">SUCESSO!</span> Registro excluído com sucesso!
				</div>
            <%
                   }%>
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Gerentes de Notas</h3>
						<ul class="tabs">
							<li class="active">
								<%: Html.ActionLink("Novo", "Create") %></li>
						</ul>
					</div>
					<div class="bcont">
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
									<th>
										Nome
									</th>
									<th style="width:115px;">
										<center>CPF</center>
									</th>
									<th>
										Email
									</th>
									<th style="width:100px;">
										<center>Telefone</center>
									</th>
									<th style="width:100px;">
										<center>Celular</center>
									</th>
									<th style="width:80px;">
										<center>Bloqueado</center>
									</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<% foreach (var item in Model) { %>
								<tr>
									<td>
										<%: item.Nome %>
									</td>
									<td>
										<center><%: item.CPF %></center>
									</td>
									<td>
										<%: item.Email%>
									</td>
									<td>
										<center><%: item.Telefone %></center>
									</td>
									<td>
										<center><%: item.Celular %></center>
									</td>
									<td>
										<% if (item.isBloqueadoParaEmpresa(empresaID))
                                        { %>
                                        <center><a href = "<%: Url.Action("Desbloquear", "GerentesNotas", new { id = item.ID })%>" title = "Bloqueado"><img src="/Content/images/onebit-icons/37.png" width="30px" alt="Bloqueado" title="Bloqueado" /></a></center>
                                        <% }
                                         else
                                         { %>
                                        <center><a href = "<%: Url.Action("Bloquear", "GerentesNotas", new { id = item.ID })%>" title = "Não Bloqueado"><img src="/Content/images/onebit-icons/39.png" width="30px" alt="Não Bloqueado" title="Não Bloqueado" /></a></center>
                                        <% } %>

									</td>
									<td class="small">
										<a class="action" href="#"></a>
										<div class="opmenu">
                                            <ul>
												<li><%: Html.ActionLink("Histórico", "Historico", new { id = item.ID }) %></li>
											</ul>
											<ul>
												<li><%: Html.ActionLink("Enviar Email", "SendEmail", new { id = item.ID }) %></li>
											</ul>
											<div class="clear">
											</div>
											<div class="foot">
											</div>
										</div>
									</td>
								</tr>
								<% } %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
