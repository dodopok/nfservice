﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.CadastroEdit>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Cadastro de Gerente de Notas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Cadastro de Gerente de Notas</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
						</ul>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("Create", "GerentesNotas", FormMethod.Post, new { id = "form", name = "form"})) {%>
						
						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>

						<%: Html.HiddenFor(model => model.ID) %>
						<div>
							<div>
							<p>
								<b>CPF</b><br />
								<%: Html.TextBoxFor(model => model.CPF, new { @class = "inputtext medium" }) %>
								<a href="javascript:void(0)" id="btnProcurar">Procurar</a>
							</p>
							</div>
						</div>
						<div id="dadosRestantes">
							<div class="esquerda">
							<p>
								<b>Nome </b><br />
								<%: Html.TextBoxFor(model => model.Nome, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Nome) %>
							</p>
							</div>
							<div class="direita">
							<p>
								<b>Email</b><br />
								<%: Html.TextBoxFor(model => model.Email, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Email)%>
							</p>
							</div>
							<div class="esquerda" id="divSenha">
							<p>
								<b>Senha</b><br />
								<%: Html.TextBoxFor(model => model.Senha, new { @class = "inputtext medium", @type = "password", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Senha)%>
							</p>
							</div>
							<div class="direita" id="divConfirmaSenha">
							<p>
								<b>Confirmação de Senha</b><br />
								<%: Html.TextBox("ConfirmaSenha", Model !=  null ? Model.Senha : "", new { @class = "inputtext medium", @type = "password" })%>
								<%: Html.ValidationMessageFor(model => model.Senha) %>
							</p>
							</div>
							<div class="esquerda">
							<p>
								<b>Telefone </b><br />
								<%: Html.TextBoxFor(model => model.Telefone, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Telefone)%>
							</p>
							</div>
							<div class="direita">
							<p>
								<b>Celular</b><br />
								<%: Html.TextBoxFor(model => model.Celular, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Celular)%>
							</p>
							</div>
						</div>
						
						<div>
						<p>
							<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
								<span>Cancelar</span>
							</button>
							<button id="enviar" name="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>
						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

<script type="text/javascript">

	$(document).ready(function () {

		$("#dadosRestantes").hide();
		$(".note.loading").hide();
		$("#processando").hide();
		$("#cancelar").show();
		$("#enviar").show();
		$("#erro").hide();

		$("#Telefone").mask("(99)9999-9999");
		$("#CPF").mask("999.999.999-99");
		$("#Celular").mask("(99)9999-9999");

		$("#enviar").hide();

		$("#btnProcurar").click(function () {

			$("#enviar").show();

			$.ajaxSetup({ cache: false, async: false });
			var cpf = $("#CPF").attr("value");
			$.post("/Sistema/GerentesNotas/GetGerenteNotas", { cpf: cpf }, function (data) {
				if (data != null && data != "") {
					$("#ID").val(data.ID);
					$("#Nome").val(data.Nome);
					$("#Email").val(data.Email);
					$("#Senha").val(data.Senha);
					$("#ConfirmaSenha").val(data.ConfirmaSenha);
					$("#Telefone").val(data.Telefone);
					$("#Celular").val(data.Celular);

					$("#Senha").val("123");
					$("#ConfirmaSenha").val("123");
				  
					$("#divSenha").hide();
					$("#divConfirmaSenha").hide();

					$("#Nome").attr('disabled', 'disabled');
					$("#Email").attr('disabled', 'disabled');
					$("#Telefone").attr('disabled', 'disabled');
					$("#Celular").attr('disabled', 'disabled');
				} else {
					$("#ID").val("");
					$("#Nome").val("");
					$("#Email").val("");
					$("#Telefone").val("");
					$("#Celular").val("");

					$("#enviar").show();
					$("#divSenha").show();
					$("#divConfirmaSenha").show();

					$("#Nome").attr('disabled', '');
					$("#Email").attr('disabled', '');
					$("#Telefone").attr('disabled', '');
					$("#Celular").attr('disabled', '');
				}
			});
			$("#dadosRestantes").show();
		});

		$("#form").validate({
			meta: "validate",
			invalidHandler: function (form, validator) {
				$("#erro").show();
				$("#erro").focus();
			},

			submitHandler: function (form) {
				$("#erro").hide();
				$("#cancelar").hide();
				$("#enviar").hide();
				$("#processando").show();

				form.submit();
			},
			rules: {
				Nome: "required",
				Email: {
					required: true,
					email: true
				},
				Senha: "required",
				ConfirmaSenha: {
					equalTo: "#Senha"
				},
				CPF: "required",
				Telefone: "required",
				Celular: "required"
			},
			messages: {
				Nome: "Este campo é obrigatório.",
				Email: {
					required: "Este campo é obrigatório.",
					email: "Formato de e-mail inválido"
				},
				Senha: "Este campo é obrigatório.",
				ConfirmaSenha: {
					equalTo: "As senhas devem ser iguais."
				},
				CPF: "Este campo é obrigatório.",
				Telefone: "Este campo é obrigatório.",
				Celular: "Este campo é obrigatório."
			}
		});
	});
</script>

</asp:Content>
