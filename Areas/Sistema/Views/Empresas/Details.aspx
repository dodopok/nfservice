﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Empresa>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Detalhes da Empresa <%: Model.RazaoSocial %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container_12">
	<div class="grid_12">
        <div class="sb-box">
			<div class="sb-box-inner content">
				<div class="header">
	                <h3>Detalhes da Empresa: <%: Model.NomeFantasia %></h3>
				</div>
                <div class="bcont">
                    <div class="detalhes">
                        <div class="esquerda">
                            <table>
                                <tr>
                                    <td>
                                        <b>Razão Social</b><br />
                                        <%: Model.RazaoSocial %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>CNPJ</b><br />
                                        <%: Model.CNPJ %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Inscrição Municipal</b><br />
                                        <%: Model.InscricaoMunicipal %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Telefone Alternativo</b><br />
                                        <%: Model.TelefoneAlternativo %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Usuário Responsável</b><br />
                                        <%: Model.UsuarioResponsavel.Nome %>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="direita">
                            <table>
                                <tr>
                                    <td>
                                        <b>Nome Fantasia</b><br />
                                        <%: Model.NomeFantasia %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Inscrição Estadual</b><br />
                                        <%: Model.InscricaoEstadual %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Telefone Principal</b><br />
                                        <%: Model.TelefonePrincipal %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Email</b><br />
                                        <%: Model.Email %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Endereço Principal</b><br />
                                        <%: Model.EnderecoPrincipal.Logradouro %>, <%: Model.EnderecoPrincipal.Bairro %>, <%: Model.EnderecoPrincipal.Complemento %><br />
                                        CEP: <%: Model.EnderecoPrincipal.CEP %><br />
                                        <%: Model.EnderecoPrincipal.Cidade.Nome %>, <%: Model.EnderecoPrincipal.Cidade.Estado.Nome %>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
