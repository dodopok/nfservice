﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Empresa>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Empresas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container_12">
		<div class="grid_12">
			<%
				if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString())
				{ %>
					<div id="sucesso" class="message success">
						<span class="strong">SUCESSO!</span> Registro salvo com sucesso!
					</div>
			<% } %>
			<%
				if (Request.QueryString["email"] == MvcExtensions.Controllers.Message.Sucess.ToString())
				{ %>
					<div id="Div1" class="message success">
						<span class="strong">SUCESSO!</span> Email enviado com Sucesso!
					</div>
			<% } %>

			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Empresas</h3>
						<ul class="tabs">
							<li class="active">
								<%: Html.ActionLink("Novo", "Edit") %></li>
						</ul>
					</div>
					<div class="bcont">
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								<th>
									Nome Fantasia
								</th>
								<th>
									CNPJ
								</th>
								<th>
									Telefone
								</th>
								<th>
									Email
								</th>
								<th>
									Endereço
								</th>
								<th>
									Status
								</th>
								<th>
								</th>
								</tr>
							</thead>
							<tbody>
								<% foreach (var item in Model)
								   { %>
								<tr>
									<td>
										<%: Html.ActionLink(item.NomeFantasia, "List", "NotasFiscais", new { empresaID = item.ID }, null)%>
									</td>
									<td>
										<%: item.CNPJ%>
									</td>
									<td>
										<%: item.TelefonePrincipal%>
									</td>
									<td>
										<%: item.Email%>
									</td>
									<td>
										<%: item.EnderecoPrincipal.Logradouro%>
									</td>
									<td>
										<%: item.Status.Rotulo%>
									</td>
									<td class="small">
										<a class="action" href="#"></a>
										<div class="opmenu">
											<ul>
												<li><%: Html.ActionLink("Detalhes", "Details", new { id = item.ID }) %></li>
											</ul>
											<ul>
												<li><%: Html.ActionLink("Enviar E-mail", "EnviarEmail", new { id = item.ID })%><!-- a href="javascript:void(0);" onclick="EnviarEmail(<%:item.ID %>)">Enviar por E-mail</a --></li>
											</ul>
											<div class="clear">
											</div>
											<div class="foot">
											</div>
										</div>
									</td>
								</tr>
								<% } %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

