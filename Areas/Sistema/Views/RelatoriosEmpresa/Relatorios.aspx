﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatórios
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% var empresaSelecionada = (Domain.Entities.Empresa)ViewData["EmpresaSelecionada"]; %>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Relatórios - Menu de Navegação</h3>
					</div>
					<div class="bcont">
						<%: Html.ActionLink("Totalizador", "Totalizador", "RelatoriosEmpresa")%>
						<br />
						<br />
						<%: Html.ActionLink("Notas de Entrada", "PeriodoEntrada", "RelatoriosEmpresa", new { tipoID = (int)Domain.Entities.TipoEnum.Entrada }, null)%>
						<br />
						<br />
						<%: Html.ActionLink("Notas de Saída", "PeriodoSaida", "RelatoriosEmpresa", new { tipoID = (int)Domain.Entities.TipoEnum.Saida }, null)%>
                        <br />
						<br />
						<%: Html.ActionLink("Notas de Reentrada", "PeriodoReentrada", "RelatoriosEmpresa")%>
                        <br />
						<br />
						<%: Html.ActionLink("Notas de Serviço", "PeriodoServico", "RelatoriosEmpresa")%>
                        <br />
						<br />
						<%: Html.ActionLink("Notas de Transporte", "PeriodoTransporte", "RelatoriosEmpresa")%>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>
