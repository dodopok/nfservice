﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioNotasBaixadas>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
   
    <title>
        RELATÓRIOS - NOTAS DE REENTRADA - POR PERÍODO
    </title>
</head>
<body style="font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;">
    <div>
        <center><h2><b>Notas de Reentrada - <%: Model.NomeFantasia %></b></h2></center>
        <center><b>Período: De <%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Model.DataInicio.Month) %>/<%: Model.DataInicio.Year %> a <%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Model.DataFim.Month) %>/<%: Model.DataFim.Year %></b></center>
        <br /><br />
        <center>
            <table style="margin-bottom:15px;text-align:left;" cellspacing="0" cellpadding="0" width="100%">
				<thead style="background-color:#f5f5f5;">
					<tr>
						<td style="border-bottom:1px solid black;line-height:normal;padding:5px 10px;text-align:left;">
							<center>Situação</center>
						</td>   
						<td style="border-bottom:1px solid black;line-height:normal;padding:5px 10px;text-align:left;">
							<center>
                                Ano
                            </center>
						</td>
						<td style="border-bottom:1px solid black;line-height:normal;padding:5px 10px;text-align:left;">
							Mês
						</td>
						<td style="border-bottom:1px solid black;line-height:normal;padding:5px 10px;text-align:left;">
							<center>Ativa</center>
						</td>
						<td style="border-bottom:1px solid black;line-height:normal;padding:5px 10px;text-align:left;">
							<center>Cancelada</center>
						</td>
						<td style="border-bottom:1px solid black;line-height:normal;padding:5px 10px;text-align:left;">
							<center>Denegada</center>
						</td>
						<td style="border-bottom:1px solid black;line-height:normal;padding:5px 10px;text-align:left;">
							<center>Não Verificada</center>
						</td>  
					</tr>
				</thead>
				<tbody>
					<%foreach (var item in Model.linhasRelatorio ) 
						{ %>										
						<tr>
							<td>
								<center><%: item.FoiBaixada.Value ? "Baixada" : "Não Baixada" %></center>
							</td>		
							<td>
								<center><%: item.Ano %></center>
							</td>
							<td>
								<b><%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Mes)%></b>
							</td>
                            <td>
                                <center>
                                    <%: item.QuantidadeAtiva %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: item.QuantidadeCancelada %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: item.QuantidadeDenegada %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: item.QuantidadeNaoVerificada %>
                                </center>
                            </td>
						</tr>
					<% }  %>
						<tr>
                            <td colspan="2">
                                Sub-Total do Período:
                            </td>
                            <td></td>
                            <td>
                                <center>
                                    <%: Model.linhaTotal.QuantidadeAtiva %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: Model.linhaTotal.QuantidadeCancelada %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: Model.linhaTotal.QuantidadeDenegada %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: Model.linhaTotal.QuantidadeNaoVerificada %>
                                </center>
                            </td>
                        </tr>
					<tr>
                        <td colspan="2" align="right">
							Total geral de Notas no período : <%: Model.TotalNotas %> 
						</td>									
					</tr>
				</tbody>
			</table>
        </center>        
    </div>
</body>
</html>