﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioTotalizador>" %>

<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Totalizador.csv\""); %>
Relatório - Totalizador Total de Notas no Sistema -
<%: Model.NomeFantasia %>
Entrada;Saída;Reentrada;Serviço;Transporte
<% var entrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada); %>
<%: entrada != null ? entrada.QuantidadeNotas : 0%>;<% var saida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida); %><%: saida != null ? saida.QuantidadeNotas : 0%>;<% var reentrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Reentrada); %><%: reentrada != null ? reentrada.QuantidadeNotas : 0%>;<% var servico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço); %><%: servico != null ? servico.QuantidadeNotas : 0%>;<% var transporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte); %><%: transporte != null ? transporte.QuantidadeNotas : 0%>
Total de Notas do Sistema:
<%:Model.TotalNotas %>