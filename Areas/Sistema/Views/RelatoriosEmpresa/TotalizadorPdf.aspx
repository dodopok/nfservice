﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioTotalizador>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<header runat="server">
<script type="text/javascript" src="/Scripts/jquery.maskMoney.0.2.js"></script>
<title>
</title>
</header>
<body>
    <div class="container_12">
        <div class="grid_12">
            <%-- <a href="<%: Url.Action("List", "Empresas") %>">Empresas</a> - <a href="<%: Url.Action("List", "NotasFiscais") %>"><%: empresaSelecionada.NomeFantasia %></a> - <a href="<%: Url.Action("Relatorios", "RelatoriosEmpresa") %>">Relatórios</a> - Totalizador
            <br /><br />--%>
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Relatório - Totalizador</h3>
                    </div>
                    <div class="bcont">
                        <center>
                            <p><b>Total de Notas no Sistema -
                                <%: Model.NomeFantasia %></b>
                        </p>
                            </center>
                            <br />
                            <br />
                        <div>
                            <table style="margin-bottom: 15px; text-align: left;" cellspacing="0" cellpadding="0"
                                width="100%">
                                <thead>
                                    <tr>
                                        <td style="border-bottom: 1px solid #DDDDDD; line-height: normal; padding: 5px 10px;
                                            text-align: left; font-weight: bold; color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Entrada
                                        </td>
                                        <td style="border-bottom: 1px solid #DDDDDD; line-height: normal; padding: 5px 10px;
                                            text-align: left; font-weight: bold; color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Saída
                                        </td>
                                        <td style="border-bottom: 1px solid #DDDDDD; line-height: normal; padding: 5px 10px;
                                            text-align: left; font-weight: bold; color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Reentrada
                                        </td>
                                        <td style="border-bottom: 1px solid #DDDDDD; line-height: normal; padding: 5px 10px;
                                            text-align: left; font-weight: bold; color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Serviço
                                        </td>
                                        <td style="border-bottom: 1px solid #DDDDDD; line-height: normal; padding: 5px 10px;
                                            text-align: left; font-weight: bold; color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);">
                                            Transporte
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <% var entrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada); %>
                                            <%: entrada != null ? entrada.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var saida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida); %>
                                            <%: saida != null ? saida.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var reentrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Reentrada); %>
                                            <%: reentrada != null ? reentrada.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var servico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço); %>
                                            <%: servico != null ? servico.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var transporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte); %>
                                            <%: transporte != null ? transporte.QuantidadeNotas : 0%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" align="right">
                                            Total de Notas do Sistema:
                                            <%:Model.TotalNotas %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</body>
</html>
