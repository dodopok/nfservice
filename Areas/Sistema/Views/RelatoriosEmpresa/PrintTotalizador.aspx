﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioTotalizador>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
        <style type="text/css">
            html, body {height:100%;}
            body {font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;}
            .tbRelatorio{margin-left: 200px;}
            table.tbRelatorio tr th{font-size: 15px;font-weight: bold;}
            
            table.infotable {margin-bottom:15px;text-align:left;}
            table.infotable  tr td, table.infotable tr th {border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left;}
            table.infotable  tr td.small, table.infotable tr th.small {width:20px;text-align:center;}
            table.infotable  tr td.small input[type="checkbox"], table.infotable tr th.small  input[type="checkbox"] {vertical-align:middle;}
            table.infotable thead tr {background-color:#f5f5f5;}
            table.infotable th {font-weight:bold;color: #305B7F;text-shadow: 1px 1px 0px rgba(255,255,255,1);}
            table.infotable tbody tr.selected {background-color:#fdffea !important;}
        </style>
    
    <title>Totalizador</title>
</head>
<body>
<% var empresaSelecionada = (Domain.Entities.Empresa)ViewData["EmpresaSelecionada"]; %>
    <div>
        <center><b>TOTAL DE NOTAS DO SISTEMA - <%: empresaSelecionada.NomeFantasia %></b></center>
        <center>
            <table class="infotable" cellspacing="0" cellpadding="0" width="800px">
                <thead>
                    <tr>
                    <th>
                        <center>Notas de Entrada</center>
                    </th>
                    <th>
                        <center>Notas de Saída</center>
                    </th>
                    <th>
                        <center>Notas de Reentrada</center>
                    </th>
                    <th>
                        <center>Notas de Serviço</center>
                    </th>
                    <th>
                        <center>Notas de Transporte</center>
                    </th>
                    <th class="small">
                    </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <% var entrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada); %>
                            <%: entrada != null ? entrada.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var saida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida); %>
                            <%: saida != null ? saida.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var reentrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Reentrada); %>
                            <%: reentrada != null ? reentrada.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var servico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço); %>
                            <%: servico != null ? servico.QuantidadeNotas : 0%>
                        </td>
                        <td>
                            <% var transporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte); %>
                            <%: transporte != null ? transporte.QuantidadeNotas : 0%>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Total de Notas do Sistema: <%:Model.TotalNotas %> </th>
                    </tr>
                </tbody>
        </table>
        </center>
    </div>

</body>
</html>