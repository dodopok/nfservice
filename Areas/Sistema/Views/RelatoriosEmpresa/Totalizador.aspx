﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master"
    Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioTotalizador>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Relatório - Totalizador
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% var empresaSelecionada = (Domain.Entities.Empresa)ViewData["EmpresaSelecionada"]; %>
    <div class="BreadCrumb">
        <%: Html.ActionLink("Relatórios","Relatorios")%>
        / Totalizador
    </div>
    <div class="container_12">
        <div class="grid_12">
            <%-- <a href="<%: Url.Action("List", "Empresas") %>">Empresas</a> - <a href="<%: Url.Action("List", "NotasFiscais") %>"><%: empresaSelecionada.NomeFantasia %></a> - <a href="<%: Url.Action("Relatorios", "RelatoriosEmpresa") %>">Relatórios</a> - Totalizador
            <br /><br />--%>
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Relatório - Totalizador</h3>
                           <% using (Html.BeginForm("PrintTotalizador", "RelatoriosEmpresa", FormMethod.Post, new { id = "form", name = "form", enctype = "multipart/form-data" }))
                           { %>

                        <ul class="tabs">
                            <li id="enviarPdf" class="active" value="Save"><a href="#">
                                <img alt="Versão em PDF" width="14px" src="/Content/images/function-icons/pdf_ico.jpg" />
                                Obter PDF </a></li>
                            <li id="enviarXls" class="active" value="Save"><a href="#">
                                <img alt="Versão em Excel" width="14px" src="/Content/images/function-icons/excel_icon1.png" />
                                Obter Excel </a></li>
                            <li id="enviarHtm" class="active" value="Save"><a href="#">
                                <img alt="Versão para Impressão" width="13px" src="/Content/images/function-icons/paper_48.png" />
                                Obter Versão para Impressão </a></li>
                            <li class="active">
                                <%: Html.ActionLink("Voltar", "Relatorios") %></li>
                        </ul>
                    </div>
                    <div class="bcont">
                        <%: Html.Hidden("tipoDestino") %>
                        <center>
                            <b>Total de Notas no Sistema -
                                <%: empresaSelecionada.NomeFantasia %></b></center>
                        <br />
                        <div>
                            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>
                                            Notas de Entrada
                                        </th>
                                        <th>
                                            Notas de Saída
                                        </th>
                                        <th>
                                            Notas de Reentrada
                                        </th>
                                        <th>
                                            Notas de Serviço
                                        </th>
                                        <th>
                                            Notas de Transporte
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <% var entrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Entrada); %>
                                            <%: entrada != null ? entrada.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var saida = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Saida); %>
                                            <%: saida != null ? saida.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var reentrada = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Reentrada); %>
                                            <%: reentrada != null ? reentrada.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var servico = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Serviço); %>
                                            <%: servico != null ? servico.QuantidadeNotas : 0%>
                                        </td>
                                        <td>
                                            <% var transporte = Model.ItensRelatorio.SingleOrDefault(i => i.Tipo == (int)Domain.Entities.TipoEnum.Transporte); %>
                                            <%: transporte != null ? transporte.QuantidadeNotas : 0%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                            Total de Notas do Sistema:
                                            <%:Model.TotalNotas %>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <p>
                                <button id="enviar" class="button green" type="submit" value="Save">
                                    <span>Gerar Relatório</span>
                                </button>
                                <%--	<span id="processando" class="note loading">Processando...</span>--%>
                            </p>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {


            $("#erro").hide();
            $(".note.loading").hide();

            $("#DataInicio").mask("99/99/9999");
            $("#DataFim").mask("99/99/9999");
            $("#Celular").mask("(99)9999-9999");

            $("#form").validate({
                meta: "validate",
                invalidHandler: function (form, validator) {
                    $("#erro").show();
                    $("#erro").focus();
                },

                submitHandler: function (form) {
                    $("#erro").hide();
                    $("#cancelar").hide();
                    $("#enviar").hide();
                    //                $("#processando").show();
                    form.submit();
                },
                rules: {
                    Nome: "required",
                    Email: {
                        email: true,
                        required: true,
                        JaExisteCadastro: true
                    },
                    Senha: "required",
                    ConfirmaSenha: {
                        equalTo: "#Senha"
                    },
                    CPF: "required",
                    Telefone: "required",
                    EmpresaID: "required"
                },
                messages: {
                    Nome: "Este campo é obrigatório.",
                    Email: {
                        required: "Este campo é obrigatório.",
                        email: "Formato de e-mail inválido.",
                        JaExisteCadastro: "Já existe cadastro para esse email"
                    },
                    Senha: "Este campo é obrigatório.",
                    ConfirmaSenha: {
                        equalTo: "As senhas devem ser iguais."
                    },
                    CPF: "Este campo é obrigatório.",
                    Telefone: "Este campo é obrigatório.",
                    EmpresaID: "Este campo é obrigatório."
                }
            });

            $('#enviarXls').click(function () {
                $("#tipoDestino").val('xls');
                $("#form").submit();
                $("#tipoDestino").val('');
            });

            $('#enviarHtm').click(function () {
                $("#tipoDestino").val('htm');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });

            $('#enviarPdf').click(function () {
                $("#tipoDestino").val('pdf');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });

        });
    </script>
</asp:Content>
