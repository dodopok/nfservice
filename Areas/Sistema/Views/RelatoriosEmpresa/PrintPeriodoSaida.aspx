﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioNotasBaixadas>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
        <style type="text/css">
            html, body {height:100%;}
            body {font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;}
            .tbRelatorio{margin-left: 200px;}
            table.tbRelatorio tr th{font-size: 15px;font-weight: bold;}
            
            table.infotable {margin-bottom:15px;text-align:left;}
            table.infotable  tr td, table.infotable tr th {border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left;}
            table.infotable  tr td.small, table.infotable tr th.small {width:20px;text-align:center;}
            table.infotable  tr td.small input[type="checkbox"], table.infotable tr th.small  input[type="checkbox"] {vertical-align:middle;}
            table.infotable thead tr {background-color:#f5f5f5;}
            table.infotable th {font-weight:bold;color: #305B7F;text-shadow: 1px 1px 0px rgba(255,255,255,1);}
            table.infotable tbody tr.selected {background-color:#fdffea !important;}
        </style>    
    <title>        
        <% var tipo = ViewData["Tipo"] == null ? 1 : 1; %>
        RELATÓRIOS - NOTAS DE SAÍDA - POR PERÍODO
    </title>
</head>
<body>
    <% 
        var dataFim = (DateTime)ViewData["DataFim"];
        var DataInicio = (DateTime)ViewData["DataInicio"];
        var empresaSelecionada = (Domain.Entities.Empresa)ViewData["EmpresaSelecionada"];
        %>
    <div>
        <center><h2><b>Notas de Saída - <%: empresaSelecionada.NomeFantasia %></b></h2></center>
        <center><b>Período: De <%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DataInicio.Month) %>/<%: DataInicio.Year %> a <%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dataFim.Month) %>/<%: dataFim.Year %> </b></center>
        <br /><br />
        <center>
            <table class="infotable" cellspacing="0" cellpadding="0" width="800px">
				<thead>
					<tr>
						<th>
							<center>Situação</center>
						</th>   
						<th>
							<center>
                                Ano
                            </center>
						</th>
						<th>
							Mês
						</th>
						<th>
							<center>Ativa</center>
						</th>
						<th>
							<center>Cancelada</center>
						</th>
						<th>
							<center>Denegada</center>
						</th>
						<th>
							<center>Não Verificada</center>
						</th>  
					</tr>
				</thead>
				<tbody>
					<%foreach (var item in Model.linhasRelatorio ) 
						{ %>										
						<tr>
							<td>
								<center><%: item.FoiBaixada.Value ? "Baixada" : "Não Baixada" %></center>
							</td>		
							<td>
								<center><%: item.Ano %></center>
							</td>
							<td>
								<b><%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Mes)%></b>
							</td>
                            <td>
                                <center>
                                    <%: item.QuantidadeAtiva %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: item.QuantidadeCancelada %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: item.QuantidadeDenegada %>
                                </center>
                            </td>
                            <td>
                                <center>
                                    <%: item.QuantidadeNaoVerificada %>
                                </center>
                            </td>
						</tr>
					<% }  %>
						<tr>
							<th>
                            </th>
                            <th>
                                Sub-Total do Período:
                            </th>
                            <th></th>
                            <th>
                                <center>
                                    <%: Model.linhaTotal.QuantidadeAtiva %>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <%: Model.linhaTotal.QuantidadeCancelada %>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <%: Model.linhaTotal.QuantidadeDenegada %>
                                </center>
                            </th>
                            <th>
                                <center>
                                    <%: Model.linhaTotal.QuantidadeNaoVerificada %>
                                </center>
                            </th>
                        </tr>
					<tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
							Total geral de Notas no período : <%: Model.TotalNotas %> 
						</th>									
					</tr>
				</tbody>
			</table>
        </center>
        
    </div>

</body>
</html>