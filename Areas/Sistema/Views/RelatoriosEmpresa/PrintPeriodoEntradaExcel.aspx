﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioNotasBaixadas>" %>
<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"PeriodoEntrada.csv\""); %>
    <% 
        var dataFim = (DateTime)ViewData["DataFim"];
        var DataInicio = (DateTime)ViewData["DataInicio"];
        var empresaSelecionada = (Domain.Entities.Empresa)ViewData["EmpresaSelecionada"];
        %>
Notas de Entrada - <%: empresaSelecionada.NomeFantasia %>
Período: De <%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DataInicio.Month) %>/<%: DataInicio.Year %> a <%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dataFim.Month) %>/<%: dataFim.Year %>
Situação;Ano;Mês;Ativa;Cancelada;Denegada;Não Verificada
<%foreach (var item in Model.linhasRelatorio ) 
{ %>										
<%: item.FoiBaixada!= null && item.FoiBaixada.Value ? "Baixada" : "Não Baixada" %>;<%: item.Ano %>;<%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Mes)%>;<%: item.QuantidadeAtiva %>;<%: item.QuantidadeCancelada %>;<%: item.QuantidadeDenegada %>;<%: item.QuantidadeNaoVerificada %>
					<% }  %>
<%:string.Empty %>;Sub-Total do Período:;<%:string.Empty %>; <%: Model.linhaTotal.QuantidadeAtiva %>;<%: Model.linhaTotal.QuantidadeCancelada %>;<%: Model.linhaTotal.QuantidadeDenegada %>;<%: Model.linhaTotal.QuantidadeNaoVerificada %>
Total geral de Notas no período :<%:string.Empty %>;<%:string.Empty %>;<%:string.Empty %>;<%:string.Empty %>; <%: Model.TotalNotas %>