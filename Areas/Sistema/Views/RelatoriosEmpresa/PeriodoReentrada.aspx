﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master"
    Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Relatorios.RelatorioNotasBaixadas>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Relatório - Notas de Reentrada
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var dataInicio = (DateTime)ViewData["dataInicio"];
        var dataFim = (DateTime)ViewData["DataFim"];
        var empresaSelecionada = (Domain.Entities.Empresa)ViewData["EmpresaSelecionada"];
    %>
    <div class="BreadCrumb">
        <%: Html.ActionLink("Relatórios","Relatorios")%>
        / Notas de Reentrada - Por Período
    </div>
    <div class="container_12">
        <div class="grid_12">
            <%--<a href="<%: Url.Action("List", "Empresas") %>">Empresas</a> - <a href="<%: Url.Action("List", "NotasFiscais") %>"><%: empresaSelecionada.NomeFantasia %></a> - <a href="<%: Url.Action("Relatorios", "RelatoriosEmpresa") %>">Relatórios</a> - Por período
            <br /><br />--%>
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Relatório - Notas de Reentrada - Por Período</h3>
                        <% using (Html.BeginForm("PeriodoReentrada", "RelatoriosEmpresa", FormMethod.Post, new { id = "form", name = "form" }))
                           { %>
                        <ul class="tabs">
                            <li class="active">
                                <%: Html.ActionLink("Voltar", "Relatorios") %></li>
                        </ul>
                    </div>
                    <div class="bcont">
                        <%: Html.Hidden("Carregar",false)%>
                        <p>
                            <b>Período Inicial </b>
                            <br />
                            <%: Html.TextBox("dataInicio", "", new { @class = "inputtext medium", @maxlength = "128" })%>
                        </p>
                        <p>
                            <b>Período Final </b>
                            <br />
                            <%: Html.TextBox("dataFim", "", new { @class = "inputtext medium", @maxlength = "128" })%>
                        </p>
                        <p>
                            <button id="enviar" class="button green" type="submit" value="Save">
                                <span>Gerar Relatório</span>
                            </button>
                            <span id="processando" class="note loading">Processando...</span>
                        </p>
                        <% } %>
                        <% if (Model != null)
                           { %>
                        <% using (Html.BeginForm("PrintPeriodoReentrada", "RelatoriosEmpresa", FormMethod.Post, new { id = "printform", name = "printform", enctype = "multipart/form-data" }))
                           { %>
                        <div>
                            <%: Html.Hidden("dataInicioPrint", dataInicio)%>
                            <%: Html.Hidden("dataFimPrint", dataFim)%>
                            <%: Html.Hidden("tipoDestino") %>
                            <center>
                                <b>Total de Notas de Reentrada -
                                    <%: empresaSelecionada.NomeFantasia%></b></center>
                            <center>
                                Período: De
                                <%: dataInicio.ToShortDateString()%>
                                a
                                <%: dataFim.ToShortDateString()%></center>
                            <br />
                            <br />
                            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>
                                            <center>
                                                Situação</center>
                                        </th>
                                        <th>
                                            <center>
                                                Ano
                                            </center>
                                        </th>
                                        <th>
                                            Mês
                                        </th>
                                        <th>
                                            <center>
                                                Ativa</center>
                                        </th>
                                        <th>
                                            <center>
                                                Cancelada</center>
                                        </th>
                                        <th>
                                            <center>
                                                Denegada</center>
                                        </th>
                                        <th>
                                            <center>
                                                Não Verificada</center>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%foreach (var item in Model.linhasRelatorio)
                                      { %>
                                    <tr>
                                        <td>
                                            <center>
                                                <%: item.FoiBaixada.Value ? "Baixada" : "Não Baixada"%></center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.Ano%></center>
                                        </td>
                                        <td>
                                            <b>
                                                <%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Mes)%></b>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeAtiva%>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeCancelada%>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeDenegada%>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeNaoVerificada%>
                                            </center>
                                        </td>
                                    </tr>
                                    <% }  %>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                            Sub-Total do Período:
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                            <center>
                                                <%: Model.linhaTotal.QuantidadeAtiva%>
                                            </center>
                                        </th>
                                        <th>
                                            <center>
                                                <%: Model.linhaTotal.QuantidadeCancelada%>
                                            </center>
                                        </th>
                                        <th>
                                            <center>
                                                <%: Model.linhaTotal.QuantidadeDenegada%>
                                            </center>
                                        </th>
                                        <th>
                                            <center>
                                                <%: Model.linhaTotal.QuantidadeNaoVerificada%>
                                            </center>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                        </th>
                                        <th>
                                            Total geral de Notas no período :
                                            <%: Model.TotalNotas%>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <table>
                                <tr>
                                    <td>
                                        <button id="btnPdf" class="button green" type="button" value="Save">
                                            <span>Gerar PDF</span>
                                        </button>
                                    </td>
                                    <td>
                                        <button id="btnXls" class="button green" type="button" value="Save">
                                            <span>Gerar Excel</span>
                                        </button>
                                    </td>
                                    <td>
                                        <button id="btnHtm" class="button green" type="submit" value="Save">
                                            <span>Gerar Impressão</span>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <% } %>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {


            $("#erro").hide();
            $(".note.loading").hide();

            $("#dataInicio").mask("99/99/9999");
            $("#dataFim").mask("99/99/9999");

            $("#dataInicio").blur(function () {
                $("#dataInicioPrint").val($("#dataInicio").val());
            });

            $("#dataFim").blur(function () {
                $("#dataFimPrint").val($("#dataFim").val());
            });

            $("#enviar").click(function () {
                $("#Carregar").val(true);
            });

            $("#form").validate({
                meta: "validate",
                invalidHandler: function (form, validator) {
                    $("#erro").show();
                    $("#erro").focus();
                },

                submitHandler: function (form) {
                    $("#erro").hide();
                    $("#cancelar").hide();
                    $("#enviar").hide();
                    $("#processando").show();
                    form.submit();
                }
            });


            $('#btnXls').click(function () {
                $("#tipoDestino").val('xls');
                $("#printform").submit();
                $("#tipoDestino").val('');
            });

            $('#btnHtm').click(function () {
                $("#tipoDestino").val('htm');
                $("#printform").attr("target", "_blank");
                $("#printform").submit();
                $("#tipoDestino").val('');
            });

            $('#btnPdf').click(function () {
                $("#tipoDestino").val('pdf');
                $("#printform").attr("target", "_blank");
                $("#printform").submit();
                $("#printform").attr("target", "_self");
                $("#tipoDestino").val('');
            });

        });
    </script>
</asp:Content>
