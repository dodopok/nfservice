<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Services.Erro>>" %>

<html>
<head>
<title>Cadastro de Nota Fiscal</title>
<meta http-equiv="content-type" content="text/html; charset=windows-1251" />

<link rel="stylesheet" type="text/css" href="/Content/css/styles.css" media="screen" />

<script type="text/javascript" src="/Scripts/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery.visualize.js"></script>
<script type="text/javascript" src="/scripts/jquery.validate.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="/Scripts/jquery.maskedinput-1.2.2.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.ui.core.js"></script>
<script type="text/javascript" src="/Scripts/custom.js"></script>
<script type="text/javascript" src="/Scripts/jquery.autotab-1.1b.js"></script>
</head>
<body>
	<div id="page-body">
		<div id="wrapper">
			<div id="header">
                <div id="logo"><%: Html.RouteLink("NFService", "Default", new { controller = "Home" })%></div>
			</div>

			
<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Cadastro de Nota Fiscal</h3>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("Danfe", "RecebimentoNotaFiscal", new { danfe = this.RouteData.Values["Danfe"] }, FormMethod.Post, new { enctype = "multipart/form-data" })) { %>
						
						<% if (Model != null && Model.Count() > 0) { %>
							<div id="erro" class="message error">
								<span class="strong">FALHA AO ENVIAR ARQUIVO!</span><br /><br />
								<% foreach (var erro in Model) { %>
									<%: string.Format("{0} - {1}", erro.Rotulo, erro.Detalhes)%>
									<br />
								<% } %>
								<br />
							</div>
						<% } %>

                    <% if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString())
                       { %>
			        <div id="sucesso" class="message success">
				        <span class="strong">SUCESSO!</span> Nota fiscal cadastrada com sucesso!
			        </div>
                    <% } else { %>

					   <div>
					   <b>Cadastro de Notas Fiscal - Envie a Nota Fiscal no formato XML</b>
						<p>
							<input type="file" id="arquivos[]" name="arquivos[]" class="files" />
						</p>						
						<hr />
						<p>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar Arquivo</span>
							</button>
						</p>
						</div>

						<% } %>
                    <% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
    
    <div class="container_12">
	    <div class="grid_12" id="footer">
		    <p>� Copyright 2011 by <%: Html.RouteLink("NFService", "Default", new { controller = "Home" })%> </p>
	    </div>
	    <div class="clearingfix"></div>
    </div>

    </div>
    
    </div>
    
    </body>
    </html>