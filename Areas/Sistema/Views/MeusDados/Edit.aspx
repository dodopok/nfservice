﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.CadastroEdit>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edição de Cadastro
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Edição de Cadastro</h3>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("Edit", "MeusDados", FormMethod.Post, new { id = "form", name = "form"})) {%>
						
						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>
						
						<div id="sucesso" class="message success">
							<span class="strong">SUCESSO!</span> Registro salvo com sucesso!
						</div>

						<%: Html.HiddenFor(model => model.ID) %>
						<div>
						<div>
						<p>
							<b>Nome </b><br />
							<%: Html.TextBoxFor(model => model.Nome, new { @class = "inputtext large", @maxlength = "128", @style="width:82%;" })%>
							<%: Html.ValidationMessageFor(model => model.Nome) %>
						</p>
						</div>
						<div class="esquerda">
						<p>
							<b>Senha</b><br />
							<%: Html.TextBoxFor(model => model.Senha, new { @class = "inputtext medium", @type = "password", @maxlength = "128" })%>
							<%: Html.ValidationMessageFor(model => model.Senha)%>
						</p>
						</div>
						<div class="direita">
						<p>
							<b>Confirmação de Senha</b><br />
							<%: Html.TextBox("ConfirmaSenha", Model !=  null ? Model.Senha : "", new { @class = "inputtext medium", @type = "password" })%>
							<%: Html.ValidationMessageFor(model => model.Senha) %>
						</p>
						</div>
						<div class="esquerda">
						<p>
							<b>CPF</b><br />
							<%: Html.TextBoxFor(model => model.CPF, new { @class = "inputtext medium", @maxlength = "128", @disabled = "disabled" })%>
							<%: Html.ValidationMessageFor(model => model.CPF) %>
						</p>
						</div>
						<div class="direita">
						<p>
							<b>Email</b><br />
							<%: Html.TextBoxFor(model => model.Email, new { @class = "inputtext medium", @maxlength = "128", @disabled = "disabled" })%>
							<%: Html.ValidationMessageFor(model => model.Email)%>
						</p>
						</div>
						<div class="esquerda">
						<p>
							<b>Telefone </b><br />
							<%: Html.TextBoxFor(model => model.Telefone, new { @class = "inputtext medium", @maxlength = "128" })%>
							<%: Html.ValidationMessageFor(model => model.Telefone)%>
						</p>
						</div>
						<div class="direita">
						<p>
							<b>Celular</b><br />
							<%: Html.TextBoxFor(model => model.Celular, new { @class = "inputtext medium", @maxlength = "128" })%>
							<%: Html.ValidationMessageFor(model => model.Celular)%>
						</p>
						</div>

						</div>

						<div>
						<hr />
						<p>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

<script type="text/javascript">

	$(document).ready(function () {

		<% if(ViewData["Sucesso"] == "true") { %>

		$("#erro").hide();
		$("#sucesso").show();

		<% } else if(ViewData["Sucesso"] == "false") { %>
		
		$("#erro").show();
		$("#sucesso").hide();

		<% } else { %>
		
		$("#erro").hide();
		$("#sucesso").hide();
		
		<% } %>
		
		$(".note.loading").hide();

		$("#Telefone").mask("(99)9999-9999");
		$("#CPF").mask("999.999.999-99");
		$("#Celular").mask("(99)9999-9999");

		$("#form").validate({
			meta: "validate",
			invalidHandler: function (form, validator) {
				$("#erro").show();
				$("#erro").focus();
			},

			submitHandler: function (form) {
				$("#erro").hide();
				$("#cancelar").hide();
				$("#enviar").hide();
				$("#processando").show();
				form.submit();
			},
			rules: {
				Nome: "required",
				Senha: "required",
				ConfirmaSenha: {
					equalTo: "#Senha"
				},
				Telefone: "required"
			},
			messages: {
				Nome: "Este campo é obrigatório.",
				Senha: "Este campo é obrigatório.",
				ConfirmaSenha: {
					equalTo: "As senhas devem ser iguais."
				},
				Telefone: "Este campo é obrigatório."
			}
		});
	});
</script>

</asp:Content>
