﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Pessoa>>" %>
<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Emitentes_Sem_Email.csv\""); %>Nome;CNPJ;Email;
<% foreach(var pessoa in Model) { %>
<%= pessoa.Nome %>;<%= pessoa.CNPJ %>;<%= pessoa.Email %>
<% } %>
