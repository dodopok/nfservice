﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Pessoa>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edição de E-mail de Empresa
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container_12">
	<div class="grid_12">
		<div class="sb-box">
			<div class="sb-box-inner content">
				<div class="header">
					<h3>Edição de e-mail da empresa <%: Model.Nome %></h3>
					<ul class="tabs">
						<li class="active"><%: Html.ActionLink("Voltar", "List")%></li>
					</ul>
				</div>
				<div class="bcont">
					<% using (Html.BeginForm("Edit", "Pessoas", new { id = Model.ID }, FormMethod.Post, new { id = "form", name = "form" })) {%>

						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>

						<div>
						<p>
							<b>E-mail</b><br />
							<%: Html.TextBox("Email", Model.Email, new { @class = "inputtext medium" })%>
						</p>
						</div>

						<div>
						<hr />
						<p>
							<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
								<span>Cancelar</span>
							</button>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>
					<% } %>
				</div>
			</div>
		</div>
	</div>
	<div class="clearingfix">
	</div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

<script type="text/javascript">

    $(document).ready(function () {

        $("#erro").hide();
        $(".note.loading").hide();


        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            },
            rules: {
                Email: {
					required: true,
					email: true
				}
            },
            messages: {
                Email: {
					required: "Esse campo é obrigatório.",
					email: "Esse campo deve conter um e-mail válido."
				}
            }
        });
    });
</script>

</asp:Content>
