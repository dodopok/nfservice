﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Pessoa>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relação de Remetentes e Destinatários
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<% 
	   var empresaSelecionada = (Domain.Entities.Empresa)ViewData["EmpresaSelecionada"];
	   var pagina = Convert.ToInt32(ViewData["Pagina"]);
	   var total = ((ViewData["Total"] == null) ? 0 : (int)ViewData["Total"]);
	   var config = new Domain.Core.Configuration();           
	   %>
	<div class="container_12">
	<div class="grid_12">
		<%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString()) { %>
		<div id="sucesso" class="message success">
			<span class="strong">SUCESSO!</span> Registro salvo com sucesso!
		</div>
		<%} %>
		<div class="sb-box">
			<div class="sb-box-inner content">
				<div class="header">
					<h3>Pessoas Físicas e Jurídicas</h3>
					<ul class="tabs">
						<li class="active"><%: Html.ActionLink("Obter CSV de Pessoas sem E-mail", "PessoasSemEmail")%></li>
						<li class="active"><%: Html.ActionLink("Enviar CSV de Pessoas sem E-mail", "EnviarListaPessoas")%></li>
					</ul>
				</div>
				<div class="bcont">
					<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
						<thead>
							<tr>
								<th>
									Nome
								</th>
								<th style="width: 170px;">
									<center>CPF / CNPJ</center>
								</th>
								<th style="width: 300px;">
									E-mail
								</th>
								<th>
								</th>
							</tr>
						</thead>
						<tbody>
							<% 
           foreach (var item in Model) { %>
							<tr>
								<td>
									<%: item.Nome %>
								</td>
								<td>
									<center><%: item.isPessoaFisica ? item.CPF : item.CNPJ %></center>
								</td>
								<td>
									<%: item.Email %>
								</td>
								<td class="small">
									<a class="action" href="#"></a>
									<div class="opmenu">
										<ul>
											<li><%: Html.ActionLink("Alterar E-mail", "Edit", new { empresaID = item.EmpresaID, id = item.ID })%></li>
										</ul>
										<div class="clear">
										</div>
										<div class="foot">
										</div>
									</div>
								</td>
							</tr>
							<% } %>
						</tbody>
					</table>
						<center>
						<% if (total > config.QuantidadePorPagina)
						   { %>
							<div class="paginacao">
								<div class="numeros">
							<% int i = (total / config.QuantidadePorPagina);

								for (int j = 1; j <= (((i * config.QuantidadePorPagina) == total) ? i : (i + 1)); j++)
							   { %>
									<a href="<%:Url.Action("List", new { empresaID = empresaSelecionada.ID, pagina = j })%>" class="<%:j==pagina ? "current" : "" %>"><%:j%></a>
							   <% } %>
								</div>
							</div>
						<% } %>
					</center>
				</div>
			</div>
		</div>
	</div>
	<div class="clearingfix">
	</div>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
