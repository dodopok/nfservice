﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Services.Erro>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Pessoas sem e-mail - Inserção
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container_12">
        <div class="grid_12">
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Pessoas sem e-mail - Inserção</h3>
                        <ul class="tabs">
                            <li class="active">
                                <%: Html.ActionLink("Voltar", "List") %></li>
                        </ul>
                    </div>
                    <div class="bcont">
                        <% using (Html.BeginForm("EnviarListaPessoas", "Pessoas", FormMethod.Post, new { enctype = "multipart/form-data" }))
                           { %>
                        <%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString())
                          { %>
                        <div id="sucesso" class="message success">
                            <span class="strong">SUCESSO!</span> Arquivo enviado com sucesso!
                        </div>
                        <%} %>
                        <% else %>
                        <%
                          {%>
                        <%
                              if (Model != null && Model.Count() > 0)
                              {%>
                        <div id="erro" class="message error">
                            <span class="strong">E-MAIL ENVIADOS:
                                <%: Model.Sum(s=>s.QtdeEnviada) %></span>
                            <br />
                            <span class="strong">E-MAIL CADASTRADOS COM SUCESSO:
                                <%: Model.Sum(s=>s.QtdeCadastrada) %></span>
                            <br />
                            <span class="strong">E-MAIL RECUSADOS:
                                <%: Model.Sum(s=>s.QtdeRecusada) %></span>
                            <br />
                            <br />
                            <span class="strong">MOTIVO DA RECUSA:</span><br />
                            <br />
                            <% foreach (var erro in Model)
                               {%>
                            <%
                                   if (erro.Rotulo != "E-MAIL ENVIADOS:" && erro.Rotulo != "SUCESSO")
                            {%>
                            <span class="strong">
                                <%:string.Format("{0} - {1}", erro.Rotulo, erro.Detalhes)%></span>
                            <br />
                            <%
                                }%>
                            <%
                            }%>
                            <br />
                        </div>
                        <% } %>

                        <%
                              }%>
                        <div>
                            <b>Pessoas sem e-mail - Envie o arquivo .csv preenchido, obtido a partir do item "Pessoas
                                Sem E-mail", na listagem</b>
                            <p>
                                <input type="file" id="arquivos[]" name="arquivos[]" class="files" />
                            </p>
                            <hr />
                            <p>
                                <button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
                                    <span>Cancelar</span>
                                </button>
                                <button id="enviar" class="button green" type="submit" value="Save">
                                    <span>Enviar Arquivos</span>
                                </button>
                            </p>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
