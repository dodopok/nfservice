﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.EmpresaEdit>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Minha Empresa
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
	<%
		var estados = (IEnumerable<Domain.Entities.Estado>)ViewData["Estados"];
		var cidadesPrincipal = (IEnumerable<Domain.Entities.Cidade>)ViewData["CidadesPrincipal"];
		var cidadesCobranca = (IEnumerable<Domain.Entities.Cidade>)ViewData["CidadesCobranca"];
	 %>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Minha Empresa</h3>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("Edit", "MinhaEmpresa", FormMethod.Post, new { id = "form", name = "form"})) {%>
						
						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>
						
						<div id="sucesso" class="message success">
							<span class="strong">SUCESSO!</span> Registro salvo com sucesso!
						</div>

						<%: Html.HiddenFor(model => model.ID) %>
						<div>
							<div class="esquerda">
							<p>
								<b>Razão Social</b><br />
								<%: Html.TextBoxFor(model => model.RazaoSocial, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.RazaoSocial) %>
							</p>
							</div>
							<div class="direita">
							<p>
								<b>Nome Fantasia</b><br />
								<%: Html.TextBoxFor(model => model.NomeFantasia, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.NomeFantasia) %>
							</p>
							</div>
							<div class="esquerda">
							<p>
								<b>CNPJ</b><br />
								<%: Html.TextBoxFor(model => model.CNPJ, new { @class = "inputtext medium", @maxlength = "128", @disabled = "disabled" })%>
								<%: Html.ValidationMessageFor(model => model.CNPJ) %>
							</p>
							</div>
							<div class="direita">
							<p>
								<b>E-mail</b><br />
								<%: Html.TextBoxFor(model => model.Email, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Email) %>
							</p>
							</div>
							<div class="esquerda">
							<p>
								<b>Inscrição Estadual</b><br />
								<%: Html.TextBoxFor(model => model.InscricaoEstadual, new { @class = "inputtext medium", @maxlength = "32", @disabled = "disabled" })%>
								<%: Html.ValidationMessageFor(model => model.InscricaoEstadual) %>
							</p>
							</div>
							<div class="direita">
							<p>
								<b>Inscrição Municipal</b><br />
								<%: Html.TextBoxFor(model => model.InscricaoMunicipal, new { @class = "inputtext medium", @maxlength = "32", @disabled = "disabled" })%>
								<%: Html.ValidationMessageFor(model => model.InscricaoMunicipal) %>
							</p>
							</div>
							<div class="esquerda">
							<p>
								<b>Telefone Principal</b><br />
								<%: Html.TextBoxFor(model => model.TelefonePrincipal, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.TelefonePrincipal) %>
							</p>
							</div>
							<div class="direita">
							<p>
								<b>Telefone Alternativo</b><br />
								<%: Html.TextBoxFor(model => model.TelefoneAlternativo, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.TelefoneAlternativo) %>
							</p>
							</div>
						</div>
						<div>
							<hr />
							<h3 class="head2" >Endereço Principal</h3>
							<div class="esquerda">
								<p>
									<b>Logradouro</b><br />
									<%: Html.TextBoxFor(model => model.LogradouroPrincipal, new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessageFor(model => model.LogradouroPrincipal)%>
								</p>
							</div>
							<div class="direita">
								<p>
									<b>Bairro</b><br />
									<%: Html.TextBoxFor(model => model.BairroPrincipal, new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessageFor(model => model.BairroPrincipal)%>
								</p>
							</div>
							<div class="esquerda">
								<p>
									<b>Complemento</b><br />
									<%: Html.TextBoxFor(model => model.ComplementoPrincipal, new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessageFor(model => model.ComplementoPrincipal)%>
								</p>
							</div>
							<div class="esquerda">
								<p>
									<b>CEP</b><br />
									<%: Html.TextBoxFor(model => model.CEPPrincipal, new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessageFor(model => model.CEPPrincipal)%>
								</p>
							</div>
							<div class="esquerda">
								<p>
									<b>Estado</b><br />

									<%: Html.DropDownListFor(model => model.EstadoIDPrincipal, new SelectList(estados, "ID", "Nome"))%>
								</p>
							</div>
							<div class="esquerda">
								<p>
									<b>Cidade</b><br />

									<%: Html.DropDownListFor(model => model.CidadeIDPrincipal, new SelectList(cidadesPrincipal, "ID", "Nome"))%>
								</p>
							</div>
						</div>

						<div>
							<hr />
							<h3 class="head2" >Endereço de Cobrança</h3>
							<div class="esquerda">
								<p>
									<b>Logradouro</b><br />
									<%: Html.TextBoxFor(model => model.LogradouroCobranca, new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessageFor(model => model.LogradouroCobranca)%>
								</p>
							</div>
							<div class="direita">
								<p>
									<b>Bairro</b><br />
									<%: Html.TextBoxFor(model => model.BairroCobranca, new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessageFor(model => model.BairroCobranca)%>
								</p>
							</div>
							<div class="esquerda">
								<p>
									<b>Complemento</b><br />
									<%: Html.TextBoxFor(model => model.ComplementoCobranca, new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessageFor(model => model.ComplementoCobranca)%>
								</p>
							</div>
							<div class="direita">
								<p>
									<b>CEP</b><br />
									<%: Html.TextBoxFor(model => model.CEPCobranca, new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessageFor(model => model.CEPCobranca)%>
								</p>
							</div>
							<div class="esquerda">
								<p>
									<b>Estado</b><br />

									<%: Html.DropDownListFor(model => model.EstadoIDCobranca, new SelectList(estados, "ID", "Nome"))%>
								</p>
							</div>
							<div class="direita">
								<p>
									<b>Cidade</b><br />

									<%: Html.DropDownListFor(model => model.CidadeIDCobranca, new SelectList(cidadesCobranca, "ID", "Nome"))%>
								</p>
							</div>
						</div>

						<div>
						<hr />
						<p>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

	$(document).ready(function () {

		Validate();

		<% if(ViewData["Sucesso"] == "true") { %>

		$("#erro").hide();
		$("#sucesso").show();

		<% } else if(ViewData["Sucesso"] == "false") { %>
		
		$("#erro").show();
		$("#sucesso").hide();

		<% } else { %>
		
		$("#erro").hide();
		$("#sucesso").hide();
		
		<% } %>

		$(".note.loading").hide();

		$("#CNPJ").mask("99.999.999/9999-99");
		$("#TelefonePrincipal").mask("(99)9999-9999");
		$("#TelefoneAlternativo").mask("(99)9999-9999");
		$("#CEPPrincipal").mask("99999-999");
		$("#CEPCobranca").mask("99999-999");

		$("#form").validate({
			meta: "validate",
			invalidHandler: function (form, validator) {
				$("#erro").show();
				$("#erro").focus();
			},

			submitHandler: function (form) {
				$("#erro").hide();
				$("#cancelar").hide();
				$("#enviar").hide();
				$("#processando").show();
				form.submit();
			},
			rules: {
				NomeFantasia: "required",
				RazaoSocial: "required",
				Email: {
					required: true,
					email: true
				},
				TelefonePrincipal: "required",
				LogradouroPrincipal: "required",
				CEPPrincipal: "required",
				BairroPrincipal: "required",
				LogradouroCobranca: "required",
				CEPCobranca: "required",
				BairroCobranca: "required",
				CidadeIDPrincipal: {
					noneSelected: true
				},
				CidadeIDCobranca: {
					noneSelected: true
				}
			},
			messages: {
				NomeFantasia: "Este campo é obrigatório.",
				RazaoSocial: "Este campo é obrigatório.",
				Email: {
					required: "Este campo é obrigatório.",
					email: "Formato de e-mail inválido"
				},
				TelefonePrincipal: "Este campo é obrigatório.",
				LogradouroPrincipal: "Este campo é obrigatório.",
				CEPPrincipal: "Este campo é obrigatório.",
				BairroPrincipal: "Este campo é obrigatório.",
				LogradouroCobranca: "Este campo é obrigatório.",
				CEPCobranca: "Este campo é obrigatório.",
				BairroCobranca: "Este campo é obrigatório.",
				CidadeIDPrincipal: "Este campo é obrigatório.",
				CidadeIDCobranca: "Este campo é obrigatório."
			}
		});

		$('#EstadoIDPrincipal').change(function () {

			$.ajaxSetup({ cache: false, async: false });
			var selectedItem = $(this).val();
			if (selectedItem == "" || selectedItem == 0) {
				//Do nothing or hide...?
			} else {
			    $.post("/Services/Localizacao/GetCidades/" + $("#EstadoIDPrincipal > option:selected").attr("value"), function (data) {
					var items = "";
					$.each(data, function (i, data) {
						items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
					});

					$("#CidadeIDPrincipal").removeAttr('disabled');
					$("#CidadeIDPrincipal").html(items);
				});
			}
		});

		$('#EstadoIDCobranca').change(function () {

			$.ajaxSetup({ cache: false, async: false });
			var selectedItem = $(this).val();
			if (selectedItem == "" || selectedItem == 0) {
				//Do nothing or hide...?
			} else {
			    $.post("/Services/Localizacao/GetCidades/" + $("#EstadoIDCobranca > option:selected").attr("value"), function (data) {
					var items = "";
					$.each(data, function (i, data) {
						items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
					});

					$("#CidadeIDCobranca").removeAttr('disabled');
					$("#CidadeIDCobranca").html(items);
				});
			}
		});
	});
	</script>
	<script type="text/javascript" src="/Scripts/woow.validateextensions.js"></script>
</asp:Content>

