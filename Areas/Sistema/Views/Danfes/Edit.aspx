﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Danfe>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript" src="/Scripts/jquery.maskMoney.0.2.js"></script>
	<script type="text/javascript">
	    $(document).ready(function () {

	        $("#erro").hide();
	        $(".note.loading").hide();

	        $("#Emitente_CNPJ").mask("99.999.999/9999-99");
            $("#Valor").maskMoney({ symbol: "R$", decimal: ",", thousands: "" });


	        $("#Emitente_CNPJ").blur(function () {
	            $.ajaxSetup({ cache: false, async: false });

	            var cnpj = $("#Emitente_CNPJ").attr("value");
	            var empresaID = <%: this.RouteData.Values["empresaID"] %>;
                
	            $.post("/Sistema/Danfes/GetEmitente", { cnpj: cnpj, empresaID: empresaID }, function (data) {

	                if (data != null) {
	                    $("#Emitente_ID").val(data.ID);
	                    $("#Emitente_Nome").val(data.Nome);
                        $("#Emitente_Email").val(data.Email);
	                } else {
	                    $("#Emitente_ID").val("");
	                    $("#Emitente_Nome").val("");
                        $("#Emitente_Email").val("");
	                }

	            });
	        });

	        $("#form").validate({
	            meta: "validate",
	            invalidHandler: function (form, validator) {
	                $("#erro").show();
	                $("#erro").focus();
	            },

	            submitHandler: function (form) {
	                $("#erro").hide();
	                $("#cancelar").hide();
	                $("#enviar").hide();
	                $("#processando").show();
	                form.submit();
	            },
	            rules: {
                    Danfe: "required",
                    Valor: "required",
	                "Emitente.CNPJ": "required",
	                "Emitente.Nome": "required",
	                "Emitente.Email": "required"
	            },
	            messages: {
                    Danfe: "Este campo é obrigatório.",
                    Valor: "Este campo é obrigatório.",
	                "Emitente.CNPJ": "Este campo é obrigatório.",
	                "Emitente.Nome": "Este campo é obrigatório.",
	                "Emitente.Email": "Este campo é obrigatório."
	            }
	        });

	    });
	</script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edição de Danfe
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Edição de Danfe</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
						</ul>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("Edit", "Danfes", FormMethod.Post, new { id = "form", name = "form"})) {%>
						
						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>

						<%: Html.HiddenFor(model => model.EmitenteID) %>

						<div>
						<p>
							<b>DANFe</b><br />
							<%: Html.TextBoxFor(model => model.CodigoDanfe, new { @class = "inputtext medium", @readonly = "true", maxlength = 54, size = 54 })%>
						</p>
						<p>
							<b>Valor</b><br />
							<%: Html.TextBoxFor(model => model.Valor, new { @class = "inputtext small" })%>
						</p>
						<p>
							<b>CNPJ do emitente</b><br />
							<%: Html.TextBoxFor(model => model.Emitente.CNPJ, new { @class = "inputtext medium", @readonly="true" })%>
						</p>
						<p>
							<b>Nome do emitente</b><br />
							<%: Html.TextBoxFor(model => model.Emitente.Nome, new { @class = "inputtext medium" })%>
						</p>
						<p>
							<b>E-mail do emitente</b><br />
							<%: Html.TextBoxFor(model => model.Emitente.Email, new { @class = "inputtext medium" })%>
						</p>
						</div>
						<div>
						<hr />
						<p>
							<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
								<span>Cancelar</span>
							</button>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>



