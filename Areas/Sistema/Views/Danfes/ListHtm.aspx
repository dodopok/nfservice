﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Danfe>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        html, body
        {
            height: 100%;
        }
        body
        {
            font: 13.34px helvetica,arial,freesans,clean,sans-serif;
            min-height: 100%;
            background-color: #f7f7f7;
        }
        .tbRelatorio
        {
            margin-left: 200px;
        }
        table.tbRelatorio tr th
        {
            font-size: 15px;
            font-weight: bold;
        }
        
        table.infotable
        {
            margin-bottom: 15px;
            text-align: left;
        }
        table.infotable tr td, table.infotable tr th
        {
            border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;
        }
        table.infotable tr td.small, table.infotable tr th.small
        {
            width: 20px;
            text-align: center;
        }
        table.infotable tr td.small input[type="checkbox"], table.infotable tr th.small input[type="checkbox"]
        {
            vertical-align: middle;
        }
        table.infotable thead tr
        {
            background-color: #f5f5f5;
        }
        table.infotable th
        {
            font-weight: bold;
            color: #305B7F;
            text-shadow: 1px 1px 0px rgba(255,255,255,1);
        }
        table.infotable tbody tr.selected
        {
            background-color: #fdffea !important;
        }
        
        .divRelatNotas
        {
            float: left;
            padding: 20px;
            text-align: left;
        }
        
        span
        {
            color: #3B6CCA;
            font-size: 25px;
            font-weight: bold;
            padding: 35px;
            text-decoration: underline;
        }
    </style>
    <title>DANFE</title>
</head>
<body>
    <%    
        var Filtros = (WebUI.Areas.Sistema.ViewModels.FiltroDanfe)ViewData["Filtros"];
    %>
    <div>
        <center>
            <b>Danfe</b></center>
        <br />
        <center>
            <b>Total de Notas Cadastradas: </b>
            <%: Model.Count() %>
        </center>
        <br />
        <br />
        <span>Filtros </span>
        <center>
            <div class="divRelatNotas">
                <b>Codigo :</b>
                <%: Filtros.CodigoDanfe ?? "Qualquer"%>
                <br />
                <br />
                <b>Nome do Emitente: </b>
                <%: Filtros.Nome ?? "Qualquer"%>
                <br />
                <br />
                <b>CNPJ do Emitente: </b>
                <%: Filtros.CNPJ ?? "Qualquer"%>
                <br />
                <br />
            </div>
            <div class="divRelatNotas">
                <b>Data Início: </b>
                <%: (DateTime)Filtros.DataInicio == DateTime.MinValue ? "Qualquer" : ((DateTime)Filtros.DataInicio).ToShortDateString()%>
                <br />
                <br />
                <b>Data Fim: </b>
                <%: (DateTime)Filtros.DataFim == DateTime.MinValue ? "Qualquer" : ((DateTime)Filtros.DataFim).ToShortDateString()%>
                <br />
                <br />
            </div>
        </center>
        <br />
        <br />
        <br />
        <center style="clear: both;">
            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                <thead>
                    <tr>
                        <th>
                            DANFE
                        </th>
                        <th>
                            CNPJ do Emitente
                        </th>
                        <th>
                            Nome do Emitente
                        </th>
                        <th>
                            Data de Cadastro
                        </th>
                        <th>
                            Valor
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% foreach (var item in Model)
                       { %>
                    <tr>
                        <td>
                            <%: item.ToString() %>
                        </td>
                        <td>
                            <%: item.Emitente.CNPJ %>
                        </td>
                        <td>
                            <%: item.Emitente.Nome%>
                        </td>
                        <td>
                            <%: item.DataCriacao.ToShortDateString() %>
                        </td>
                        <td>
                            <%: item.Valor %>
                        </td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>
