﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.Sistema.ViewModels.Danfe>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript" src="/Scripts/jquery.maskMoney.0.2.js"></script>
	<script type="text/javascript">
	    $(document).ready(function () {

	        $("#erro").hide();
	        $(".note.loading").hide();

	        $('#PrimeiroSegmento, #SegundoSegmento, #TerceiroSegmento, #QuartoSegmento, #QuintoSegmento, #SextoSegmento, #SetimoSegmento, #OitavoSegmento, #NonoSegmento, #DecimoSegmento, #DecimoPrimeiroSegmento').autotab_magic().autotab_filter('numeric');

	        $("#EmpresaEmitente_CNPJ").mask("99.999.999/9999-99");
            $("#Valor").maskMoney({ symbol: "R$", decimal: ",", thousands: "" });


	        $("#EmpresaEmitente_CNPJ").blur(function () {
	            $.ajaxSetup({ cache: false, async: false });

	            var cnpj = $("#EmpresaEmitente_CNPJ").attr("value");
	            var empresaID = <%: this.RouteData.Values["empresaID"] %>;
                
	            $.post("/Sistema/Danfes/GetEmitente", { cnpj: cnpj, empresaID: empresaID }, function (data) {

	                if (data != null) {
	                    $("#EmpresaEmitente_ID").val(data.ID);
	                    $("#EmpresaEmitente_Nome").val(data.Nome);
                        $("#EmpresaEmitente_Email").val(data.Email);
	                } else {
	                    $("#EmpresaEmitente_ID").val("");
	                    $("#EmpresaEmitente_Nome").val("");
                        $("#EmpresaEmitente_Email").val("");
	                }

	            });
	        });

	        $("#form").validate({
	            meta: "validate",
	            invalidHandler: function (form, validator) {
	                $("#erro").show();
	                $("#erro").focus();
	            },

	            submitHandler: function (form) {
	                $("#erro").hide();
	                $("#cancelar").hide();
	                $("#enviar").hide();
	                $("#processando").show();
	                form.submit();
	            },
	            rules: {

	                PrimeiroSegmento: "required",
	                SegundoSegmento: "required",
	                TerceiroSegmento: "required",
	                QuartoSegmento: "required",
	                QuintoSegmento: "required",
	                SextoSegmento: "required",
	                SetimoSegmento: "required",
	                OitavoSegmento: "required",
	                NonoSegmento: "required",
	                DecimoSegmento: "required",
	                DecimoPrimeiroSegmento: "required",
                    Valor: "required",
	                "EmpresaEmitente.CNPJ": "required",
	                "EmpresaEmitente.Nome": "required",
	                "EmpresaEmitente.Email": "required"
	            },
	            messages: {
	                PrimeiroSegmento: "Este campo é obrigatório.",
	                SegundoSegmento: "Este campo é obrigatório.",
	                TerceiroSegmento: "Este campo é obrigatório.",
	                QuartoSegmento: "Este campo é obrigatório.",
	                QuintoSegmento: "Este campo é obrigatório.",
	                SextoSegmento: "Este campo é obrigatório.",
	                SetimoSegmento: "Este campo é obrigatório.",
	                OitavoSegmento: "Este campo é obrigatório.",
	                NonoSegmento: "Este campo é obrigatório.",
	                DecimoSegmento: "Este campo é obrigatório.",
	                DecimoPrimeiroSegmento: "Este campo é obrigatório.",
                    Valor: "Este campo é obrigatório.",
	                "EmpresaEmitente.CNPJ": "Este campo é obrigatório.",
	                "EmpresaEmitente.Nome": "Este campo é obrigatório.",
	                "EmpresaEmitente.Email": "Este campo é obrigatório."
	            }
	        });

	    });
	</script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Cadastro de Danfe
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Cadastro de Danfe</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
						</ul>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("Create", "Danfes", FormMethod.Post, new { id = "form", name = "form"})) {%>
						
						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>

						<%: Html.HiddenFor(model => model.EmpresaEmitente.ID) %>

						<div>
						<p>
							<b>DANFe</b><br />
							<%: Html.TextBoxFor(model => model.PrimeiroSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%> - <%: Html.TextBoxFor(model => model.SegundoSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%>  - <%: Html.TextBoxFor(model => model.TerceiroSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%>  - 
							<%: Html.TextBoxFor(model => model.QuartoSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%> - <%: Html.TextBoxFor(model => model.QuintoSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%> - <%: Html.TextBoxFor(model => model.SextoSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%> - 
							<%: Html.TextBoxFor(model => model.SetimoSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%>  - <%: Html.TextBoxFor(model => model.OitavoSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%>  - <%: Html.TextBoxFor(model => model.NonoSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%>  - 
							<%: Html.TextBoxFor(model => model.DecimoSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%>  - <%: Html.TextBoxFor(model => model.DecimoPrimeiroSegmento, new { @class = "inputtext tiny", maxlength = 4, size = 4 })%>  
						</p>
						<p>
							<b>Valor</b><br />
							<%: Html.TextBoxFor(model => model.Valor, new { @class = "inputtext small" })%>
						</p>
						<p>
							<b>CNPJ do emitente</b><br />
							<%: Html.TextBoxFor(model => model.EmpresaEmitente.CNPJ, new { @class = "inputtext medium" })%>
						</p>
						<p>
							<b>Nome do emitente</b><br />
							<%: Html.TextBoxFor(model => model.EmpresaEmitente.Nome, new { @class = "inputtext medium" })%>
						</p>
						<p>
							<b>E-mail do emitente</b><br />
							<%: Html.TextBoxFor(model => model.EmpresaEmitente.Email, new { @class = "inputtext medium" })%>
						</p>
						</div>
						<div>
						<hr />
						<p>
							<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
								<span>Cancelar</span>
							</button>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>



