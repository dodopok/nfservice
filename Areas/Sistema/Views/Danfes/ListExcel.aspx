<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Danfe>>" %>
<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Danfes.csv\""); %>

Número NF-e;Nome do Emitente;CNPJ do Emitente;Data de Cadastro;Valor 

<% foreach(var nota in Model)
   { %>
        <%= nota.ToString()%>;<%= nota.Emitente != null ? nota.Emitente.Nome : string.Empty %>;<%= nota.Emitente!= null? nota.Emitente.CNPJ: string.Empty %>;<%= nota.DataCriacao.ToShortDateString() %>;<%= nota.Valor %>
<% } %>