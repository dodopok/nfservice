﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Danfe>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Enviar email para Empresa Emitente
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container_12">
	<div class="grid_12">
		<div class="sb-box">
			<div class="sb-box-inner content">
				<div class="header">
					<h3>Enviar email para Empresa Emitente</h3>
					<ul class="tabs">
						<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
					</ul>
				</div>
				<div class="bcont">
					<% using (Html.BeginForm("Enviar", "Danfes", FormMethod.Post, new { id = "form", name = "form"})) {%>
						
					<div id="erro" class="message error">
						<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
					</div>

					<%: Html.HiddenFor(model => model.ID) %>
					<div>
                    <div class="esquerda">
					    <p>
						    <b>Assunto</b><br />
                            <%: Html.TextBox("Assunto", null, new { @class = "inputtext medium" }) %>
					    </p>
                    </div>
                    <div class="direita">
                        <p>
                            <b>E-mail do destinatário</b><br />
                            <%: Html.TextBox("Email", Model.Emitente.Email, new { @class= "inputtext medium" }) %>
                        </p>
                    </div>
                    <p>
                        <b>Mensagem</b><br />
                        <%: Html.TextArea("Mensagem", null, new { @class = "inputtext medium"}) %>
                    </p>
                    </div>

					<div>
					<hr />
					<p>
						<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
							<span>Cancelar</span>
						</button>
						<button id="enviar" class="button green" type="submit" value="Save">
							<span>Enviar</span>
						</button>
						<span id="processando" class="note loading">Processando...</span>
					</p>
					</div>
					<% } %>
				</div>
			</div>
		</div>
	</div>
	<div class="clearingfix">
	</div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

<script type="text/javascript">

    $(document).ready(function () {

        $("#erro").hide();
        $(".note.loading").hide();


        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            },
            rules: {
                Assunto: "required",
                Mensagem: "required",
                Email: "required"
            },
            messages: {
                Assunto: "Esse campo é obrigatório.",
                Mensagem: "Esse campo é obrigatório.",
                Email: "Esse campo é obrigatório."
            }
        });
    });
</script>

</asp:Content>
