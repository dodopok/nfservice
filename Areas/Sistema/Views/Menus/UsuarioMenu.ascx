﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<int>" %>

<div class="scroll-menu">
  <div class="smc-1">
    <div class="smc-2">
      <div class="smc-3">
        <div id="content-scroll">
          <div id="content-holder">
            <div class="pane">
              <ul class="menu-items">
                <li><a href="<%: Url.Action("Edit", "MeusDados") %>"> <img src="/content/images/function-icons/user_48.png" alt="" /><span>Meus Dados</span></a>
                  <div class="tooltip">Meus Dados</div>
                </li>
                <li><a href="<%: Url.Action("Edit", "MinhaEmpresa") %>"> <img src="/content/images/function-icons/industry_service_48.png" alt="" /><span>Minha Empresa</span></a>
                  <div class="tooltip">Minha Empresa</div>
                </li>
                <li><a href="<%: Url.Action("List", "GerentesNotas") %>"> <img src="/content/images/function-icons/users_two_48.png" alt="" /><span>Gerentes de Notas</span></a>
                  <div class="tooltip">Gerentes de Notas</div>
                </li>
                <li><a href="<%: Url.RouteUrl("Interna_Empresas", new { controller = "NotasFiscais", empresaID = Model }) %>"> <img src="/content/images/function-icons/app_48.png" alt="" /><span>Notas Fiscais</span></a>
                  <div class="tooltip">Notas Fiscais</div>
                </li>
                <li><a href="<%: Url.RouteUrl("Interna_Empresas", new { controller = "Danfes", empresaID = Model }) %>"> <img src="/content/images/function-icons/paper_content_pencil_48.png" alt="" /><span>Danfes</span></a>
                  <div class="tooltip">Danfes</div>
                </li>
                <li><a href="<%: Url.RouteUrl("Pessoas", new { empresaID = Model, action = "List" }) %>"> <img src="/content/images/function-icons/computer_48.png" alt="" /><span>E-mails de Emitentes e Destinatários</span></a>
                  <div class="tooltip">E-mails de Emitentes e Destinatários</div>
                </li>
                <li> <a href="<%: Url.Action("Relatorios", "RelatoriosEmpresa", new { empresaID = Model })%>"> <img src="/content/images/function-icons/paper_content_chart_48.png" alt="" /><span>Relatórios</span></a>
                  <div class="tooltip">Relatórios</div>
                </li>
                <li><a href="<%: Url.Action("List", "Financeiro") %>"> <img src="/content/images/function-icons/13sal.png" alt="" /><span>Financeiro</span></a>
                  <div class="tooltip">Financeiro</div>
                </li>
                <li><a href="/content/arquivos/guia_pratico_usuario_versao_1.0.pdf" target="_blank"> <img src="/content/images/function-icons/lightbulb_48.png" alt="Manual de Utilização" /><span>Manual de Utilização</span></a>
                  <div class="tooltip">Manual de Utilização</div>
                </li>
              </ul>
              <div style="width:100px; float:right; margin-top:-60px; margin-right:20px;"><script type="text/javascript" src="http://settings.messenger.live.com/controls/1.0/PresenceButton.js"></script>
                <div id="Microsoft_Live_Messenger_PresenceButton_c44691782e036cc7" msgr:width="100" msgr:backColor="#D7E8EC" msgr:altBackColor="#FFFFFF" msgr:foreColor="#424542" msgr:conversationUrl="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=c44691782e036cc7@apps.messenger.live.com&mkt=pt-BR"></div>
                <script type="text/javascript" src="http://messenger.services.live.com/users/c44691782e036cc7@apps.messenger.live.com/presence?dt=&mkt=pt-BR&cb=Microsoft_Live_Messenger_PresenceButton_onPresence"></script></div>
            </div>
          </div>
        </div>
        <div class="scrollbar">
          <div class="sb-1">
            <div class="sb-2">
              <div class="sb-3"> <a id="scrollleft" href="#">Left</a> <a id="scrollright" href="#">Right</a>
                <div class="ui-slider ui-slider-horizontal">
                  <div id="scrollbar" style="left: 0%;" class="ui-slider-handle"> <a href="#"><span><span></span></span></a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearingfix"> </div>
      </div>
    </div>
  </div>
</div>
