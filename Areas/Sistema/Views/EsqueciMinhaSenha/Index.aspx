﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title>NFService - Esqueci Minha Senha</title>
	<link rel="stylesheet" type="text/css" href="/Content/css/styles.css" media="screen" />
	<script type="text/javascript" src="/Scripts/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-1.8.2.custom.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery.validate.js"></script>
	<script type="text/javascript" src="/Scripts/jquery.visualize.js"></script>
	<script type="text/javascript" src="/Scripts/custom.auth.js"></script>
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="/content/ie7.css" media="screen" />
	<![endif]-->
	<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="/content/ie8.css" media="screen" />
	<![endif]-->
	<!--[if IE]>
	<script language="javascript" type="text/javascript" src="/scripts/excanvas.js"></script>
	<![endif]-->

	<script type="text/javascript">

		$(document).ready(function () {
			$("#processando").hide();
			$("#form").validate({
				meta: "validate",
				invalidHandler: function (form, validator) {
					$("#erro").html("Preencha o campo de e-mail!");
					$("#erro").show();
				},

				submitHandler: function (form) {
					$("#erro").hide();
					$("#login").hide();
					$("#processando").show();
					form.submit();
				},

				rules: {
					Email: {
						required: true,
						email: true
					}
				},
				messages: {
					Email: {
						required: "Preencha o e-mail",
						email: "E-mail inválido"
					}
				}
			});
		});
	</script>
</head>
<body>
    <div id="page-body">
	    <div id="wrapper">
            <div class="sb-box auth">
			    <div class="sb-box-inner content">
                    <div class="header"><h3>Recuperar Senha</h3></div>
                    <div class="bcont">
                        <% using (Html.BeginForm("Enviar", "EsqueciMinhaSenha", FormMethod.Post, new { id = "form", name = "form"})) 
                        {%>
                            <%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString()) { %>
			                    <div id="sucesso" class="message success">
				                    <span class="strong">SUCESSO!</span> E-mail enviado com sucesso!
			                    </div>
                            <%} else if(Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Failure.ToString()) { %>
                                <div id="erro" class="message error">
				                    <span class="strong">ERRO!</span> Esse email não existe em nossa base!
			                    </div>
                            <% } %>
						    <div>
						    <p>
							    <label>Email</label>
							    <%: Html.TextBox("Email", null, new { @class = "inputtext small" }) %>
						    </p>
						    <p>
							    <button type="submit" class="button green floatright"><span>Enviar</span></button>
							    <span id="processando" class="note loading">Processando...</span>
						    </p>
						    </div>
						    <div class="clearingfix"></div>
					    <% } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
