﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Pagamento>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Financeiro
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container_12">
	<div class="grid_12">
		<div class="sb-box">
			<div class="sb-box-inner content">
				<div class="header">
					<h3>Boletos</h3>
                </div>
                <div class="bcont">
					<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
						<thead>
							<tr>
                                <th style="width: 115px;">
                                    <center>Data de Emissão</center>
                                </th>
                                <th style="width: 130px;">
                                    <center>Data de Pagamento</center>
                                </th>
                                <th>
                                    Plano
                                </th>
                                <th style="width: 115px;">
                                    <center>Valor</center>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
							
						<% using(Html.BeginForm("List", "Financeiro", FormMethod.Post, new { id = "downloadSelectedForm", name = "downloadSelectedForm"} )) { %>
                            <% foreach (var item in Model) { %>
							    <tr>
								    <td>
										<center><%: item.DataEmissaoBoleto.ToShortDateString()%></center>
								    </td>
                                    <% if (item.DataPagamentoBoleto == null) { %>
								    <td>
										<%: item.DataPagamentoBoleto %>
								    </td>
                                    <% } else { %>
                                    <td>
                                        <center><%: item.DataPagamentoBoleto.Value.ToShortDateString()%></center>
                                    </td>
                                    <% } %>
								    <td>
										<%: item.Plano.Nome %>
								    </td>
								    <td>
										<center><%: item.Valor.ToString("C") %></center>
								    </td>
							    </tr>
                            <% } %>
						<% } %>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
