﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Plano>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Edição de Plano
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Cadastro de Plano</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
						</ul>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("Edit", "Planos", FormMethod.Post, new { id = "form", name = "form"})) {%>
						
						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>

						<%: Html.HiddenFor(model => model.ID) %>
						<div>
							<div class="esquerda">
							<p>
								<b>Nome</b><br />
								<%: Html.TextBoxFor(model => model.Nome, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Nome) %>
							</p>
							</div>
							<div class="direita">
							<p>
								<b>Valor</b><br />
								<%: Html.TextBoxFor(model => model.Valor, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Valor)%>
							</p>
							</div>
							<div class="esquerda">
							<p>
								<b>Quantidade Mínima de Notas a Serem Arquivadas</b><br />
								<%: Html.TextBoxFor(model => model.QuantidadeMinimaNotasArquivadas, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.QuantidadeMinimaNotasArquivadas)%>
							</p>
							</div>
							<div class="direita">
							<p>
								<b>Quantidade Máxima de Notas a Serem Arquivadas (Zero para sem limite)</b><br />
								<%: Html.TextBoxFor(model => model.QuantidadeMaximaNotasArquivadas, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.QuantidadeMaximaNotasArquivadas)%>
							</p>
							</div>
							<p>
								<b>Descrição</b><br />
								<%: Html.TextAreaFor(model => model.Descricao, new { @class = "inputtext medium", @maxlength = "128" })%>
								<%: Html.ValidationMessageFor(model => model.Descricao)%>
							</p>
						</div>
						<div>
						<hr />
						<p>
							<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
								<span>Cancelar</span>
							</button>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
	<script type="text/javascript">
		$(document).ready(function () {
		    Validate();
			$("#erro").hide();
			$(".note.loading").hide();
						
			$("#Valor").maskMoney({ symbol: "R$", decimal: ",", thousands: "" });

			$('#QuantidadeMinimaNotasArquivadas').live('keypress', function (e) {
				var keyPressed;
				if ((e.charCode) && (e.keyCode == 0))
					keyPressed = e.charCode
				else
					keyPressed = e.keyCode;
				if ((keyPressed < 48 || keyPressed > 57) && keyPressed != 8 && keyPressed != 44) {
					return false;
				}
				return true;
			});

			$('#QuantidadeMaximaNotasArquivadas').live('keypress', function (e) {
				var keyPressed;
				if ((e.charCode) && (e.keyCode == 0))
					keyPressed = e.charCode
				else
					keyPressed = e.keyCode;
				if ((keyPressed < 48 || keyPressed > 57) && keyPressed != 8 && keyPressed != 44) {
					return false;
				}
				return true;
			});

			$("#form").validate({
				meta: "validate",
				invalidHandler: function (form, validator) {
					$("#erro").show();
					$("#erro").focus();
				},

				submitHandler: function (form) {
					$("#erro").hide();
					$("#cancelar").hide();
					$("#enviar").hide();
					$("#processando").show();
					form.submit();
				},
				rules: {
					Nome: "required",
					Descricao: "required",
					QuantidadeMinimaNotasArquivadas: {
						required: true
					},
					QuantidadeMaximaNotasArquivadas: {
					    greatherThan: "#QuantidadeMinimaNotasArquivadas"
					},
					Valor: {
						required: true
					}
				},
				messages: {
					Nome: "Este campo é obrigatório.",
					Descricao: "Este campo é obrigatório.",
					QuantidadeMinimaNotasArquivadas: {
						required: "Este campo é obrigatório.",
						min: "Este campo deve ter um valor maior que zero"
					},
					QuantidadeMaximaNotasArquivadas: {
					    greatherThan: "Este campo deve ser maior que a quantidade mínima."
					},
					Valor: {
						required: "Este campo é obrigatório."
					}
				}
			});
		});
	</script>
    <script type="text/javascript" src="/Scripts/woow.validateextensions.js"></script>
	
</asp:Content>
