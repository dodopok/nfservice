﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Plano>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Planos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container_12">
		<div class="grid_12">
            <%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString() && Request.QueryString["from"] == "MudaEstado" ) { %>
            <div id="Div1" class="message success">
				<span class="strong">SUCESSO!</span> O estado do plano escolhido foi alterado.
			</div>
            <%} else if((Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString())) { %>
			<div id="sucesso" class="message success">
				<span class="strong">SUCESSO!</span> Registro salvo com sucesso! Um novo plano é criado como "Inativo". Para disponibilizá-lo, clique na opção "Ativar" no menu correspondente.
			</div>
			<%} %>
            
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Planos</h3>
						<ul class="tabs">
							<li class="active">
								<%: Html.ActionLink("Novo", "Edit") %></li>
						</ul>
					</div>
					<div class="bcont">
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								<th>
									Nome
								</th>
								<th>
									Descrição
								</th>
								<th style="width:132px;">
									<center>Qtde Mínima de Notas do Plano</center>
								</th>
                                <th style="width:132px;">
									<center>Qtde Máxima de Notas do Plano</center>
								</th>
								<th style="width:60px;">
									<center>Valor</center>
								</th>
								<th style="width:60px;">
									<center>Ativo</center>
								</th>
								<th class="small">
								</th>
								</tr>
							</thead>
							<tbody>
								<% foreach (var item in Model)
								   { %>
								<tr>
									<td>
										<%: item.Nome %>
									</td>
									<td>
										<%: item.Descricao %>
									</td>
									<td>
										<center><%: item.QuantidadeMinimaNotasArquivadas %></center>
									</td>
                                    <td>
										<center><%: item.QuantidadeMaximaNotasArquivadas %></center>
									</td>
									<td>
										<center><%: item.Valor.ToString("C") %></center>
									</td>
									<td>
										<% if (item.Ativo)
                                         { %>
                                        <center><a href = "<%: Url.Action("MudaEstado", "Planos", new { id = item.ID })%>" title = "Ativo"><img src="/Content/images/onebit-icons/success.png" alt="Ativo" title="Ativo" /></a></center>
                                        <% }
                                         else
                                         { %>
                                        <center><a href = "<%: Url.Action("MudaEstado", "Planos", new { id = item.ID })%>" title = "Inativo"><img src="/Content/images/onebit-icons/error.png" alt="Inativo" title="Inativo" /></a></center>
                                        <% } %>
									</td>
									<td class="small">
										<a class="action" href="#"></a>
										<div class="opmenu">
                                            <ul>   
                                                <li><%: Html.ActionLink("Editar", "Edit", new { id = item.ID }) %></li>
                                            </ul>
											<div class="clear">
											</div>
											<div class="foot">
											</div>
										</div>
									</td>
								</tr>
								<% } %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>

