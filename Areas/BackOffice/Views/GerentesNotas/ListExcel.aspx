﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.GerenteNotas>>" %>
<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"GerentesNotas.csv\""); %>
Nome;Email;CPF;Telefone;Celular;Status
<% foreach(var gerenteNotas in Model)
   { %>
        <%= gerenteNotas.Nome %>;<%= gerenteNotas.Email %>;<%= gerenteNotas.CPF %>;<%= gerenteNotas.Telefone %>;<%= gerenteNotas.Celular %>;<%= ((gerenteNotas.Ativo) ? "Ativa" : "Inativa ") %>
<% } %>
