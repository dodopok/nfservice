﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.GerenteNotas>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Detalhes do Gerente de Notas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Detalhes do Gerente de Notas</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
						</ul>
					</div>
					<div class="bcont">
						<div class="esquerda">
						<p>
							<b>Nome </b><br />
							<%: Html.TextBoxFor(model => model.Nome, new { @class = "inputtext medium", @disabled = "disabled" })%>
						</p>
						</div>
						<div class="direita">
						<p>
							<b>Email</b><br />
							<%: Html.TextBoxFor(model => model.Email, new { @class = "inputtext medium", @disabled = "disabled" })%>
						</p>
						</div>
						<div class="esquerda">
						<p>
							<b>CPF</b><br />
							<%: Html.TextBoxFor(model => model.CPF, new { @class = "inputtext medium", @disabled = "disabled" })%>
						</p>
						</div>
						<div class="direita">
						<p>
							<b>Telefone </b><br />
							<%: Html.TextBoxFor(model => model.Telefone, new { @class = "inputtext medium", @disabled = "disabled" })%>
						</p>
						</div>
						<div class="esquerda">
						<p>
							<b>Celular</b><br />
							<%: Html.TextBoxFor(model => model.Celular, new { @class = "inputtext medium", @disabled = "disabled" })%>
						</p>
						</div>
						<div class="direita">
						<p>
							<b>Ativa</b><br />
							<%: Html.TextBox("Ativa", Model.Ativo ? "Sim" : "Não", new { @class = "inputtext medium", @disabled = "disabled" })%>
						</p>
						</div>
						<div>
							<p>
							<b>Empresas</b><br />
							<% foreach (var empresa in Model.Empresas) { %>
								<label>- <%: empresa.NomeFantasia %></label>
								<br />
							<% } %>
							</p>  
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
