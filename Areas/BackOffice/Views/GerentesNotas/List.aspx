﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master"
    Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.GerenteNotas>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Gerentes de Notas
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container_12">
        <div class="grid_12">
            <%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString())
              { %>
            <div id="sucesso" class="message success">
                <span class="strong">SUCESSO!</span> Mensagem enviada!
            </div>
            <% } %>
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div id="headerFiltro">
                        <h3>
                            Filtragem</h3>
                        &nbsp;&nbsp;
                        <img src="/Content/images/function-icons/search_48.png" alt="Mostrar ou Esconder"
                            class="fright" width="35px" />
                    </div>
                    <div class="bcont">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <% using (Html.BeginForm("List", "GerentesNotas", FormMethod.Post, new { id = "form", name = "form" }))
                                           {  %>
                                        <%: Html.Hidden("tipoDestino") %>
                                        <div id="formularioGerentesNotas">
                                            <div class="esquerda">
                                                <b>Nome</b><br />
                                                <p>
                                                    <%: Html.TextBox("Nome", null, new { @maxlength = "128" })%>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>Empresa</b><br />
                                                <p>
                                                    <%: Html.TextBox("Empresa", null, new { @maxlength = "128" })%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>CPF</b><br />
                                                <p>
                                                    <%: Html.TextBox("CPF") %>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>CNPJ Empresa</b><br />
                                                <p>
                                                    <%: Html.TextBox("CNPJ") %>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <p>
                                                    <button id="enviar" class="button green" type="submit" value="Save">
                                                        <span>Filtrar</span>
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                        <% } %>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br />
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Gerentes de Notas</h3>
                        <ul class="tabs">
                            <li id="enviarPdf" class="active" value="Save"><a href="#">
                                <img alt="Versão em PDF" width="14px" src="/Content/images/function-icons/pdf_ico.jpg" />
                                Obter PDF </a></li>
                            <li id="enviarXls" class="active" value="Save"><a href="#">
                                <img alt="Versão em Excel" width="14px" src="/Content/images/function-icons/excel_icon1.png" />
                                Obter Excel </a></li>
                            <li id="enviarHtm" class="active" value="Save"><a href="#">
                                <img alt="Versão para Impressão" width="13px" src="/Content/images/function-icons/paper_48.png" />
                                Obter Versão para Impressão </a></li>
                            <li class="active">
                                <%: Html.ActionLink("Voltar", "List", "Empresas") %></li>
                        </ul>
                    </div>
                    <div class="bcont">
                        <% if (Model.Any())
                           { %>
                        <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                            <thead>
                                <tr>
                                    <th>
                                        Nome
                                    </th>
                                    <th>
                                        E-mail
                                    </th>
                                    <th>
                                        CPF
                                    </th>
                                    <th>
                                        Telefone
                                    </th>
                                    <th>
                                        Celular
                                    </th>
                                    <th>
                                        Ativo
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                               foreach (var item in Model)
                               {%>
                                <tr>
                                    <td>
                                        <%:item.Nome%>
                                    </td>
                                    <td>
                                        <%:item.Email%>
                                    </td>
                                    <td>
                                        <%:item.CPF%>
                                    </td>
                                    <td>
                                        <%:item.Telefone%>
                                    </td>
                                    <td>
                                        <%:item.Celular%>
                                    </td>
                                    <td>
                                        <%:item.Ativo ? "SIM" : "NÃO"%>
                                    </td>
                                    <td class="small">
                                        <a class="action" href="#"></a>
                                        <div class="opmenu">
                                            <ul>
                                                <li>
                                                    <%:Html.ActionLink("Detalhes", "Details",
                                                                     new
                                                                         {
                                                                             id = item.ID
                                                                         })%></li>
                                            </ul>
                                            <div class="clear">
                                            </div>
                                            <div class="foot">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <%
                               }%>
                                <%
                           }%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {

            $("#CPF").mask("999.999.999-99");
            $("#CNPJ").mask("99.999.999/9999-99");

            $("#formularioGerentesNotas").hide();

            $("#headerFiltro").click(function () {

                var visible = $("#formularioGerentesNotas").css("display");

                if (visible == "none")
                    $("#formularioGerentesNotas").show();
                else
                    $("#formularioGerentesNotas").hide();
            });

            $('#enviarXls').click(function () {
                $("#tipoDestino").val('xls');
                $("#form").submit();
                $("#tipoDestino").val('');
            });

            $('#enviarHtm').click(function () {
                $("#tipoDestino").val('htm');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });

            $('#enviarPdf').click(function () {
                $("#tipoDestino").val('pdf');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });
        });

    </script>
</asp:Content>
