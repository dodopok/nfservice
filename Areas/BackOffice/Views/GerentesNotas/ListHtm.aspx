﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.GerenteNotas>>" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style type="text/css">
        html, body
        {
            height: 100%;
        }
        body
        {
            font: 13.34px helvetica,arial,freesans,clean,sans-serif;
            min-height: 100%;
            background-color: #f7f7f7;
        }
        .tbRelatorio
        {
            margin-left: 200px;
        }
        table.tbRelatorio tr th
        {
            font-size: 15px;
            font-weight: bold;
        }
        
        table.infotable
        {
            margin-bottom: 15px;
            text-align: left;
        }
        table.infotable tr td, table.infotable tr th
        {
            border-bottom: 1px solid #DDDDDD;
            line-height: normal;
            padding: 5px 10px;
            text-align: left;
        }
        table.infotable tr td.small, table.infotable tr th.small
        {
            width: 20px;
            text-align: center;
        }
        table.infotable tr td.small input[type="checkbox"], table.infotable tr th.small input[type="checkbox"]
        {
            vertical-align: middle;
        }
        table.infotable thead tr
        {
            background-color: #f5f5f5;
        }
        table.infotable th
        {
            font-weight: bold;
            color: #305B7F;
            text-shadow: 1px 1px 0px rgba(255,255,255,1);
        }
        table.infotable tbody tr.selected
        {
            background-color: #fdffea !important;
        }
        
        
        .divRelatNotas
        {
            float: left;
            padding: 20px;
            text-align: left;
        }
        
        span
        {
            color: #3B6CCA;
            font-size: 25px;
            font-weight: bold;
            padding: 35px;
            text-decoration: underline;
        }
    </style>
    <title>Gerentes de Notas </title>
</head>
<body>
    <%    
    
        var nome = ViewData["Nome"];
        var cpf = ViewData["CPF"];
        var empresa = ViewData["Empresa"];
        var cnpj = ViewData["CNPJ"];
    %>
    <div>
        <center>
            <b>GERENTE DE NOTAS</b></center>
        <br />
        <center>
            <b>Total de Gerentes de Notas Cadastrados: </b>
            <%: Model.Count() %>
        </center>
        <br />
        <br />
        <span>Filtros </span>
        <center>
            <div class="divRelatNotas">
                <b>Nome :</b>
                <%: nome ?? "Qualquer"%>
                <br />
                <br />
                <b>CPF: </b>
                <%: cpf ?? "Qualquer"%>
                <br />
                <br />
                <b>Empresa: </b>
                <%: empresa ?? "Qualquer"%>
                <br />
                <br />
                <b>CNPJ:</b>
                <%: cnpj ?? "Qualquer"%>
            </div>
        </center>
        <br />
        <br />
        <br />
        <center style="clear: both;">
            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                <thead>
                    <tr>
                        <th>
                            Nome
                        </th>
                        <th>
                            E-mail
                        </th>
                        <th>
                            CPF
                        </th>
                        <th>
                            Telefone
                        </th>
                        <th>
                            Celular
                        </th>
                        <th>
                            Ativo
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        foreach (var item in Model)
                        {%>
                    <tr>
                        <td>
                            <%:item.Nome%>
                        </td>
                        <td>
                            <%:item.Email%>
                        </td>
                        <td>
                            <%:item.CPF%>
                        </td>
                        <td>
                            <%:item.Telefone%>
                        </td>
                        <td>
                            <%:item.Celular%>
                        </td>
                        <td>
                            <%:item.Ativo ? "SIM" : "NÃO"%>
                        </td>
                    </tr>
                    <%
                               }%>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>
