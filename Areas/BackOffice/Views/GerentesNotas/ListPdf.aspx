﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.GerenteNotas>>" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Gerentes de Notas </title>
</head>
<body>
    <%    
    
        var nome = ViewData["Nome"];
        var cpf = ViewData["CPF"];
        var empresa = ViewData["Empresa"];
        var cnpj = ViewData["CNPJ"];
    %>
    <div>
        <center>
            <b>GERENTE DE NOTAS</b></center>
        <br />
        <center>
            <b>Total de Gerentes de Notas Cadastrados: </b>
            <%: Model.Count() %>
        </center>
        <br />
        <br />
        <center style="clear: both;">
            <table cellspacing="3" cellpadding="3" width="100%">
                <thead>
                    <tr>
                        <td bgcolor="#f5f5f5" color="#305B7F">
                            Nome
                        </td>
                        <td bgcolor="#f5f5f5" color="#305B7F">
                            E-mail
                        </td>
                        <td bgcolor="#f5f5f5" color="#305B7F"  >
                            CPF
                        </td>
                        <td bgcolor="#f5f5f5" color="#305B7F" >
                            Telefone
                        </td>
                        <td bgcolor="#f5f5f5" color="#305B7F" >
                            Celular
                        </td>
                        <td bgcolor="#f5f5f5" color="#305B7F" >
                            Ativo
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <%
                        foreach (var item in Model)
                        {%>
                    <tr>
                        <td>
                            <font size="2"><%:item.Nome%></font>
                        </td>
                        <td >
                            <font size="2"><%:item.Email%></font>
                        </td>
                        <td >
                            <font size="2"><%:item.CPF%></font>
                        </td>
                        <td >
                            <font size="2"><%:item.Telefone%></font>
                        </td>
                        <td >
                            <font size="2"><%:item.Celular%></font>
                        </td>
                        <td >
                            <font size="2"><%:item.Ativo ? "SIM" : "NÃO"%></font>
                        </td>
                    </tr>
                    <%
                               }%>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>
