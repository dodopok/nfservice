﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<div id="menu-tabs" class="center">
    <ul>
        <li class="active"><a href="#">Principal</a></li>
    </ul>
</div>
<div class="scroll-menu">
    <div class="smc-1">
        <div class="smc-2">
            <div class="smc-3">
                <div id="content-scroll">
                    <div id="content-holder">
                        <div class="pane">
                            <ul class="menu-items">
                                <li><a href="<%: Url.Action("List", "Empresas") %>">
                                    <img src="/Content/images/function-icons/book_48.png" alt="" /><span>Empresas</span></a>
                                    <div class="tooltip">
                                        Empresas</div>
                                </li>
                                <li><a href="<%: Url.Action("Relatorios","RelatoriosGerenciais") %> ">
                                    <img src="/Content/images/function-icons/table_green_48.png" alt="" /><span>Relatórios
                                        Gerenciais </span></a>
                                    <div class="tooltip">
                                        Gerencia seus Relatórios Gerenciais</div>
                                </li>
                                <li><a href="<%: Url.Action("Relatorios","RelatoriosFinanceiros") %> ">
                                    <img src="/Content/images/function-icons/table_48.png" alt="" /><span>Relatórios Financeiros
                                    </span></a>
                                    <div class="tooltip">
                                        Gerencia seus Relatórios Financeiros</div>
                                </li>
                                <li><a href="<%: Url.Action("List", "Planos") %>">
                                    <img src="/Content/images/function-icons/adianta.png" alt="" /><span>Planos </span>
                                </a>
                                    <div class="tooltip">
                                        Planos </</div>
                                </li>
                                <li><a href="<%: Url.Action("BoletosEmAberto","Pagamentos") %>">
                                    <img src="/Content/images/function-icons/paper_content_chart_48.png" alt="" /><span>Boletos
                                        Em Aberto</span></a>
                                    <div class="tooltip">
                                        Boletos Em Aberto</</div>
                                </li>
                                <li><a href="<%: Url.Action("BoletosPagos","Pagamentos") %>">
                                    <img src="/Content/images/function-icons/paper_content_chart_48.png" alt="" /><span>Boletos
                                        Pagos</span></a>
                                    <div class="tooltip">
                                        Boletos Pagos</</div>
                                </li>
                                <li><a href="<%: Url.Action("BoletosBloqueados","Pagamentos") %>">
                                    <img src="/Content/images/function-icons/paper_content_chart_48.png" alt="" /><span>Boletos
                                        Bloqueados</span></a>
                                    <div class="tooltip">
                                        Boletos Bloqueados</</div>
                                </li>
                                <li><a href="<%: Url.Action("List", "Usuarios") %>">
                                    <img src="/content/images/function-icons/users_two_48.png" alt="" /><span>Usuários</span></a>
                                    <div class="tooltip">
                                        Usuários</div>
                                </li>
                                <li><a href="<%: Url.Action("List", "GerentesNotas") %>">
                                    <img src="/content/images/function-icons/users_two_48.png" alt="" /><span>Gerentes de Notas</span></a>
                                    <div class="tooltip">
                                        Gerentes de Notas</div>
                                </li>
                                <li><a href="<%: Url.Action("DownloadCSV", "Newsletters") %>" target="_blank">
                                    <img src="/content/images/function-icons/mail_48.png" alt="" style="" /><span>Obter
                                        e-mails de Newsletter</span></a>
                                    <div class="tooltip">
                                        Obter e-mails de Newsletter</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="scrollbar">
                    <div class="sb-1">
                        <div class="sb-2">
                            <div class="sb-3">
                                <a id="scrollleft" href="#">Left</a> <a id="scrollright" href="#">Right</a>
                                <div class="ui-slider ui-slider-horizontal">
                                    <div id="scrollbar" style="left: 0%;" class="ui-slider-handle">
                                        <a href="#"><span><span></span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearingfix">
                </div>
            </div>
        </div>
    </div>
</div>
