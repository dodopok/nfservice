﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div id="adminbar">Bem-vindo, 
<strong>
<%: new MvcExtensions.Security.Session.Context(this.ViewContext.HttpContext).GetLoggedUser().Name %>
</strong> <%: Html.ActionLink("Sair", "LogOut", "Home") %>
</div>