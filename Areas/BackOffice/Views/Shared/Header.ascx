<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<meta http-equiv="content-type" content="text/html; charset=windows-1251" />

<link rel="stylesheet" type="text/css" href="/Content/css/styles.css" media="screen" />

<script type="text/javascript" src="/Scripts/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery.visualize.js"></script>
<script type="text/javascript" src="/scripts/jquery.validate.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="/Scripts/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="/Scripts/jquery.maskedinput-1.2.2.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.ui.core.js"></script>
<script type="text/javascript" src="/Scripts/custom.js"></script>
<script type="text/javascript" src="/scripts/jquery.maskMoney.0.2.js"></script>

