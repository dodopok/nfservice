﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Newsletter>>" %>
<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Newsletter.csv\""); %>
Nome;Email
<%foreach (var item in Model ) 
{ %>
<%:item.Nome %>;<%:item.Email %>
<%} %>
