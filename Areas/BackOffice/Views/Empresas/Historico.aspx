﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.HistoricoEmpresa>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Histórico de Bloqueio/Desbloqueio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%
    var empresa = (Domain.Entities.Empresa)ViewData["empresa"];
%>
<div class="container_12">
	<div class="grid_12">
		<div class="sb-box">
			<div class="sb-box-inner content">
				<div id="headerFiltro">
					<h3>Filtragem</h3>&nbsp;&nbsp;
					<img src="/Content/images/function-icons/search_48.png" alt="Mostrar ou Esconder" class="fright" width="35px" />
				</div>
				<div class="bcont">
					<table cellspacing="0" cellpadding="0" width="100%">
						<tbody>
							<tr>
								<td>
								<% using (Html.BeginForm("Historico", "Empresas", FormMethod.Post, new { id = "form", name = "form" })) {  %>
									<%: Html.Hidden("id", empresa.ID)%>
										<div id="formularioEmpresas" style="height:auto;">
											<div class="esquerda">
												<b>Data Início</b><br />
												<p>
													<%: Html.TextBox("DataInicio", null, new { @maxlength = "128" })%>
												</p>
											</div>
											<div class="direita">
												<b>Data Fim</b><br />
												<p>
													<%: Html.TextBox("DataFim", null, new { @maxlength = "128" })%>
												</p>
											</div>								   
											<div class="esquerda">
												<p>
													<button id="Button1" class="button green" type="submit" value="Save">
														<span>Filtrar</span>
													</button>
												</p>
											</div>
										</div>
									<% } %>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
		    </div>
        </div>
        <br />        
		<div class="sb-box">  
            <div class="sb-box-inner content">
				<div class="header">
					<h3>Histórico de Bloqueio/Desbloqueio</h3>
					<ul class="tabs">
						<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
					</ul>
				</div>

			<div class="bcont">
                <% foreach(var item in Model) 
                    { %>                       
                        
				<div id="divDesbloqueio" >
                    <div class="esquerda" style="width:100%;height:auto;float:none;">
						<b class="titDestaqueBloqueio"><%: item.Nome %> </b> - <span class="titConteudoBloqueio"><%: item.Data %></span>
                        <br />
                    </div>
					<div class="esquerda" style="padding-left: 10px;height:auto;width:100%;float:none;">
						<span class="titConteudoBloqueio">Descrição</span>
                        <br />
                        <%: item.Descricao %>
					</div>
				</div>
                <% } %>						
			</div>
			</div>
		</div>
	</div>
	<div class="clearingfix">
	</div>
</div>
</asp:Content>

<asp:Content ID="Content3" contentplaceholderid="HeaderContent"  runat="server">

<script type="text/javascript">

    $(document).ready(function () {

        $("#DataInicio").mask("99/99/9999");
        $("#DataFim").mask("99/99/9999");

        $("#formularioEmpresas").hide();

        $("#headerFiltro").click(function () {

            var visible = $("#formularioEmpresas").css("display");

            if (visible == "none")
                $("#formularioEmpresas").show();
            else
                $("#formularioEmpresas").hide();
        });

        $("#erro").hide();
        $(".note.loading").hide();

        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            }
        });
    });
</script>

</asp:Content>

