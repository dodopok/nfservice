﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Empresa>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <style type="text/css">
            html, body {height:100%;}
            body {font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;}
            .tbRelatorio{margin-left: 200px;}
            table.tbRelatorio tr th{font-size: 15px;font-weight: bold;}
            
            table.infotable {margin-bottom:15px;text-align:left;}
            table.infotable  tr td, table.infotable tr th {border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left;}
            table.infotable  tr td.small, table.infotable tr th.small {width:20px;text-align:center;}
            table.infotable  tr td.small input[type="checkbox"], table.infotable tr th.small  input[type="checkbox"] {vertical-align:middle;}
            table.infotable thead tr {background-color:#f5f5f5;}
            table.infotable th {font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);}
            table.infotable tbody tr.selected {background-color:#fdffea !important;}
            
            
            .divRelatNotas
            {    
                float: left;
                padding: 20px;
                text-align: left;
            }
            
            span
            {       
                color: #3B6CCA;
                font-size: 25px;
                font-weight: bold;
                padding: 35px;
                text-decoration: underline;            
            }
        </style>
    
    <title>        
        EMPRESA
    </title>
</head>
<body>
<%    
    var Cidade = ViewData["cidade"];
    var Estado = ViewData["estado"];
    var Status = ViewData["Status"];
    
    var filtro = (WebUI.Areas.BackOffice.ViewModels.Relatorios.FiltroEmpresas)ViewData["Filtro"];
    
    var CNPJ = filtro.CNPJ;
    var NomeFantasia = filtro.NomeFantasia;
    var RazaoSocial = filtro.RazaoSocial;
    var UsuarioResponsavel = filtro.UsuarioResponsavel;
    
%>
    <div>
        <center><b>EMPRESA</b></center>
        <br />
            <center>
                <b>Total de Empresas Cadastradas: </b><%: Model.Count() %>
            </center>
        <br />
        <br />

        <span> Filtros </span>
        <center>
        <div class="divRelatNotas">
            <b>Razão Social :</b> <%: RazaoSocial == null ? "Qualquer" : RazaoSocial%>
            <br />
            <br />
            <b>Nome Fantasia: </b><%: NomeFantasia == null ? "Qualquer" : NomeFantasia%>
            <br />
            <br />
            <b>CNPJ:</b> <%: CNPJ == null ? "Qualquer" : CNPJ%>
            <br />
            <br />
            <b>Usuário Responsável: </b><%: UsuarioResponsavel == null ? "Qualquer" : UsuarioResponsavel%>
        </div>
        <div class="divRelatNotas" >
            <b>Estado: </b><%: Estado == null ? "Qualquer" : Estado%>
            <br />
            <br />
            <b>Cidade: </b><%: Cidade == null ? "Qualquer" : Cidade%>
            <br />
            <br />
            <b>Status: </b><%: Status == null ? "Qualquer" : Status%>
        </div>
        </center>

        <br />
        <br />
        <br />

        <center style="clear:both;">
            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
				<thead>
					<tr>
						<th>
							Razão Social
						</th>
						<th>
							Nome Fantasia
						</th>
						<th>
							CNPJ
						</th>
						<th>
							E-mail
						</th>
						<th>
							Status
						</th>
                        <th>
                            Responsável
                        </th>
                        <th>
                            Estado / Cidade
                        </th>   
					</tr>
				</thead>
			    <tbody>
				    <% foreach (var item in Model) { %>
				    <tr>
					    <td>
						    <%: item.RazaoSocial %>
					    </td>
					    <td>
						    <%: item.NomeFantasia %>
					    </td>
					    <td>
						    <%: item.CNPJ %>
					    </td>
					    <td>
						    <%: item.Email %>
					    </td>
					    <td>
						    <%: ((item.isAtiva) ? "Ativa" : "Inativa") %>
					    </td>
                        <td>
                            <%: item.UsuarioResponsavel.Nome %>
                        </td>
                        <td>
                            <%: string.Format("{0} / {1}", item.EnderecoPrincipal.Cidade.Estado.Sigla, item.EnderecoPrincipal.Cidade.Nome) %>
                        </td>
				    </tr>
				    <% } %>
			    </tbody>
			</table>
        </center>
    </div>
</body>
</html>
