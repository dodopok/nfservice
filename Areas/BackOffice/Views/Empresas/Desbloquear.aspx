﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/Sistema/Views/Shared/Sistema.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Empresa>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Desbloqueio da Empresa <%: Model.RazaoSocial %>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="container_12">
	<div class="grid_12">
		<div class="sb-box">
			<div class="sb-box-inner content">
				<div class="header">
					<h3>Desbloqueio de Empresa</h3>
					<ul class="tabs">
						<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
					</ul>
				</div>
				<div class="bcont">
					<% using (Html.BeginForm("Desbloquear", "Empresas", FormMethod.Post, new { id = "form", name = "form"})) {%>
					<%: Html.Hidden("ID", Model.ID) %>

					<div id="dadosRestantes">
						<p>
							<b>Motivo </b><br />
							<%: Html.TextBox("Nome", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							<%: Html.ValidationMessage("Nome") %>
						</p>
						<p>
							<b>Descrição </b><br />
							<%: Html.TextArea("Descricaos", "", new { @class = "inputtext medium", @maxlength = "250"})%>
							<%: Html.ValidationMessage("Descricaos")%>
						</p>
					<div>
					<p>
						<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
							<span>Cancelar</span>
						</button>
						<button id="enviar" class="button green" type="submit" value="Save">
							<span>Desbloquear</span>
						</button>
					</p>
					</div>
					<% } %>
				</div>
                </div>
			</div>
		</div>
	</div>
	<div class="clearingfix">
	</div>
</div>
</asp:Content>

<asp:Content ID="Content3" contentplaceholderid="HeaderContent"  runat="server">
<script type="text/javascript">

    $(document).ready(function () {

        $("#erro").hide();
        $(".note.loading").hide();

        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            },
            rules: {
                Nome: "required",
                Descricaos: "required"
            },
            messages: {
                Nome: "Este campo é obrigatório.",
                Descricaos: "Este campo é obrigatório."
            }
        });
    });
</script>
</asp:Content>