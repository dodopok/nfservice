﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master"
    Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Empresa>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Empresas
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% 
        var status = ViewData["StatusID"] == null ? new List<Domain.Entities.StatusEmpresa>() : (IEnumerable<Domain.Entities.StatusEmpresa>)ViewData["StatusID"];
        var estados = ViewData["Estados"] == null ? new List<Domain.Entities.Estado>() : (IEnumerable<Domain.Entities.Estado>)ViewData["Estados"];
        var cidades = ViewData["Cidades"] == null ? new List<Domain.Entities.Cidade>() : (IEnumerable<Domain.Entities.Cidade>)ViewData["Cidades"];
        var estadoSelecionado = ViewData["estadoSelecionado"] == null ? "" : ViewData["estadoSelecionado"];
        var cidadeSelecionada = ViewData["cidadeSelecionada"] == null ? "" : ViewData["cidadeSelecionada"];
    %>
    <div class="container_12">
        <div class="grid_12">
            <%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString())
              { %>
            <div id="sucesso" class="message success">
                <span class="strong">SUCESSO!</span> Registro salvo com sucesso!
            </div>
            <%} %>
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div id="headerFiltro">
                        <h3>
                            Filtragem</h3>
                        &nbsp;&nbsp;
                        <img src="/Content/images/function-icons/search_48.png" alt="Mostrar ou Esconder"
                            class="fright" width="35px" />
                    </div>
                    <div class="bcont">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <% using (Html.BeginForm("List", "Empresas", FormMethod.Post, new { id = "form", name = "form" }))
                                           {  %>
                                        <%: Html.Hidden("tipoDestino") %>
                                        <div id="formularioEmpresas">
                                            <div class="esquerda">
                                                <b>Razão Social</b><br />
                                                <p>
                                                    <%: Html.TextBox("RazaoSocial", null, new { @maxlength = "128" })%>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>Nome Fantasia</b><br />
                                                <p>
                                                    <%: Html.TextBox("NomeFantasia", null, new { @maxlength = "128" })%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>CNPJ</b><br />
                                                <p>
                                                    <%: Html.TextBox("CNPJ") %>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>Usuário Responsável</b><br />
                                                <p>
                                                    <%: Html.TextBox("UsuarioResponsavel") %>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>Estado</b><br />
                                                <p>
                                                    <%: Html.DropDownList("EstadoID", new SelectList(estados, "ID", "Nome",estadoSelecionado), "Selecione")%>
                                                </p>
                                            </div>
                                            <div class="direita">
                                                <b>Cidade</b><br />
                                                <p>
                                                    <%: Html.DropDownList("CidadeID", new SelectList(cidades, "ID", "Nome",cidadeSelecionada), "Selecione", new { @disabled = "disabled" })%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <b>Status</b><br />
                                                <p>
                                                    <%: Html.DropDownList("StatusID", new SelectList(status, "ID", "Rotulo"), "Selecione")%>
                                                </p>
                                            </div>
                                            <div class="esquerda">
                                                <p>
                                                    <button id="enviar" class="button green" type="submit" value="Save">
                                                        <span>Filtrar</span>
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                        <% } %>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br />
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <h3>
                            Empresas</h3>
                        <% if (Model != null && Model.Count() > 0)
                           {%>
                        <ul class="tabs">
                            <li id="enviarPdf" class="active" value="Save"><a href="#">
                                <img alt="Versão em PDF" width="14px" src="/Content/images/function-icons/pdf_ico.jpg" />
                                Obter PDF </a></li>
                            <li id="enviarXls" class="active" value="Save"><a href="#">
                                <img alt="Versão em Excel" width="14px" src="/Content/images/function-icons/excel_icon1.png" />
                                Obter Excel </a></li>
                            <li id="enviarHtm" class="active" value="Save"><a href="#">
                                <img alt="Versão para Impressão" width="13px" src="/Content/images/function-icons/paper_48.png" />
                                Obter Versão para Impressão </a></li>
                        </ul>
                        <%
                            }%>
                    </div>
                    <div class="bcont">
                        <%  if (Model.Any())
                                             { %>
                        <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                            <thead>
                                <tr>
                                    <th>
                                        Nome Fantasia
                                    </th>
                                    <th>
                                        E-mail
                                    </th>
                                    <th>
                                        <center>
                                            CNPJ</center>
                                    </th>
                                    <th>
                                        Responsável
                                    </th>
                                    <th style="width: 70px;">
                                        <center>
                                            Status</center>
                                    </th>
                                    <th style="width: 70px;">
                                        <center>
                                            Bloqueada</center>
                                    </th>
                                    <th style="width: 70px;">
                                        <center>
                                            Notas Emitidas</center>
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                        foreach (var item in Model)
                                        { %>
                                <tr>
                                    <td>
                                        <%: item.NomeFantasia%>
                                        <br />
                                        <i style='font-size: 10px;'>
                                            <%: string.Format("{0}/{1}", item.EnderecoPrincipal.Cidade.Nome, item.EnderecoPrincipal.Cidade.Estado.Sigla)%></i>
                                    </td>
                                    <td>
                                        <%: item.Email%>
                                    </td>
                                    <td>
                                        <center>
                                            <%: item.CNPJ%></center>
                                    </td>
                                    <td>
                                        <%: item.UsuarioResponsavel.Nome%>
                                    </td>
                                    <td>
                                        <center>
                                            <%: ((item.isAtiva) ? "Ativa" : "Inativa") %></center>
                                    </td>
                                    <td>
                                        <% if (item.isBloqueada)
                                           { %>
                                        <center>
                                            <a href="<%: Url.Action("Desbloquear", "Empresas", new { id = item.ID })%>" title="Bloqueada">
                                                <img src="/Content/images/onebit-icons/37.png" width="30px" alt="Bloqueada" title="Bloqueada" /></a></center>
                                        <% }
                                           else
                                           { %>
                                        <center>
                                            <a href="<%: Url.Action("Bloquear", "Empresas", new { id = item.ID })%>" title="Não Bloqueada">
                                                <img src="/Content/images/onebit-icons/39.png" width="30px" alt="Não Bloqueada" title="Não Bloqueada" /></a></center>
                                        <% } %>
                                    </td>
                                    <td>
                                        <center>
                                            <%: item.NotasFiscais.Count%></center>
                                    </td>
                                    <td class="small">
                                        <a class="action" href="#"></a>
                                        <div class="opmenu">
                                            <ul>
                                                <li>
                                                    <%: Html.RouteLink("Exibir Gerentes de Notas", "BackOffice_Empresas_Gerentes_Notas", new { empresaID = item.ID, action = "List" })%></li>
                                                <li>
                                                    <%: Html.RouteLink("Exibir Detalhes", "BackOffice_Empresas_Detalhes", new { empresaID = item.ID })%></li>
                                                <li>
                                                    <%: Html.ActionLink("Histórico", "Historico", new { id = item.ID })%></li>
                                            </ul>
                                            <div class="clear">
                                            </div>
                                            <div class="foot">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <% }  %>
                            </tbody>
                        </table>
                        <% }
                                             else
                                             {%>
                        <center>
                            <h3 style="color: #FF0000;">
                                NENHUM REGISTRO FOI ENCONTRADO.</h3>
                        </center>
                        <%} %>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <% var cidadeSelecionada = ViewData["cidadeSelecionada"] == null ? "" : ViewData["cidadeSelecionada"]; %>
    <script type="text/javascript">

        $(document).ready(function () {
            if ($("#EstadoID").val() != "") {
                $.ajaxSetup({ cache: false, async: false });
                var selectedItem = $("#EstadoID").val();
                if (selectedItem == "" || selectedItem == 0) {
                    //Do nothing or hide...?
                } else {
                    $.post("/Services/Localizacao/GetCidades/" + $("#EstadoID > option:selected").attr("value"), function (data) {
                        var items = "";
                        items += "<option>Selecione</option>";
                        $.each(data, function (i, data) {
                            items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
                        });
                        $("#CidadeID").removeAttr('disabled');
                        $("#CidadeID").html(items);
                    });
                }
                $("#CidadeID").val("<%: cidadeSelecionada %>");
            }

            $('#EstadoID').change(function () {
                $.ajaxSetup({ cache: false });
                var selectedItem = $(this).val();
                if (selectedItem == "" || selectedItem == 0) {
                    //Do nothing or hide...?
                } else {
                    $.post("/Services/Localizacao/GetCidades/" + $("#EstadoID > option:selected").attr("value"), function (data) {
                        var items = "";
                        items += "<option>Selecione</option>";
                        $.each(data, function (i, data) {
                            items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
                        });
                        $("#CidadeID").removeAttr('disabled');
                        $("#CidadeID").html(items);
                    });
                }
            });

            $("#CNPJ").mask("99.999.999/9999-99");

            $("#formularioEmpresas").hide();

            $("#headerFiltro").click(function () {

                var visible = $("#formularioEmpresas").css("display");

                if (visible == "none")
                    $("#formularioEmpresas").show();
                else
                    $("#formularioEmpresas").hide();
            });

            $('#enviarXls').click(function () {
                $("#tipoDestino").val('xls');
                $("#form").submit();
                $("#tipoDestino").val('');
            });
            $('#enviarHtm').click(function () {
                $("#tipoDestino").val('htm');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });
            $('#enviarPdf').click(function () {
                $("#tipoDestino").val('pdf');
                $("#form").attr("target", "_blank");
                $("#form").submit();
                $("#form").attr("target", "_self");
                $("#tipoDestino").val('');
            });
        });

    </script>
</asp:Content>
