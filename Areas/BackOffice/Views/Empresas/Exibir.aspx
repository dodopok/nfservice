﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Empresa>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Detalhes da empresa <%: Model.RazaoSocial %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container_12">
	<div class="grid_12">
        <div class="sb-box">
			<div class="sb-box-inner content">
				<div class="header">
					<h3>Detalhes</h3>
				</div>
                <div class="bcont">
                    <table width="950">
                        <tr>
                            <td style="width:50%;">
                                <p>
                                    <b>Razão Social</b><br />
                                    <%: Model.RazaoSocial %>
                                </p>
                            </td>
                            <td style="width:50%;">
                                <p>
                                    <b>CNPJ</b><br />
                                    <%: Model.CNPJ %>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <p>
                                    <b>Inscrição Municipal</b><br />
                                    <%: Model.InscricaoMunicipal %>
                                </p>
                            </td>
                            <td style="width:50%;">
                                <p>
                                    <b>Telefone Alternativo</b><br />
                                    <%: Model.TelefoneAlternativo %>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <p>
                                    <b>Status</b><br />
                                    <%: Model.Status.Rotulo %>
                                </p>
                            </td>
                            <td style="width:50%;">
                                <p>
                                    <b>Endereço de Cobrança</b><br />
                                    <%: Model.EnderecoCobranca.Logradouro %>, <%: Model.EnderecoCobranca.Bairro %><br />
                                    <%: Model.EnderecoCobranca.CEP %><br /><%: Model.EnderecoCobranca.Cidade.Nome %> - 
                                    <%: Model.EnderecoCobranca.Cidade.Estado.Nome %>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <p>
                                    <b>Usuário Responsável</b><br />
                                    Nome: <%: Model.UsuarioResponsavel.Nome %><br />
                                    CPF: <%: Model.UsuarioResponsavel.CPF%><br />
                                    E-mail: <%: Model.UsuarioResponsavel.Email%><br />
                                    Telefone: <%: Model.UsuarioResponsavel.Telefone%>
                                </p>
                            </td>
                            <td style="width:50%;">
                                <p>
                                    <b>Nome Fantasia</b><br />
                                    <%: Model.NomeFantasia %>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <p>
                                    <b>Inscrição Estadual</b><br />
                                    <%: Model.InscricaoEstadual %>
                                </p>
                            </td>
                            <td style="width:50%;">
                                <p>
                                    <b>Telefone Principal</b><br />
                                    <%: Model.TelefonePrincipal %>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <p>
                                    <b>E-mail</b><br />
                                    <%: Model.Email %>
                                </p>
                            </td>
                            <td style="width:50%;">
                                <p>
                                    <b>Endereço Principal</b><br />
                                    <%: Model.EnderecoPrincipal.Logradouro %>, <%: Model.EnderecoPrincipal.Bairro %><br />
                                    <%: Model.EnderecoPrincipal.CEP %><br /><%: Model.EnderecoPrincipal.Cidade.Nome %> - 
                                    <%: Model.EnderecoPrincipal.Cidade.Estado.Nome %>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <p>
                                    <b>Número de Notas Fiscais Cadastradas</b><br />
                                    <%: Model.NotasFiscais.Count() %>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
			</div>
		</div>
    </div>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
