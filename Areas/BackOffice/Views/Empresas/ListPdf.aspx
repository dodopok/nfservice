﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Empresa>>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>        
        EMPRESA
    </title>
</head>
<body>
    <div>
        <center><b>EMPRESA</b></center>
        <br />
            <center>
                <b>Total de Empresas Cadastradas: </b><%: Model.Count() %>
            </center>
        <br />
        <br />

        <center style="clear:both;">
            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
				<thead>
					<tr bgcolor="#f5f5f5" color="#305B7F">
						<td >
							Razão Social
						</td>
						<td>
							Nome Fantasia
						</td>
						<td>
							CNPJ
						</td>
						<td>
							E-mail
						</td>
						<td>
							Status
						</td>
                        <td>
                            Responsável
                        </td>
                        <td>
                            Estado / Cidade
                        </td>   
					</tr>
				</thead>
			    <tbody>
				    <% foreach (var item in Model) { %>
				    <tr>
					    <td>
						    <%: item.RazaoSocial %>
					    </td>
					    <td>
						    <%: item.NomeFantasia %>
					    </td>
					    <td>
						    <%: item.CNPJ %>
					    </td>
					    <td>
						    <%: item.Email %>
					    </td>
					    <td>
						    <%: ((item.isAtiva) ? "Ativa" : "Inativa") %>
					    </td>
                        <td>
                            <%: item.UsuarioResponsavel.Nome %>
                        </td>
                        <td>
                            <%: string.Format("{0} / {1}", item.EnderecoPrincipal.Cidade.Estado.Sigla, item.EnderecoPrincipal.Cidade.Nome) %>
                        </td>
				    </tr>
				    <% } %>
			    </tbody>
			</table>
        </center>
    </div>
</body>
</html>
