<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Empresa>>" %>
<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Empresas.csv\""); %>

Razão Social;Nome Fantasia;CNPJ;Email;Status;Usuário Responsável;Notas Emitidas

<% foreach(var empresa in Model)
   { %>
        <%= empresa.RazaoSocial %>;<%= empresa.NomeFantasia %>;<%= empresa.CNPJ %>;<%= empresa.Email %>;<%= ((empresa.isAtiva) ? "Ativa" : "Inativa ") %>;<%= empresa.UsuarioResponsavel.Nome %>;<%= empresa.NotasFiscais.Count %>
<% } %>