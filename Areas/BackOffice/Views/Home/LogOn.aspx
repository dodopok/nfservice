﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.ViewModels.LogOnModel>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
	<title>NFService</title>
	<link rel="stylesheet" type="text/css" href="/Content/css/styles.css" media="screen" />
	<script type="text/javascript" src="/Scripts/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery-ui-1.8.2.custom.min.js"></script>
	<script type="text/javascript" src="/Scripts/jquery.validate.js"></script>
	<script type="text/javascript" src="/Scripts/jquery.visualize.js"></script>
	<script type="text/javascript" src="/Scripts/custom.auth.js"></script>
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="/content/ie7.css" media="screen" />
	<![endif]-->
	<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="/content/ie8.css" media="screen" />
	<![endif]-->
	<!--[if IE]>
	<script language="javascript" type="text/javascript" src="/scripts/excanvas.js"></script>
	<![endif]-->

	<script type="text/javascript">

		$(document).ready(function () {

			$("#processando").hide();
			$("#erro").hide();

			<% if (Model.MensagemErro != null && Model.MensagemErro != string.Empty) { %>
			    $("#erro").show();
			<% } %>

			$("#form").validate({
				meta: "validate",
				invalidHandler: function (form, validator) {
					$("#erro").html("Cheque os campos abaixo");
					$("#erro").show();
				},

				submitHandler: function (form) {
					$("#erro").hide();
					$("#login").hide();
					$("#processando").show();
					form.submit();
				},

				rules: {
					Email: {
						required: true,
						email: true
					},
					Senha: "required"
				},
				messages: {
					Email: {
						required: "Preencha o e-mail",
						email: "E-mail inválido"
					},
					Senha: "Preencha a senha"
				}
			});
		});
	</script>
</head>
<body>
<div id="page-body">
	<div id="wrapper">
		<!-- Authorization [begin] -->
		<div class="sb-box auth">
			<div class="sb-box-inner content">
				<!-- Authorization block header [begin] -->
				<div class="header"><h3>Por favor, faça o login</h3></div>
				<!-- Authorization block header [end] -->
				<div class="bcont">
					<!-- Authorization content [begin] -->

					<!-- form elements [start] -->
					<% using (Html.BeginForm("LogOn", "Home", FormMethod.Post, new { id = "form", name = "form"})) 
                    {%>
						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> 
							<% if (Model.MensagemErro != null && Model.MensagemErro != string.Empty)
							   { %>
							    <%: Model.MensagemErro %>
							<% } 
                                else 
                                { %>
							        Verifique todos os campos abaixo.
							<%  } %>
						</div>
						<div>
						<p>
							<label>Email</label>
							<%: Html.TextBoxFor(model => model.Email, new { @class = "inputtext small" }) %>
						</p>
						<p>
							<label>Senha</label>
							<%: Html.PasswordFor(model => model.Senha, new { @class = "inputtext small" }) %>
						</p>
						<p>
							<button id="login" name="login" type="submit" class="button blue floatright"><span>Login</span></button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>
						<div class="clearingfix"></div>
					<% } %>
					<!-- form elements [end] -->

					<!-- Authorization content [end] -->
				</div>
			</div>
		</div>
		<!-- Authorization [end] -->
	</div>
</div></body>
</html>