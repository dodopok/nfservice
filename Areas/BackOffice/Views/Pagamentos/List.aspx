﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Pagamento>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Pagamentos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%
    var filtroID = ViewData["FiltroID"] != null ? Convert.ToInt32(ViewData["FiltroID"]) : 1;
%>
	<div class="container_12">
		<div class="grid_12">
            <table>
            <tr>
                <td style="width:100px">
                    <%: Html.ActionLink("Pagos", "List", new { filtroID = (int)WebUI.Areas.BackOffice.Controllers.Filtro.Pagos }) %>
                </td>
                <td style="width:100px">
                    <%: Html.ActionLink("Em Aberto", "List", new { filtroID = (int)WebUI.Areas.BackOffice.Controllers.Filtro.EmAberto }) %>
                </td>
                <td style="width:100px">
                    <%: Html.ActionLink("Bloqueados", "List", new { filtroID = (int)WebUI.Areas.BackOffice.Controllers.Filtro.Bloqueados }) %>
                </td>
            </tr>
            </table>
            <br />

			<%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString()) { %>
			    <div id="sucesso" class="message success">
				    <span class="strong">SUCESSO!</span> Registro salvo com sucesso!
			    </div>
			<%} %>
			<%if (Request.QueryString["NovaData"] == MvcExtensions.Controllers.Message.Sucess.ToString()) { %>
			    <div id="Div1" class="message success">
				    <span class="strong">SUCESSO!</span> Boleto enviado com sucesso!
			    </div>
			<%} %>
			<%if (Request.QueryString["Desbloqueio"] == MvcExtensions.Controllers.Message.Sucess.ToString())
            { %>
			    <div id="Div2" class="message success">
				    <span class="strong">SUCESSO!</span> Desbloqueio efetuado com sucesso!
			    </div>
			<% } %>
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Pagamentos</h3>
						<ul class="tabs">
							<li id="enviarXls" class="active" value="Save">
								<a href="#" >
									<img alt="Versão em Excel" width="14px" src="/Content/images/function-icons/excel_icon1.png" />
										Obter Excel
								</a>
							</li>
							<li id="enviarHtm" class="active" value="Save">
								<a href="#" >
									<img alt="Versão para Impressão" width="13px" src="/Content/images/function-icons/paper_48.png" />
										Obter Versão para Impressão
								</a>
							</li>
                            <li id="obterArquivoRemessa" class="active" value="Save">
                                <%: Html.ActionLink("Obter arquivo de Remessa", "GetArquivoRemessa") %>
							</li>
						</ul>
					</div>
					<div class="bcont">
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								<th>
									<center>
										Data de Emissão
									</center>
								</th>
								<th>
									Razão Social
								</th>
								<th>
									CNPJ
								</th>
								<th>
									<center>
										Notas Emitidas
									</center>
								</th>
								<th>
									<center>
										Notas Possíveis
									</center>
								</th>
								<th>
									<center>
										Foi Pago
									</center>
								</th>
								<th class="small">
								</th>
								</tr>
							</thead>
							<tbody>
								<% foreach (var item in Model) { %>
								<tr>
									<td>
										<center>
											<%: item.DataEmissaoBoleto.ToShortDateString() %>
										</center>
									</td>
									<td>
										<%: item.Empresa.NomeFantasia %>
									</td>
									<td>
										<%: item.Empresa.CNPJ %>
									</td>
									<td>
										<center>
											<%: item.QuantidadeNotasEmitidas %>
										</center>
									</td>
									<td>
										<center>
											<%: item.FoiPago ? "Sim" : "Não"  %>
										</center>
									</td>
                                    <td>    </td>
                                    
									<td class="small">
                                    <% if (filtroID != (int)WebUI.Areas.BackOffice.Controllers.Filtro.Pagos)
                                       { %>
										<a class="action" href="#"></a>
										<div class="opmenu">
                                            <% if (filtroID == (int)WebUI.Areas.BackOffice.Controllers.Filtro.Bloqueados)
                                               { %>
                                                <ul>
                                                    <a onclick="Desbloquear('<%: item.ID %>');" hidefocus="true" style="outline: medium none;">Desbloquear</a>
                                                    <%--<%: Html.ActionLink("Desbloquear", "", new { pagamentoID = item.ID, filtroID = filtroID }, new { @onclick = "Desbloquear(" + item.ID + ");" }) %>--%>
                                                </ul>
                                            <% } %>
											<ul>
												<%: Html.ActionLink("Reenviar Boleto", "ReenviarBoleto", new { pagamentoID = item.ID, filtroID = filtroID })%>
											</ul>
											<div class="clear">
											</div>
											<div class="foot">
											</div>
										</div>
                                    <% } %>
									</td>
								</tr>
								<% } %>
                                <% using (Html.BeginForm("DesbloquearBoleto", "Pagamentos", FormMethod.Post, new { id = "form-desbloqueio", name = "form-desbloqueio" }))
                                   { %>
                                    <%: Html.Hidden("idDesbloqueio") %>
                                   <% } %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

    function Desbloquear(ID) {
        var resp = confirm('Deseja realmente desbloquear esse pagamento ?');
        if (resp) {
            alert('ID  ' + ID);
            $("#idDesbloqueio").val(ID);
            $("#form-desbloqueio").submit();
        }
    }

	$(document).ready(function () {

		$("#DataInicioPagamento").mask("99/99/9999");
		$("#DataFimPagamento").mask("99/99/9999");
		$("#DataInicioEmissao").mask("99/99/9999");
		$("#DataFimEmissao").mask("99/99/9999");
		$("#DataInicioValidadeBoleto").mask("99/99/9999");
		$("#DataFimValidadeBoleto").mask("99/99/9999");
		$("#DataInicioValidadeAcesso").mask("99/99/9999");
		$("#DataFimValidadeAcesso").mask("99/99/9999");

		$("#formularioPagamentos").hide();

		$("#headerFiltro").click(function () {

			var visible = $("#formularioPagamentos").css("display");

			if (visible == "none")
				$("#formularioPagamentos").show();
			else
				$("#formularioPagamentos").hide();
		});

		$('#enviarXls').click(function () {
			$("#tipoDestino").val('xls');
			$("#form").submit();
		});

		$('#enviar').click(function () {
			$("#tipoDestino").val('');
		});

		$('#enviarHtm').click(function () {

			$("#form").attr('target', '_blank');
			$("#tipoDestino").val('htm');
			$("#form").submit();
			$("#tipoDestino").val('');
			$("#form").attr('target', '_self');
        });
	});

</script>
</asp:Content>