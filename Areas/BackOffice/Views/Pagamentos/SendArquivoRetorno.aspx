﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Envio de arquivo de retorno
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Envio de arquivo de retorno bancário</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "PagamentosEmAberto") %></li>
						</ul>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("SendArquivoRetorno", "Pagamentos", FormMethod.Post, new { enctype = "multipart/form-data", id = "form", name = "form" }))
                        { %>
						
						<% if (ViewData["MensagemErro"] != null) { %>
							<div id="erro" class="message error">
								<span class="strong">OCORREU UM ERRO AO ENVIAR O ARQUIVO!</span> 
								<%: ViewData["MensagemErro"].ToString()  %>
							</div>
						<% } %>

					   <div>
					   <b>Envio de arquivo de retorno bancário</b>
						<p>
							<input type="file" id="arquivos[]" name="arquivos[]" class="files" />
						</p>						
						<hr />
						<p>
							<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("BoletosEmAberto") %>';">
								<span>Cancelar</span>
							</button>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar Arquivos</span>
							</button>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">

</asp:Content>
