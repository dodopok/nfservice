﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Pagamento>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Pagamentos em Aberto
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% 
	var estados = ViewData["Estados"] == null ? new List<Domain.Entities.Estado>() : (IEnumerable<Domain.Entities.Estado>)ViewData["Estados"];
	var cidades = ViewData["Cidades"] == null ? new List<Domain.Entities.Cidade>() : (IEnumerable<Domain.Entities.Cidade>)ViewData["Cidades"];
    var estadoSelecionado = ViewData["Estado"] == null ? null : ViewData["Estado"];
    var cidadeSelecionado = ViewData["Cidade"] == null ? null : ViewData["Cidade"];
%>
<div class="container_12">
		<div class="grid_12">
            <div class="sb-box">
				<div class="sb-box-inner content">
					<div id="headerFiltro">
						<h3>Filtragem</h3>&nbsp;&nbsp;
						<img src="/Content/images/function-icons/search_48.png" alt="Mostrar ou Esconder" class="fright" width="35px" />
					</div>
					<div class="bcont">
					    <table cellspacing="0" cellpadding="0" width="100%">
							<tbody>
								<tr>
									<td>
									<% using (Html.BeginForm("BoletosEmAberto", "Pagamentos", FormMethod.Post, new { id = "form", name = "form" })) {  %>
											<div id="formularioBoletos">
												<div class="esquerda">
													<b>Razão Social</b><br />
													<p>
														<%: Html.TextBox("RazaoSocial", null, new { @maxlength = "128" })%>
													</p>
												</div>
												<div class="direita">
													<b>CNPJ</b><br />
													<p>
														<%: Html.TextBox("CNPJ") %>
													</p>
												</div>
												<div class="esquerda">
													<b>Estado</b><br />
													<p>
														<%: Html.DropDownList("EstadoID", new SelectList(estados, "ID", "Nome",estadoSelecionado != null ? estadoSelecionado : ""), "Selecione")%>
													</p>
												</div>
												<div class="direita">
													<b>Cidade</b><br />
													<p>
														<%: Html.DropDownList("CidadeID", new SelectList(cidades, "ID", "Nome", cidadeSelecionado != null ? cidadeSelecionado : ""), "Selecione", cidadeSelecionado == null ? new { @disabled = "disabled" } : null)%>
													</p>
												</div>

												 <div class="esquerda">
													<b>Data Início</b><br />
													<p>
														<%: Html.TextBox("DataInicio", null, new { @maxlength = "128" }) %>
													</p>
												</div>
												 <div class="esquerda">
													<b>Data Fim</b><br />
													<p>
														<%: Html.TextBox("DataFim", null, new { @maxlength = "128" }) %>
													</p>
												</div>
								   
												<div class="esquerda">
													<p>
														<button id="enviar" class="button green" type="submit" value="Save">
															<span>Filtrar</span>
														</button>
													</p>
												</div>
											</div>
										<% } %>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
        <br />
        <br />
		<div class="grid_12">
			
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Pagamentos Em Aberto</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Obter Arquivo de Remessa", "GetArquivoRemessa")%></li>
                            <li class="active"><%: Html.ActionLink("Enviar Arquivo de Retorno", "SendArquivoRetorno")%></li>
						</ul>
					</div>
					<div class="bcont">
                    <% if (Model.Any())
                    { %>
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								<th style="width:115px;">
									<center>Data de Emissão</center>
								</th>
								<th style="">
									Razão Social
								</th>
								<th style="width:115px;">
									<center>CNPJ</center>
								</th>
								<th style="width:135px;">
									<center>Qtde Notas Emitidas</center>
								</th>
								<th>
									Plano
								</th>
                                <th style="width:75px;">
                                    <center>Valor Pago</center>
                                </th>
								</tr>
							</thead>
							<tbody>
								<% foreach (var item in Model) { %>
								<tr>
									<td>
										<center><%: item.DataEmissaoBoleto.ToShortDateString() %></center>
									</td>
                                    <td>
                                    <%: item.Empresa.RazaoSocial %>
                                    </td>
                                    <td>
                                    <center><%: item.Empresa.CNPJ %></center>
                                    </td>
									<td>
                                        <center><%: item.QuantidadeNotasEmitidas %></center>
									</td>
                                    <td>
                                        <%: item.Plano.Nome %>
                                    </td>
                                    <td>
                                        <center><%: item.Valor.ToString("C") %></center>
                                    </td>
								</tr>
								<% } %>
							</tbody>
						</table>
                        <% }
                        else
                        {%> 
                        <center><h3 style="color:#FF0000;">NENHUM REGISTRO FOI ENCONTRADO.</h3></center>
                        <%} %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">    
    $(document).ready(function () {

        $('#EstadoID').change(function () {
            $.ajaxSetup({ cache: false });
            var selectedItem = $(this).val();
            if (selectedItem == "" || selectedItem == 0) {
                //Do nothing or hide...?
            } else {
                $.post("/Services/Localizacao/GetCidades/" + $("#EstadoID > option:selected").attr("value"), function (data) {
                    var items = "";
                    items += "<option>Selecione</option>";
                    $.each(data, function (i, data) {
                        items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
                    });
                    $("#CidadeID").removeAttr('disabled');
                    $("#CidadeID").html(items);
                });
            }
        });

        $("#CNPJ").mask("99.999.999/9999-99");
        $("#DataInicio").mask("99/99/9999");
        $("#DataFim").mask("99/99/9999");

        $("#formularioBoletos").hide();

        $("#headerFiltro").click(function () {

            var visible = $("#formularioBoletos").css("display");

            if (visible == "none")
                $("#formularioBoletos").show();
            else
                $("#formularioBoletos").hide();
        });

        $('#enviarXls').click(function () {
            $("#tipoDestino").val('xls');
            $("#form").submit();
            $("#tipoDestino").val('');
        });
        $('#enviarHtm').click(function () {
            $("#tipoDestino").val('htm');
            $("#form").attr("target", "_blank");
            $("#form").submit();
            $("#form").attr("target", "_self");
            $("#tipoDestino").val('');
        });
    });

</script>
</asp:Content>