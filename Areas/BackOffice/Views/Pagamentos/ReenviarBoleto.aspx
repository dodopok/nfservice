﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Pagamento>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Reenvio de Boleto
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%
    var FiltroID = ViewData["FiltroID"];
%>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Reenvio de Boleto</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
						</ul>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("EnviarBoleto", "Pagamentos", FormMethod.Post, new { id = "form", name = "form"})) {%>
						
						<div id="erro" class="message error">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>

						<%: Html.HiddenFor(model => model.ID) %>
                        <%: Html.Hidden("FiltroID", FiltroID)%>
						<div>
							<div class="esquerda">
							    <p>
								    <b>Nova Data de Vencimento</b><br />
								    <%: Html.TextBox("NovoVencimento","", new { @class = "inputtext medium", @maxlength = "128" })%>
								    <%: Html.ValidationMessage("NovoVencimento")%>
							    </p>
							</div>
                            <div class="direita">
                                <p>
                                    <b>Email de Destino</b>
                                    <br />
                                    <%: Html.TextBox("EmailDestino", Model.Empresa.UsuarioResponsavel.Email, new { @class = "inputtext medium", @maxlength = "128" }) %>
                                    <%: Html.ValidationMessage("EmailDestino") %>
                                </p>
                            </div>
						</div>
						<div>
						<hr />
						<p>
							<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
								<span>Cancelar</span>
							</button>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
	<script type="text/javascript">
	    $(document).ready(function () {
	        Validate();
	        $("#erro").hide();
	        $(".note.loading").hide();

	        $("#NovoVencimento").mask("99/99/9999");

	        $("#form").validate({
	            meta: "validate",
	            invalidHandler: function (form, validator) {
	                $("#erro").show();
	                $("#erro").focus();
	            },

	            submitHandler: function (form) {
	                $("#erro").hide();
	                $("#cancelar").hide();
	                $("#enviar").hide();
	                $("#processando").show();
	                form.submit();
	            },
	            rules: {
	                NovoVencimento: "required",
	                EmailDestino: {
	                    required: true,
	                    email: true
	                }
	            },
	            messages: {
	                NovoVencimento: "Este campo é obrigatório.",
	                EmailDestino: {
	                    required: "Este campo é obrigatório.",
	                    email: "Formato de e-mail inválido"
	                }
	            }
	        });
	    });
	</script>
    <script type="text/javascript" src="/Scripts/woow.validateextensions.js"></script>
	
</asp:Content>
