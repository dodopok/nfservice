﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<Domain.Entities.Pagamento>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Alteração de Vencimento
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Alteração de Vencimento</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "BoletosBloqueados") %></li>
						</ul>
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("DefinirNovoVencimento", "Pagamentos", FormMethod.Post, new { id = "form", name = "form" })) {%>
						
						<div id="erro" class="message error" style="display:none">
							<span class="strong">ERRO!</span> Verifique todos os campos abaixo.
						</div>

						<div>
							<div>
								<p>
									<b>Nova Data de Vencimento</b><br />
									<%: Html.TextBox("DataVencimento", "", new { @class = "inputtext medium", @maxlength = "128" })%>
									<%: Html.ValidationMessage("DataVencimento")%>
								</p>
							</div>
						</div>
						<div>
						<hr />
						<p>
							<button id="cancelar" class="button blue" type="reset" onclick="location.href='<%: Url.Action("List") %>';">
								<span>Cancelar</span>
							</button>
							<button id="enviar" class="button green" type="submit" value="Save">
								<span>Enviar</span>
							</button>
							<span id="processando" class="note loading">Processando...</span>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
	<script type="text/javascript">
		$(document).ready(function () {
			Validate();
			$(".note.loading").hide();

			$("#DataVencimento").mask("99/99/9999");

			$("#form").validate({
				meta: "validate",
				invalidHandler: function (form, validator) {
					$("#erro").show();
					$("#erro").focus();
				},

				submitHandler: function (form) {
					$("#erro").hide();
					$("#cancelar").hide();
					$("#enviar").hide();
					$("#processando").show();
					form.submit();
				},
				rules: {
				    DataVencimento: "required"
				},
				messages: {
				    DataVencimento: "Este campo é obrigatório."
				}
			});
		});
	</script>
	<script type="text/javascript" src="/Scripts/woow.validateextensions.js"></script>
	
</asp:Content>
