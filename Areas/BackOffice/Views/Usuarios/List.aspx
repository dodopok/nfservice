﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Usuario>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	List
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<% 
	   var estados = ViewData["Estados"] == null ? new List<Domain.Entities.Estado>() : (IEnumerable<Domain.Entities.Estado>)ViewData["Estados"];
	   var cidades = ViewData["Cidades"] == null ? new List<Domain.Entities.Cidade>() : (IEnumerable<Domain.Entities.Cidade>)ViewData["Cidades"];
	%>

	<div class="container_12">
		<div class="grid_12">
			<%if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Sucess.ToString())
			  { %>
			<div id="sucesso" class="message success">
				<span class="strong">SUCESSO!</span> Registro salvo com sucesso!    
			</div>
			<%} %>
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div id="headerFiltro">
						<h3>Filtragem</h3>&nbsp;&nbsp;
						<img src="/Content/images/function-icons/search_48.png" alt="Mostrar ou Esconder" class="fright" width="35px" />
					</div>
					<div class="bcont">
						<% using (Html.BeginForm("List", "Usuarios", FormMethod.Post, new { id = "form", name = "form" }))
						   {  %>
							<%: Html.Hidden("tipoDestino") %>
						<table cellspacing="0" cellpadding="0" width="100%">
							<tbody>
								<tr>
									<td>
										<div id="formularioUsuario">
											<div class="esquerda">
												<b>Nome</b><br />
												<p>
													<%: Html.TextBox("Nome", null, new { @maxlength = "128" })%>
												</p>
												<b>Email</b><br />
												<p>
													<%: Html.TextBox("Email", null, new { @maxlength = "128" })%>
												</p>
												<b>Data Início</b><br />
												<p>
													<%: Html.TextBox("DataCriacao") %>
												</p>
												<b>Nome da Empresa</b><br />
												<p>
													<%: Html.TextBox("NomeEmpresa", null, new { @maxlength = "128" }) %>
												</p>

											</div>
											<div class="direita">
											<%--<b>Tipo</b><br />
												<p>
													<%: Html.TextBox("Tipo") %>
												</p>--%>
												<b>CPF</b><br />
												<p>
													<%: Html.TextBox("CPF")%>
												</p>
												<b>Data Fim</b><br />
												<p>
													<%: Html.TextBox("DataAlteracao") %>
												</p>
												<b>Estado e Cidade</b><br />
												<p>
													<%: Html.DropDownList("Estados", new SelectList(estados, "ID", "Nome"), "Selecione")%>
													<select id="CidadeID" name="CidadeID"><option value="">Selecione</option></select>
												</p>
                                                <p>
											<button id="enviar" class="button green" type="submit" value="Save">
												<span>Filtrar</span>
											</button>
                                                </p>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<% } %>
					</div>
				</div>
			</div>
			<br />
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Usuários</h3>
						<ul class="tabs">
							<li id="enviarXls" class="active" value="Save">
								<a href="#" >
									<img alt="Versão em Excel" width="14px" src="/Content/images/function-icons/excel_icon1.png" />
										Obter Excel
								</a>
							</li>
							<li id="enviarHtm" class="active" value="Save">
								<a href="#" >
									<img alt="Versão para Impressão" width="13px" src="/Content/images/function-icons/paper_48.png" />
										Obter Versão para Impressão
								</a>
							</li>
							<!-- li class="active">
								<%: Html.ActionLink("Novo", "Create") %>
							</li-->
						</ul>
					</div>
					<div class="bcont">
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
									<th>
										Nome
									</th>
									<th>
										E-mail
									</th>
									<th>
										CPF
									</th>
									<th>
										<center>
											Status
										</center>
									</th>
								  <%--  <th>
									</th>--%>
								</tr>
							</thead>
							<tbody>
								<% foreach (var item in Model)
								   { %>
								<tr>
									<td>
										<%: item.Nome %>
									</td>
									<td>
										<%: item.Email %>
									</td>
									<td>
										<%: item.CPF %>
									</td>
									<td>
										<% if (item.Ativo)
										{ %>
											<center>
												<img alt="Desbloqueado" src="/Content/images/onebit-icons/39.png" width="30px" title="Desbloqueado" />
											</center>
										<%
										}
										else
										{  %>
											<center>
												<img alt="Bloqueado" src="/Content/images/onebit-icons/37.png" width="30px" title="Bloqueado" />
											</center>
									<%  } %>
									</td>
									<%--<td class="small">
										<a class="action" href="#"></a>
										<div class="opmenu">
											<ul>
												<li><%: Html.ActionLink("Excluir", "Delete", new { id = item.ID })%></li>
												<li><%: Html.ActionLink("Gerentes de Notas", "List", "GerentesNotas", new { usuarioID = item.ID }, null) %></li>
												<% if (item.Ativo)
												   { %>
													<li><%: Html.ActionLink("Bloquear", "MudarEstado", new { ID = item.ID })%></li>
												<% } %>
												<% else
												   { %>
													<li><%: Html.ActionLink("Desbloquear", "MudarEstado", new { ID = item.ID })%></li>
												<% } %>
											</ul>
											<div class="clear">
											</div>
											<div class="foot">
											</div>
										</div>
									</td>--%>
								</tr>
								<% } %>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

	$(document).ready(function () {

		$("#DataCriacao").mask("99/99/9999");
		$("#DataAlteracao").mask("99/99/9999");
		$("#CPF").mask("999.999.999-99");

		$("#formularioUsuario").hide();

		if ($("#EstadoEmitente > option:selected").attr("value") == "")
			$("#CidadeEmitenteID").attr("disabled", "disabled");
		if ($("#EstadoDestinatario > option:selected").attr("value") == "")
			$("#CidadeDestinatarioID").attr("disabled", "disabled");

		$("#headerFiltro").click(function () {

			var visible = $("#formularioUsuario").css("display");

			if (visible == "none")
				$("#formularioUsuario").show();
			else
				$("#formularioUsuario").hide();
		});

		$('#enviarXls').click(function () {
			$("#tipoDestino").val('xls');
			$("#form").submit();
		});
		$('#enviarHtm').click(function () {

			$("#form").attr('target', '_blank');
			$("#tipoDestino").val('htm');
			$("#form").submit();
			$("#tipoDestino").val('');
			$("#form").attr('target', '_self');
		});

		$('#Estados').change(function () {
			$.ajaxSetup({ cache: false });
			var selectedItem = $(this).val();
			if (selectedItem == "" || selectedItem == 0) {
				//Do nothing or hide...?
			} else {
				$.post("/Services/Localizacao/GetCidades/" + $("#Estados > option:selected").attr("value"), function (data) {
					var items = "";

					items += "<option>Selecione</option>";
					$.each(data, function (i, data) {
						items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
					});

					$("#CidadeID").removeAttr('disabled');
					$("#CidadeID").html(items);
				});
			}
		});

	});

</script>
</asp:Content>
