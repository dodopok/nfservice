﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.NotaFiscal>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatório de Usuários Cadastrados por Período
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%
	var dataInicio = (DateTime)ViewData["dataInicio"];
	var dataFim = (DateTime)ViewData["DataFim"];
    var isCidade = ViewData["isCidade"] != null ? (bool)ViewData["isCidade"] : false;
	
%>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Relatório - Nota de <%: (Domain.Entities.TipoEnum)ViewData["Tipo"] %> - Por Período</h3>
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "List") %></li>
						</ul>
					</div>
					<div class="bcont">
						
						<% using (Html.BeginForm("Periodo", "Usuarios", FormMethod.Post, new { id = "form", name = "form" }))
						   { %>
							<p>
								<b>Período Inicial </b><br />
								<%: Html.TextBox("dataInicio", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							</p>
							<p>
								<b>Período Final </b><br />
								<%: Html.TextBox("dataFim", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							</p>
							<p>
								<b>Por Cidade </b><br />
								<%: Html.CheckBox("isMuncipio")%>
							</p>
							<p>
								<button id="enviar" class="button green" type="submit" value="Save">
									<span>Gerar Relatório</span>
								</button>
							<span id="processando" class="note loading">Processando...</span>
							</p>
						<% } %>
					<% using (Html.BeginForm("PrintPeriodo", "Relatorios", FormMethod.Post, new { id = "form", name = "form" , @target = "_blank"})){ %>
						<div>
						<%: Html.Hidden("dataInicioPrint", dataInicio) %>
						<%: Html.Hidden("dataFimPrint", dataFim) %>
						<%: Html.Hidden("isCidadePrint", isCidade)%>
						<center><b>Total de Usuários Cadastrados: <%: Model.Count() %></b></center>
						<center><b>Usuários Funcionários: <%: Model.Count(u=>u.TipoID == WebUI.Areas) %></b></center>
                        <center><b>Usuários Contatadores: <%: Model.Count() %></b></center>
                        <center><b>Usuários Tereirizados: <%: Model.Count() %></b></center>

						<br /><br />
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								<th>
									Mês
								</th>
								<th>
									Ano
								</th>
								<th>
									<center>Notas Ativas</center>
								</th>
								<th>
									<center>Notas Canceladas</center>
								</th>
								<th>
									<center>Notas Denegadas</center>
								</th>
								<th>
									<center>Notas Baixadas</center>
								</th>
								<th class="small">
								</th>
								</tr>
							</thead>
							<tbody>
								<%for (DateTime data = dataInicio; data <= dataFim; data = data.AddMonths(1) ) 
								  { %>
								  <% 
									  var ativas = Model.Count(m => m.DataCriacao.Month == data.Month && m.DataCriacao.Year == data.Year && (Domain.Entities.StatusNotaFiscalEnum)m.StatusID == Domain.Entities.StatusNotaFiscalEnum.Ativa);
									  var canceladas = Model.Count(m => m.DataCriacao.Month == data.Month && m.DataCriacao.Year == data.Year && (Domain.Entities.StatusNotaFiscalEnum)m.StatusID == Domain.Entities.StatusNotaFiscalEnum.Cancelada);
									  var denegadas = Model.Count(m => m.DataCriacao.Month == data.Month && m.DataCriacao.Year == data.Year && (Domain.Entities.StatusNotaFiscalEnum)m.StatusID == Domain.Entities.StatusNotaFiscalEnum.Denegada);
									  var baixadas = Model.Count(m => m.DataCriacao.Month == data.Month && m.DataCriacao.Year == data.Year && (Domain.Entities.StatusNotaFiscalEnum)m.StatusID == Domain.Entities.StatusNotaFiscalEnum.Baixada);
									 
									 TotalAtivas += ativas;
									 TotalBaixadas += baixadas;
									 TotalCanceladas += canceladas;
									 TotalDenegadas += denegadas;
									   %>
									   <% if (ativas > 0 || canceladas > 0 || denegadas > 0 || baixadas > 0)
										  { %>
												<tr>
									
													<td>
														<%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(data.Month) %>
													</td>
													<td>
														<%: data.Year %>
													</td>
													<td>
														<center><%: ativas %></center>
													</td>
													<td>
														<center><%: canceladas %></center>
													</td>
													<td>
														<center><%: denegadas %></center>
													</td>
													<td>
														<center><%: baixadas %></center>
													</td>
												</tr>
										<% } %>
								<% }  %>
								<tr>
									<td>
										<b>Sub-Total do Período</b>
									</td>
									<td></td>
									<td>
										<center><%: TotalAtivas %></center>
									</td>
									<td>
										<center><%: TotalCanceladas %></center>
									</td>
									<td>
										<center><%: TotalDenegadas %></center>
									</td>
									<td>
										<center><%: TotalBaixadas %></center>
									</td>
								</tr>
									
								<tr>
									<td>
										<b>Total geral de Notas no período : </b>
									</td>
									<td>
										<%: TotalAtivas + TotalCanceladas + TotalDenegadas + TotalBaixadas %>
									</td>
								</tr>
							</tbody>
						</table>

						<hr />
						<p>
							<button class="button green" type="submit" value="Save">
								<span>Gerar Impressão</span>
							</button>
						</p>
						</div>

						<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

    $(document).ready(function () {


        $("#erro").hide();
        $(".note.loading").hide();

        $("#dataInicio").mask("99/99/9999");
        $("#DataFim").mask("99/99/9999");
        $("#Celular").mask("(99)9999-9999");

        $("#dataInicio").blur(function () {
            $("#dataInicioPrint").val($("#dataInicio").val());
        });

        $("#DataFim").blur(function () {
            $("#DataFimPrint").val($("#DataFim").val());
        });

        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            },
            rules: {
                Nome: "required",
                Email: {
                    email: true,
                    required: true,
                    JaExisteCadastro: true
                },
                Senha: "required",
                ConfirmaSenha: {
                    equalTo: "#Senha"
                },
                CPF: "required",
                Telefone: "required",
                EmpresaID: "required"
            },
            messages: {
                Nome: "Este campo é obrigatório.",
                Email: {
                    required: "Este campo é obrigatório.",
                    email: "Formato de e-mail inválido.",
                    JaExisteCadastro: "Já existe cadastro para esse email"
                },
                Senha: "Este campo é obrigatório.",
                ConfirmaSenha: {
                    equalTo: "As senhas devem ser iguais."
                },
                CPF: "Este campo é obrigatório.",
                Telefone: "Este campo é obrigatório.",
                EmpresaID: "Este campo é obrigatório."
            }
        });
    });
	</script>
</asp:Content>