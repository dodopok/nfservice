﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Usuario>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <style type="text/css">
            html, body {height:100%;}
            body {font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;}
            .tbRelatorio{margin-left: 200px;}
            table.tbRelatorio tr th{font-size: 15px;font-weight: bold;}
            
            table.infotable {margin-bottom:15px;text-align:left;}
            table.infotable  tr td, table.infotable tr th {border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left;}
            table.infotable  tr td.small, table.infotable tr th.small {width:20px;text-align:center;}
            table.infotable  tr td.small input[type="checkbox"], table.infotable tr th.small  input[type="checkbox"] {vertical-align:middle;}
            table.infotable thead tr {background-color:#f5f5f5;}
            table.infotable th {font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);}
            table.infotable tbody tr.selected {background-color:#fdffea !important;}
            
            
            .divRelatNotas
            {    
                float: left;
                padding: 20px;
                text-align: left;
            }
            
            span
            {       
                color: #3B6CCA;
                font-size: 25px;
                font-weight: bold;
                padding: 35px;
                text-decoration: underline;            
            }
        </style>
    
    <title>        
        USUÁRIOS
    </title>
</head>
<body>

<%    
    var CidadeID = ViewData["CidadeID"];
    var CPF = ViewData["CPF"];
    var DataAlteracao = ViewData["DataAlteracao"];
    var DataCriacao = ViewData["DataCriacao"];
    var Email = ViewData["Email"];
    var Nome = ViewData["Nome"] ;
    var NomeEmpresa = ViewData["NomeEmpresa"] ;
    var Tipo = ViewData["Tipo"] ;
%>
    <div>
        <center><b>USUÁRIOS</b></center>
        <br />
            <center>
                <b>Total de Usuários Cadastrados: </b><%: Model.Count() %>
            </center>
        <br />
        <br />

        <span> Filtros </span>
        <center>
        <div class="divRelatNotas">
            <b>CidadeID :</b> <%: (int)CidadeID == 0 ? "Qualquer" : CidadeID %>
            <br />
            <br />
            <b>CPF: </b><%: CPF == null ? "Qualquer" : CPF %>
            <br />
            <br />
            <b>Data Alteração:</b> <%: (DateTime)DataAlteracao == DateTime.MinValue? "Qualquer" : ((DateTime)DataAlteracao).ToShortDateString() %>
            <br />
            <br />
            <b>Data Criação:</b> <%: (DateTime)DataCriacao == DateTime.MinValue ? "Qualquer" : ((DateTime)DataCriacao).ToShortDateString() %>
        </div>
        <div class="divRelatNotas" >
            <b>Email: </b><%: Email == null ? "Qualquer" : Email %>
            <br />
            <br />
            <b>Nome :</b> <%: Nome == null ? "Qualquer" : Nome %>
            <br />
            <br />
            <b>Nome Empresa :</b> <%: NomeEmpresa == null ? "Qualquer" : NomeEmpresa %>
            <br />
            <br />
            <b>Tipo :</b> <%: Tipo == null ? "Qualquer" : Tipo %>
        </div>
        </center>

        <br />
        <br />
        <br />

        <center style="clear:both;">
            <table class="infotable" cellspacing="0" cellpadding="0" width="1200">
				<thead>
                <tr>
					<th>
						Nome
					</th>
					<th>
						Email
					</th>
					<th>
						CPF
					</th>
                    <th>
                    Ativo
                    </th>
                    </tr>
				</thead>
				<tbody>
					<% foreach (var item in Model)
						{ %>
					<tr>
						<td>
							<%: item.Nome %>
						</td>
						<td>
							<%: item.Email %>
						</td>
						<td>
							<%: item.CPF %>
						</td>
                        <td>
                        <%= ((item.Ativo) ? "Ativo" : "Inativo")%>
                        </td>
					</tr>
					<% } %>
				</tbody>
			</table>
        </center>
    </div>
</body>
</html>
