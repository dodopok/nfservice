<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Usuario>>" %>
<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Usuarios.csv\""); %>

Nome;Email;CPF;Status

<% foreach(var usuario in Model)
   { %>
        <%= usuario.Nome %>;<%= usuario.Email %>;<%= usuario.CPF %>;<%= ((usuario.Ativo) ? "Ativo" : "Inativo") %>
<% } %>