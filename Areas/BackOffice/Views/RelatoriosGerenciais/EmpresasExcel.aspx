﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioEmpresas>" %>

<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Empresas.csv\""); %>
<%
    var dataInicio = (DateTime)ViewData["dataInicio"];
    var dataFim = (DateTime)ViewData["DataFim"];
    var isCidade = ViewData["isCidade"] != null ? (bool)ViewData["isCidade"] : false;
%>
Total de Empresas Cadastrados:<%: Model.TotalEmpresas%>
Relatório de
<%: dataInicio.ToShortDateString()%>
a
<%: dataFim.ToShortDateString()%>
<% if (!isCidade)
{ %>
Empresas Cadastrados por Estado
Estado;Quantidade
<% foreach (var item in Model.ItensRelatorio)
   { %>
<%: item.SiglaNomeEstado%>;<%: item.QuantidadeEmpresas%>
<% } %>
<%}
else
{ %>
Empresas Cadastrados por Cidade
Estado;Cidade;Quantidade
<% foreach (var item in Model.ItensRelatorio)
{ %>
<%: item.SiglaNomeEstado%>;<%: item.QuantidadeEmpresas%>
<% } %>
<%} %>
Total : <%: Model.TotalEmpresas%>