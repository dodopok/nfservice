﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioUsuarios>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>RELATÓRIOS DE USUÁRIOS CADASTRADOS </title>
</head>
<body>
    <div>
        <center>
            <b>RELATÓRIOS DE USUÁRIOS CADASTRADOS - POR MUNICÍPIO</b></center>
        <br />
        <center>
            <b>Total de Usuários Cadastrados:
                <%: Model.TotalGerentesNotas + Model.TotalUsuarios %></b></center>
        <% if (Model.IsUsuario)
           { %>
        <center>
            Usuários Principais:
            <%: Model.TotalUsuarios%></center>
        <% } %>
        <% if (Model.IsGerente)
           { %>
        <center>
            Usuários Gerentes:
            <%: Model.TotalGerentesNotas%></center>
        <% } %>
        <br />
        <% if (Model.IsCidade)
           {%>
        <center>
            <table class="infotable" cellspacing="0" cellpadding="0" width="800px">
                <thead>
                    <tr>
                        <th>
                            Estado
                        </th>
                        <th>
                            <center>
                                Cidade</center>
                        </th>
                        <th>
                            <center>
                                Tipo</center>
                        </th>
                        <th>
                            <center>
                                Quantidade</center>
                        </th>
                        <th class="small">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <%
               foreach (var item in Model.ItensRelatorio)
               {%>
                    <tr>
                        <td>
                            <b>
                                <%:item.SiglaNomeEstado%></b>
                        </td>
                        <td>
                            <center>
                                <b>
                                    <%:item.NomeCidade%></b></center>
                        </td>
                        <td>
                            <center>
                                <%:item.Tipo%></center>
                        </td>
                        <td>
                            <center>
                                <%:item.QuantidadeUsuarios%></center>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <%
               }%>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <center>
                            </center>
                        </td>
                        <td>
                            <center>
                                <b>Total :</b>
                                <%:Model.TotalUsuarios + Model.TotalGerentesNotas%></center>
                        </td>
                        <td>
                        </td>
                    </tr>
                </tbody>
            </table>
        </center>
        <%
           }
           else
           {
        %>
        <center>
            <table class="infotable" cellspacing="0" cellpadding="0" width="800px">
                <thead>
                    <tr>
                        <th>
                            Estado
                        </th>
                        <th>
                            <center>
                                Tipo</center>
                        </th>
                        <th>
                            <center>
                                Quantidade</center>
                        </th>
                        <th class="small">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <%
               foreach (var item in Model.ItensRelatorio)
               {%>
                    <tr>
                        <td>
                            <b>
                                <%:item.SiglaNomeEstado%></b>
                        </td>
                        <td>
                            <center>
                                <%:item.Tipo%></center>
                        </td>
                        <td>
                            <center>
                                <%:item.QuantidadeUsuarios%></center>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <%
               }%>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <center>
                            </center>
                        </td>
                        <td>
                            <center>
                                <b>Total :</b>
                                <%:Model.TotalUsuarios + Model.TotalGerentesNotas%></center>
                        </td>
                        <td>
                        </td>
                    </tr>
                </tbody>
            </table>
        </center>
        <%
           }%>
    </div>
</body>
</html>
