﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioNotas>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatório de Notas Cadastradas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%
	var dataInicio = (DateTime)ViewData["dataInicio"];
	var dataFim = (DateTime)ViewData["DataFim"];
%>
<div class="BreadCrumb">
    <%: Html.ActionLink("Relatórios Gerenciais","Relatorios")%> / Notas Fiscais por Período
</div>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "Relatorios") %></li>
						</ul>
					</div>
					<div class="bcont">
						
						<% using (Html.BeginForm("Notas", "RelatoriosGerenciais", FormMethod.Post, new { id = "form", name = "form" }))
						   { %>
                           <%: Html.Hidden("Carregar",false)%>
							<p>
								<b>Período Inicial </b><br />
								<%: Html.TextBox("dataInicio", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							</p>
							<p>
								<b>Período Final </b><br />
								<%: Html.TextBox("dataFim", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							</p>
							<p>
								<button id="enviar" class="button green" type="submit" value="Save">
									<span>Gerar Relatório</span>
								</button>
							<span id="processando" class="note loading">Processando...</span>
							</p>
						<% } %>
                    
                <% if (Model != null)
                   { %>
					<div>
						<%--<%: Html.Hidden("isCidadePrint", isCidade)%>--%>
						<center><b>Total de Notas no Sistema: <%: Model.TotalNotas%></b></center>                        
                        <br />
                        <br />

                        <center><b>Relatório de <%: dataInicio.ToShortDateString()%> a <%: dataFim.ToShortDateString()%></b></center>

                        <br /><br />
                   
					    <% using (Html.BeginForm("PrintNotas", "RelatoriosGerenciais", FormMethod.Post, new { id = "form", name = "form", @target = "_blank" }))
            { %>
						<%: Html.Hidden("printDataInicio", dataInicio)%>
						<%: Html.Hidden("printDataFim", dataFim)%>
                        <center><b>Notas Fiscais por Período</b></center>

						<br /><br />
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								<th>
									Mês
								</th>
								<th>
									<center>Ano</center>
								</th>
								<th>
									<center>Quantidade</center>
								</th>
								<th class="small">
								</th>
								</tr>
							</thead>
							<tbody>
                                <% foreach (var item in Model.ItensRelatorio)
                                   { %>
                                        <tr>
									        <td>
										        <b><%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Mes)%></b>
									        </td>
									        <td>
										        <center><%: item.Ano%></center>
									        </td>
									        <td>
										        <center><%: item.QuantidadeNotas%></center>
									        </td>
                                            <td></td>
								        </tr>
                                <% } %>
                                <tr>
                                    <td></td>
                                    <td>
                                        <center></center>
                                    </td>
                                    <td>
                                        <center><b>Total :</b> <%: Model.TotalNotas%></center>
                                    </td>
                                    <td></td>
                                </tr>
									
							</tbody>
						</table>

						<hr />
						<p>
							<button class="button green" type="submit" value="Save">
								<span>Gerar Impressão</span>
							</button>
						</p>
						<% } %>
					</div>
                    
					<% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

    $(document).ready(function () {


        $("#erro").hide();
        $(".note.loading").hide();

        $("#dataInicio").mask("99/99/9999");
        $("#dataFim").mask("99/99/9999");

        $("#dataInicio").blur(function () {
            $("#printDataInicio").val($("#dataInicio").val());
        });

        $("#dataFim").blur(function () {
            $("#printDataFim").val($("#dataFim").val());
        });
        
        $("#enviar").click(function () {
            $("#Carregar").val(true);
        });

        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            },
        });
    });
	</script>
</asp:Content>
