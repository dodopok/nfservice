﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatorios
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="container_12">
		<div class="grid_12">
			
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<h3>Relatórios Gerenciais - Menu de Navegação</h3>
					</div>
					<div class="bcont">
						<%: Html.ActionLink("Empresas por Cidade / Estado", "Empresas", "RelatoriosGerenciais")%>
						<br />
						<br />
						<%: Html.ActionLink("Usuários por Cidade / Estado", "Usuarios", "RelatoriosGerenciais")%>
						<br />
						<br />
						<%: Html.ActionLink("Notas Fiscais por Período", "Notas", "RelatoriosGerenciais")%>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
