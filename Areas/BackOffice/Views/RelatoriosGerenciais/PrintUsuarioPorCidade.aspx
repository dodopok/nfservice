﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioUsuarios>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <style type="text/css">
            html, body {height:100%;}
            body {font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;}
            .tbRelatorio{margin-left: 200px;}
            table.tbRelatorio tr th{font-size: 15px;font-weight: bold;}
            
            table.infotable {margin-bottom:15px;text-align:left;}
            table.infotable  tr td, table.infotable tr th {border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left;}
            table.infotable  tr td.small, table.infotable tr th.small {width:20px;text-align:center;}
            table.infotable  tr td.small input[type="checkbox"], table.infotable tr th.small  input[type="checkbox"] {vertical-align:middle;}
            table.infotable thead tr {background-color:#f5f5f5;}
            table.infotable th {font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);}
            table.infotable tbody tr.selected {background-color:#fdffea !important;}
        </style>
    
    <title>        
        RELATÓRIOS DE USUÁRIOS CADASTRADOS
    </title>
</head>
<body>
<%
    var isGerente = ViewData["isGerente"] != null ? (bool)ViewData["isGerente"] : false;
    var isUsuario = ViewData["isUsuario"] != null ? (bool)ViewData["isUsuario"] : false;
%>
    <div>
        <center><b>RELATÓRIOS DE USUÁRIOS CADASTRADOS - POR MUNICÍPIO</b></center>
        <br />
            <center><b>Total de Usuários Cadastrados: <%: Model.TotalGerentesNotas + Model.TotalUsuarios %></b></center>
            <% if (isUsuario)
               { %>
			<center>Usuários Principais: <%: Model.TotalUsuarios%></center>
            <% } %>
            <% if (isGerente)
               { %>
            <center>Usuários Gerentes: <%: Model.TotalGerentesNotas%></center>
            <% } %>
        <br />

        <center>
            <table class="infotable" cellspacing="0" cellpadding="0" width="800px">
				<thead>
					<tr>
					<th>
						Estado
					</th>
					<th>
						<center>Cidade</center>
					</th>
					<th>
						<center>Tipo</center>
					</th>
					<th>
						<center>Quantidade</center>
					</th>
					<th class="small">
					</th>
					</tr>
				</thead>
				<tbody>
                    <% foreach (var item in Model.ItensRelatorio)
                        { %>
					<tr>
						<td>
							<b><%: item.SiglaNomeEstado %></b>
						</td>
						<td>
							<center><b><%: item.NomeCidade%></b></center>
						</td>
						<td>
							<center><%: item.Tipo%></center>
						</td>
						<td>
							<center><%: item.QuantidadeUsuarios%></center>
						</td>
                        <td></td>
					</tr>
                    <% } %>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <center></center>
                        </td>
                        <td>
                            <center><b>Total :</b> <%: Model.TotalUsuarios + Model.TotalGerentesNotas%></center>
                        </td>
                        <td></td>
                    </tr>
									
				</tbody>
			</table>
        </center>
    </div>
</body>
</html>
