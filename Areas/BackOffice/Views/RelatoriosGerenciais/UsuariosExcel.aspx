﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioUsuarios>" %>

<% this.Response.ContentType = "application/csv"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.Unicode; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"Usuarios.csv\""); %>
<%
    var dataInicio = (DateTime)ViewData["dataInicio"];
    var dataFim = (DateTime)ViewData["DataFim"];
    var isCidade = ViewData["isCidade"] != null ? (bool)ViewData["isCidade"] : false;
    var isGerente = ViewData["isGerente"] != null ? (bool)ViewData["isGerente"] : false;
    var isUsuario = ViewData["isUsuario"] != null ? (bool)ViewData["isUsuario"] : false;	
%>
Total de Usuários Cadastrados:
<%: Model.TotalGerentesNotas + Model.TotalUsuarios%>
<% if (isUsuario)
   { %>
Usuários Principais:
<%: Model.TotalUsuarios%>
<% } %>
<% if (isGerente)
   { %>
Usuários Gerentes:
<%: Model.TotalGerentesNotas%>
<% } %>
Relatório de
<%: dataInicio.ToShortDateString()%>
a
<%: dataFim.ToShortDateString()%>
<% if (!isCidade)
   { %>
Usuários Cadastrados por Estado Estado;Tipo;Quantidade
<% foreach (var item in Model.ItensRelatorio)
   { %>
<%: item.SiglaNomeEstado%>;<%: item.Tipo%>;<%: item.QuantidadeUsuarios%>
<% } %>
Total :
<%: Model.TotalUsuarios + Model.TotalGerentesNotas%>
<% }
   else
   {%>
Usuários Cadastrados por Cidade Estado;Cidade;Tipo;Quantidade
<% foreach (var item in Model.ItensRelatorio)
   { %>
<%: item.SiglaNomeEstado%>;<%: item.NomeCidade%>;<%: item.Tipo%>;<%: item.QuantidadeUsuarios%>
<% } %>
Total :
<%: Model.TotalUsuarios + Model.TotalGerentesNotas%>
<% } %>
