﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioEmpresas>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Relatório de Empresas Cadastradas por Período </title>
</head>
<body>
    <div class="container_12">
        <div class="grid_12">
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="bcont">
                        <% if (Model != null)
                           { %>
                        <div>
                            <%--<%: Html.Hidden("isCidadePrint", isCidade)%>--%>
                            <center>
                                <b>Total de Empresas Cadastrados:
                                    <%: Model.TotalEmpresas%></b></center>
                            <br />
                            <br />
                            <center>
                                <b>Relatório de
                                    <%: Model.DataInicio.ToShortDateString()%>
                                    a
                                    <%: Model.DataFim.ToShortDateString()%></b></center>
                            <br />
                            <br />
                            <% if (!Model.IsCidade)
                               { %>
                            <center>
                                <b>Empresas Cadastrados por Estado</b></center>
                            <br />
                            <br />
                            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            <center>
                                                Quantidade</center>
                                        </th>
                                        <th class="small">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% foreach (var item in Model.ItensRelatorio)
                                       { %>
                                    <tr>
                                        <td>
                                            <b>
                                                <%: item.SiglaNomeEstado%></b>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeEmpresas%></center>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <% } %>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <center>
                                                <b>Total :</b>
                                                <%: Model.TotalEmpresas%></center>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <%}

                               else
                               { %>
                            <center>
                                <b>Empresas Cadastrados por Cidade</b></center>
                            <br />
                            <br />
                            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            <center>
                                                Cidade</center>
                                        </th>
                                        <th>
                                            <center>
                                                Quantidade</center>
                                        </th>
                                        <th class="small">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% foreach (var item in Model.ItensRelatorio)
                                       { %>
                                    <tr>
                                        <td>
                                            <b>
                                                <%: item.SiglaNomeEstado%></b>
                                        </td>
                                        <td>
                                            <center>
                                                <b>
                                                    <%: item.NomeCidade%></b></center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeEmpresas%></center>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <% } %>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <center>
                                                <b>Total :</b>
                                                <%: Model.TotalEmpresas%></center>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <% } %>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</body>
</html>
