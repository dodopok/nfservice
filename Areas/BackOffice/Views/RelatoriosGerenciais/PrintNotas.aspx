﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioNotas>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
        <style type="text/css">
            html, body {height:100%;}
            body {font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;}
            .tbRelatorio{margin-left: 200px;}
            table.tbRelatorio tr th{font-size: 15px;font-weight: bold;}
            
            table.infotable {margin-bottom:15px;text-align:left;}
            table.infotable  tr td, table.infotable tr th {border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left;}
            table.infotable  tr td.small, table.infotable tr th.small {width:20px;text-align:center;}
            table.infotable  tr td.small input[type="checkbox"], table.infotable tr th.small  input[type="checkbox"] {vertical-align:middle;}
            table.infotable thead tr {background-color:#f5f5f5;}
            table.infotable th {font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);}
            table.infotable tbody tr.selected {background-color:#fdffea !important;}
        </style>
    
    <title>
        
        RELATÓRIOS DE NOTAS POR PERÍODO
    </title>
</head>
<body>
        <%
	        var dataInicio = (DateTime)ViewData["dataInicio"];
	        var dataFim = (DateTime)ViewData["DataFim"];
        %>
    <div>
        <center><b>RELATÓRIOS DE NOTAS CADASTRADAS - POR PERÍODO</b></center>
        <br />
            <center><b>Total de Notas Cadastradas no Período: <%: Model.TotalNotas %></b></center>
        <br />

        <center>
            <table class="infotable" cellspacing="0" cellpadding="0" width="800px">
				<thead>
					<tr>
					<th>
						Mês
					</th>
					<th>
						<center>Ano</center>
					</th>
					<th>
						<center>Quantidade</center>
					</th>
					<th class="small">
					</th>
					</tr>
				</thead>
				<tbody>
                    <% foreach(var item in Model.ItensRelatorio)
                            { %>
							<tr>
								<td>
									<b><%: System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Mes) %></b>
								</td>
								<td>
									<center><%: item.Ano %></center>
								</td>
								<td>
									<center><%: item.QuantidadeNotas %></center>
								</td>
                                <td></td>
							</tr>
                    <% } %>
                    <tr>
                        <td></td>
                        <td>
                            <center></center>
                        </td>
                        <td>
                            <center><b>Total :</b> <%: Model.TotalNotas %></center>
                        </td>
                        <td></td>
                    </tr>
									
				</tbody>
			</table>
        </center>
    </div>
</body>
</html>
