﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master"
    Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioUsuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Relatório de Usuários Cadastrados por Período
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var dataInicio = (DateTime)ViewData["dataInicio"];
        var dataFim = (DateTime)ViewData["DataFim"];
        var isCidade = ViewData["isCidade"] != null ? (bool)ViewData["isCidade"] : false;
        var isGerente = ViewData["isGerente"] != null ? (bool)ViewData["isGerente"] : false;
        var isUsuario = ViewData["isUsuario"] != null ? (bool)ViewData["isUsuario"] : false;
	
    %>
    <div class="BreadCrumb">
        <%: Html.ActionLink("Relatórios Gerenciais","Relatorios")%>
        / Usuários por Cidade / Estado
    </div>
    <div class="container_12">
        <div class="grid_12">
            <div class="sb-box">
                <div class="sb-box-inner content">
                    <div class="header">
                        <ul class="tabs">
                            <li class="active">
                                <%: Html.ActionLink("Voltar", "Relatorios") %></li>
                        </ul>
                    </div>
                    <div class="bcont">
                        <% using (Html.BeginForm("Usuarios", "RelatoriosGerenciais", FormMethod.Post, new { id = "form", name = "form" }))
                           { %>
                        <%: Html.Hidden("Carregar",false)%>
                        <p>
                            <b>Período Inicial </b>
                            <br />
                            <%: Html.TextBox("dataInicio", "", new { @class = "inputtext medium", @maxlength = "128" })%>
                        </p>
                        <p>
                            <b>Período Final </b>
                            <br />
                            <%: Html.TextBox("dataFim", "", new { @class = "inputtext medium", @maxlength = "128" })%>
                        </p>
                        <table>
                            <tr>
                                <td>
                                    <center>
                                        <b>Por Estado </b>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <b>Por Cidade</b></center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%: Html.RadioButton("isCidade", false, true, new { @style="width:150px;" })%>
                                </td>
                                <td>
                                    <%: Html.RadioButton("isCidade", true, new { @style="width:150px;" })%>
                                    <%: Html.ValidationMessage("isCidade")%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <center>
                                        <b>Usuários Principais </b>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <b>Gerentes de Notas</b></center>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" value="<%: isUsuario %>" style="width: 150px;" name="Usuarios"
                                        id="Usuarios" <%: isUsuario ? "checked='true'" : "" %> />
                                    <%--<%: Html.CheckBox("Usuarios", isUsuario, new { @style="width:150px;" })%>--%>
                                </td>
                                <td>
                                    <input type="checkbox" value="<%: isGerente %>" style="width: 150px;" name="Gerentes"
                                        id="Gerentes" <%: isGerente ? "checked='true'" : "" %> />
                                    <%--<%: Html.CheckBox("Gerentes", isGerente, new { @style="width:150px;" })%>--%>
                                </td>
                            </tr>
                        </table>
                        <p>
                            <button id="enviar" class="button green" type="submit" value="Save">
                                <span>Gerar Relatório</span>
                            </button>
                            <span id="processando" class="note loading">Processando...</span>
                        </p>
                        <% } %>
                        <% if (Model != null)
                           { %>
                        <div>
                            <%--<%: Html.Hidden("isCidadePrint", isCidade)%>--%>
                            <center>
                                <b>Total de Usuários Cadastrados:
                                    <%: Model.TotalGerentesNotas + Model.TotalUsuarios%></b></center>
                            <% if (isUsuario)
                               { %>
                            <center>
                                Usuários Principais:
                                <%: Model.TotalUsuarios%></center>
                            <% } %>
                            <% if (isGerente)
                               { %>
                            <center>
                                Usuários Gerentes:
                                <%: Model.TotalGerentesNotas%></center>
                            <% } %>
                            <br />
                            <br />
                            <center>
                                <b>Relatório de
                                    <%: dataInicio.ToShortDateString()%>
                                    a
                                    <%: dataFim.ToShortDateString()%></b></center>
                            <br />
                            <br />
                            <% if (!isCidade)
                               { %>
                            <% using (Html.BeginForm("PrintUsuarioPorEstado", "RelatoriosGerenciais", FormMethod.Post, new { id = "printForm", name = "printForm", @target = "_blank" }))
                               { %>
                            <%: Html.Hidden("tipoDestino") %>
                            <%: Html.Hidden("printDataInicio", dataInicio)%>
                            <%: Html.Hidden("printDataFim", dataFim)%>
                            <%: Html.Hidden("printIsGerente", isGerente)%>
                            <%: Html.Hidden("printIsUsuario", isUsuario)%>
                            <center>
                                <b>Usuários Cadastrados por Estado</b></center>
                            <br />
                            <br />
                            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            <center>
                                                Tipo</center>
                                        </th>
                                        <th>
                                            <center>
                                                Quantidade</center>
                                        </th>
                                        <th class="small">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% foreach (var item in Model.ItensRelatorio)
                                       { %>
                                    <tr>
                                        <td>
                                            <b>
                                                <%: item.SiglaNomeEstado%></b>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.Tipo%></center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeUsuarios%></center>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <% } %>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <center>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <b>Total :</b>
                                                <%: Model.TotalUsuarios + Model.TotalGerentesNotas%></center>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <table>
                                <tr>
<%--                                    <td>
                                        <button class="button green" id="enviarPdf" type="button" value="Save">
                                            <span>Gerar Pdf</span>
                                        </button>
                                    </td>
                                    <td>
                                        <button class="button green" id="enviarXls" type="button" value="Save">
                                            <span>Gerar Excel</span>
                                        </button>
                                    </td>--%>
                                    <td>
                                        <button class="button green" type="submit" value="Save">
                                            <span>Gerar Impressão</span>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            <% }
                               }

                               else
                               {

                                   using (Html.BeginForm("PrintUsuarioPorCidade", "RelatoriosGerenciais", FormMethod.Post, new { id = "printForm", name = "printForm", @target = "_blank" }))
                                   { %>
                            <%: Html.Hidden("tipoDestino") %>
                            <%: Html.Hidden("printDataInicio", dataInicio)%>
                            <%: Html.Hidden("printDataFim", dataFim)%>
                            <%: Html.Hidden("printIsGerente", isGerente)%>
                            <%: Html.Hidden("printIsUsuario", isUsuario)%>
                            <center>
                                <b>Usuários Cadastrados por Cidade</b></center>
                            <br />
                            <br />
                            <table class="infotable" cellspacing="0" cellpadding="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            <center>
                                                Cidade</center>
                                        </th>
                                        <th>
                                            <center>
                                                Tipo</center>
                                        </th>
                                        <th>
                                            <center>
                                                Quantidade</center>
                                        </th>
                                        <th class="small">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% foreach (var item in Model.ItensRelatorio)
                                       { %>
                                    <tr>
                                        <td>
                                            <b>
                                                <%: item.SiglaNomeEstado%></b>
                                        </td>
                                        <td>
                                            <center>
                                                <b>
                                                    <%: item.NomeCidade%></b></center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.Tipo%></center>
                                        </td>
                                        <td>
                                            <center>
                                                <%: item.QuantidadeUsuarios%></center>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <% } %>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <center>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <b>Total :</b>
                                                <%: Model.TotalUsuarios + Model.TotalGerentesNotas%></center>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <table>
                                <tr>
<%--                                    <td>
                                        <button class="button green" id="enviarPdf" type="button" value="Save">
                                            <span>Gerar Pdf</span>
                                        </button>
                                    </td>
                                    <td>
                                        <button class="button green" id="enviarXls" type="button" value="Save">
                                            <span>Gerar Excel</span>
                                        </button>
                                    </td>--%>
                                    <td>
                                        <button class="button green" type="submit" value="Save">
                                            <span>Gerar Impressão</span>
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            <% } %>
                        </div>
                        <% } %>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearingfix">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {


            $("#erro").hide();
            $(".note.loading").hide();

            $("#dataInicio").mask("99/99/9999");
            $("#dataFim").mask("99/99/9999");

            $("#dataInicio").blur(function () {
                $("#printDataInicio").val($("#dataInicio").val());
            });

            $("#dataFim").blur(function () {
                $("#printDataFim").val($("#dataFim").val());
            });

            $("#Usuarios").click(function () {
                var usu = $("#printIsUsuario").val();
                if (usu == 'False') {
                    $("#printIsUsuario").val('True');
                    $("#Usuarios").val('True');
                }
                else {
                    $("#printIsUsuario").val('False');
                    $("#Usuarios").val('False');
                }
                //$("#printIsUsuario").val($("#Usuarios").checked());
            });

            $("#Gerentes").click(function () {
                var ger = $("#printIsGerente").val();
                if (ger == 'False') {
                    $("#printIsGerente").val('True');
                    $("#Gerentes").val('True');
                }
                else {
                    $("#printIsGerente").val('False');
                    $("#Gerentes").val('False');
                }
                //$("#printIsGerente").val($("#Gerentes").val());
            });

            $("#enviar").click(function () {
                $("#Carregar").val(true);
            });
            $('#enviarXls').click(function () {
                $("#tipoDestino").val('xls');
                $("#printForm").submit();
                $("#tipoDestino").val('');
            });

            $('#enviarHtm').click(function () {
                $("#tipoDestino").val('htm');
                $("#printForm").attr("target", "_blank");
                $("#printForm").submit();
                $("#printForm").attr("target", "_self");
                $("#tipoDestino").val('');
            });

            $('#enviarPdf').click(function () {
                $("#tipoDestino").val('pdf');
                $("#printForm").attr("target", "_blank");
                $("#printForm").submit();
                $("#printForm").attr("target", "_self");
                $("#tipoDestino").val('');
            });

            $("#form").validate({
                meta: "validate",
                invalidHandler: function (form, validator) {
                    $("#erro").show();
                    $("#erro").focus();
                },

                submitHandler: function (form) {
                    $("#erro").hide();
                    $("#cancelar").hide();
                    $("#enviar").hide();
                    $("#processando").show();
                    form.submit();
                }
            });
        });
    </script>
</asp:Content>
