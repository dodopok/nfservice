﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioPagamentos>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    <style type="text/css">
        html, body {height:100%;}
        body {font:13.34px helvetica,arial,freesans,clean,sans-serif;min-height:100%;background-color:#f7f7f7;}
        .tbRelatorio{margin-left: 200px;}
        table.tbRelatorio tr th{font-size: 15px;font-weight: bold;}
            
        table.infotable {margin-bottom:15px;text-align:left;}
        table.infotable  tr td, table.infotable tr th {border-bottom:1px solid #DDDDDD;line-height:normal;padding:5px 10px;text-align:left;}
        table.infotable  tr td.small, table.infotable tr th.small {width:20px;text-align:center;}
        table.infotable  tr td.small input[type="checkbox"], table.infotable tr th.small  input[type="checkbox"] {vertical-align:middle;}
        table.infotable thead tr {background-color:#f5f5f5;}
        table.infotable th {font-weight:bold;color: #305B7F; text-shadow: 1px 1px 0px rgba(255,255,255,1);}
        table.infotable tbody tr.selected {background-color:#fdffea !important;}
            
        .divRelatNotas
        {    
            float: left;
            padding: 20px;
            text-align: left;
        }
            
        span
        {       
            color: #3B6CCA;
            font-size: 25px;
            font-weight: bold;
            padding: 35px;
            text-decoration: underline;    
            left:-380px;
            position:relative;
        }
    </style>
    
    <title>
        
        RELATÓRIO DE PAGAMENTOS POR EMPRESA
    </title>
</head>
<body>
        <%
	        var dataInicio = (DateTime)ViewData["dataInicio"];
	        var dataFim = (DateTime)ViewData["DataFim"];
            var razao = ViewData["Razao"];
            var estado = ViewData["EstadoID"];
            var cidade = ViewData["CidadeID"];
        %>
    <center>
    <div style="width:900px;" >
        <span > Filtros </span>
        <center>
            <div class="divRelatNotas">
                <b>Data Início: </b><%: (DateTime)dataInicio == DateTime.MinValue ? "Qualquer" : ((DateTime)dataInicio).ToShortDateString()%>
                <br />
                <br />
                <b>Data Fim: </b><%:  (DateTime)dataFim == DateTime.MinValue ? "Qualquer" : ((DateTime)dataFim).ToShortDateString()%>
                <br />
                <br />
                <b>Razão Social: </b><%: razao == "" ? "Qualquer" : razao%>
            </div>
            <div class="divRelatNotas" >
                <b>Estado: </b><%: estado == "" ? "Qualquer" : estado%>
                <br />
                <br />
                <b>Cidade: </b><%: cidade == "" ? "Qualquer" : cidade%>
            </div>
        </center>
        <center style="clear:both;">
            <center><b>Total de Pagamentos Cadastrados : <%: Model.TotalPagamentos  %></b></center>
            <br />
            <table class="infotable" cellspacing="0" cellpadding="0" width="800px">
				<thead>
					<tr>
					<th>
						Razão Social
					</th>
					<th>
						<center>Valores (R$)</center>
					</th>
					<th>
						<center>Número de Empresas</center>
					</th>
					<th>
						<center>Valor Médio</center>
					</th>
					</tr>
				</thead>
				<tbody>
                    <% foreach (var item in Model.ItensRelatorio)
                        { %>
					<tr>
						<td>
							<b><%: item.Razao%></b>
						</td>
						<td>
							<center><%: item.Valores %></center>
						</td>
						<td>
							<center><%: item.TotalItens %></center>
						</td>
						<td>
							<center><%: item.ValorMedio %></center>
						</td>
					</tr>
                    <% } %>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <center><b>Total : R$ </b> <%: Model.TotalValor %></center>
                        </td>
                    </tr>
									
				</tbody>
			</table>
        </center>
    </div>
    </center>
</body>
</html>
