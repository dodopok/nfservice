﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioPagamentos>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatório de Pagamentos Cadastrados por Período
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%
	var dataInicio = (DateTime)ViewData["dataInicio"];
    var dataFim = (DateTime)ViewData["DataFim"];
    var razao = ViewData["Razao"];
    var estado = ViewData["NomeEstado"];
    var cidade = ViewData["NomeCidade"];
    var estadoID = ViewData["EstadoID"];
    var cidadeID = ViewData["CidadeID"];
    var estados = ViewData["Estados"] == null ? new List<Domain.Entities.Estado>() : (IEnumerable<Domain.Entities.Estado>)ViewData["Estados"];
    var cidades = ViewData["Cidades"] == null ? new List<Domain.Entities.Cidade>() : (IEnumerable<Domain.Entities.Cidade>)ViewData["Cidades"];
%>
<div class="BreadCrumb">
    <%: Html.ActionLink("Relatórios Financeiros","Relatorios")%> / Pagamentos - Valores por Empresa
</div>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "Relatorios") %></li>
						</ul>
					</div>
					<div class="bcont">
						
						<% using (Html.BeginForm("PagamentosEmpresa", "RelatoriosFinanceiros", FormMethod.Post, new { id = "form", name = "form" }))
						   { %>
                           
						    <%: Html.Hidden("Carregar",false)%>
                            <div class="esquerda">
							    <p>
								    <b>Período Inicial </b><br />
								    <%: Html.TextBox("dataInicio", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							    </p>
                            </div>
                            <div class="direita">
							    <p>
								    <b>Período Final </b><br />
								    <%: Html.TextBox("dataFim", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							    </p>
                            </div>
                            <div class="esquerda">
							    <p>
								    <b>Razão Social </b><br />
								    <%: Html.TextBox("razao", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							    </p>
                            </div>
                            <div class="direita">
								<p>
								<b>Estado</b><br />
									<%: Html.DropDownList("EstadoID", new SelectList(estados, "ID", "Nome"), "Selecione")%>
								</p>
                            </div>
                            <div class="esquerda">   
								<p>                             
								<b>Cidade</b><br />
									<%: Html.DropDownList("CidadeID", new SelectList(cidades, "ID", "Nome"), "Selecione", new { @disabled = "disabled" })%>
								</p>
                            </div>
                            
							<p>
								<button id="enviar" class="button green" type="submit" value="Save">
									<span>Gerar Relatório</span>
								</button>
							<span id="processando" class="note loading">Processando...</span>
							</p>
						<% } %>
                    
					<div>
                    
                        <% if (Model != null) { %>

						<%--<%: Html.Hidden("isCidadePrint", isCidade)%>--%>
						<center><b>Total de Pagamentos Cadastrados: <%: Model.TotalPagamentos  %></b></center>
						
                        <br />
                        <br />
                        <span class="titFiltros"> Filtros </span>
                        <center>
                            <div class="divRelatNotas">
                                <b>Data Início: </b><%: (DateTime)dataInicio == DateTime.MinValue ? "Qualquer" : ((DateTime)dataInicio).ToShortDateString()%>
                                <br />
                                <br />
                                <b>Data Fim: </b><%:  (DateTime)dataFim == DateTime.MinValue ? "Qualquer" : ((DateTime)dataFim).ToShortDateString()%>
                                <br />
                                <br />
                                <b>Razão Social: </b><%: razao == "" ? "Qualquer" : razao%>
                            </div>
                            <div class="divRelatNotas" >
                                <b>Estado: </b><%: estado == "" ? "Qualquer" : estado%>
                                <br />
                                <br />
                                <b>Cidade: </b><%: cidade == "" ? "Qualquer" : cidade%>
                            </div>
                        </center>
                        
                        <br /><br />
					    <% using (Html.BeginForm("PrintPagamentoEmpresa", "RelatoriosFinanceiros", FormMethod.Post, new { id = "form", name = "form", @target = "_blank" }))
                        { %>
						<%: Html.Hidden("printDataInicio", dataInicio)%>
						<%: Html.Hidden("printDataFim", dataFim)%>
						<%: Html.Hidden("printRazao", razao)%>
                        <%: Html.Hidden("printEstadoID", estadoID)%>
                        <%: Html.Hidden("printCidadeID", cidadeID)%>
                        <center style="clear:both;" ><b>Pagamentos Cadastrados por Empresa</b></center>

						<br /><br />
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								<th>
									Razão Social
								</th>
								<th>
									<center>Valores (R$)</center>
								</th>
								<th>
									<center>Número de Empresas</center>
								</th>
								<th>
									<center>Valor Médio</center>
								</th>
								</tr>
							</thead>
							<tbody>
                                <% foreach (var item in Model.ItensRelatorio)
                                   { %>
								<tr>
									<td>
										<b><%: item.Razao%></b>
									</td>
									<td>
										<center><%: item.Valores %></center>
									</td>
									<td>
										<center><%: item.TotalItens %></center>
									</td>
									<td>
										<center><%: item.ValorMedio %></center>
									</td>
								</tr>
                                <% } %>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <center><b>Total : R$ </b> <%: Model.TotalValor %></center>
                                    </td>
                                </tr>
									
							</tbody>
						</table>

						<hr />
						<p>
							<button class="button green" type="submit" value="Save">
								<span>Gerar Impressão</span>
							</button>
						</p>
                        <% } %>
                    <% } %>

					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

    $(document).ready(function () {

        $("#EstadoID").val("");

		$('#EstadoID').change(function () {
			$.ajaxSetup({ cache: false });
			var selectedItem = $(this).val();
			if (selectedItem == "" || selectedItem == 0) {
				//Do nothing or hide...?
			} else {
				$.post("/Services/Localizacao/GetCidades/" + $("#EstadoID > option:selected").attr("value"), function (data) {
					var items = "";
					items += "<option>Selecione</option>";
					$.each(data, function (i, data) {
						items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
					});
					$("#CidadeID").removeAttr('disabled');
					$("#CidadeID").html(items);
				});
			}
		});

        $("#erro").hide();
        $(".note.loading").hide();

        $("#dataInicio").mask("99/99/9999");
        $("#dataFim").mask("99/99/9999");

        $("#dataInicio").blur(function () {
            $("#printDataInicio").val($("#dataInicio").val());
        });

        $("#dataFim").blur(function () {
            $("#printDataFim").val($("#dataFim").val());
        });
        
        $("#razao").blur(function () {
            $("#printRazao").val($("#razao").val());
        });
        
        $("#EstadoID").blur(function () {
            $("#printEstadoID").val($("#EstadoID").val());
        });
        
        $("#CidadeID").blur(function () {
            $("#printCidadeID").val($("#CidadeID").val());
        });
        
        $("#enviar").click(function () {
            $("#Carregar").val(true);
        });

        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            },
        });
    });
	</script>
</asp:Content>