﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Areas/BackOffice/Views/Shared/BackOffice.Master" Inherits="System.Web.Mvc.ViewPage<WebUI.Areas.BackOffice.ViewModels.Relatorios.RelatorioPagamentos>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Relatório de Pagamentos Cadastrados por Período
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%
	var dataInicio = (DateTime)ViewData["dataInicio"];
    var dataFim = (DateTime)ViewData["DataFim"];
    var isCidade = ViewData["isCidade"] != null ? (bool)ViewData["isCidade"] : false;
	
%>
<div class="BreadCrumb">
    <%: Html.ActionLink("Relatórios Financeiros","Relatorios")%> / Pagamentos - Valores por Estado / Cidade
</div>
	<div class="container_12">
		<div class="grid_12">
			<div class="sb-box">
				<div class="sb-box-inner content">
					<div class="header">
						<ul class="tabs">
							<li class="active"><%: Html.ActionLink("Voltar", "Relatorios")%></li>
						</ul>
					</div>
					<div class="bcont">
						
						<% using (Html.BeginForm("PagamentosEstadoCidade", "RelatoriosFinanceiros", FormMethod.Post, new { id = "form", name = "form" }))
						   { %>
                           
						    <%: Html.Hidden("Carregar",false)%>

							<p>
								<b>Período Inicial </b><br />
								<%: Html.TextBox("dataInicio", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							</p>
							<p>
								<b>Período Final </b><br />
								<%: Html.TextBox("dataFim", "", new { @class = "inputtext medium", @maxlength = "128" })%>
							</p>
							<table>
                                <tr>
								    <td style="width:50px;">
                                        <center><b>Por Estado </b></center>
                                    </td>
                                    <td>
                                        <center><b>Por Cidade</b></center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%: Html.RadioButton("isCidade", false, true)%>
                                    </td>
                                    <td><%: Html.RadioButton("isCidade", true)%></td>
                                    <%: Html.ValidationMessage("isCidade")%>
                                </tr>
							</table>
							<p>
								<button id="enviar" class="button green" type="submit" value="Save">
									<span>Gerar Relatório</span>
								</button>
							<span id="processando" class="note loading">Processando...</span>
							</p>
						<% } %>
                    
					<div>
                    
                    <% if (Model != null) { %>
						<%--<%: Html.Hidden("isCidadePrint", isCidade)%>--%>
						<center><b>Total de Pagamentos Cadastrados: <%: Model.TotalPagamentos %></b></center>
                        
                        <br />
                        <br />

                        <center><b>Relatório de <%: dataInicio.ToShortDateString() %> a <%: dataFim.ToShortDateString() %></b></center>

                        <br /><br />
                <% if (!isCidade)
                   { %>
					    <% using (Html.BeginForm("PrintPagamentoEstado", "RelatoriosFinanceiros", FormMethod.Post, new { id = "form", name = "form", @target = "_blank" }))
            { %>
						<%: Html.Hidden("printDataInicio", dataInicio)%>
						<%: Html.Hidden("printDataFim", dataFim)%>
                        <center><b>Pagamentos Cadastrados por Estado</b></center>

						<br /><br />
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								    <th>
									    Estado
								    </th>
								    <th>
									    <center>Valores (R$)</center>
								    </th>
								    <th>
									    <center>
                                            Quantidade
                                        </center>
								    </th>
								    <th>
									    <center>Valor Médio</center>
								    </th>
								    <th class="small">
								    </th>
								</tr>
							</thead>
							<tbody>
                                <% foreach (var item in Model.ItensRelatorio)
                                   { %>
								<tr>
									<td>
										<b><%: item.SiglaNomeEstado%></b>
									</td>
									<td>
										<center><%: item.Valores%></center>
									</td>
									<td>
										<center><%: item.TotalItens%></center>
									</td>
									<td>
										<center><%: item.ValorMedio%></center>
									</td>
                                    <td></td>
								</tr>
                                <% } %>
                                <tr>
                                    <td>
                                        <center></center>
                                    </td>
                                    <td>
                                        <center><b>Total :</b> <%: Model.TotalValor%></center>
                                    </td>
                                    <td></td>
                                </tr>
									
							</tbody>
						</table>

						<hr />
						<p>
							<button class="button green" type="submit" value="Save">
								<span>Gerar Impressão</span>
							</button>
						</p>
                        <% }
                   } %>

                   <% else
                   {

                       using (Html.BeginForm("PrintPagamentoCidade", "RelatoriosFinanceiros", FormMethod.Post, new { id = "form", name = "form", @target = "_blank" }))
                       { %>
						    <%: Html.Hidden("printDataInicio", dataInicio)%>
						    <%: Html.Hidden("printDataFim", dataFim)%>
						    <%: Html.Hidden("Carregar", false)%>
                        <center><b>Pagamentos Cadastrados por Cidade</b></center>

						<br /><br />
						<table class="infotable" cellspacing="0" cellpadding="0" width="100%">
							<thead>
								<tr>
								    <th>
									    Estado
								    </th>
								    <th>
									    <center>Cidade</center>
								    </th>
								    <th>
									    <center>Valores (R$)</center>
								    </th>
								    <th>
									    <center>Quantidade</center>
								    </th>
								    <th>
									    <center>Valor Médio</center>
								    </th>
								    <th class="small">
								    </th>
								</tr>
							</thead>
							<tbody>
                                <% foreach (var item in Model.ItensRelatorio)
                                   { %>
								<tr>
									<td>
										<b><%: item.SiglaNomeEstado %></b>
									</td>
									<td>
										<center><b><%: item.NomeCidade%></b></center>
									</td>
									<td>
										<center><%: item.Valores%></center>
									</td>
									<td>
										<center><%: item.TotalItens%></center>
									</td>
									<td>
										<center><%: item.ValorMedio%></center>
									</td>
                                    <td></td>
								</tr>
                                <% } %>
                                <tr>
                                    <td></td>
                                    <td>
                                        <center></center>
                                    </td>
                                    <td>
                                        <center><b>Total :</b> <%: Model.TotalValor%></center>
                                    </td>
                                    <td></td>
                                </tr>
									
							</tbody>
						</table>

						<hr />
						<p>
							<button class="button green" type="submit" value="Save">
								<span>Gerar Impressão</span>
							</button>
						</p>
                    <% } %>
                    
                    <% } %>
                <% } %>
					</div>
				</div>
			</div>
		</div>
		<div class="clearingfix">
		</div>
	</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeaderContent" runat="server">
<script type="text/javascript">

    $(document).ready(function () {


        $("#erro").hide();
        $(".note.loading").hide();

        $("#dataInicio").mask("99/99/9999");
        $("#dataFim").mask("99/99/9999");

        $("#dataInicio").blur(function () {
            $("#printDataInicio").val($("#dataInicio").val());
        });

        $("#dataFim").blur(function () {
            $("#printDataFim").val($("#dataFim").val());
        });
        
        $("#enviar").click(function () {
            $("#Carregar").val(true);
        });

        $("#form").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
            },

            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#enviar").hide();
                $("#processando").show();
                form.submit();
            },
        });
    });
	</script>
</asp:Content>