﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> Armazenamento de NFe, (Notas Fiscais Eletrônicas, Arquivos xml), conheça o sistema</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Com um sistema fácil, seguro e eficiente, o envio e armazenamento das suas Notas Fiscais eletrônicas, (NFe, Arquivos xml) proporciona maior agilidade e produtividade" />
<meta name="keywords" content="NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, guardar notas, armazenamento nota fiscal, nota fiscal eletrônica, nota, fiscal, documentos eletrônicos, Danfe, CTe, Como Funciona, O Sistema, Software, Arquivo xml, emissão NF-e, Emissor Nota Fiscal Eletrônica, Emissor NFe" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="wrapper">
	    <h2>O <span>Armazenamento e Gerenciamento de Arquivos NF-e</span></h2>
	    <p>O <strong>NFSERVICE</strong> é um conjunto de soluções para empresas emitentes e destinatárias de nota  fiscal eletrônica, disponibilizando e armazenando através do nosso site, tanto  os arquivos NF-e emitidos quanto os arquivos recebidos de forma simples, fácil  e prática. Estes arquivos ficam armazenados pelo período de até 06 (seis) anos,  ou enquanto durar o contrato de prestação de serviços. <br />
	      <br />
	      Conforme previsto na cláusula décima do AJUSTE SINIEF 07/05, o <strong>emitente</strong> e o <strong>destinatário</strong> deverão armazenar o   arquivo digital as Notas Fiscais Eletrônicas pelo prazo estabelecido na  legislação tributária para guarda dos documentos fiscais, devendo ser  apresentada à administração tributária, quando solicitadas. <br />
  <br />
        Com o Armazenamento NF-e <strong>(NFSERVICE)</strong> sua empresa legaliza-se  perante o fisco, satisfazendo assim a exigência da cláusula décima do AJUSTE  SINIEF 07/05, que determina que o emissor da Nota Fiscal Eletrônica deve enviar  o arquivo digital da NF-e para o destinatário, seja em forma eletrônica ou por  qualquer outro meio que possibilite o destinatário ter acesso ao arquivo  digital. Nosso sistema de armazenamento NF-e  foi criado para cuidar desta responsabilidade  para você com total sigilo e segurança, e o mais importante é que tudo é feito  de forma simples e automática, com alta tecnologia. Os arquivos das NF-e são  armazenados em nosso servidor web e só podem ser acessados através de senha de  segurança. </p>
<br />
<p><strong>Praticidade  e agilidade no dia-a-dia da sua empresa.</strong><br />
  <br />
  Como Funciona:<br />
  Conforme  definido pela SEFAZ, é responsabilidade do contribuinte emissor e receptor  armazenar cada arquivo eletrônico NF-e, NFS-e, CT-e autorizada em local seguro,  pelo prazo exigido pela legislação tributária, para exibição ao fisco, quando  for solicitado. Esta NF-e, NFS-e, CT-e é um documento jurídico e serve de  histórico para consultas realizadas pelo fisco. <br />
  <br />
  O NFSERVICE foi criado para cuidar desta responsabilidade para você com total  sigilo e segurança, e o mais importante é que tudo é feito de forma simples e  automática, com alta tecnologia. </p>
<br /><br />
        
 
      <span class="tit2">A)  Confira abaixo o fluxo de funcionamento do armazenamento NF-e, NFS-e de Saída:</span> <br />
      
      <img src="/Content/images/fluxo-saida.gif" alt="Fluxo de saída" /><br />
<br />

      
      <br />
 
      <span class="tit2">B)  Confira abaixo o fluxo de funcionamento do armazenamento NF-e, NFS-e de ENTRADA:</span> <br />
            <img src="/Content/images/fluxo-entrada.gif" alt="Fluxo de entrada" /><br />
<br />


      <br />
        <span class="tit2">C)  Confira abaixo o fluxo de funcionamento com o Contador: </span>
      <p>O  Contador com o seu usuário e senha, acessa o site www.nfservice.com.br, e tem a  sua disposição todos os arquivos eletrônicos NF-e, NFS-e, CT-e, de entrada e  saída para fazer downloads. A NFSERVICE oferece ao contador um relatório  gerencial da situação mensal de todos os arquivos eletrônicos recebidos e que  ainda não recebemos. </p>
      <p><br />
        Implementação  Rápida:<br />
A  NFSERVICE atua em todo o território nacional, você acessa de qualquer ponto via  internet. <br />
Os  requisitos mínimos para o bom desempenho do sistema são:<br />
Mínimo de Banda: 1mb<br />
Versão Mínima dos Navegadores compatíveis: Firefox 3, Internet Explorer 7,  Google Chrome 10, Opera 10, Safari 5.<br />
<br />
<strong>Recomendamos:</strong> o navegador Google Chrome 10 para uso, pois possibilita o  envio de multiplos arquivos de uma só vez! <br />
<strong>Recursos</strong><br />
Nosso  sistema foi desenvolvido não somente para realizar o armazenamento de NF-e,  CT-e, NFS-e, mas também para agilizar seu trabalho e garantir que todas suas  obrigatoriedades perante o fisco sejam cumpridas. Com este objetivo,  apresentamos os seguintes recursos para sua empresa: </p>
      <p><strong>• </strong>Cadastrando o DANFE no sistema, a NFSERVICE  executará a cobrança do arquivo XML referente ao DANFE até que o mesmo seja  enviado ao sistema, evitando que as notas não sejam declaradas.<br />
        <strong>• </strong>Relatórios gerenciais sobre as  NF-e, CTe, NFSe arquivadas, trazendo as principais informações necessárias  como:<br />
        <strong>• </strong>Número total de NF-e, CTe, NFSe.<br />
        <strong>• </strong>NF-e, CTe, NFSe canceladas,<br />
        <strong>• </strong>NF-e, CTe, NFSe Denegadas.<br />
        <strong>• </strong>NF-e, CTe, NFSe por estado e município (Podendo  saber qual região do Brasil sua empresa mais se destacou).<br />
        <strong>• </strong>Download de NF-e, CTe, NFSe a qualquer momento.<br />
        <strong>• </strong>Envio de NF-e, CTe, NFSe por e-mail.<br />
        <strong>• </strong>Cadastro de contadores (Gerentes de Notas) para  manipulação de NF-e, CTe, NFSe.</p>
      <p><strong>• </strong>Tenha os dados de seus emitentes e destinatários a um  clique, em qualquer lugar do país e do mundo.    </p><br />

      <img src="/Content/images/comparacao-sefaz.gif" alt="Comparação com Sefaz" />

<br />
<br />

      <p><iframe width="420" height="315" src="http://www.youtube.com/embed/t0c6Qf26TuI" frameborder="0" allowfullscreen></iframe></p>
      <p>Quer  testar o sistema Gratuitamente <a href="/Cadastro/Create">clique aqui</a></p>
      <p>Blog: <a href="http://www.nfservice.blog.br" target="_blank" style="text-transform:lowercase;">www.nfservice.blog.br</a> <br />
        Loja Virtual: <a href="http://www.nfstore.com.br" target="_blank" style="text-transform:lowercase;">www.nfstore.com.br</a> <br />
        Facebook: <strong>NFSERVICE</strong></p>
<p>&nbsp;</p>
    </div>
    <div class="voltar">
		<a class="floatImage" href="<%: Url.Action("Index", "Home") %>"><img src="/Content/images/setaEsquerdaTransp.png" alt="Voltar à Home" /></a><br /><br />
		<%: Html.ActionLink("Voltar à Home", "Index", "Home", null, new { @class="float" }) %>
	</div>

</asp:Content>
