﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Domain.Services.Erro>>" %>
<div style="width: 300px; padding: 0px !important">
    <h2>
        E<span>nvie sua Nota</span> <span>por aqui.</span></h2>
    <div id="filelist">
    </div>
    <hr />
    <div id="loading" style="display: none">
        <div style="background-image: url('../../Content/images/carregando.gif'); background-repeat: no-repeat;
            height: 35px; width: 35px; float: left;">
        </div>
        <div style="padding-top: 15px; padding-left: 45px">
            <b>Aguarde...</b></div>
    </div>
    <div id="message-sucesso">
    <div id="sucesso" class="message success" style="display: none">
        <span class="strong">SUCESSO!</span> Nota(s) fiscal(is) cadastrada(s) com sucesso!<br/>
    </div>
    </div>
    <div id="message-erro">
        <div id="erro" class="message error" style="display: none">
            <span class="strong">FALHA AO ENVIAR ARQUIVO!</span>
            <br />
            <br />
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        do_plupload();
        //            'onError': function (a, b, c, d) {
        //                if (d.status == 404)
        //                    alert('Não foi possível encontrar o script de upload. use um caminho relativo para: ' + '<?= getcwd() ?>');
        //                else if (d.type === "HTTP")
        //                    alert('error ' + d.type + ": " + d.status);
        //                else if (d.type === "File Size")
        //                    alert(c.name + ' ' + d.type + ' Limite: ' + Math.round(d.sizeLimit / 1024) + 'KB');
        //                else
        //                    alert('error ' + d.type + ": " + d.text);
    });

        function do_plupload() {
            $("#filelist").pluploadQueue({
                // General settings
                runtimes: 'html5, gears,flash,silverlight',
                url: '/Home/UploadXml',
                max_file_size: '10mb',
                chunk_size: '1mb',
                preinit: attachCallbacks,
                setup: function (q) {
                    var tries = 0, maxTries = 5;

                    var remove_startupload = function () {
                        var $button = $("a.plupload_start");

                        if ($button.length) {
                            $button.hide();
                        }
                        else if (tries < maxTries) {
                            ++tries;
                            setTimeout(remove_startupload, 1000);
                        }
                    };

                    remove_startupload();
                },
                unique_names: true,
                dragdrop: false,
                multiple_queues: false,
                // Specify what files to browse for
                filters: [
            { title: "Arquivos Xml", extensions: "xml" }
        ],
                flash_swf_url: '/Scripts/plupload/plupload.flash.swf',
                // Silverlight settings
                silverlight_xap_url: '/Scripts/plupload/plupload.silverlight.xap'
            });

        }

            // added redirect function after uploaded
            function attachCallbacks(uploader) {
                uploader.bind('FilesAdded', function (up, files) {
                    setTimeout(function () { up.start(); }, 100);
                });

                uploader.bind('FileUploaded', function (up, file, res) {
                    var myJson = eval('(' + res.response + ')')
                    $("#loading").hide();
                    if (myJson[0].Tipo == "Sucess") {
                        if ($("#sucesso")[0]) {
                            $("#sucesso").show();
                        }
                        else {
                            $("#message-sucesso").html("<div id='sucesso' class='message success' style='display: none'><span class='strong'>SUCESSO!</span> Nota(s) fiscal(is) cadastrada(s) com sucesso!<br/></div>")
                        }
                        $.each(myJson, function (i, item) {
                            $("#sucesso").append("<span class='wrapper'>" + item.Rotulo + "</span>");
                        });
                    }
                    else {
                        if ($("#erro")[0]) {
                            $("#erro").show();
                        }
                        else {
                            $("#message-erro").html("<div id='erro' class='message error' style='display: block'><span class='strong'>FALHA AO ENVIAR ARQUIVO!</span><br /><br /></div>")
                        }
                        $.each(myJson, function (i, item) {
                            $("#erro").append("<span class='wrapper'>" + item.Detalhes + "</span></br>");
                        });

                    }
                });

                uploader.bind('UploadComplete', function () {
                    do_plupload();
                });
            }
</script>
