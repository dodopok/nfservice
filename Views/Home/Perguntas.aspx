﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Perguntas e Respostas
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="wrapper">
		<h2>P<span>erguntas e Respostas</span></h2>
		<h4>01. Como os contabilistas poderão escriturar uma NF-e recebida por uma empresa?</h4>		
        <br />
        <p>
        Os procedimentos e obrigatoriedade de escrituração fiscal não foram alterados com a NF-e, a NF-e é um 
            documento eletrônico, digital, não podendo ser impressa. A impressão de seu conteúdo não tem valor jurídico, 
            tampouco contábil/fiscal. É obrigação do destinatário verificar a autenticidade da NF-e por consulta ao Portal 
            Nacional ou ao site da SEFAZ autorizadora. Portanto em nenhuma hipótese o Contador poderá efetuar a escrituração 
            pelo Danfe impresso. Apenas pelo Arquivo eletrônico NF-e,  o emitente e o destinatário deverão manter em arquivo 
            digital as NF-e pelo prazo estabelecido na legislação tributária para a guarda dos documentos fiscais.
            <br />
            <br />
        A NF-e permite, no entanto, uma potencial simplificação do procedimento, ou seja, dependendo do nível de adaptação 
            que seja feita nos sistemas internos de escrituração, esta poderá ser automatizada em maior ou menor escala por 
            meio, por exemplo, de recuperação automática de informações do arquivo de uma NF-e. 
            <br />
            <br />
        OBS: Antes de escriturar uma NF-e,  deverá sempre verificar a validade da assinatura digital e a autenticidade do 
            arquivo digital da NF-e e a concessão da autorização de uso da NF-e, mediante consulta eletrônica ao Portal 
            Nacional da NF-e ou à SEFAZ da circunscrição do contribuinte emitente
            <br />
            <br />
        </p>
        
		<h4>02. A consulta da validade, existência e autorização de uma NF-e é obrigatória ou facultativa?</h4>
        <br />
        <p>
            A consulta da validade, existência e autorização de uma NF-e é obrigatória por parte do destinatário da NF-e. 
            <br /><br />
            Nos termos da cláusula décima, parágrafo primeiro, do Ajuste SINIEF 07/05, com redação dada pelo Ajuste SINIEF 04/06: 
            <br /><br />
            "§ 1º O destinatário deverá verificar a validade e autenticidade da NF-e e a existência de Autorização de Uso da NF-e. pelo arquivo eletrônico XML" 
            <br />
            <br />
        </p>
        
		<h4>03. Qual a forma estabelecida para a entrega da NF-e ao meu cliente? Esta entrega é obrigatória ou basta entregar o DANFE?</h4>		
        <br />
        <p>
        Não há regras estabelecidas da forma como o fornecedor irá entregar a NF-e a seu cliente, de modo que esta entrega pode 
            ocorrer da melhor maneira que aprouver às partes envolvidas. A transmissão, em comum acordo com as partes poderá 
            ocorrer, por exemplo: por e-mail, disponibilizada num site e acessível mediante uma senha etc. 
        <br /><br />
        Com relação à obrigatoriedade da entrega, o § 7º da cláusula sétima do Ajuste SINIEF 07/05 determina que o emitente da 
            NF-e deverá, obrigatoriamente, encaminhar ou disponibilizar download do arquivo eletrônico da NF-e e seu respectivo 
            protocolo de autorização ao destinatário, observado leiaute e padrões técnicos definidos em Ato COTEPE. 
            A cláusula décima do mesmo Ajuste determina que o emitente e o destinatário deverão manter em arquivo digital as 
            NF-e pelo prazo estabelecido na legislação tributária para a guarda dos documentos fiscais, sendo que, caso o 
            destinatário não seja contribuinte credenciado para a emissão de NF-e, alternativamente ao disposto acima, deverá 
            manter em arquivo o DANFE relativo à NF-e da operação.
            <br />
            <br />

        </p>
        
		<h4>04. Como enviar a Nota Fiscal Eletrônica para o destinatário (comprador) que não tenha acesso à internet? E no caso de exportação de mercadorias?</h4>		
        <br />
        <p>
            A legislação não estabeleceu uma forma específica de envio da NF-e para o destinatário. Cabe ao vendedor 
                (emissor da NF-e) encontrar a melhor forma de encaminhar ao seu cliente a NF-e. No caso do exportador, 
                vale a mesma regra.
            <br />
            <br />
        </p>
        
		<h4>05. Os clientes têm obrigatoriedade de acessar algum site e imprimir a NF-e? Em quantas vias? </h4>		
        <br />
        <p>
            A NF-e é um documento eletrônico, digital, não podendo ser impressa. A impressão de seu conteúdo não tem valor 
            jurídico, tampouco contábil/fiscal. É obrigação do destinatário verificar a autenticidade da NF-e por consulta 
            ao Portal Nacional ou ao site da SEFAZ autorizadora.
            <br />
            <br />
        </p>
        
		<h4>06. Ao receber uma fiscalização de ICMS ou IPI, ou comunicado de fiscalização, oque tenho que apresentar?</h4>		
        <br />
        <p>
            Como foi informado anteriormente, os livros de entradas e saídas não poderão ser feitos pelo DANFE, e sim pelos 
                arquivos eletrônicos. Portanto o Fiscal irá solicitar os arquivos eletrônicos de compra e venda conforme.
            <br />
            <br />
        </p>
        
		<h4>07. Estou recebendo uma mercadoria comprada, mas não recebi o arquivo eletrônico, estou apenas com o DANFE, o que devo fazer?</h4>
        <br />
        <p>
            O DANFE não tem valor jurídico, tampouco contábil/fiscal. O seu fornecedor tem por obrigação enviar o arquivo eletrônico, 
                e você tem por obrigação guardar, armazenar este arquivo pelo prazo estabelecido na legislação tributária para 
                guarda de documentos fiscais. Nesta situação você tem apenas duas saídas:
            <br />
            <br />
 
            <h4>1. </h4>Devolver a Mercadoria.
            <br />
            <h4>2. </h4> Confiar no Fornecedor, receber a mercadoria, e ficar lingando para ele solicitando o arquivo eletrônico, 
                            até que ele envie.
            <br />
            <h4>3. </h4> OBS: Uma vez recebido a mercadoria sem o arquivo eletrônico, o destinatário assume todas as responsabilidades, 
                            pois ele tem por obrigação receber o arquivo eletrônico, e conferir a sua validade.
                            
            <br />
            <br />
        </p>
        
		<h4>08. Estou escriturando meus livros fiscais pelo DANFE, qual risco estou correndo?</h4>		
        <br />
        <p>
            <h4>1. </h4>Levar multa, pois o DANFE não tem valor jurídico, tampouco contábil/fiscal. 
            <br />
            <h4>2. </h4>Perder o controle de qual arquivo eletrônico está faltando, e na fiscalização levar outra multa por 
                            não apresentar todos os arquivos eletrônicos 
            <br />
            <h4>3. </h4>Por não ter o arquivo eletrônico, você não tem condições de analisar o status do arquivo eletrônico. 
                            Portando se o arquivo estiver inválido ou cancelado você terá que recolher os impostos de seu fornecedor e levará multa por registrar documentos falsos.

            <br />
            <br />
        </p>
        
		<h4>09. Como os contabilistas terão acesso às NF-e de seus clientes?</h4>		
        <br />
        <p>
        Com relação às NF-e emitidas/recebidas, os contabilistas poderão requisitá-las junto a seus clientes, ou seja, todo mês 
            os clientes deverá enviar por algum modo todos os arquivos eletrônicos ao seu contador.
            <br />
            <br />
        </p>
        
		<h4>10. Quais são as condições e prazos para o cancelamento de uma NF-e? (atualizado em 31/12/08)</h4>		
        <br />
        <p>
            Somente poderá ser cancelada uma NF-e cujo uso tenha sido previamente autorizado pelo Fisco 
                (protocolo “Autorização de Uso”) e desde que não tenha ainda ocorrido o fato gerador, ou seja, ainda não tenha 
                ocorrido a saída da mercadoria do estabelecimento. Atualmente o prazo máximo para cancelamento de uma NF-e é de 
                168 horas (7 dias), contado a partir da autorização de uso.
            <br /><br />
            O Pedido de Cancelamento de NF-e deverá ser assinado pelo emitente com assinatura digital certificada por entidade 
                credenciada pela Infra-estrutura de Chaves Públicas Brasileira - ICP-Brasil, contendo o nº do CNPJ de qualquer dos 
                estabelecimentos do contribuinte, a fim de garantir a autoria do documento digital. A transmissão poderá ser realizada 
                por meio de software desenvolvido ou adquirido pelo contribuinte ou disponibilizado pela administração tributária. 
                Da mesma forma que a emissão de uma NF-e de circulação de mercadorias, o pedido de cancelamento também deverá ser 
                autorizado pela SEFAZ. O leiaute do arquivo de solicitação de cancelamento poderá ser consultado no Manual de Integração 
                do Contribuinte. 
            <br /><br />
            O status de uma NF-e (autorizada, cancelada, etc.) sempre poderá ser consultado no site da SEFAZ autorizadora 
                (Sefaz da unidade federada do emitente ou Sefaz-Virtual) ou no Portal Nacional da NF-e . 
            <br /><br />
            As NF-e canceladas, denegadas e os números inutilizados devem ser escriturados, sem valores monetários, de acordo com a 
                legislação tributária vigente. 
            <br />
            <br />
        </p>
        
        <b>OBS: Já foram constatados casos onde o emitente emite o arquivo eletrônico envia para o destinatário. 
            Para o emitente não pagar impostos ele cancela o arquivo eletrônico. Onde por sua vez que se o destinatário não 
            conferir, será responsabilizado por pagar os impostos do registro fiscal de uma NF-e cancelada.</b>

        <br />
        <br />
        <h4>Links</h4>
        <br />
        <br />
        <ul class="list">
            <li>
                <a href="http://www.nfe.fazenda.gov.br/portal/">Portal Nota Fiscal Eletrônica </a>
            </li>
            <li>
                <a href="http://www1.receita.fazenda.gov.br/Sped/">Portal SPED Fiscal</a> 
            </li>
            <li>
                <a href="http://www.cte.fazenda.gov.br/">Portal Conhecimento Transporte Eletrônico</a> 
            </li>
            <li>
                <a href="#">Agenda tributária</a> 
            </li>
        </ul>
</div>

</asp:Content>
