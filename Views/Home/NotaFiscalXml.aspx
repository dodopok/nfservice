﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<System.Xml.XmlDocument>" %>
<% this.Response.ContentType = "text/xml"; %>
<% this.Response.Charset = ""; %>
<% this.Response.ContentEncoding = Encoding.UTF8; %>
<% this.Response.AddHeader("Content-Disposition", "attachment;filename=\"NotasFiscais.xml\""); %>

<%= Model.InnerXml %>
