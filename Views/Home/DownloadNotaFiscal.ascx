﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<% using (Html.BeginForm("DownloadNota", "RecebimentoNotaFiscal", FormMethod.Get, new { enctype = "multipart/form-data", id = "formDownload", name = "formDownload" }))
   { %>
<h2>
    D<span>ownload da</span> <span>sua Nota Fiscal aqui.</span></h2>
Para efetuar o download de sua Nota Fiscal, insira o código do DANFe e clique em
Download.
<%
       if (Request.QueryString["cod"] == MvcExtensions.Controllers.Message.Failure.ToString())
       { %>
<div id="erroDanfe2" class="message error" style="display:none">
    <span class="strong">ERRO!</span><label>
        Não existe nota fiscal associada a este DANFe.</label>
</div>
<% } %>
<div id="error-danfe">
<div id="erroDanfe1" class="message error">
    <span class="strong">ERRO!</span><label>
        Código de DANFe inexistente.</label>
</div>
</div>
<div style="padding-top: 21px;">
    Código do Danfe
    <br />
    <%: Html.TextBox("Danfe1", "", new { @style = "width: 250px;" })%>
     <br />              <span id="em-danfe-validar" class="field-validation-error"></span>
    <p class="boxes">
        <table style="width: 100%">
            <tr>
                <td style="width: 30px;">
                    <div id="loading-waiting" style="display: none">
                        <img title="Excluir" alt="Loading" style="width: 30px; height: 30px;" src="/Content/images/loading.gif" />
                    </div>
                </td>
                <td align="center">
                    <input class="link" style="height: 40px; border: 0px; cursor: pointer; cursor: hand;
                        font-weight: bold;" type="button" name="btnDownloadDanfe" id="btnDownloadDanfe"
                        border="0" value="Download" />
                    <input class="link" name="btn" id="btn" type="button" style="display: none" />
                </td>
            </tr>
        </table>
    </p>
</div>
<div id="cadastro-email" class="jqmWindow">
    <h2>
        <span>Cadastro de E-mail</span></h2>
        <span style="font-size:11px;color:#29467D">Cadastre um e-mail para receber os arquivos e receba automaticamente em seu e-mail o XML a cada nova nota que entrar no sistema.</span>
    <br /><br />
    <table style="width: 100%">
        <tr>
            <td colspan="2" align="center" style="padding-left: 15px;">
                <%: Html.TextBox("Email1", "Email", new { @onclick = "if(this.value=='Email') this.value=''", @onblur = "if(this.value=='') this.value='Email'", @style = "", @class = "Login" })%>
                <span id="em-email-validar" class="field-validation-error"></span>
            </td>
        </tr>
        <tr>
            <td align="left">
                <br />
                <input class="link" style="height: 20px; border: 0px; cursor: pointer; cursor: hand;
                    font-weight: bold; padding-top: 0px;" type="button" name="btnSalvar" id="btnSalvar"
                    border="0" value="Salvar" />
            </td>
            <td align="right">
                <br />
                <input class="jqmClose link" style="height: 20px; border: 0px; cursor: pointer; cursor: hand;
                    font-weight: bold; padding-top: 0px;" type="button" name="btnCancelar" id="btnCancelar"
                    border="0" value="Cancelar" />
            </td>
        </tr>
    </table>
</div>
<% } %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#Danfe1").mask("9999-9999-9999-9999-9999-9999-9999-9999-9999-9999-9999");
        $("#error-danfe").html("");
        $("#btnDownloadDanfe").click(function () {
            $('#Email1').val("");
            var danfeID = $("#Danfe1").val();
            if (danfeID != '') {
                $("#em-danfe-validar").html("");
                $('#loading-waiting').show();
                $('#btnDownloadDanfe').attr("disabled", true);
                $.ajax(
                {
                    type: "GET",
                    url: "Home/VerificaEmailDestinatario",
                    cache: false,
                    data: { danfeID: danfeID },
                    success: function (data) {
                        if (data[0].Tipo == "Sucess") {
                            $('#loading-waiting').hide();
                            $('#btnDownloadDanfe').attr("disabled", false);
                            $("#formDownload").submit();
                        }
                        else
                            if (data[0].Tipo == "EmailError") {
                                $('#btnDownloadDanfe').attr("disabled", false);
                                $('#loading-waiting').hide();
                                $('#cadastro-email').jqmShow();
                            }
                            else
                                if (data[0].Tipo == "Error") {
                                    $('#btnDownloadDanfe').attr("disabled", false);
                                    $('#loading-waiting').hide();
                                    if ($("#erroDanfe1")[0]) {
                                        $("#erroDanfe1").show();
                                    }
                                    else {
                                        $("#error-danfe").html("<div id='erroDanfe1' class='message error' style='display:block'><span class='strong'>ERRO!</span><label>" + data[0].Detalhes + "</label></div>")
                                    }
                                }
                    },
                    error: function (req, status, error) {
                        $('#btnDownloadDanfe').attr("disabled", false);
                        $('#loading-waiting').hide();
                        alert("Erro na execução da ação. Tente mais tarde ou contacte o nosso suporte.");
                    }
                });
            }
            else {
                $("#em-danfe-validar").html("Danfe é obrigatório");
                $("#Danfe1").focus();
            }
        });

        $('#btnSalvar').click(function () {
            if ($("#Email1").val() != '') {
                var regmail = /^[\w!#$%&amp;'*+\/=?^`{|}~-]+(\.[\w!#$%&amp;'*+\/=?^`{|}~-]+)*@(([\w-]+\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
                if (regmail.test($("#Email1").val())) {
                    $("#em-email-validar").html("");
                    $('#cadastro-email').jqmHide();
                    $("#btn").click();
                    $('#Email1').val("");
                }
                else {
                    $("#em-email-validar").html("Digite um e-mail válido");
                }
            }
            else {
                $("#em-email-validar").html("E-mail é obrigatório");
                $("#Email1").focus();
            }
        });

        $('#btn').click(function () {
            $("#formDownload").submit();
        });

        $('#cadastro-email').jqm({ modal: true
        });
    });
</script>
