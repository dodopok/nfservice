﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Home
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="wrapper">
	<article class="col-1 col-indent">
		<div class="box">
			<h2>NFService</h2>
  <p>Somos uma empresa de tecnologia da informação, especializada em prover soluções e desenvolvimento de sistemas para o segmento empresarial. Com departamentos exclusivos para programação, análise, pesquisa, atualização e testes, a NFSERVICE está ganhando mercado com o desenvolvimento de softwares emissão NF-e, e Armazenamento e Gerenciamento de arquivos NF-e, de fácil utilização, adaptáveis a qualquer ramo da empresa Cliente, e sobretudo, atualizados diariamente de acordo com a legislação brasileira.</p>
            <p class="boxes"><%: Html.ActionLink("saiba mais", "Index", "NFService", null, new { @class = "link" })%></p>
		</div>
	</article>
	<article class="col-1 col-indent">
		<div class="box">
			<h2>I<span>nformações</span></h2>
			<ul class="list" >
                <li>
                    <a href="../../Content/Arquivos/Calendário de Pagamentos PIS PASEP.pdf" target="_blank">Calendário de Pagamentos - PIS/PASEP</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/CRCs - Conselho Regionais de Contabilidade dos Estados.pdf" target="_blank">CRC's</a>
                </li>
				<li>
                    <a href="../../Content/Arquivos/Obrigações Fiscais e Tabelas Praticas.pdf" target="_blank">Obrigações Fiscais e Tabelas Praticas</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/Postos Fiscais da Secretaria da Fazenda do Estado de São Paulo.pdf" target="_blank">Postos Fiscais da Secretaria da Fazenda - SP</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/Relação de Códigos de Receita (Contribuições Previdenciárias - GPS).pdf" target="_blank">Relação de Códigos de Receita (Contribuições Previdenciárias - GPS)</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/Secretarias de Fazenda.pdf" target="_blank">Secretarias de Fazenda</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/Tabela Alíquotas Simples Nacional.pdf" target="_blank">Tabela Alíquotas Simples Nacional</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/Tabela de Guarda de Documentos PF.pdf" target="_blank">Tabela de Guarda de Documentos PF</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/Tabela de Guarda de Documentos.pdf" target="_blank">Tabela de Guarda de Documentos</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/Tabela de Indicadores Econômicos 2009 2010 2011.pdf" target="_blank">Tabela de Indicadores Econômicos - 2009/2010/2011</a>
                </li>
				<li>
                    <a href="../../Content/Arquivos/TabelaCFOP.pdf" target="_blank">CFOP</a>
                    </li>
				<li>
                    <a href="../../Content/Arquivos/TabelaDDI.pdf" target="_blank">DDI</a>
                </li>
				<li>
                    <a href="../../Content/Arquivos/TabelaDistanciaCidades.pdf" target="_blank">Distância entre Cidades</a>
                </li>
				<li class="last">
                    <a href="../../Content/Arquivos/TabelaEstadosCapitais.pdf" target="_blank">Estados e Capitais</a>
                </li>
                <li>
                    <a href="../../Content/Arquivos/TabelaInflacao.pdf" target="_blank">Inflação</a>
                </li>
			</ul>
			<%--<p class="boxes"><%: Html.ActionLink("saiba mais", "Index", "OSistema", null, new { @class = "link" })%></p>--%>
		</div>
	</article>
	<article class="col-1">
		<div class="box" style="padding:0 25px 37px;">
			<h2>R<span>edução de</span> <span>Custos</span></h2>
            <ul class="listagem" style="padding:0">
                <li>
			        Elimina os custos ocultos da NF-e
                </li>
                <li>
                    Cobra seus fornecedores para enviarem os documentos fiscais eletrônicos

                </li>
                <li>
                    Reduz custos operacionais, agregando tecnologia e segurança as operações

                </li>
                <li>
                    Softwares de última geração
                </li>
            </ul>

			<p class="boxes"><%: Html.ActionLink("saiba mais", "Index", "Vantagens", null, new { @class = "link" })%></p>
            <br />
		</div>
       
        <div id="facebook" style="margin-bottom:5px; margin-top:20px;"><fb:like-box href="http://www.facebook.com/pages/NFSERVICE/134550983289298" width="300" height="240" show_faces="true" stream="false" header="false"></fb:like-box>
        </div>
	</article>
</div>
<div class="wrapper" style="margin-top: 10px;">
	<article class="col-1 col-indent">
		<div class="box">
            <% //Html.RenderPartial("EnviarNotaFiscal"); %><br />
<br />
<br />

           <strong style="color:#F00;"> Box de Upload de NFe em atualização...</strong>
        </div>
    </article>
	<article class="col-1 col-indent">
		<div class="box">
            <% Html.RenderPartial("DownloadNotaFiscal"); %>
        </div>
    </article>
	<article class="col-1 col-indent" style="width: 300px; padding-right: 0px;">
		<div class="box">
		    <h2>N<span>ewsletter</span></h2>
		    <strong class="color"></strong><br />
		    <p>Cadastre-se e receba nossa Newsletter. Receba novidades sobre o sistema, e tudo sobre notas fiscais eletrônicas.</p>
            <p class="boxes"><%: Html.ActionLink("saiba mais", "Index", "Newsletter", null, new { @class = "link" })%></p>
        </div>
        <div style="width:299px; text-align:center;"><img src="/Content/images/chatonlinehome.gif" alt="Atendimento OnLine" style="margin-top:25px; margin-bottom:10px;" /><br /><script type="text/javascript" src="http://settings.messenger.live.com/controls/1.0/PresenceButton.js"></script>
<div id="Microsoft_Live_Messenger_PresenceButton_ad3363a35e6f59ca" msgr:width="100" msgr:backColor="#77ADCF" msgr:altBackColor="#FFFFFF" msgr:foreColor="#424542" msgr:conversationUrl="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=ad3363a35e6f59ca@apps.messenger.live.com&mkt=pt-BR&useTheme=true&themeName=blue&foreColor=333333&backColor=E8F1F8&linkColor=333333&borderColor=AFD3EB&buttonForeColor=333333&buttonBackColor=EEF7FE&buttonBorderColor=AFD3EB&buttonDisabledColor=EEF7FE&headerForeColor=0066A7&headerBackColor=8EBBD8&menuForeColor=333333&menuBackColor=FFFFFF&chatForeColor=333333&chatBackColor=FFFFFF&chatDisabledColor=F6F6F6&chatErrorColor=760502&chatLabelColor=6E6C6C" style="margin-left:100px;"></div>
<script type="text/javascript" src="http://messenger.services.live.com/users/ad3363a35e6f59ca@apps.messenger.live.com/presence?dt=&mkt=pt-BR&cb=Microsoft_Live_Messenger_PresenceButton_onPresence"></script></div>
    </article>
</div>
</asp:Content>

