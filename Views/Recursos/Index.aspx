﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> Armazenamento de NFe, (Notas Fiscais Eletrônicas, Arquivos xml), recursos</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Recursos, um sistema fácil, seguro e eficiente, o envio e armazenamento das suas Notas Fiscais eletrônicas, (NFe, Arquivos xml) proporciona maior agilidade e produtividade" />
<meta name="keywords" content="Recursos, NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, guardar notas, armazenamento nota fiscal, nota fiscal eletrônica, nota, fiscal, documentos eletrônicos" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="wrapper">
		<h2>R<span>ecursos</span></h2>
		<div class="wrapper top3">
			Nosso sistema foi desenvolvido não somente para realizar o arquivamento de NF-e, CT-e, NFS-e, mas também para agilizar seu trabalho e garantir que todas suas obrigatoriedades perante o fisco sejam cumpridas. Com este objetivo, apresentamos os seguintes recursos para sua empresa:
		</div>
        <br />
        <h4>•</h4>	Verificação junto ao SEFAZ se a nota foi cancelada por um período de sete dias.<br />
        <h4>•</h4>	Cadastrando o DANFE no sistema, a NFSERVICE executará a cobrança do arquivo XML referente ao DANFE até que o mesmo seja enviado ao sistema, evitando que as notas não sejam declaradas.<br />
        <h4>•</h4>	Relatórios gerenciais sobre as NF-e, CTe, NFSe arquivadas, trazendo as principais informações necessárias como:<br />
        <h4><span>•</span></h4>	Número total de NF-e, CTe, NFSe.<br />
        <h4><span>•</span></h4>	NF-e, CTe, NFSe canceladas,<br />
        <h4><span>•</span></h4>	NF-e, CTe, NFSe Denegadas.<br />
        <h4><span>•</span></h4>	NF-e, CTe, NFSe por estado e município (Podendo saber qual região do Brasil sua empresa mais se destacou).<br />
        <h4>•</h4>	Download de NF-e, CTe, NFSe a qualquer momento.<br />
        <h4>•</h4>	Envio de NF-e, CTe, NFSe por e-mail.<br />
        <h4>•</h4>	Cadastro de contadores (Gerentes de Notas) para manipulação de NF-e, CTe, NFSe.<br />
        <h4>•</h4>	Tenha os dados de seus emitentes e destinatários a um clique, em qualquer lugar do país e do mundo. 

</div>
<div class="voltar">
	<a class="floatImage" href="<%: Url.Action("Index", "Home") %>"><img src="/Content/images/setaEsquerdaTransp.png" alt="Voltar à Home" /></a><br /><br />
	<%: Html.ActionLink("Voltar à Home", "Index", "Home", null, new { @class="float" }) %>
</div>

</asp:Content>
