﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> Armazenamento de NFe, (Notas Fiscais Eletrônicas, Arquivos xml), conheça nossos Parceiros</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Conheça alguns de nossos parceiros" />
<meta name="keywords" content="NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, emissão NF-e, Emissor NFe, Websocorro, Afsystems, Focontabil, Netcetera" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <h2>Parceiros</h2>
  Confira alguns de nossos parceiros<br />
  <br />
  <br />
  <a href="http://www.websocorro.com.br/" target="_blank"><img src="/content/images/bannerWebSocorro.jpg" alt="Agência WebSocorro" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" /></a>
  <a href="http://www.afsystems.com.br/" target="_blank"><img src="/content/images/banner-af-systems.gif" alt="AF Systems" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" /></a> 
  <a href="http://www.itamarmariano.com.br/" target="_blank"><img src="/content/images/banner-itamar-mariano.gif" alt="Itamar Mariano" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" /></a> 
  <img src="/content/images/banner-foco-contabil.gif" alt="Foco Contábil" border="0" style="margin-right:3px; margin-bottom: 10px; border:none;" /> <br />
  <a href="http://www.agblues.com.br/" target="_blank"><img src="/content/images/banner-blues.gif" alt="Blues" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" /></a> 
  <a href="http://www.netcetera.co.uk/" target="_blank"><img src="/content/images/banner-netcetera.gif" alt="Net Cetera" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" /></a> 
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server"> </asp:Content>
