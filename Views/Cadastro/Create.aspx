﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master"
    Inherits="System.Web.Mvc.ViewPage<WebUI.ViewModels.Cadastro>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Cadastro
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="" />
<meta name="keywords" content="" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            $('#cobrancaBuscaCepButton').click(function () {
                var cep = $('#EmpresaCEPCobranca').val();
                if (cep.length == 9) {
                    cep = cep.toString().replace('-', '');
                    CallCepService(cep, 'cobranca');
                }
            });

            $('#empresaBuscaCepButton').click(function () {
                var cep = $('#EmpresaCEPPrincipal').val();
                if (cep.length == 9) {
                    cep = cep.toString().replace('-', '');
                    CallCepService(cep, 'empresa');
                }
            });

            $('#buscaCepButton').click(function () {
                var cep = $('#UsuarioCEP').val();
                if (cep.length == 9) {
                    cep = cep.toString().replace('-', '');
                    CallCepService(cep, 'usuario');
                }
            });
        });

        function scrollWin($obj) {
            $('html,body').animate({
                scrollTop: $obj.offset().top
            }, 2000);
        }

        function DefinirCidade(cidade, $cidade) {
            var idcidade = -1;
            $('option', $cidade).each(function () {

                if (cidade == $(this).html().toLowerCase()) {
                    idcidade = eval($(this).attr('value'));
                }
            });
            $cidade.val(idcidade);
        }

        var __tipo = '';

        function CallCepService(_cep, tipo) {
            $.ajaxSetup({ cache: false });
            __tipo = tipo;
            $.getJSON("PreencheCep", { cep: _cep }, PreencheEndereco);
        }

        function PreencheEndereco(data, tipo) {
            tipo = __tipo;
            if (tipo == 'usuario') {
                $cep = $('#erro');
                $logradouro = $('#UsuarioLogradouro');
                $bairro = $('#UsuarioBairro');
                $cidade = $('#UsuarioCidadeID');
                $estado = $('#UsuarioEstadoID');
            }
            else if (tipo == 'empresa') {
                $cep = $('#erro');
                $logradouro = $('#EmpresaLogradouroPrincipal');
                $bairro = $('#EmpresaBairroPrincipal');
                $cidade = $('#EmpresaCidadeIDPrincipal');
                $estado = $('#EmpresaEstadoIDPrincipal');
            }
            else if (tipo == 'cobranca') {
                $cep = $('#erro');
                $logradouro = $('#EmpresaLogradouroCobranca');
                $bairro = $('#EmpresaBairroCobranca');
                $cidade = $('#EmpresaCidadeIDCobranca');
                $estado = $('#EmpresaEstadoIDCobranca');
            }

            if (data.Cep == "ERRO") {
                $('#erro label').html(data.Logradouro);
                $logradouro.val('');
                $bairro.val('');
                $cidade.val('');
                $estado.val('');
                $('#erro').show();
                scrollWin($cep);
            } else {
                $('#erro').hide();

                $logradouro.val(data.Logradouro);
                $bairro.val(data.Bairro);

                var estado = data.Estado.toLowerCase();
                var cidade = data.Cidade.toLowerCase();
                var idestado = -1;
                $('option', $estado).each(function () {
                    if (estado == $(this).html().toLowerCase()) {
                        idestado = eval($(this).attr('value'));
                    }
                });
                $estado.val(idestado);
                $estado.trigger('change');
                setTimeout(function () { DefinirCidade(cidade, $cidade); }, 1500);
                $.ajaxSetup({ cache: true });
            }
        }
    </script>
    <% Html.EnableClientValidation(); %>
    <div id="sucesso" class="message success">
        <span class="strong">SUCESSO!</span><label>
            Cadastro efetuado com sucesso.</label>
    </div>
    <div id="erro" class="message error">
        <span class="strong">ERRO!</span><label>
            Verifique todos os campos abaixo.</label>
    </div>
    <%
        var estados = ViewData["Estados"] == null ? new List<Domain.Entities.Estado>() : (IEnumerable<Domain.Entities.Estado>)ViewData["Estados"];
        var cidades = ViewData["Cidades"] == null ? new List<Domain.Entities.Cidade>() : (IEnumerable<Domain.Entities.Cidade>)ViewData["Cidades"];
        var status = ViewData["Status"] == null ? new List<Domain.Entities.StatusEmpresa>() : (IEnumerable<Domain.Entities.StatusEmpresa>)ViewData["Status"];   
    %>
    <% using (Html.BeginForm("Create", "Cadastro", FormMethod.Post, new { id = "form", name = "form" }))
       {  %>
    <%: Html.HiddenFor(model => model.UsuarioID) %>
    <br />
    <div id="form">
        <br />
        <h4>
            Usuário</h4>
        <div class="formulario">
            <div class="esquerda">
                <p>
                    <b>Nome</b><br />
                    <%: Html.TextBoxFor(model => model.Nome, new { @class="grande" })%>
                    <%: Html.ValidationMessageFor(model => model.Nome) %>
                </p>
            </div>
            <div class="direita">
                <p>
                    <b>Email</b><br />
                    <%: Html.TextBoxFor(model => model.Email, new { @class = "medio" })%>
                    <%: Html.ValidationMessageFor(model => model.Email) %>
                </p>
            </div>
            <div class="esquerda">
                <p>
                    <b>Senha</b><br />
                    <%: Html.PasswordFor(model => model.Senha, new { @maxlength = "128"})%>
                    <%: Html.ValidationMessageFor(model => model.Senha) %>
                </p>
            </div>
            <div class="direita">
                <p>
                    <b>Confirmação de Senha</b><br />
                    <%: Html.Password("ConfirmaSenha", Model !=  null ? Model.Senha : "", new { @class = ""})%>
                    <%: Html.ValidationMessage("ConfirmaSenha")%>
                </p>
            </div>
            <div class="esquerda">
                <p>
                    <b>CPF</b><br />
                    <%: Html.TextBoxFor(model => model.CPF) %>
                    <%: Html.ValidationMessageFor(model => model.CPF) %>
                </p>
            </div>
            <div class="direita">
                <p>
                    <b>Telefone</b><br />
                    <%: Html.TextBoxFor(model => model.Telefone) %>
                    <%: Html.ValidationMessageFor(model => model.Telefone) %>
                </p>
            </div>
            <div class="esquerda">
                <p>
                    <b>Celular</b><br />
                    <%: Html.TextBoxFor(model => model.Celular) %>
                    <%: Html.ValidationMessageFor(model => model.Celular) %>
                </p>
                <br />
            </div>
        </div>
        <br />
        <div>
            <hr />
            <h4>
                Endereço do Usuário</h4>
            <div class="formulario">
                <div class="esquerda">
                    <p>
                        <b>CEP</b><br />
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <%: Html.TextBoxFor(model => model.UsuarioCEP)%>
                                    <%: Html.ValidationMessageFor(model => model.UsuarioCEP)%>
                                </td>
                                <td>
                                    <input type="button" id="buscaCepButton" value="Pesquisar" />
                                </td>
                            </tr>
                        </table>
                    </p>
                </div>
                <div class="direita">
                </div>
                <div class="esquerda">
                    <p>
                        <b>Logradouro</b><br />
                        <%: Html.TextBoxFor(model => model.UsuarioLogradouro, new { @class = "grande" })%>
                        <%: Html.ValidationMessageFor(model => model.UsuarioLogradouro)%>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Bairro</b><br />
                        <%: Html.TextBoxFor(model => model.UsuarioBairro)%>
                        <%: Html.ValidationMessageFor(model => model.UsuarioBairro)%>
                    </p>
                </div>
                <div class="esquerda">
                    <p>
                        <b>Complemento</b><br />
                        <%: Html.TextBoxFor(model => model.UsuarioComplemento)%>
                        <%: Html.ValidationMessageFor(model => model.UsuarioComplemento)%>
                    </p>
                </div>
                <div class="direita">
                </div>
                <div class="esquerda">
                    <p>
                        <b>Estado</b><br />
                        <%: Html.DropDownListFor(model => model.UsuarioEstadoID, new SelectList(estados, "ID", "Nome"), "Selecione")%>
                        <%: Html.ValidationMessageFor(model => model.UsuarioEstadoID)%>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Cidade</b><br />
                        <%: Html.DropDownListFor(model => model.UsuarioCidadeID, new SelectList(cidades, "ID", "Nome"), "Selecione")%>
                        <%: Html.ValidationMessageFor(model => model.UsuarioCidadeID)%>
                    </p>
                </div>
            </div>
        </div>
        <div>
            <hr />
            <h4>
                Empresa</h4>
            <div class="formulario">
                <div class="esquerda">
                    <p>
                        <b>Razão Social</b><br />
                        <%: Html.TextBoxFor(model => model.RazaoSocial, new { @class = "grande" })%>
                        <%: Html.ValidationMessageFor(model => model.RazaoSocial) %>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>CNPJ</b><br />
                        <%: Html.TextBoxFor(model => model.CNPJ) %>
                        <%: Html.ValidationMessageFor(model => model.CNPJ)%>
                    </p>
                </div>
                <div class="esquerda">
                    <p>
                        <b>Nome Fantasia</b><br />
                        <%: Html.TextBoxFor(model => model.NomeFantasia, new { @class = "grande" })%>
                        <%: Html.ValidationMessageFor(model => model.NomeFantasia) %>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Inscrição Estadual</b><br />
                        <%: Html.TextBoxFor(model => model.InscricaoEstadual, new { @class = "inputtext medium", @maxlength = "32"})%>
                        <%: Html.ValidationMessageFor(model => model.InscricaoEstadual)%>
                    </p>
                </div>
                <div class="esquerda">
                    <p>
                        <b>Inscrição Municipal</b><br />
                        <%: Html.TextBoxFor(model => model.InscricaoMunicipal, new { @class = "inputtext medium", @maxlength = "32"})%>
                        <%: Html.ValidationMessageFor(model => model.InscricaoMunicipal)%>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Email da Empresa</b><br />
                        <%: Html.TextBoxFor(model => model.EmpresaEmail, new { @class = "medio" })%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaEmail)%>
                    </p>
                </div>
                <div class="esquerda">
                    <p>
                        <b>Telefone Principal</b><br />
                        <%: Html.TextBoxFor(model => model.TelefonePrincipal) %>
                        <%: Html.ValidationMessageFor(model => model.TelefonePrincipal)%>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Telefone Alternativo</b><br />
                        <%: Html.TextBoxFor(model => model.TelefoneAlternativo) %>
                        <%: Html.ValidationMessageFor(model => model.TelefoneAlternativo)%>
                    </p>
                </div>
            </div>
        </div>
        <div>
            <br />
            <br />
            <hr />
            <h4>
                Endereço Principal da Empresa</h4>
            <div class="formulario">
                <div class="esquerda">
                    <p>
                        <b>CEP</b><br />
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <%: Html.TextBoxFor(model => model.EmpresaCEPPrincipal) %>
                                    <%: Html.ValidationMessageFor(model => model.EmpresaCEPPrincipal)%>
                                </td>
                                <td>
                                    <input type="button" id="empresaBuscaCepButton" value="Pesquisar" />
                                </td>
                            </tr>
                        </table>
                    </p>
                </div>
                <div class="direita">
                </div>
                <div class="esquerda">
                    <p>
                        <b>Logradouro</b><br />
                        <%: Html.TextBoxFor(model => model.EmpresaLogradouroPrincipal, new { @class = "grande" })%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaLogradouroPrincipal) %>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Bairro</b><br />
                        <%: Html.TextBoxFor(model => model.EmpresaBairroPrincipal) %>
                        <%: Html.ValidationMessageFor(model => model.EmpresaBairroPrincipal)%>
                    </p>
                </div>
                <div class="esquerda">
                    <p>
                        <b>Complemento</b><br />
                        <%: Html.TextBoxFor(model => model.EmpresaComplementoPrincipal) %>
                        <%: Html.ValidationMessageFor(model => model.EmpresaComplementoPrincipal) %>
                    </p>
                </div>
                <div class="direita">
                </div>
                <div class="esquerda">
                    <p>
                        <b>Estado</b><br />
                        <%: Html.DropDownListFor(model => model.EmpresaEstadoIDPrincipal, new SelectList(estados, "ID", "Nome"), "Selecione")%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaEstadoIDPrincipal) %>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Cidade</b><br />
                        <%: Html.DropDownListFor(model => model.EmpresaCidadeIDPrincipal, new SelectList(cidades, "ID", "Nome"), "Selecione")%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaCidadeIDPrincipal)%>
                    </p>
                </div>
            </div>
        </div>
        <div>
            <hr />
            <h4>
                Endereço de Cobrança da Empresa</h4>
            <br />
            <button id="btnCopiarEndereco" value="CopiarEnderecoPrincipal" type="button">
                <span>Usar mesmos dados do endereço principal</span>
            </button>
            <div class="formulario">
                <div class="esquerda">
                    <p>
                        <b>CEP</b><br />
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <%: Html.TextBoxFor(model => model.EmpresaCEPCobranca)%>
                                    <%: Html.ValidationMessageFor(model => model.EmpresaCEPCobranca)%>
                                </td>
                                <td>
                                    <input type="button" id="cobrancaBuscaCepButton" value="Pesquisar" />
                                </td>
                            </tr>
                        </table>
                    </p>
                </div>
                <div class="direita">
                </div>
                <div class="esquerda">
                    <p>
                        <b>Logradouro</b><br />
                        <%: Html.TextBoxFor(model => model.EmpresaLogradouroCobranca, new { @class = "grande" })%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaLogradouroCobranca)%>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Bairro</b><br />
                        <%: Html.TextBoxFor(model => model.EmpresaBairroCobranca)%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaBairroCobranca)%>
                    </p>
                </div>
                <div class="esquerda">
                    <p>
                        <b>Complemento</b><br />
                        <%: Html.TextBoxFor(model => model.EmpresaComplementoCobranca)%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaComplementoCobranca)%>
                    </p>
                </div>
                <div class="direita">
                </div>
                <div class="esquerda">
                    <p>
                        <b>Estado</b><br />
                        <%: Html.DropDownListFor(model => model.EmpresaEstadoIDCobranca, new SelectList(estados, "ID", "Nome"), "Selecione")%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaEstadoIDCobranca)%>
                    </p>
                </div>
                <div class="direita">
                    <p>
                        <b>Cidade</b><br />
                        <%: Html.DropDownListFor(model => model.EmpresaCidadeIDCobranca, new SelectList(cidades, "ID", "Nome"), "Selecione")%>
                        <%: Html.ValidationMessageFor(model => model.EmpresaCidadeIDCobranca)%>
                    </p>
                </div>
            </div>
        </div>
        <hr />
        <h4>
            Como chegou até nós</h4>
        <br />
        <%: Html.RadioButton("ComoChegou", "Representantes",true, new { onclick = "MostraComoChegou('Representantes')" })%>Representantes
        <%: Html.RadioButton("ComoChegou", "Tele-Vendas", new { onclick = "MostraComoChegou('Tele-Vendas')" })%>Tele-Vendas
        <%: Html.RadioButton("ComoChegou", "Google", new { onclick = "MostraComoChegou('Google')" })%>Google
        <%: Html.RadioButton("ComoChegou", "TV", new { onclick = "MostraComoChegou('TV')" })%>TV
        <%: Html.RadioButton("ComoChegou", "Revistas", new { onclick = "MostraComoChegou('Revistas')" })%>Revistas
        <%: Html.RadioButton("ComoChegou", "Rádios", new { onclick = "MostraComoChegou('Rádios')" })%>Rádios
        <br />
        <div id="divComoChegou" class="formulario">
            <div class="esquerda">
                <p>
                    <b>Nome do Representante</b><br />
                    <%: Html.TextBox("txtComoChegou","", new { @class = "grande" })%>
                    <%: Html.ValidationMessage("txtComoChegou") %>
                </p>
            </div>
        </div>
        <hr />
        <br />
        <a href="../../Content/Arquivos/Contrato_NF-Service.pdf" target="_blank">Clique aqui
        </a>para ler o "Contrato de Uso"
        <br />
        <br />
        <%: Html.CheckBox("TermosdeUso",false) %>
        Aceito os Termos do "Contrato de Uso"
        <p>
            <input type="submit" id="Enviar" value="Enviar" disabled="disabled" />
        </p>
    </div>
    <% } %>
    <div class="voltar">
        <a class="floatImage" href="<%: Url.Action("Index", "Home") %>">
            <img src="/Content/images/setaEsquerdaTransp.png" alt="Voltar à Home" /></a><br />
        <br />
        <%: Html.ActionLink("Voltar à Home", "Index", "Home", null, new { @class="float" }) %>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
    <% var sucess = ViewData["Sucess"] != null ? (bool)ViewData["Sucess"] : false;%>
    <script type="text/javascript">
	function MostraComoChegou(value)    {
		if(value == 'Representantes'){
				$("#divComoChegou").show();
				$("#txtComoChegou").attr("disabled",false);
		}
		else{
		    $("#divComoChegou").hide();
		    $("#txtComoChegou").attr("disabled",true);
		}
	};

	$(document).ready(function () {

		Validate();

		$("#Enviar").attr("disabled", !$("#TermosdeUso").attr("checked"));
		$("#TermosdeUso").click(function () {
			 $("#Enviar").attr("disabled", !$("#TermosdeUso").attr("checked"));
		});

		

		$("#btnCopiarEndereco").click(function () {
			$("#EmpresaLogradouroCobranca").val($("#EmpresaLogradouroPrincipal").val())
			$("#EmpresaBairroCobranca").val($("#EmpresaBairroPrincipal").val())
			$("#EmpresaComplementoCobranca").val($("#EmpresaComplementoPrincipal").val())
			$("#EmpresaCEPCobranca").val($("#EmpresaCEPPrincipal").val())
			$("#EmpresaEstadoIDCobranca").val($("#EmpresaEstadoIDPrincipal").val())
			$("#EmpresaEstadoIDCobranca").trigger("change")
			$("#EmpresaCidadeIDCobranca").val($("#EmpresaCidadeIDPrincipal").val())
		});

		$.validator.addMethod(
			"JaExisteCadastro",
			function (value, element) {
				$.ajaxSetup({ cache: false, async: false });

				var result = false;

				$.get("/Services/Cadastro/EstaCadastrado?email=" + value, function (data) {
					result = !data;
				});

				return result;

			},
			"Cadastro já efetuado para este endereço de e-mail"
		);

		if ($("#EmpresaEstadoIDPrincipal > option:selected").attr("value") == "")
			$("#EmpresaCidadeIDPrincipal").attr("disabled", "disabled");

		if ($("#EmpresaEstadoIDCobranca > option:selected").attr("value") == "")
			$("#EmpresaCidadeIDCobranca").attr("disabled", "disabled");

		if ($("#UsuarioEstadoID > option:selected").attr("value") == "")
			$("#UsuarioCidadeID").attr("disabled", "disabled");

		$("#sucesso").hide();
		$("#erro").hide();
		
		<% if(sucess) { %>
			$("#sucesso").show();
			$("#erro").hide();
			$("#form").hide();
		<% } else if (ViewData["Sucess"] != null && !sucess) { %>
			$("#sucesso").hide();
			$("#erro").show();
		<% } %>

		$(".note.loading").hide();

		$("#Endereco_CEP").mask("99999-999");
		$("#Telefone").mask("(99)9999-9999");
		$("#Celular").mask("(99)9999-9999");
		$("#CPF").mask("999.999.999-99");
		$("#TelefonePrincipal").mask("(99)9999-9999");
		$("#TelefoneAlternativo").mask("(99)9999-9999");
		$("#DataCriacao").mask("99/99/9999");
		$("#DataAlteracao").mask("99/99/9999");
		$("#CNPJ").mask("99.999.999/9999-99");
		$("#EmpresaCEPPrincipal").mask("99999-999");
		$("#EmpresaCEPCobranca").mask("99999-999");
		$("#UsuarioCEP").mask("99999-999");

		$("#form").validate({
			meta: "validate",
			invalidHandler: function (form, validator) {
				$("#erro").show();
				$("#erro").focus();
			},

			submitHandler: function (form) {
				$("#erro").hide();
				$("#cancelar").hide();
				$("#enviar").hide();
				$("#processando").show();
				form.submit();
			},
			rules: {
				Nome: "required",
				Email: {
					required: true,
					email: true,
					JaExisteCadastro: true
				},
				Senha: "required",
				ConfirmaSenha: {
					required: true,
					equalTo: "#Senha"
				},
				CPF: "required",
				Telefone: "required",
				Celular: "required",
				UsuarioLogradouro: "required",
				UsuarioBairro: "required",
				UsuarioCEP: "required",
				UsuarioCidadeID:
				{
					noneSelected: true
				},
				RazaoSocial: "required",
				NomeFantasia: "required",
				CNPJ: "required",
				TelefonePrincipal: "required",
				EmpresaEmail: {
					required: true,
					email: true
				},
				StatusID: {
					required: true
				},
				UsuarioResponsavel: "required",
				EmpresaLogradouroPrincipal: "required",
				EmpresaBairroPrincipal: "required",
				EmpresaCEPPrincipal: "required",
				EmpresaCidadeIDCobranca: {
					noneSelected: true
				},
				EmpresaCidadeIDPrincipal: {
					noneSelected: true
				},
				EmpresaLogradouroCobranca: "required",
				EmpresaBairroCobranca: "required",
				EmpresaCEPCobranca: "required",
				txtComoChegou: "required"
			},
			messages: {
				Nome: "Este campo é obrigatório.",
				Email: {
					required: "Este campo é obrigatório.",
					email: "Formato de e-mail inválido",
					JaExisteCadastro: "Já existe cadastro para esse email"
				},
				Senha: "Este campo é obrigatório.",
				ConfirmaSenha: {
					required: "Este campo é obrigatório.",
					equalTo: "As senhas devem ser iguais."
				},
				CPF: "Este campo é obrigatório.",
				Telefone: "Este campo é obrigatório.",
				Celular: "Este campo é obrigatório.",
				UsuarioLogradouro: "Este campo é obrigatório.",
				UsuarioBairro: "Este campo é obrigatório.",
				UsuarioCEP: "Este campo é obrigatório.",
				UsuarioCidadeID: {
					noneSelected: "Este campo é obrigatório."
				},
				RazaoSocial: "Este campo é obrigatório.",
				NomeFantasia: "Este campo é obrigatório.",
				CNPJ: "Este campo é obrigatório.",
				TelefonePrincipal: "Este campo é obrigatório.",
				StatusID:
				{
					required: "Este campo é obrigatório."
				},
				EmpresaEmail: {
					required: "Este campo é obrigatório.",
					email: "Formato de e-mail inválido"
				},
				EmpresaLogradouroPrincipal: "Este campo é obrigatório.",
				EmpresaBairroPrincipal: "Este campo é obrigatório.",
				EmpresaCEPPrincipal: "Este campo é obrigatório.",
				EmpresaCidadeIDCobranca: "Este campo é obrigatório.",
				EmpresaCidadeIDPrincipal: "Este campo é obrigatório.",
				EmpresaLogradouroCobranca: "Este campo é obrigatório.",
				EmpresaBairroCobranca: "Este campo é obrigatório.",
				EmpresaCEPCobranca: "Este campo é obrigatório.",
				txtComoChegou: "Este campo é obrigatório."
			}
		});

		$('#UsuarioEstadoID').change(function () {
			$.ajaxSetup({ cache: false });
			var selectedItem = $(this).val();
			if (selectedItem == "" || selectedItem == 0) {
				//Do nothing or hide...?
			} else {
				$.post("/Services/Localizacao/GetCidades/" + $("#UsuarioEstadoID > option:selected").attr("value"), function (data) {
					var items = "";

					items += "<option>Selecione</option>";
					$.each(data, function (i, data) {
						items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
					});

					$("#UsuarioCidadeID").removeAttr('disabled');
					$("#UsuarioCidadeID").html(items);
				});
			}
		});

		$('#EmpresaEstadoIDPrincipal').change(function () {
			$.ajaxSetup({ cache: false });
			var selectedItem = $(this).val();
			if (selectedItem == "" || selectedItem == 0) {
				//Do nothing or hide...?
			} else {
				$.post("/Services/Localizacao/GetCidades/" + $("#EmpresaEstadoIDPrincipal > option:selected").attr("value"), function (data) {
					var items = "";

					items += "<option>Selecione</option>";
					$.each(data, function (i, data) {
						items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
					});

					$("#EmpresaCidadeIDPrincipal").removeAttr('disabled');
					$("#EmpresaCidadeIDPrincipal").html(items);
				});
			}
		});

		$('#EmpresaEstadoIDCobranca').change(function () {
			$.ajaxSetup({ cache: false, async: false });
			var selectedItem = $(this).val();
			if (selectedItem == "" || selectedItem == 0) {
				//Do nothing or hide...?
			} else {
				$.post("/Services/Localizacao/GetCidades/" + $("#EmpresaEstadoIDCobranca > option:selected").attr("value"), function (data) {
					var items = "";
					items += "<option>Selecione</option>";
					$.each(data, function (i, data) {
						items += "<option value='" + data.ID + "'>" + data.Nome + "</option>";
					});

					$("#EmpresaCidadeIDCobranca").removeAttr('disabled');
							 $("#EmpresaCidadeIDCobranca").html(items);
				});
			}
		});
	});
    </script>
    <script type="text/javascript" src="/Scripts/woow.validateextensions.js"></script>
</asp:Content>
