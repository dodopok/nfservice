﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Cadastre-se, um sistema fácil, seguro e eficiente, o envio e armazenamento das suas Notas Fiscais eletrônicas, (NFe, Arquivos xml) proporciona maior agilidade e produtividade" />
<meta name="keywords" content="Cadastre-se, NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, guardar notas, armazenamento nota fiscal, nota fiscal eletrônica, nota, fiscal, documentos eletrônicos" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <table width="900" align="center">
        <tbody>
            <tr>
                <td>
                    <h2>
                        Confirmação de cancelamento.</h2>
                    <br />
                    <br />
                    <div style="height:120px">
                        <div id="email_enviado" class="box">
                            <h4>
                                Cancelamento realizado!</h4>
                            <br />
                            Esta é a confirmação de que o email <strong>
                                <%= ViewData["Email"] %></strong> foi cancelado de nossa lista de notificações
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
