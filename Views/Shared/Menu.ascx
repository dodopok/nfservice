﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>


<% var controllerName = this.ViewContext.RequestContext.RouteData.Values["controller"].ToString(); %>
<div class="main">
<% var failure = Request.QueryString["email"] != null ?
	   Request.QueryString["email"] == MvcExtensions.Controllers.Message.Failure.ToString() : false; %>
<script type="text/javascript">

	$(document).ready(function () {

		$(".note.loading").hide();

		var fail = "<%: failure %>";
		if (fail == "True") {
			alert('Email não encontrado no sistema');
		}

		$("#form-Login").validate({
			meta: "validate",
			invalidHandler: function (form, validator) {
				$("#erro").show();
				$("#erro").focus();
				if ($("#Login").val() == "")
					$("#Login").val("Login");
				if ($("#SenhaLogin").val() == "")
					$("#SenhaLogin").val("Senha");
			},

			submitHandler: function (form) {
				$("#erro").hide();
				$("#cancelar").hide();
				$("#enviar").hide();
				$("#processando").show();
				form.submit();
			},
			rules: {
				Login: {
					required: true,
					email: true
				},
				Senha: "required"
			},
			messages: {
				Login: {
					required: "Este campo é obrigatório.",
					email: "Formato de e-mail inválido"
				},
				Senha: "Este campo é obrigatório."
			}
		});

		$("#enviarLogin").click(function () {
			if ($("#Login").val() == "Login")
				$("#Login").val("");
			if ($("#SenhaLogin").val() == "Senha")
				$("#SenhaLogin").val("");
		});
	});
</script>
	<div class="wrapper">
		<h1><%: Html.ActionLink("NFservice", "Index", "Home")%></h1>
		<div class="fright">
			<% using (Html.BeginForm("LogOn", "Home", FormMethod.Post, new { id = "form-Login", name = "form-Login"})) {%>
			<ul class="menu">
				<li>
				<div style="width: 540px;height:20px;">
					<p style="float: right;padding-top: 0;width: 64px;">
						<button id="enviarLogin" type="submit" value="Save" >
							<span>Enviar</span>
						</button>
					</p>
					<div style="width:220px;float:left;">
						<%: Html.TextBox("Login", "Login", new { @onclick = "if(this.value=='Login') this.value=''", @onblur = "if(this.value=='') this.value='Login'", @style = "", @class = "Login" }) %>
						<%: Html.ValidationMessage("Login") %>
					</div>
					<div style="width:220px;float:left;">
						<%: Html.TextBox("SenhaLogin", "Senha", new { @onclick = "if(this.value=='Senha') this.value=''", @onblur = "if(this.value=='') this.value='Senha'", @type = "password", @class = "Login" }) %>
						<%: Html.ValidationMessage("Senha")%>
					</div>
				</div>
				</li>
			</ul>
			<% } %>
			<div class="clear"></div>
				<nav>
					<ul id="menu">
					<% if (controllerName == "OSistema") { %>
						<li><%: Html.ActionLink("O SISTEMA", "Index", "OSistema", new { @class = "active" })%></li>
					<% } else { %>
						<li><%: Html.ActionLink("O SISTEMA", "Index", "OSistema")%></li>
					<% } %>

					<% if (controllerName == "SegurancaFiscal")
                        { %>
						<li><%: Html.ActionLink("SEGURANCA", "Index", "SegurancaFiscal", new { @class = "active" })%></li>
					<% } else { %>
						<li><%: Html.ActionLink("SEGURANCA", "Index", "SegurancaFiscal")%></li>
					<% } %>

					<% if (controllerName == "Vantagens")
						{ %>
						<li><%: Html.ActionLink("VANTAGENS", "Index", "Vantagens", new { @class = "active" })%></li>
					<% } else { %>
						<li><%: Html.ActionLink("VANTAGENS", "Index", "Vantagens")%></li>
					<% } %>

					<% if (controllerName == "Planos")
						{ %>
						<li><%: Html.ActionLink("PLANOS", "Index", "Planos", new { @class = "active" })%></li>
					<% } else { %>
						<li><%: Html.ActionLink("PLANOS", "Index", "Planos")%></li>
					<% } %>

					<% if (controllerName == "Cadastro")
						{ %>
						<li><%: Html.ActionLink("CADASTRE-SE", "Create", "Cadastro", new { @class = "active" })%></li>
					<% } else { %>
						<li><%: Html.ActionLink("CADASTRE-SE", "Create", "Cadastro")%></li>
					<% } %>
					<% if (controllerName == "FaleConosco")
						{ %>
						<li><%: Html.ActionLink("CONTATO", "Index", "FaleConosco", new { @class = "active" })%></li>
					<% } else { %>
						<li><%: Html.ActionLink("CONTATO", "Index", "FaleConosco")%></li>
					<% } %>
				</ul>
			</nav>
		</div>
	</div>
</div>