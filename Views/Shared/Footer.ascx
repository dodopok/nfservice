﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<footer>
	<div class="main">
		<div class="wrapper">
			<nav>
				<ul>
					<li><%: Html.ActionLink("O Sistema", "Index", "OSistema") %></li>
					<li><%: Html.ActionLink("Segurança", "Index", "SegurancaFiscal")%></li>
					<li><%: Html.ActionLink("Vantagens", "Index", "Vantagens") %></li>
					<li><%: Html.ActionLink("Planos", "Index", "Planos") %></li>
					<li><%: Html.ActionLink("Cadastre-se", "Create", "Cadastro") %></li>
					<li><%: Html.ActionLink("Contato", "Index", "FaleConosco") %></li>
					<li><%: Html.ActionLink("Newsletter", "Index", "Newsletter")%></li>
				</ul>
			</nav>
            <p>Desenvolvido por <a href="http://www.websocorro.com.br/" target="_blank">Agência WebSocorro</a>
            
                <a href="" onClick="Javascript: newWindow=openPopup(
                   'https://www.certificadodigital.com.br/selo/selo?serial=66E344D2819F0D1&aki=B760A85BF9B2A6AE00ED74EBD54AC9966866F55C',
                   'SERASA','width=690,height=540,status=0,scrollbars=yes,top=0,left=50');
                   newWindow.focus()">

                   <img src="../../Content/images/SeloSerasa.gif" border="0">
                <a>
                </p>
		</div>
		<div style="">
		</div>
		<!-- {%FOOTER_LINK} -->
	</div>
</footer>

<script language="JavaScript">
    function openPopup(windowURL, windowName, windowFeatures) {
        return window.open(windowURL, windowName, windowFeatures);
    }
</script>


