﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<style type="text/css">
    b.titFlash
    {
        font-family: Verdana;
        font-size: 29px;
        letter-spacing: -1px;
        line-height: 1.2em;
        margin: 0;
        padding: 20px 0 7px;
        text-transform: uppercase;
    }
</style>

<div id="faded">
	<ul>
		<li>
			<img src="/Content/images/imposto.jpg" alt="" />
			<div class="text">
				<b class="titFlash">Segurança Fiscal
				<span></span></b>
				<p>
                    Mais confiabilidade e segurança para todos os seus documentos fiscais eletrônicos. 
                        Soluções para a gestão de todo o ciclo de vida dos documentos eletrônicos com flexibilidade e ampla capacidade 
                        de gerenciamento dos processos de negócios. <%--Centralização do acesso e das operações envolvendo documentos fiscais 
                        eletrônicos, oferecendo maior produtividade e facilidade de uso para os usuários, gerando eficiência e redução 
                        de custos para as empresas.--%>
                        <br />
                </p>
				<%: Html.ActionLink("Saiba Mais","Index","SegurancaFiscal") %>
			</div>
		</li>
		<li>
			<img src="/Content/images/seguranca_e_privacidade.jpg" alt="" />
			<div class="text">
				<b class="titFlash">Segurança e Privacidade
				<span></span></b>
				<p>
                Nosso Servidor NETCETERA tem Certificação ISO27001 – (Técnicas de segurança — Sistemas de gestão de segurança da informação). Esta Norma foi preparada para prover um modelo para estabelecer, implementar, operar, monitorar, analisar criticamente, manter e melhorar um Sistema de Gestão de Segurança da Informação (SGSI). 
                </p>
                 <%: Html.ActionLink("Saiba Mais","Index","SegurancaFiscal") %>
			</div>
		</li>
		<li>
			<img src="/Content/images/slide3.jpg" alt="" />
			<div class="text">
				<b class="titFlash">Blog de Perguntas e Respostas
				<span></span></b>
				<p>Tire suas dúvidas aqui, aproveite, respondemos as principais perguntas que estão sendo feitas referente a emissão eletrônica e escrituração eletronica. </p>
                
                <a href='http://www.nfservice.blog.br' target="_blank">Saiba Mais</a>
			</div>
		</li>
	</ul>
</div>
