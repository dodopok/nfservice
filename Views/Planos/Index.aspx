﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Domain.Entities.Plano>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> Armazenamento de NFe, (Notas Fiscais Eletrônicas, Arquivos xml), confira nossos planos</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Nosso sistema de armazenamento de NF-e, foi projetada para atender qualquer tipo e tamanho de empresas, por isso temos um pacote de planos comerciais bem diversificados, variando desde pequena empresa para grandes empresas" />
<meta name="keywords" content="NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, guardar notas, armazenamento nota fiscal, nota fiscal eletrônica, nota, fiscal, documentos eletrônicos, Danfe, CTe, Planos, Mensalidades, Preço, Valor, Licença, emissão NF-e, Emissor Nota Fiscal Eletrônica, Emissor NFe" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="wrapper">
    <h2>P<span>lanos</span> <div style="float:right; width:200px;"><img src="/Content/images/chatonline.gif" alt="Atendimento OnLine" style="float:left; margin-right:3px; margin-top:3px;" /><script type="text/javascript" src="http://settings.messenger.live.com/controls/1.0/PresenceButton.js"></script>
<div id="Microsoft_Live_Messenger_PresenceButton_ad3363a35e6f59ca" msgr:width="100" msgr:backColor="#77ADCF" msgr:altBackColor="#FFFFFF" msgr:foreColor="#424542" msgr:conversationUrl="http://settings.messenger.live.com/Conversation/IMMe.aspx?invitee=ad3363a35e6f59ca@apps.messenger.live.com&mkt=pt-BR&useTheme=true&themeName=blue&foreColor=333333&backColor=E8F1F8&linkColor=333333&borderColor=AFD3EB&buttonForeColor=333333&buttonBackColor=EEF7FE&buttonBorderColor=AFD3EB&buttonDisabledColor=EEF7FE&headerForeColor=0066A7&headerBackColor=8EBBD8&menuForeColor=333333&menuBackColor=FFFFFF&chatForeColor=333333&chatBackColor=FFFFFF&chatDisabledColor=F6F6F6&chatErrorColor=760502&chatLabelColor=6E6C6C" style="float:right;"></div>
<script type="text/javascript" src="http://messenger.services.live.com/users/ad3363a35e6f59ca@apps.messenger.live.com/presence?dt=&mkt=pt-BR&cb=Microsoft_Live_Messenger_PresenceButton_onPresence"></script></div></h2>
    <p>A NFSERVICE oferece armazenamento seguro de notas fiscais e conhecimentos de transporte durante 5 anos com planos que cabem no seu bolso. Você cliente tem a liberdade de escolher qual plano melhor se enquadra para sua empresa.</p>
    <p align="center" style="text-align:center;"><img src="/Content/images/banner-sapa.jpg" alt="Sendo nosso cliente você ajuda os animais abandonados" /></p>
    <br />
    <br />
    <div class="box1" style="width: 95%;">
        <div class="tail-top" style="background: lightgray;">
            <div class="padding-box" style="width: initial; height: 80px;">
                <div class="tituloPlano">
                    <h3>Taxa de inscrição por CNPJ</h3>
                </div>
                <center><p class="preco">Preço: R$ 100,00.</p></center>
            </div>
        </div>
    </div>
</div>

<div class="wrapper">
    <% foreach (var plano in Model){ %>
    <div class="box1">
        <div class="tail-top">
            <div class="tail-right">
                <div class="tail-bot">
                    <div class="tail-left">
                        <div class="corner-top-left">
                            <div class="corner-top-right">
                                <div class="corner-bot-right">
                                    <div class="corner-bot-left">
                                        <div class="padding-box">
                                            <div class="tituloPlano">
                                                <h3><%: plano.Nome %></h3>
                                            </div>
                                            <p class="descricao"><%: plano.Descricao %></p>
                                            <center><p class="preco">Preço: R$<%: plano.Valor %> mensais.</p></center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <% } %>
</div>
<div class="voltar">
	<a class="floatImage" href="<%: Url.Action("Index", "Home") %>"><img src="/Content/images/setaEsquerdaTransp.png" alt="Voltar à Home" /></a><br /><br />
	<%: Html.ActionLink("Voltar à Home", "Index", "Home", null, new { @class="float" }) %>
</div>

</asp:Content>
