﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> Armazenamento de NFe, (Notas Fiscais Eletrônicas, Arquivos xml), receba nosso informativo</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Receba por e-mail boletins diários sobre legislação Contábil, Fiscal e Tributária, Cadastre-se em nossa Newsletter" />
<meta name="keywords" content="NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, emissão NF-e, Emissor NFe, e-mail,  boletins, legislação,  Contábil, Fiscal, Tributária, Newsletter" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<% Html.EnableClientValidation(); %>


<div class="wrapper">
<div id="erro" class="message error">
	<span style="font-size:20px;">ERRO!</span>
     Verifique todos os campos abaixo.
</div>
<%if (Request.QueryString["Newsletter"] == MvcExtensions.Controllers.Message.Sucess.ToString()) { %>
	<div id="sucesso" class="message success">
		<span style="font-size:20px;">SUCESSO!</span> Foi enviado um email de confirmação!
	</div>
<%} %>
        <h2>R<span>eceber Newsletter</span></h2>
            <% using (Html.BeginForm("CadastrarNewsletter", "Newsletter", FormMethod.Post, new { id = "form-News", name = "form-News" }))
               {  %>
                <div class="formularioFaleConosco" style="width:360px;float:left;margin:0 75px;">
                    <p>
                        <strong>Nome</strong>
                        <br />
                        <%: Html.TextBox("NomeNews", null, new { @class = "inputtext medium" })%>
                    </p>
                    <p>
                        <strong>E-mail</strong>
                        <br />
                        <%: Html.TextBox("EmailNews", null, new { @class = "inputtext medium" })%>
                    </p>
                    <p>
			            <input type="submit" value="Enviar" />
		            </p>
                </div>
            <% } %>
        <div class="box faleConosco" >
            <h4>Contato Comercial</h4>
            <p>
                <h4>Telefone:</h4> Ligue agora para (19)3542-8677 e fale com um dos nossos vendedores.
                <br /> 
                    Ou pelo e-mail vendas@nfservice.com.br, e solicite uma apresentação.
                <br />
                <br />
                <h4>SKYPE:</h4>  nfservice.gerenciamento.nfe
                <br />
                <br />
                Cadastre-se em nosso twitter ou facebook e receba informações diárias sobre alterações de leis fiscais, e atualizações dos sistema!<br />
                <a href="http://www.facebook.com/profile.php?id=100002115500630" target="_blank"><img src="/Content/images/botao_facebook.png" alt="Facebook" /></a>
                <a href="http://twitter.com/#!/nfservice" target="_blank"><img src="/Content/images/botao_twitter.png" alt="Twitter" /></a>
            </p>
        </div>
    <%--<div class="box">
    </div>--%>
</div>
<div class="voltar">
	<a class="floatImage" href="<%: Url.Action("Index", "Home") %>"><img src="/Content/images/setaEsquerdaTransp.png" alt="Voltar à Home" /></a><br /><br />
	<%: Html.ActionLink("Voltar à Home", "Index", "Home", null, new { @class="float" }) %>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server">

<script type="text/javascript">
    $(document).ready(function () {
        $("#erro").hide();
        $(".note.loading").hide();
        $("#form-News").validate({
            meta: "validate",
            invalidHandler: function (form, validator) {
                $("#erro").show();
                $("#erro").focus();
                $("#sucesso").hide();
            },
            submitHandler: function (form) {
                $("#erro").hide();
                $("#cancelar").hide();
                $("#sucesso").hide();
                $("#enviarNews").hide();
                $("#processandoNews").show();
                form.submit();
            },
            rules: {
                NomeNews: "required",
                EmailNews: {
                    required: true,
                    email: true
                }
            },
            messages: {
                NomeNews: "Este campo é obrigatório.",
                EmailNews: {
                    required: "Este campo é obrigatório.",
                    email: "Formato de e-mail inválido",
                }
            }
        });
    });

</script>

</asp:Content>
