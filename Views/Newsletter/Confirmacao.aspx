﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Confirmacao
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Receba nossos informativos, um sistema fácil, seguro e eficiente, o envio e armazenamento das suas Notas Fiscais eletrônicas, (NFe, Arquivos xml) proporciona maior agilidade e produtividade" />
<meta name="keywords" content="Informativos, NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, guardar notas, armazenamento nota fiscal, nota fiscal eletrônica, nota, fiscal, documentos eletrônicos" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Confirmação</h2>
    <div style="height: 120px">
        <div id="sucesso" class="message success">
            <span style="font-size: 20px;">SUCESSO!</span> Newsletter cadastrado com sucesso!
        </div>
    </div>
</asp:Content>
