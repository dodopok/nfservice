﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> Armazenamento de NFe, (Notas Fiscais Eletrônicas, Arquivos xml), conheça nossa empresa</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Somos uma empresa de tecnologia da informação, especializada em prover soluções e desenvolvimento de sistemas para o segmento empresarial. Com departamentos exclusivos para programação, análise, pesquisa, atualização e testes, a NFSERVICE está ganhando mercado com softwares emissão NF-e, e Armazenamento e Gerenciamento de arquivos NF-e, de fácil utilização" />
<meta name="keywords" content="NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, guardar notas, armazenamento nota fiscal, Danfe, CTe, Programação, tecnologia da informação, emissão NF-e, Emissor Nota Fiscal Eletrônica, Emissor NFe" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <div class="wrapper">
    <h2>NFSERVICE</h2>
    <p> <img src="/content/images/fotoPredio.jpg" alt="Prédio principal" style="float:right; margin-left:15px;" />Somos uma empresa de tecnologia da informação, especializada em prover soluções e desenvolvimento de sistemas para o segmento empresarial.<br />
      <br />
      Com departamentos exclusivos para programação, análise, pesquisa, atualização e testes, a NFSERVICE está ganhando mercado com o desenvolvimento de softwares emissão NF-e, e Armazenamento e Gerenciamento de arquivos NF-e, de fácil utilização, adaptáveis a qualquer ramo da empresa Cliente, e sobretudo, atualizados diariamente de acordo com a legislação brasileira. <br />
      <br />
      Com uma equipe altamente preparada, atualizada e com larga experiência no mercado, oferecemos suporte ao atendimento de fiscalização e diagnóstico detalhado da empresa, no que diz respeito ao atendimento das especificações definidas pelo fisco. Obtendo resultados satisfatórios, com muita segurança e praticidade. Oferecemos as ferramentas necessárias que se ajustam às necessidades de todas as empresas, processando grandes volumes de notas fiscais, com experiências.<br />
      <br />
      Um dos diferenciais da empresa é não se limitar a ser uma simples fornecedora de softwares. A NFSERVICE se dedica à manutenção e à atualização dos softwares de seus Clientes. O empresário e/ou contador utilizam os Sistemas NFSERVICE dentro de seus escritórios e tiram suas dúvidas por telefone ou on-line, de maneira fácil, prática e rápida. O empenho de toda a equipe, aliado aos investimentos constantes em estrutura e pessoal, alavancam o crescimento da NFSERVICE.<br />
      <br />
      O Suporte Técnico representa hoje de 50% do quadro de colaboradores da NFSERVICE e dispõe de grande capacidade de atendimento devido à tecnologia utilizada, que foi desenvolvido pela própria empresa e que é atualizado constantemente; hoje estamos no processo de certificação ISO 27001 com isso, a NFSERVICE tem o controle de todo o andamento do processo, desde a primeira ligação até a resolução do problema, tudo em tempo real. <br />
      <br />
      <img src="/content/images/fotoTelemarketing.jpg" alt="Telemarketing" style="float:right; margin-left:15px; margin-bottom: 10px;" />O Gerenciador e Armazenamento de arquivos NF-e, CT-e nasceu da visão de um futuro que já se apresenta, onde a convergência e difusão dos documentos fiscais eletrônicos se tornam essenciais para a vida das pessoas e empresas. A emissão das notas fiscais e dos conhecimentos de transporte em forma eletrônica (NF-e e CT-e) são apenas os estágios iniciais de uma iniciativa que está transformando a maneira como as organizações fazem negócios.<br />
      <br />
      Os documentos eletrônicos precisam ser preservados em segurança, organizados e disponíveis para todas as partes interessadas, conferindo maior agilidade aos processos logísticos, reduzindo erros operacionais e convertendo as informações num importante ativo organizacional.<br />
      <br />
      A plataforma tecnológica da NFSERVICE, em conjunto com a eficiência e especialização da sua equipe operacional, oferece mais confiabilidade para as empresas, que passam a contar com a certeza que seus documentos são enviados e recebidos de forma imediata, confiável, transparente e segura. O gerenciamento dos documentos, feito de forma centralizada através de sistemas especialmente desenvolvidos e contando com o suporte da nossa central de atendimento Suporte, permite o pronto atendimento de clientes, fornecedores, bancos, auditores e demais integrantes do ecossistema de negócios da empresa, além, é claro, do Fisco.<br />
      <br />
      A NFSERVICE está hoje sediada na cidade de Araras – SP á 180 km da Capital São Paulo. <br />
      <br />
      <a href="parceiros"><strong>&raquo; Confira alguns de nossos Parceiros... </strong></a> <br />
     
      <a href="clientes"><strong>&raquo; Confira alguns de nossos Clientes... </strong></a> <br />
      <br />
      <strong>Telefone:</strong><br />
      19-3542-8677 / 19-7823-9348<br />
      <br />
      <strong>Endereço:</strong><br />
      Avenida Washington Luiz , 250 – Sala 301 - Caixa Postal 324 - Centro - Araras/SP - CEP: 13600-720<br />
      <br />
      <iframe marginheight="0" marginwidth="0" src="http://www.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Avenida+Washington+Luiz+,+250+-+Centro+-+Araras%2FSP&amp;aq=&amp;sll=-14.239424,-53.186502&amp;sspn=42.743284,78.837891&amp;ie=UTF8&amp;hq=&amp;hnear=Av.+Washington+Luiz,+250+-+Centro,+Araras+-+S%C3%A3o+Paulo,+13600-720&amp;z=14&amp;ll=-22.35905,-47.379631&amp;output=embed" frameborder="0" height="600" scrolling="no" width="938"></iframe>
      <br>
      <small><a href="http://www.google.com.br/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=Avenida+Washington+Luiz+,+250+-+Centro+-+Araras%2FSP&amp;aq=&amp;sll=-14.239424,-53.186502&amp;sspn=42.743284,78.837891&amp;ie=UTF8&amp;hq=&amp;hnear=Av.+Washington+Luiz,+250+-+Centro,+Araras+-+S%C3%A3o+Paulo,+13600-720&amp;z=14&amp;ll=-22.35905,-47.379631" style="color:#0000FF;text-align:left">Exibir mapa ampliado</a></small> </p>
  </div>
  <div class="voltar"> <a class="floatImage" href="<%: Url.Action("Index", "Home") %>"><img src="/Content/images/setaEsquerdaTransp.png" alt="Voltar à Home" /></a><br />
    <br />
    <%: Html.ActionLink("Voltar à Home", "Index", "Home", null, new { @class="float" }) %>
  </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server"> </asp:Content>
