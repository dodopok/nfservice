﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterSemBanner.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server"> Armazenamento de NFe, (Notas Fiscais Eletrônicas, Arquivos xml), confira nossos Clientes</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MetaTags" runat=server>
<meta name="description" content="Conheça alguns de nossos clientes" />
<meta name="keywords" content="NFe, Armazenamento, armazenagem, Nota Fiscal Eletronica, Nota Fiscal, Armazenar Notas, emissão NF-e, Emissor NFe, KBG Carnes, Orion, Fuganholi, Schuf Fetterolf do Brasil" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <h2>Clientes</h2>
  Confira alguns de nossos clientes<br />
  <br />
  <br />
  <img src="/content/images/banner-kbg.gif" alt="KBG" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" />
  <img src="/content/images/banner-comercial_bressan.gif" alt="Comercial Bressan" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" />
  <img src="/content/images/banner-fuganholi.gif" alt="Metalúrgica Fuganholi" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" />
  <img src="/content/images/banner-sebal.gif" alt="Sebal Comércio de Balanças" border="0" style="margin-right:3px; margin-bottom: 10px; border:none;" />
  <img src="/content/images/banner-fama.gif" alt="Fama Usinagem de Torno, Mandrilhadora e Radial" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" />
  <img src="/content/images/banner-alma.gif" alt="Alma Equipamentos para Pulverização" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" />
  <img src="/content/images/banner-orion.gif" alt="Órion Automação" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" />
  <img src="/content/images/banner-schuf.gif" alt="Schuf Fetterolf do Brasil" border="0" style="margin-right:3px; margin-bottom: 10px; border:none;" />
  <img src="/content/images/banner-sovan.gif" alt="Sovan Equipamentos de Segurança" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" />
  <img src="/content/images/banner-wns.gif" alt="WNS Informática" border="0" style="margin-right:3px; margin-bottom: 10px; border:none; float:left;" />

  <br /><br />
<br />

  <br />
  <br />
  <br />
  <br /><br />
<br />
<br />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="server"> </asp:Content>
